import sys
import math
import argparse
try: from ROOT import *
except ImportError: "ROOT is not setup - lsetup root"

parser = argparse.ArgumentParser('Plots histograms')
parser.add_argument('-o', dest='outputName', required=True,
                    help='output file name')
parser.add_argument("-j",dest="jetReweight",
                    default=False,action="store_true",help="Do jet reweighting")
parser.add_argument("-m",dest="massReweight",
                    default=False,action="store_true",help="Do m_yy mass reweighting")
args = parser.parse_args()

if args.jetReweight:
    jetReweighting = [1,1,0.96,1.015,1.05,1,1] #pulled from plot
else:
    jetReweighting = [1,1,1,1,1,1]

def get_flavor(jet_label_1, jet_label_2):

    if   jet_label_1 == 5  and jet_label_2 == 5:
        flavor = "bb"
    elif (jet_label_1 == 5 and jet_label_2 == 4) or (jet_label_1 == 4 and jet_label_2 == 5):
        flavor = "bc"
    elif jet_label_1 == 4  and jet_label_2  ==  4:
        flavor = "cc"
    elif (jet_label_1 == 5 and jet_label_2 == 0) or (jet_label_1 == 0 and jet_label_2 == 5):
        flavor = "bj"
    elif (jet_label_1 == 4 and jet_label_2 == 0) or (jet_label_1 == 0 and jet_label_2 == 4):
        flavor = "cj"
    else:
        flavor = "jj"
    return flavor


def fill_histograms(histoDict, weight, tag, mass,flavor, tree):
    region = "SR"
    if tag == 0: region = "CR"


    if mass == "l": histoDict['h_{0}_{1}_bbyy_{2}t_nominal_{3}'.format(region,mass,tag,flavor)].Fill(tree.m_yybb_low,weight)
    if mass == "h": histoDict['h_{0}_{1}_bbyy_{2}t_nominal_{3}'.format(region,mass,tag,flavor)].Fill(tree.m_yybb_high,weight)
    histoDict['h_{0}_{1}_yy_{2}t_nominal_{3}'.format(region,mass,tag,flavor)].Fill(tree.m_yy, weight )
    histoDict['h_{0}_{1}_{2}t_nominal_photon_n_{3}'.format(region,mass,tag,flavor)].Fill(tree.photon_n, weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_n_{3}'.format(region,mass,tag,flavor)].Fill(tree.jet_n, weight)

    corrected_jet_pTs = [ tree.jet_pt_allMu[j] for j in range(tree.jet_n)]
    if mass == "l": jet_pT1, jet_pT2 = tree.yybb_lowMass_pT_j1, tree.yybb_lowMass_pT_j2
    if mass == "h": jet_pT1, jet_pT2 = tree.yybb_highMass_pT_j1, tree.yybb_highMass_pT_j2

    diff_leading_pt,diff_subleading_pt = [],[]
    diff_leading_pt = [abs(jet_pT-jet_pT1) for jet_pT in corrected_jet_pTs]
    diff_subleading_pt = [abs(jet_pT-jet_pT2) for jet_pT in corrected_jet_pTs]
    idx_pT1, idx_pT2 = diff_leading_pt.index(min(diff_leading_pt)),diff_subleading_pt.index(min(diff_subleading_pt))



    lvector_photon1 =TLorentzVector()
    lvector_photon2 =TLorentzVector()
    # Set first photon
    lvector_photon1.SetPtEtaPhiM(tree.photon_pt[0],
                                 tree.photon_eta[0],
                                 tree.photon_phi[0],
                                 tree.photon_m[0]
                                 )
    # Fill first photon Variables
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_pt_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon1.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_eta_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon1.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_phi_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon1.Phi(), weight )

    # Set second photon
    lvector_photon2.SetPtEtaPhiM(tree.photon_pt[1],
                                 tree.photon_eta[1],
                                 tree.photon_phi[1],
                                 tree.photon_m[1]
                                 )
    # sanity check
    if tree.photon_pt[1] > tree.photon_pt[0]: print "subleading photon has higher pT than leading photon!"
    # Fill second photon variables
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_pt_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon2.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_eta_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon2.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_phi_{3}'.format(region,mass,tag,flavor)].Fill(lvector_photon2.Phi(), weight )

    # set deta, dphi, dr between photons
    deta = tree.photon_eta[0]-tree.photon_eta[1]
    dphi = tree.photon_phi[0]-tree.photon_phi[1]
    while (dphi > math.pi): dphi = dphi-2*math.pi;
    while (dphi < -math.pi): dphi = dphi+2*math.pi;
    dr = math.sqrt(deta*deta+dphi*dphi);
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaPhi_{3}'.format(region,mass,tag,flavor)].Fill(math.fabs(dphi),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaEta_{3}'.format(region,mass,tag,flavor)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaR_{3}'.format(region,mass,tag,flavor)].Fill(dr,weight)


    # Jet analysis
    jet1 =TLorentzVector()
    jet1.SetPtEtaPhiM(tree.jet_pt_allMu[idx_pT1] , # later sub these for muon corrected
                      tree.jet_eta_allMu[idx_pT1],
                      tree.jet_phi_allMu[idx_pT1],
                      tree.jet_m_allMu[idx_pT1]
                      )
    # Fill leading jet
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_pt_{3}'.format(region,mass,tag,flavor)].Fill(jet1.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_eta_{3}'.format(region,mass,tag,flavor)].Fill(jet1.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_phi_{3}'.format(region,mass,tag,flavor)].Fill(jet1.Phi(), weight )


    jet2 =TLorentzVector();
    jet2.SetPtEtaPhiM(tree.jet_pt_allMu[idx_pT2] , # later sub these for muon corrected
                      tree.jet_eta_allMu[idx_pT2],
                      tree.jet_phi_allMu[idx_pT2],
                      tree.jet_m_allMu[idx_pT2]
                      )
    # Fill subleading jet
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_pt_{3}'.format(region,mass,tag,flavor)].Fill(jet2.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_eta_{3}'.format(region,mass,tag,flavor)].Fill(jet2.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_phi_{3}'.format(region,mass,tag,flavor)].Fill(jet2.Phi(), weight )


    # set deta, dphi, dr between jets
    deta = tree.jet_eta[idx_pT1]-tree.jet_eta[idx_pT2]
    dphi = tree.jet_phi[idx_pT1]-tree.jet_phi[idx_pT2]
    while (dphi > math.pi): dphi = dphi-2*math.pi;
    while (dphi < -math.pi): dphi = dphi+2*math.pi;
    dr = math.sqrt(deta*deta+dphi*dphi);
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaPhi_{3}'.format(region,mass,tag,flavor)].Fill(math.fabs(dphi),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaEta_{3}'.format(region,mass,tag,flavor)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaR_{3}'.format(region,mass,tag,flavor)].Fill(dr,weight)



    dijetSystem = jet1+jet2
    diphotonSystem = lvector_photon1+lvector_photon2


    # set deta, dphi, dr between leading jet and photon
    deta = dijetSystem.Eta()-diphotonSystem.Eta()
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaPhi_{3}'.format(region,mass,tag,flavor)].Fill(dijetSystem.DeltaPhi(diphotonSystem),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaEta_{3}'.format(region,mass,tag,flavor)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaR_{3}'.format(region,mass,tag,flavor)].Fill(dijetSystem.DeltaR(diphotonSystem),weight)


    # Output problems if jet pT's are outside of cuts
    jet1.SetPtEtaPhiM(tree.jet_pt_allMu[idx_pT1],
                      tree.jet_eta_allMu[idx_pT1],
                      tree.jet_phi_allMu[idx_pT1],
                      tree.jet_m_allMu[idx_pT1]
                      )

    if jet1.Pt() < 40:
        print "Leading Jet pT < 40"
        print "  lead    idx", idx_pT1
        print "  sublead idx", idx_pT2
        print "           pT", jet1.Pt()
    if jet1.Pt() <100 and mass is "h":
        print "Leading Jet pT < 100"
        print "  lead    idx", idx_pT1
        print "  sublead idx", idx_pT2
        print "           pT", jet1.Pt()


if __name__=='__main__':

    sample = '/eos/atlas/user/e/ebrost/public/tiny/v5/sherpa.root'
    # Get file
    treename = 'mini'
    infile = TFile(sample)
    # Get tree
    tree = infile.Get(treename)
    nEntries = tree.GetEntries()
    print "sample: ", sample," has entries ", nEntries


    if nEntries == 0:
        print "no entries :("
        sys.exit()
 #   if nEntries > 1000000 : nEntries = 100000

    print "using nEntries = ", nEntries

    # Define a ton of  histograms
    print "making histograms"


######
    plotType = ["pt","eta","phi"]
    bins =     [100,  50,    35 ]
    x_min =    [  0,  -3,  -3.5 ]
    x_max =    [300,   3,   3.5 ]
    objectType = ["leadingJet","subleadingJet","leadingPhoton","subleadingPhoton"]

    deltaPlotType = ["deltaPhi","deltaEta","deltaR"]
    dbins =     [ 40, 55, 60 ]
    dx_min =    [ 0 ,  0,  0 ]
    dx_max =    [ 4 ,  5.5,  6 ]
    dobjectType = ["jet","photon","system"]

    flavors = ["bb","bc","bj","cc","cj","jj"]


    lowMassHistos={}
    for tag in range(3):
        region = "SR"
        if tag is 0: region = "CR"

        for f in flavors:
            name = "h_{0}_l_{1}t_nominal_photon_n_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,4,0,3)
            name = "h_{0}_l_{1}t_nominal_jet_n_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,7,1,8)
            name = "h_{0}_l_bbyy_{1}t_nominal_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,38,250,1200)
            name = "h_{0}_l_yy_{1}t_nominal_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,22,105,160)

            for p in deltaPlotType:
                for o in dobjectType:
                    idx = deltaPlotType.index(p)
                    name = "h_{0}_l_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    lowMassHistos[name] = TH1F(name,name,dbins[idx],dx_min[idx],dx_max[idx])
            for p in plotType:
                for o in objectType:
                    idx = plotType.index(p)
                    name = "h_{0}_l_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    lowMassHistos[name] = TH1F(name,name,bins[idx],x_min[idx],x_max[idx])

    highMassHistos={}
    for tag in range(3):
        region = "SR"
        if tag is 0: region = "CR"

        for f in flavors:
            name = "h_{0}_h_{1}t_nominal_photon_n_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,4,0,3)
            name = "h_{0}_h_{1}t_nominal_jet_n_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,7,1,8)
            name = "h_{0}_h_bbyy_{1}t_nominal_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,38,250,1200)
            name = "h_{0}_h_yy_{1}t_nominal_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,22,105,160)

            for p in deltaPlotType:
                for o in dobjectType:
                    idx = deltaPlotType.index(p)
                    name = "h_{0}_h_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    highMassHistos[name] = TH1F(name,name,dbins[idx],dx_min[idx],dx_max[idx])
            for p in plotType:
                for o in objectType:
                    idx = plotType.index(p)
                    name = "h_{0}_h_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    highMassHistos[name] = TH1F(name,name,bins[idx],x_min[idx],x_max[idx])




    for evt in range(nEntries):

        # get current entry
        tree.GetEntry(evt)
        if evt % 10000000 == 0: print "on event", evt


        y1TI = False
        y2TI = False
        isYY = False
        isYJ = False
        isJJ = False


        if not tree.hgam_isPassed: continue
        if tree.jet_n < 2: continue
        if (tree.photon_isTight[0]==1 and tree.photon_iso_Loose[0]==1): y1TI=True
        if (tree.photon_isTight[1]==1 and tree.photon_iso_Loose[1]==1): y2TI=True
        if (y1TI and y1TI): isYY = True
        elif (y1TI and not y2TI) or (y2TI and not y1TI): isYJ=True
        else: isJJ = True

        if not tree.isMC and isYY and (tree.yybb_low_btagCat==1 or tree.yybb_low_btagCat==2) and tree.m_yy > 120 and tree.m_yy < 130: continue
        if not isYY and tree.isMC: continue


        # get weights
        if tree.isMC and tree.jet_n < 6:
            w   = tree.hgam_weight * tree.yybb_low_weight  * 36500 * 41.50 * 0.49904 * 0.74337360626 * jetReweighting[tree.jet_n] / 80604609.6
            w_h = tree.hgam_weight * tree.yybb_high_weight * 36500 * 41.50 * 0.49904 * 0.74337360626 * jetReweighting[tree.jet_n] / 80604609.6
            #https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/Run2/HGamma/xAOD/HGamAnalysisFramework/trunk/data/MCSamples.config#L1856
            # 0.74337360626 is DxAOD to MxAOD scaling
        elif tree.isMC and tree.jet_n >= 6:
            w   = tree.hgam_weight * tree.yybb_low_weight  * 36500 * 41.50 * 0.49904 * 0.74337360626 * jetReweighting[5] / 80604609.6
            w_h = tree.hgam_weight * tree.yybb_high_weight * 36500 * 41.50 * 0.49904 * 0.74337360626 * jetReweighting[5] / 80604609.6
        else:
            print "Data weights being applied - probably a problem!"
            w,w_h = 1,1


        # Classify Flavor
        jet_label_1 = tree.yybb_lowMass_truth_label_j1
        jet_label_2 = tree.yybb_lowMass_truth_label_j2
        low_mass_flavor = get_flavor(jet_label_1,jet_label_2)
        jet_label_1_h = tree.yybb_highMass_truth_label_j1
        jet_label_2_h = tree.yybb_highMass_truth_label_j2
        high_mass_flavor = get_flavor(jet_label_1_h,jet_label_2_h)



        # classify btag category
        btag   = tree.yybb_low_btagCat
        btag_h = tree.yybb_high_btagCat



        # fill appropriate histograms
        if tree.yybb_cutList_low > 3:

            fill_histograms(lowMassHistos,w,btag,"l",low_mass_flavor, tree)

        if tree.yybb_cutList_high > 3:
            fill_histograms(highMassHistos,w_h,btag_h,"h",high_mass_flavor, tree)



        # Print some of them
    print "Done! Creating File"
    outfile = TFile(args.outputName,'RECREATE')
    print "file created: ", outfile


    for key in lowMassHistos:
        lowMassHistos[key].Write()
    for key in highMassHistos:
        highMassHistos[key].Write()


    outfile.Close()
