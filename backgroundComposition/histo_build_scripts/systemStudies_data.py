##### Fix the bounds!


import sys
import math
from glob import glob
import argparse
try: from ROOT import *
except ImportError: "ROOT is not setup - lsetup root"
try: import numpy as np
except ImportError: "PyAnalysis not setup - lsetup \"lcgenv -p LCG_latest x86_64-slc6-gcc49-opt pyanalysis\""


#if len(RWmass) != len(high_yy_weight) or len(high_yy_weight) != len(high_yj_weight) or len(high_yy_weight) != len(high_jy_weight) or len(high_yy_weight) != len(high_jj_weight) or len(high_yy_weight) != len(low_yj_weight) or len(high_yy_weight) != len(low_jy_weight) or len(high_yy_weight) != len(low_jj_weight):
 #   print "arrays do not match!!!" #check input factors are right length
  #  print len(RWmass) ,  len(high_yy_weight) ,    len(high_yj_weight) ,    len(high_jy_weight) ,    len(high_jj_weight) ,    len(low_yj_weight) ,    len(low_jy_weight) ,    len(low_jj_weight)
parser = argparse.ArgumentParser('Plots histograms')
parser.add_argument('-o', dest='outputName', required=True,
                    help='output file name')
parser.add_argument("-m",dest="massReweight",
                    default=False,action="store_true",help="Do m_yy mass reweighting")
args = parser.parse_args()

def eventType(tree):
    isYY, isJY, isYJ, isJJ = False,False,False,False

    # Case for YY
    a,b = False,False
    if (tree.photon_isTight[0]==1 and tree.photon_iso_Loose[0]==1):
        a = True
    if (tree.photon_isTight[1]==1 and tree.photon_iso_Loose[1]==1):
        b = True
    if a and b: isYY=True

    # Case for JY
    a,b = False,False
    if (tree.photon_isTight[0]==0 or tree.photon_iso_Loose[0]==0):
        a = True
    if (tree.photon_isTight[1]==1 and tree.photon_iso_Loose[1]==1):
        b = True
    if a and b: isJY=True

    # Case for YJ
    a,b = False,False
    if (tree.photon_isTight[0]==1 and tree.photon_iso_Loose[0]==1):
        a = True
    if (tree.photon_isTight[1]==0 or tree.photon_iso_Loose[1]==0):
        b = True
    if a and b: isYJ=True

    # Case for JJ
    a,b = False,False
    if (tree.photon_isTight[0]==0 or tree.photon_iso_Loose[0]==0):
        a = True
    if (tree.photon_isTight[1]==0 or tree.photon_iso_Loose[1]==0):
        b = True
    if a and b: isJJ=True

    if (isYY and isYJ) or (isYY and isJY) or (isYY and isJJ) or (isJY and isYJ) or (isJY and isJJ) or (isYJ and isJJ):
        print "LOGIC PROBLEM"
        if isYY: print "isYY"
        if isYJ: print "isYJ"
        if isJY: print "isJY"
        if isJJ: print "isJJ"
        sys.exit("There's a logic problem in classification")

    return isYY,isJY,isYJ,isJJ


def getRWidx(mass,fakeType):
    """ Gets proper reweight array from imported file """
    if mass == 'h':
        if fakeType == 'data': return 0
        elif fakeType == 'fakes_yj': return 1
        elif fakeType == 'fakes_jy': return 2
        else: return 3
    else:
        if fakeType == 'data': return 4
        elif fakeType == 'fakes_yj': return 5
        elif fakeType == 'fakes_jy': return 6
        else: return 7


def mass_Reweight(tree,massSelection,fakeType):
    """ Gets mass reweighting factor from array"""
    rw = np.load("reweighting_factors.npy")
    idx = getRWidx(massSelection,fakeType)
    return np.interp(tree.m_yy,np.split(rw,8)[idx]['x'],np.split(rw,8)[idx]['y'])

def fill_histograms(histoDict, weight, tag, mass,fakeType, tree):
    if not tree.isMC and args.massReweight: weight = mass_Reweight(tree, mass,fakeType)
    region = "SR"
    if tag == 0: region = "CR"

    if mass == "l": histoDict['h_{0}_{1}_bbyy_{2}t_nominal_{3}'.format(region,mass,tag,fakeType)].Fill(tree.m_yybb_low,weight)
    if mass == "h": histoDict['h_{0}_{1}_bbyy_{2}t_nominal_{3}'.format(region,mass,tag,fakeType)].Fill(tree.m_yybb_high,weight)
    histoDict['h_{0}_{1}_yy_{2}t_nominal_{3}'.format(region,mass,tag,fakeType)].Fill(tree.m_yy, weight )
    histoDict['h_{0}_{1}_{2}t_nominal_photon_n_{3}'.format(region,mass,tag,fakeType)].Fill(tree.photon_n, weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_n_{3}'.format(region,mass,tag,fakeType)].Fill(tree.jet_n, weight)

    # Get indices of leading and subleading jets
    corrected_jet_pTs = [ tree.jet_pt_allMu[j] for j in range(tree.jet_n)]
    if mass == "l": jet_pT1, jet_pT2 = tree.yybb_lowMass_pT_j1, tree.yybb_lowMass_pT_j2
    if mass == "h": jet_pT1, jet_pT2 = tree.yybb_highMass_pT_j1, tree.yybb_highMass_pT_j2

    diff_leading_pt,diff_subleading_pt = [],[]
    diff_leading_pt = [abs(jet_pT-jet_pT1) for jet_pT in corrected_jet_pTs]
    diff_subleading_pt = [abs(jet_pT-jet_pT2) for jet_pT in corrected_jet_pTs]
    idx_pT1, idx_pT2 = diff_leading_pt.index(min(diff_leading_pt)),diff_subleading_pt.index(min(diff_subleading_pt))


    lvector_photon1 =TLorentzVector()
    lvector_photon2 =TLorentzVector()
    # Set first photon
    lvector_photon1.SetPtEtaPhiM(tree.photon_pt[0],
                                 tree.photon_eta[0],
                                 tree.photon_phi[0],
                                 tree.photon_m[0]
                                 )
    # Fill first photon Variables
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_pt_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon1.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_eta_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon1.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingPhoton_phi_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon1.Phi(), weight )

    # Set second photon
    lvector_photon2.SetPtEtaPhiM(tree.photon_pt[1],
                                 tree.photon_eta[1],
                                 tree.photon_phi[1],
                                 tree.photon_m[1]
                                 )
    # sanity check
    if tree.photon_pt[1] > tree.photon_pt[0]: print "subleading photon has higher pT than leading photon!"
    # Fill second photon variables
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_pt_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon2.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_eta_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon2.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingPhoton_phi_{3}'.format(region,mass,tag,fakeType)].Fill(lvector_photon2.Phi(), weight )

    # set deta, dphi, dr between photons
    deta = tree.photon_eta[0]-tree.photon_eta[1]
    dphi = tree.photon_phi[0]-tree.photon_phi[1]
    while (dphi > math.pi): dphi = dphi-2*math.pi;
    while (dphi < -math.pi): dphi = dphi+2*math.pi;
    dr = math.sqrt(deta*deta+dphi*dphi);
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaPhi_{3}'.format(region,mass,tag,fakeType)].Fill(math.fabs(dphi),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaEta_{3}'.format(region,mass,tag,fakeType)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_photon_deltaR_{3}'.format(region,mass,tag,fakeType)].Fill(dr,weight)


    # Jet analysis
    jet1 =TLorentzVector()
    jet1.SetPtEtaPhiM(tree.jet_pt_allMu[idx_pT1] , # later sub these for muon corrected
                      tree.jet_eta_allMu[idx_pT1],
                      tree.jet_phi_allMu[idx_pT1],
                      tree.jet_m_allMu[idx_pT1]
                      )
    # Fill leading jet
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_pt_{3}'.format(region,mass,tag,fakeType)].Fill(jet1.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_eta_{3}'.format(region,mass,tag,fakeType)].Fill(jet1.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_leadingJet_phi_{3}'.format(region,mass,tag,fakeType)].Fill(jet1.Phi(), weight )

    jet2 =TLorentzVector();
    jet2.SetPtEtaPhiM(tree.jet_pt_allMu[idx_pT2] , # later sub these for muon corrected
                      tree.jet_eta_allMu[idx_pT2],
                      tree.jet_phi_allMu[idx_pT2],
                      tree.jet_m_allMu[idx_pT2]
                      )
    # Fill subleading jet
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_pt_{3}'.format(region,mass,tag,fakeType)].Fill(jet2.Pt(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_eta_{3}'.format(region,mass,tag,fakeType)].Fill(jet2.Eta(), weight )
    histoDict['h_{0}_{1}_{2}t_nominal_subleadingJet_phi_{3}'.format(region,mass,tag,fakeType)].Fill(jet2.Phi(), weight )


    # set deta, dphi, dr between jets
    deta = tree.jet_eta_allMu[idx_pT1]-tree.jet_eta_allMu[idx_pT2]
    dphi = tree.jet_phi_allMu[idx_pT1]-tree.jet_phi_allMu[idx_pT2]
    while (dphi > math.pi): dphi = dphi-2*math.pi;
    while (dphi < -math.pi): dphi = dphi+2*math.pi;
    dr = math.sqrt(deta*deta+dphi*dphi);
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaPhi_{3}'.format(region,mass,tag,fakeType)].Fill(math.fabs(dphi),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaEta_{3}'.format(region,mass,tag,fakeType)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_jet_deltaR_{3}'.format(region,mass,tag,fakeType)].Fill(dr,weight)



    dijetSystem = jet1+jet2
    diphotonSystem = lvector_photon1+lvector_photon2


    # set deta, dphi, dr between leading jet and photon
    deta = dijetSystem.Eta()-diphotonSystem.Eta()
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaPhi_{3}'.format(region,mass,tag,fakeType)].Fill(dijetSystem.DeltaPhi(diphotonSystem),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaEta_{3}'.format(region,mass,tag,fakeType)].Fill(math.fabs(deta),weight)
    histoDict['h_{0}_{1}_{2}t_nominal_system_deltaR_{3}'.format(region,mass,tag,fakeType)].Fill(dijetSystem.DeltaR(diphotonSystem),weight)



if __name__=='__main__':

    files = glob("/eos/atlas/user/e/ebrost/public/tiny/v5/data*.root")
    tree = TChain("mini")
    for f in files:
        tree.Add(f)
        print "added file: ",f, "\n Chain now has nEntries", tree.GetEntries()

    nEntries = tree.GetEntries()
    print "TChain has entries ", nEntries

    #if nEntries > 100: nEntries = 10596320

    if nEntries == 0:
        print "no entries :("
        sys.exit()

    print "using nEntries = ", nEntries

    # Define a ton of  histograms
    print "making histograms"


######
    plotType = ["pt","eta","phi"]
    bins =     [100,  50,    35 ]
    x_min =    [  0,  -3,  -3.5 ]
    x_max =    [300,   3,   3.5 ]
    objectType = ["leadingJet","subleadingJet","leadingPhoton","subleadingPhoton"]

    deltaPlotType = ["deltaPhi","deltaEta","deltaR"]
    dbins =     [ 40, 55, 60 ]
    dx_min =    [ 0 ,  0,  0 ]
    dx_max =    [ 4 ,  5.5,  6 ]
    dobjectType = ["jet","photon","system"]

    fake_type = ["data","fakes_yj","fakes_jy","fakes_jj"]



    lowMassHistos={}
    for tag in range(3):
        region = "SR"
        if tag is 0: region = "CR"

        for f in fake_type:
            name = "h_{0}_l_{1}t_nominal_photon_n_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,4,0,3)
            name = "h_{0}_l_{1}t_nominal_jet_n_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,7,1,8)
            name = "h_{0}_l_bbyy_{1}t_nominal_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,38,250,1200)
            name = "h_{0}_l_yy_{1}t_nominal_{2}".format(region,tag,f)
            lowMassHistos[name] = TH1F(name,name,22,105,160)

            for p in deltaPlotType:
                for o in dobjectType:

                    idx = deltaPlotType.index(p)
                    name = "h_{0}_l_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    lowMassHistos[name] = TH1F(name,name,dbins[idx],dx_min[idx],dx_max[idx])
            for p in plotType:
                for o in objectType:
                    if o == "leadingJet" and p == "pt": minimum,maxiumum = 40,200
                    elif o == "subleadingJet" and p == "pt": minimum,maxiumum = 25,150
                    elif o == "subleadingPhoton" and p == "pt": minimum,maxiumum = 20,150
                    else: minimum,maximum= x_min[idx],x_max[idx]

                    idx = plotType.index(p)
                    name = "h_{0}_l_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    lowMassHistos[name] = TH1F(name,name,bins[idx],x_min[idx],x_max[idx])

    highMassHistos={}
    for tag in range(3):
        region = "SR"
        if tag is 0: region = "CR"

        for f in fake_type:
            name = "h_{0}_h_{1}t_nominal_photon_n_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,4,0,3)
            name = "h_{0}_h_{1}t_nominal_jet_n_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,7,1,8)
            name = "h_{0}_h_bbyy_{1}t_nominal_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,38,250,1200)
            name = "h_{0}_h_yy_{1}t_nominal_{2}".format(region,tag,f)
            highMassHistos[name] = TH1F(name,name,22,105,160)

            for p in deltaPlotType:
                for o in dobjectType:
                    idx = deltaPlotType.index(p)
                    name = "h_{0}_h_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    highMassHistos[name] = TH1F(name,name,dbins[idx],dx_min[idx],dx_max[idx])
            for p in plotType:
                for o in objectType:
                    idx = plotType.index(p)
                    name = "h_{0}_h_{1}t_nominal_{2}_{3}_{4}".format(region,tag,o,p,f)
                    highMassHistos[name] = TH1F(name,name,bins[idx],x_min[idx],x_max[idx])




    for evt in range(nEntries):

        # get current entry
        tree.GetEntry(evt)
        if evt % 10000000 == 0: print "on event", evt

        isYY = False
        isJY = False
        isYJ = False
        isJJ = False

        #if not tree.hgam_isPassed: continue
        if not tree.hgam_cutList > 10: continue
        if tree.jet_n < 2: continue

        if (tree.photon_pt[0] < 0.35 * tree.m_yy or
            tree.photon_pt[1] < 0.25 * tree.m_yy or
            tree.m_yy <  105 or
            tree.m_yy > 160): continue


        isYY, isJY, isYJ, isJJ = eventType(tree)

        if not tree.isMC and isYY and (tree.yybb_low_btagCat==1 or tree.yybb_low_btagCat==2) and tree.m_yy > 120 and tree.m_yy < 130: continue
        if not isYY and tree.isMC: continue


        # get weights
        if tree.isMC and tree.jet_n < 6:
            print "Monte Carlo weights being applied - probably a problem!"
            w   = tree.hgam_weight * tree.yybb_low_weight  * 36500 * 41.50 * 0.49904 * 0.74337360626 / 80604609.6
            w_h = tree.hgam_weight * tree.yybb_high_weight * 36500 * 41.50 * 0.49904 * 0.74337360626 / 80604609.6
            #https://svnweb.cern.ch/trac/atlasoff/browser/PhysicsAnalysis/HiggsPhys/Run2/HGamma/xAOD/HGamAnalysisFramework/trunk/data/MCSamples.config#L1856
        else: w, w_h =1,1


        # classify btag category
        btag   = tree.yybb_low_btagCat
        btag_h = tree.yybb_high_btagCat



        # fill appropriate histograms
        if tree.yybb_cutList_low > 3:

            if   isYY: fType = "data"
            elif isYJ: fType = "fakes_yj"
            elif isJY: fType = "fakes_jy"
            elif isJJ: fType = "fakes_jj"
            fill_histograms(lowMassHistos,w,btag,"l",fType, tree)

        if tree.yybb_cutList_high > 3:

            if   isYY: fType = "data"
            elif isYJ: fType = "fakes_yj"
            elif isJY: fType = "fakes_jy"
            elif isJJ: fType = "fakes_jj"
            fill_histograms(highMassHistos,w_h,btag_h,"h",fType, tree)



        # Print some of them
    print "Done! Creating File"
    outfile = TFile(args.outputName,'RECREATE')
    print "file created: ", outfile


    for key in lowMassHistos:
        lowMassHistos[key].Write()
    for key in highMassHistos:
        highMassHistos[key].Write()



    outfile.Close()
