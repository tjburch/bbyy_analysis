from ROOT import *
import sys
from AtlasStyle import *
import gc
import argparse

parser = argparse.ArgumentParser('Check for consistencies within a file')
parser.add_argument('-D', dest='dataFile', required=True,
                    help='Input Data File')
parser.add_argument('-M', dest='mcFile', required=False,
                    help='Input MC File')
parser.add_argument("-f","--fit",dest="fit",
              default=False,action="store_true",help="Fit ratio plots?")
parser.add_argument("-n","--noRebin",dest="rebin",
              default=True,action="store_false",help="No rebinning (1 and 2 tag)")

args = parser.parse_args()


def plotHistos(h_bb,
               h_bc,
               h_bj,
               h_cc,
               h_cj,
               h_jj,
               h_fakes_yj,
               h_fakes_jy,
               h_fakes_jj,
               h_data,
               tag,
               mass,
               title,
               x_label,
               plotType):



    myCanvas = TCanvas("a","b",400,800)

    histo_list =[]
    histo_list.append(h_fakes_yj)
    histo_list.append(h_fakes_jy)
    histo_list.append(h_fakes_jj)
    histo_list.append(h_bb)
    histo_list.append(h_bc)
    histo_list.append(h_bj)
    histo_list.append(h_cc)
    histo_list.append(h_cj)
    histo_list.append(h_jj)
    histo_list.append(h_data)

    stack_histo_list = histo_list[:-1]

    #Set Colors
    h_bb.SetFillColor(kRed)
    h_bc.SetFillColor(kGreen)
    h_bj.SetFillColor(kBlue)
    h_cc.SetFillColor(kOrange-3)
    h_cj.SetFillColor(kMagenta)
    h_jj.SetFillColor(kCyan)
    h_fakes_yj.SetFillColor(kYellow)
    h_fakes_jy.SetFillColor(kYellow+2)
    h_fakes_jj.SetFillColor(kYellow-9)

    if args.rebin:
        if h_bb.GetNbinsX() % 5 is 0:
            for h in stack_histo_list:
                if tag is 1: h.Rebin(5)
                if tag is 2: h.Rebin(5)
            if tag is 1: h_data.Rebin(5)
            if tag is 2: h_data.Rebin(5)

        elif h_bb.GetNbinsX() % 4 is 0:
            for h in stack_histo_list:
                if tag is 1: h.Rebin(4)
                if tag is 2: h.Rebin(4)
            if tag is 1: h_data.Rebin(4)
            if tag is 2: h_data.Rebin(4)
        elif h_bb.GetNbinsX() % 3 is 0:
            for h in stack_histo_list:
                if tag is 1: h.Rebin(3)
                if tag is 2: h.Rebin(3)
            if tag is 1: h_data.Rebin(3)
            if tag is 2: h_data.Rebin(3)
        elif h_bb.GetNbinsX() % 2 is 0:
            for h in stack_histo_list:
                if tag is 1: h.Rebin(2)
                if tag is 2: h.Rebin(2)
            if tag is 1: h_data.Rebin(2)
            if tag is 2: h_data.Rebin(2)


    # 2x2d https://indico.cern.ch/event/610831/contributions/2462939/attachments/1406740/2149792/Introduction_20170203.pdf
    if mass is "l":
        if tag == 0 :
            yyContribution = 0.831
            yjfakeContribution =  0.082
            jyfakeContribution =  0.077
            jjfakeContribution = 0.011
        if tag == 1:
            yyContribution = 0.850
            yjfakeContribution =  0.073
            jyfakeContribution =  0.067
            jjfakeContribution = 0.010
        if tag == 2:
            yyContribution = 0.859
            yjfakeContribution =  0.076
            jyfakeContribution =  0.042
            jjfakeContribution = 0.023
    if mass is "h":
        if tag == 0 :
            yyContribution = 0.798
            yjfakeContribution =  0.082
            jyfakeContribution =  0.105
            jjfakeContribution = 0.015
        if tag == 1:
            yyContribution = 0.864
            yjfakeContribution =  0.052
            jyfakeContribution =  0.066
            jjfakeContribution = 0.018
        if tag == 2:
            yyContribution = 0.727
            yjfakeContribution =  0.110
            jyfakeContribution =  0.112
            jjfakeContribution = 0.051

    MC_integral_sum = (h_bb.Integral() + h_bc.Integral() + h_bj.Integral() + h_cc.Integral() + h_cj.Integral() + h_jj.Integral())
    integral_sum = MC_integral_sum + yjfakeContribution * h_fakes_yj.Integral() +  jyfakeContribution * h_fakes_jy.Integral() + jjfakeContribution * h_fakes_jj.Integral()



    if h_data.Integral(h_data.GetXaxis().FindBin(121),h_data.GetXaxis().FindBin(129)) == 0:
        hasBlindedRegion = True
    else: hasBlindedRegion = False


    h_dClone = h_data.Clone("h_dClone")
    SetOwnership( h_dClone, True )
    h_dClone.Sumw2()
    h_dClone.Scale(1/h_dClone.Integral())
    h_stack = THStack("Stack","")



    for h in stack_histo_list:
        if h.Integral() == 0:
            stack_histo_list.remove(h)
            continue
        if h is h_fakes_yj: h.Scale(yjfakeContribution/h.Integral()) #Scales to approporiate contribution
        elif h is h_fakes_jy: h.Scale(jyfakeContribution/h.Integral())
        elif h is h_fakes_jj: h.Scale(jjfakeContribution/h.Integral())
        else: h.Scale(yyContribution/MC_integral_sum)
        h_stack.Add(h)



    h_fullMC = h_stack.GetStack().Last()

    myCanvas = TCanvas()
    gStyle.SetOptStat(000000)

    #Make top pad
    pad1 = TPad("pad1", "pad1",0.,0.29,1.,1.)
    pad1.Draw()
    pad1.cd()

    # Set maximum
    if (h_stack.GetMaximum() > h_dClone.GetMaximum()): h_stack.SetMaximum(1.3*h_stack.GetMaximum())
    else: h_stack.SetMaximum(1.3*h_dClone.GetMaximum())


    h_dClone.SetMarkerStyle(20)
    h_stack.Draw("hist")
    h_dClone.Draw("e SAME")

    anchor = "top_right"
    if plotType is "phi" or plotType is "eta" or plotType is "dphi": anchor = "top"
    elif plotType is "n" and "photon" in x_label: anchor = "left"
    elif plotType is "deta"  or plotType is "dR" or plotType is "n" : anchor = "right"
    elif plotType is "m" or plotType is "pt": anchor = "top_right"
    leg = setLegend(anchor,mass)
    # Make Legend
    leg.SetFillStyle(0)
    leg.AddEntry(h_bb,"#gamma#gamma MC (bb)","f")
    leg.AddEntry(h_bc,"#gamma#gamma MC (bc)","f")
    leg.AddEntry(h_bj,"#gamma#gamma MC (bj)","f")
    leg.AddEntry(h_cc,"#gamma#gamma MC (cc)","f")
    leg.AddEntry(h_cj,"#gamma#gamma MC (cj)","f")
    leg.AddEntry(h_jj,"#gamma#gamma MC (jj)","f")
    leg.AddEntry(h_fakes_yj,"#gammaj fakes","f")
    leg.AddEntry(h_fakes_jy,"j#gamma fakes","f")
    leg.AddEntry(h_fakes_jj,"jj fakes","f")
    leg.AddEntry(h_dClone,"data","pl");
    leg.SetBorderSize(0)
    leg.Draw()


    myCanvas.cd()

    # Start Ratio plot
    ## Make second pad
    pad2 = TPad("pad2", "pad2",0.,0.,1.,0.36,0)
    pad2.Draw()
    pad2.cd()
    pad2.SetBottomMargin(0.25)

    h_dataClone = h_dClone.Clone("h_dataClone")
    SetOwnership( h_dataClone, True )

#    h_dataClone.Divide(h_stack.GetStack().Last())
    h_dataClone.Divide(h_fullMC)


    # uncomment for fit statistics
    if args.fit:
        h_dataClone.Fit("pol1")
        #gStyle.SetOptFit(1)
        h_dataClone.GetFunction("pol1").SetLineColor(kRed)


    h_dataClone.SetMarkerStyle(20)
    h_dataClone.Draw("ep")
    h_dataClone.SetTitle("")

    #      // Y axis ratio plot settings
    h_dataClone.GetYaxis().SetTitle(" Data/MC ")
    h_dataClone.GetYaxis().SetNdivisions(505)
    h_dataClone.GetYaxis().SetTitleSize(.1)
    h_dataClone.GetYaxis().SetTitleOffset(.5)
    h_dataClone.GetYaxis().SetLabelFont(43)
    h_dataClone.GetYaxis().SetLabelSize(15)
    h_dataClone.GetYaxis().SetRangeUser(0.,2.)

  #  // X axis ratio plot settings
    h_dataClone.GetXaxis().SetTitle(x_label)
    h_dataClone.GetXaxis().SetTitleSize(.09)
    h_dataClone.GetXaxis().SetTitleOffset(1.2)
    h_dataClone.GetXaxis().SetLabelFont(43)
    h_dataClone.GetXaxis().SetLabelSize(15)

    h_stack.SetTitle(title)

    #Scale

    #add in scaling correction for blinded region here



    tl = TLine(h_dataClone.GetXaxis().GetXmin(),1,h_dataClone.GetXaxis().GetXmax(),1)
    tl.SetLineWidth(2)
    tl.SetLineStyle(2)
    tl.Draw()

    h_stack.GetHistogram().GetXaxis().SetLabelOffset(999)
    h_stack.GetYaxis().SetTitle(" Events (Normalized to 1) ")

    if args.fit:
        myCanvas.Print("plots/fit_"+title+".pdf")
        c2 = TCanvas()
        c2.cd()
        gStyle.SetOptFit(1)
        h_dataClone.GetXaxis().SetTitleOffset(1)
        h_dataClone.GetYaxis().SetTitleOffset(1)
        h_dataClone.GetXaxis().SetTitleSize(.05)
        h_dataClone.GetYaxis().SetTitleSize(.05)
        h_dataClone.Draw()

        tl.Draw()
        c2.Print("plots/fitStats_"+title+".pdf")

    else:      myCanvas.Print("plots/"+title+".pdf")


def setLegend(anchor,mass):

    if anchor is "top_right":
        ATLASLabel(0.56, 0.88, "Internal", 1)
        myLabel(0.50, 0.83, 1, "#int L dt = 36.1 fb^{-1}")
        myLabel(0.53, 0.78, 1, "#sqrt{s}= 13 TeV")
        myLabel(0.63,0.83,1,str(tag)+"-tag nominal")
        if mass == "l" : myLabel(0.63,0.78,1,"low mass")
        elif mass == "h" : myLabel(0.63,0.78,1,"high mass")
        leg = TLegend(0.73,0.53,0.93,0.94);
        leg.SetNColumns(2)


    elif anchor is "top":
        ATLASLabel(0.15, 0.88, "Internal", 1)
        myLabel(0.15, 0.83, 1, "#int L dt = 36.5 fb^{-1}")
        myLabel(0.18, 0.78, 1, "#sqrt{s}= 13 TeV")
        myLabel(0.28,0.83,1,str(tag)+"-tag nominal")
        if mass == "l" : myLabel(0.28,0.78,1,"low mass")
        elif mass == "h" : myLabel(0.28,0.78,1,"high mass")
        leg = TLegend(0.40,0.78,0.93,0.93);
        leg.SetNColumns(4)


    elif anchor is "left":
        ATLASLabel(0.15, 0.88, "Internal", 1)
        myLabel(0.15, 0.83, 1, "#int L dt = 36.5 fb^{-1}")
        myLabel(0.18, 0.78, 1, "#sqrt{s}= 13 TeV")
        myLabel(0.28,0.83,1,str(tag)+"-tag nominal")
        if mass == "l" : myLabel(0.28,0.78,1,"low mass")
        elif mass == "h" : myLabel(0.28,0.78,1,"high mass")
        leg = TLegend(0.15,0.35,0.35,0.74);
        leg.SetNColumns(2)

    elif anchor is "right":
        ATLASLabel(0.76, 0.88, "Internal", 1)
        myLabel(0.70, 0.83, 1, "#int L dt = 36.5 fb^{-1}")
        myLabel(0.73, 0.78, 1, "#sqrt{s}= 13 TeV")
        myLabel(0.83,0.83,1,str(tag)+"-tag nominal")
        if mass == "l" : myLabel(0.83,0.78,1,"low mass")
        elif mass == "h" : myLabel(0.83,0.78,1,"high mass")
        leg = TLegend(0.73,0.35,0.93,0.74);
        leg.SetNColumns(2)

    else:
        leg = TLegend(0.75,0.7,0.89,0.89);
        leg.SetNColumns(2)


    return leg


if __name__=='__main__':

    gc.set_debug(gc.DEBUG_LEAK)
    AtlasStyle()
    mc_input_file = TFile(args.mcFile)
    data_input_file = TFile(args.dataFile)

    mass = ["l","h"]
    objectType = ["leadingPhoton","subleadingPhoton","leadingJet","subleadingJet"]
    plotTypesMass = ["bbyy","yy"]
    plotTypesKinematic = ["eta","phi","pt"]
    plotTypesOther = ["n","deltaPhi","deltaEta","deltaR"]

    for m in mass:
        for tag in range(3):
            region = "SR"
            if tag == 0: region ="CR"
            for p in plotTypesMass:
                title = "h_{0}_{1}_{2}_{3}t_nominal".format(region,m,p,tag)
                mc_bb   = mc_input_file.Get(title+"_bb")
                mc_bc   = mc_input_file.Get(title+"_bc")
                mc_bj   = mc_input_file.Get(title+"_bj")
                mc_cc   = mc_input_file.Get(title+"_cc")
                mc_cj   = mc_input_file.Get(title+"_cj")
                mc_jj   = mc_input_file.Get(title+"_jj")
                fakes_yj = data_input_file.Get(title+"_fakes_yj")
                fakes_jy = data_input_file.Get(title+"_fakes_jy")
                fakes_jj = data_input_file.Get(title+"_fakes_jj")
                data_hist = data_input_file.Get(title+"_data")

                data_hist = data_input_file.Get(title+"_data")
                if p is "bbyy": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,"m_{bb#gamma#gamma} [GeV]","m")
                if p is "yy": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,"m_{#gamma#gamma} [GeV]","m")


            for p in plotTypesOther:
                for obj in ["photon","jet","system"]:
                    if obj is "system" and p is "n": continue
                    title = "h_{0}_{1}_{2}t_nominal_{3}_{4}".format(region,m,tag,obj,p)
                    mc_bb   = mc_input_file.Get(title+"_bb")
                    mc_bc   = mc_input_file.Get(title+"_bc")
                    mc_bj   = mc_input_file.Get(title+"_bj")
                    mc_cc   = mc_input_file.Get(title+"_cc")
                    mc_cj   = mc_input_file.Get(title+"_cj")
                    mc_jj   = mc_input_file.Get(title+"_jj")
                    fakes_yj = data_input_file.Get(title+"_fakes_yj")
                    fakes_jy = data_input_file.Get(title+"_fakes_jy")
                    fakes_jj = data_input_file.Get(title+"_fakes_jj")
                    data_hist = data_input_file.Get(title+"_data")
                    if obj == "system":
                        if p is "deltaPhi": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,"#Delta #phi (#gamma#gamma,jj)","dphi")
                        if p is "deltaEta": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title," #Delta #eta (#gamma#gamma,jj)","deta")
                        if p is "deltaR": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title," #Delta R (#gamma#gamma,jj)","dR")
                    else:
                        if p is "n":  plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,obj+" multiplicity","n")
                        if p is "deltaPhi": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,obj+" #Delta #phi","dphi")
                        if p is "deltaEta": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,obj+" #Delta #eta","deta")
                        if p is "deltaR": plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,obj+" #Delta R","dR")

            for o in objectType:
                for p in plotTypesKinematic:
                    title = "h_{0}_{1}_{2}t_nominal_{3}_{4}".format(region,m,tag,o,p)
                    mc_bb   = mc_input_file.Get(title+"_bb")
                    mc_bc   = mc_input_file.Get(title+"_bc")
                    mc_bj   = mc_input_file.Get(title+"_bj")
                    mc_cc   = mc_input_file.Get(title+"_cc")
                    mc_cj   = mc_input_file.Get(title+"_cj")
                    mc_jj   = mc_input_file.Get(title+"_jj")
                    fakes_yj = data_input_file.Get(title+"_fakes_yj")
                    fakes_jy = data_input_file.Get(title+"_fakes_jy")
                    fakes_jj = data_input_file.Get(title+"_fakes_jj")
                    data_hist = data_input_file.Get(title+"_data")

                    if p is "pt" : plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,o.split("leading")[0] + "leading " + o.split("leading")[1]+" pT [GeV]",p)
                    else: plotHistos(mc_bb,mc_bc,mc_bj,mc_cc,mc_cj,mc_jj,fakes_yj,fakes_jy,fakes_jj,data_hist,tag,m,title,o.split("leading")[0] + "leading " + o.split("leading")[1]+" "+p,p)



    data_input_file.Close()
    mc_input_file.Close()
