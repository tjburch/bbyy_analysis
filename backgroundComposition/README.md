# backgroundComposition:        
Tools for background composition analysis


    histo_build_scripts/:
   	 	 systemStudies_data.py - makes histograms of interest from data, including data driven fakes plots for yj, yj, jj.
   	 	 systemStudies_mc.py - makes histograms of interest from mc, including data driven fakes plots for yj, yj, jj.

		 all files in this directory require tiny nTuples created by liza
		 Located on NIU's t3 @ /xdata/ebrost/tiny/
		 	 or lxplus   @ /afs/cern.ch/work/e/ebrost/tiny_ntuples/

	plottingCode/:
		systemPlotting.py - Makes plots from histograms created from systemStudies*.py