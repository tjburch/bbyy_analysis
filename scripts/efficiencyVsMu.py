""" Efficiency vs Mu Plotting """
from ROOT import *
import argparse
from AtlasStyle import *
from tinyFunctions import *
import sys

parser = argparse.ArgumentParser('')
parser.add_argument('-n', dest='limitEntriesTo', required=False,
                    help='nEntries to Use')
parser.add_argument('-f', dest='inputFile', required=True,
                    help='input file name')
parser.add_argument('--nonres', dest='nonres', default=False, action="store_true",
                    help='Is nonresonant selection?')
args = parser.parse_args()

# open file
print args.inputFile
sample = args.inputFile
# Get file
treename = 'mini'
infile = TFile(sample)
# Get tree
tree = infile.Get(treename)
nEntries = tree.GetEntries()
print "sample: ", sample," has entries ", nEntries

if args.limitEntriesTo is not None:
	if nEntries < int(args.limitEntriesTo):
		sys.exit('Not that many entries, we only have %s' % nEntries)
	else:
		nEntries = int(args.limitEntriesTo)
		print "Using %i" % nEntries
if nEntries == 0:
    print "no entries :("
    sys.exit()


mu_inital = TH1F('init','init', 9,0,45)

muFinalEfficiency={}

for btag in range (0,3):
	for cut in ["resonant","nonresonant"]:
		for mass in ["low","high"]:
			muFinalEfficiency["%stag_%sMass_%sSelection" % (btag,mass,cut)] = TH1F("%stag_%sMass_%sSelection" % (btag,mass,cut),"%stag_%sMass_%sSelection" % (btag,mass,cut), 9,0,45)


mu_hgam = TH1F('hgam','hgam', 9,0,45)



############################ Fill a bunch of junk #############################
for evt in range(nEntries):
# 
	# get current entry
    tree.GetEntry(evt)

    mu_inital.Fill(tree.mu)

    if tree.hgam_isPassed:
	    mu_hgam.Fill(tree.mu)


	    if passesNonResonantSelection(tree,'low'):
	    
		    muFinalEfficiency["%stag_%sMass_%sSelection" % (tree.yybb_low_btagCat,'low','nonresonant')].Fill(tree.mu)
		    if passesResonantSelection(tree,'low'):
			    muFinalEfficiency["%stag_%sMass_%sSelection" % (tree.yybb_low_btagCat,'low','resonant')].Fill(tree.mu)


	    if passesNonResonantSelection(tree,'high'):
		    muFinalEfficiency["%stag_%sMass_%sSelection" % (tree.yybb_low_btagCat,'high','nonresonant')].Fill(tree.mu)
	    
		    if passesResonantSelection(tree,'high'):
			    muFinalEfficiency["%stag_%sMass_%sSelection" % (tree.yybb_low_btagCat,'high','resonant')].Fill(tree.mu)
	    




############################ Make plots look decent enough  #############################

def plotStyle(effPlot,btag,mass,cut):
	AtlasStyle()
	massPoint = args.inputFile.split("/")[-1:][0].replace('.root','')
	AtlasStyle()
	effPlot.Draw()
	ATLASLabel(0.68, 0.85, "Internal", 1)
        myLabel(0.66, 0.81, 1, ("%s Mass Selection" % (mass)).title())
        if args.nonres: myLabel(0.60, 0.77, 1, 'Nonresonant hh#rightarrow#gamma#gammab#bar{b}, '+str(btag)+'-tag')
        else: 
		myLabel(0.74, 0.77, 1, "X"+args.inputFile.split('_')[2][1:]+', '+str(btag)+'-tag')
		myLabel(0.74, 0.73, 1, cut.title())
	effPlot.SetTitle("BLAH ; <#mu> ; Efficiency ")
	gPad.Update()

	effPlot.GetPaintedGraph().GetYaxis().SetLabelSize(0.040)
	mycanvas.Print('plots/%s_%stag_%sMass_%sSelection_efficiencyVsMu.pdf' % (massPoint,btag,mass,cut))
	#effPlot.Write()




############################ Create and Draw the TEfficiency plots  #############################

efficiencyPlots = {}
mycanvas = TCanvas()
AtlasStyle()
#outfile = TFile("%s_%sTag_effvmu.root" % args.inputFile.split("/")[-1:][0],'RECREATE')
for btag in range (0,3):
	for cut in ["resonant","nonresonant"]:
		for mass in ["low","high"]:
			efficiencyPlots["%stag_%sMass_%sSelection" % (btag,mass,cut)] = TEfficiency( muFinalEfficiency["%stag_%sMass_%sSelection" % (btag,mass,cut)] , mu_inital)
			plotStyle(efficiencyPlots["%stag_%sMass_%sSelection" % (btag,mass,cut)], btag, mass, cut)



