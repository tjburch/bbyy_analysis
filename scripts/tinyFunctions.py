""" analysis functions """
import sys


def get_flavor(jet_label_1, jet_label_2):

    if   jet_label_1 == 5  and jet_label_2 == 5:
        flavor = "bb"
    elif (jet_label_1 == 5 and jet_label_2 == 4) or (jet_label_1 == 4 and jet_label_2 == 5):
        flavor = "bc"
    elif jet_label_1 == 4  and jet_label_2  ==  4:
        flavor = "cc"
    elif (jet_label_1 == 5 and jet_label_2 == 0) or (jet_label_1 == 0 and jet_label_2 == 5):
        flavor = "bj"
    elif (jet_label_1 == 4 and jet_label_2 == 0) or (jet_label_1 == 0 and jet_label_2 == 4):
        flavor = "cj"
    else:
        flavor = "jj"
    return flavor


def integerRound(value,base):
	return int(base*round(float*(value)/base))

def passesNonResonantSelection(tree,massSelection):

    if not tree.hgam_isPassed: return False

    if massSelection == 'low':
	if tree.yybb_cutList_low > 3: return True
        else: return False
    elif massSelection == 'high':
	if tree.yybb_cutList_high >3: return True
	else: return False
    else: sys.exit('ill defined mass selection')

def passesResonantSelection(tree,massSelection):
    """ assumes passesNonResonantSelection """
    if not tree.hgam_isPassed: return False

    if massSelection == 'low':
	if tree.yybb_cutList_low > 4: return True
	else:	return False
    elif massSelection == 'high':
	if tree.yybb_cutList_high > 4: return True
	else: return False
    else: sys.exit('ill defined mass selection')
