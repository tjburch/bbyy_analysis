"""
tyler.james.burch@cern.ch
1: pass the file you want
"""
from ROOT import *
from AtlasStyle import *
import glob
import sys
AtlasStyle()
from array import array
gStyle.SetOptFit(1111111)
gStyle.SetLegendBorderSize(0)

inputFile=sys.argv[1]

massPoints = ['260', '275', '300', '325','350','400','450','500','750', '1000']

Efficiency_ZeroTag, Efficiency_OneTag,Efficiency_TwoTag = [],[],[]
Error_ZeroTag,Error_OneTag,Error_TwoTag = [],[],[]

def getValueArrays(massPoints,y,yerr):
    allmass, massLow,massHigh =[],[],[]
    yFloatLow,yFloatHigh = [],[]
    yErrorLow,yErrorHigh = [],[]
    for m in map(float, massPoints):
        allmass.append(m)
        if m < 400:
            massLow.append(m)
            yFloatLow.append(map(float,y)[allmass.index(m)])
            yErrorLow.append(yerr[allmass.index(m)])
        else:
            massHigh.append(m)
            yFloatHigh.append(map(float,y)[allmass.index(m)])
            yErrorHigh.append(yerr[allmass.index(m)])

    return massLow,massHigh,yFloatLow,yFloatHigh,yErrorLow,yErrorHigh

def zerolistmaker(n):
    listofzeros = [0.0] * n
    return listofzeros

def makeGraph(x,y,yerr,title,tag,mLabel):
    xerr = array('f',zerolistmaker(len(yerr)))
    g = TGraphErrors(len(x),x,y,xerr,yerr)
    c1 = TCanvas()
    g.Fit("pol2")
    g.GetXaxis().SetTitle("Mass [GeV]")
    g.GetYaxis().SetTitle("Efficiency")
    g.GetFunction("pol2").SetLineColor(2)
    g.SetLineWidth(3)
    g.GetYaxis().SetTitleOffset(1.25)
    g.Draw("ap")
    if mLabel == "low" and tag <2:
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(.45)
        ATLASLabel(0.2, 0.9, "Internal", 1)
        myLabel(0.2,0.87,1,str(tag)+"-tag, "+mLabel+" mass selection")
        myLabel(0.2,0.84,1,"Continuum")
    elif mLabel == "low" and tag==2:
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(.45)
        ATLASLabel(0.2, 0.29, "Internal", 1)
        myLabel(0.2,0.26,1,str(tag)+"-tag, "+mLabel+" mass selection")
        myLabel(0.2,0.23,1,"Continuum")
    else:
        gStyle.SetStatX(0.9)
        gStyle.SetStatY(.9)
        print str(tag)+"-tag, "+mLabel+" mass selection"
        ATLASLabel(0.2, 0.29, "Internal", 1)
        myLabel(0.2,0.26,1,str(tag)+"-tag, "+mLabel+" mass selection")
        myLabel(0.2,0.23,1,"Continuum")
    if (0.75 * min(y)) < 0.15: g.SetMinimum(0)
    else: g.SetMinimum(0.85 * min(y))
    c1.Print("continuumEfficiency/"+title+"_continuum.pdf")
    g.Delete()

def generatePlots(massPoints,y,yerr,title,tag):
    xLow,xHigh,yLow,yHigh,yErrorLow,yErrorHigh = getValueArrays(massPoints,y,yerr)
    makeGraph(array("f",xLow),array("f",yLow),array("f",yErrorLow),title+"_Low",tag,"low")
    makeGraph(array("f",xHigh),array("f",yHigh),array("f",yErrorHigh),title+"_High",tag,"high")



inFile = open(inputFile,'r')
content = inFile.readlines()
inFile.close()
zeroTag, oneTag, twoTag = False, False, False

for i,x in enumerate(content):
    x = x.replace(" ","").replace("\n","")
    if x == "0":
        zeroTag = True
        continue
    elif x == "1":
        oneTag = True
        zeroTag = False
        continue
    elif x == "2":
        twoTag = True
        oneTag = False
        continue
    elif i % 2 != 0: #odd line
        if zeroTag: Efficiency_ZeroTag.append(float(x))
        elif oneTag: Error_OneTag.append(float(x)) # odd/even flips for 1 tag
        elif twoTag: Efficiency_TwoTag.append(float(x))
    elif i % 2 == 0: #odd line
        if zeroTag: Error_ZeroTag.append(float(x))
        elif oneTag: Efficiency_OneTag.append(float(x)) # odd/even flips for 1 tag
        elif twoTag: Error_TwoTag.append(float(x))


generatePlots(massPoints,Efficiency_ZeroTag,Error_ZeroTag,"ZeroTag",0)
generatePlots(massPoints,Efficiency_OneTag ,Error_OneTag,"OneTag",1)
generatePlots(massPoints,Efficiency_TwoTag ,Error_TwoTag,"TwoTag",2)
