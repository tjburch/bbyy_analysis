"""
Tyler.james.burch@cern.ch
"""

import glob

inputFilesLow={}
inputFilesHigh={}
for name in glob.glob("*.tex"):
    if name.startswith('mc15c.'):
        if 'lowMass' in name: inputFilesLow[name] = name
        else: inputFilesHigh[name] = name

f_rbh = open('ResBkgYieldHigh.tex','w+')
f_nbh = open('nonResBkgYieldHigh.tex','w+')
f_rbl = open('ResBkgYieldLow.tex','w+')
f_nbl = open('nonResBkgYieldLow.tex','w+')

f_rs = open('ResSigYield.tex','w+')
f_ns = open('nonResSigYield.tex','w+')

files =[f_rbh, f_nbh, f_rbl, f_nbl, f_rs, f_ns]
rType=['resonant','non-resonant','resonant','non-resonant','resonant','non-resonant']
mType=['high','high','low','low','na','na'] #quick hack
sType=['bkg','bkg','bkg','bkg','signal','signal']

backgrounds = ['_ggH', '_VBFH', '_WmH', '_WpH', '_ZH','_ggZH','_ttH','_bbH125_yb2', '_bbH125_ybyt']
signals = ['X260', 'X275', 'X300', 'X325','X350', 'X400','X450','X500','X750', 'X1000']
def getYield(inputFileName,YieldType,idx): #idx == 1 for yield, 2 for error
    """ Returns 0,1,2 tag yields for inputFile """

    inFile = open(inputFileName,'r')
    content = inFile.readlines()
    inFile.close()

    values=[]
    for line in content:
        if YieldType in line:
            values.append(line.split("&")[idx].replace(" ",""))
    return values


for f in files:

    if sType[files.index(f)] == 'bkg': f.write('\\begin{{table}}[] \n\\centering \n\caption[Single Higgs boson yields ({} search)]{{Expected number of background events in the {} analysis (``{} mass\'\' selection) containing single Higgs bosons}}\n'.format(rType[files.index(f)],rType[files.index(f)],mType[files.index(f)]))
    else: f.write('\\begin{{table}}[] \n\\centering \n\caption[Signal Yields]{{Expected number of signal events in the {} analysis.}}\n'.format(rType[files.index(f)]))

    # Could be adjusted to add _res in resonant case
    if(sType[files.index(f)] == 'bkg'):     f.write('\\label{{tab:singleHiggsexpected{}}} \n\\begin{{tabular}}{{|c|c|c|c|}} \n\\hline'.format(mType[files.index(f)]))
    if(sType[files.index(f)] == 'signal'):  f.write('\\label{{tab:Signalexpected}} \n\\begin{{tabular}}{{|c|c|c|c|}} \n\\hline \n'.format(mType[files.index(f)]))

    f.write('Sample &  Number of 0 $b$-tag events  & Number of 1 $b$-tag events & Number of 2 $b$-tag events\\\\ \n\\hline \n\\hline \n')
    for b in backgrounds:
        if sType[files.index(f)] == 'bkg':
            if mType[files.index(f)] == 'low':
                for infile in inputFilesLow:
                    if b in infile:
                        print infile
                        if rType[files.index(f)] == 'resonant': cutToFind='$\gamma\gamma\ SR$'
                        else: cutToFind='$m_{\it{bb}}\ Cut$'

                        yields =getYield(infile,cutToFind,1)
                        if b == '_bbH125_ybyt': f.write('\\bbh pos. & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
                        elif b == '_bbH125_yb2':  f.write('\\bbh neg. & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
                        else:   f.write('\\'+b[1:]+' & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
            else:
                for infile in inputFilesHigh:
                    if b in infile:
                        if rType[files.index(f)] == 'resonant': cutToFind='$\gamma\gamma\ SR$'
                        else: cutToFind='$m_{\it{bb}}\ Cut$'

                        yields = getYield(infile,cutToFind,1)
                        if b == '_bbH125_ybyt':  f.write('\\bbh pos. & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
                        elif b == '_bbH125_yb2':  f.write('\\bbh neg. & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
                        else: f.write('\\'+b[1:]+' & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')

    for s in signals:
        if sType[files.index(f)] == 'signal':
            for infile in inputFilesLow:
                if s[1:] in infile:
                    if rType[files.index(f)] == 'resonant': cutToFind='$\gamma\gamma\ SR$'
                    else: cutToFind='$m_{\it{bb}}\ Cut$'

                    yields =getYield(infile,cutToFind,1)
                    f.write(s+' & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
            for infile in inputFilesHigh:
                if s[1:] in infile:
                    if rType[files.index(f)] == 'resonant': cutToFind='$\gamma\gamma\ SR$'
                    else: cutToFind='$m_{\it{bb}}\ Cut$'

                    yields =getYield(infile,cutToFind,1)
                    f.write(s+' & '+yields[0]+' & '+yields[1]+' & '+yields[2]+' \\\\\n')
    f.write('\hline \n\\end{tabular} \n\\end{table}')
