python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_NNLOPS_ggH125.MxAODDetailed.p3015.h015d_Cutflows.tex -s ggH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_NNPDF30_VBFH125.MxAODDetailed.p3015.h015d_Cutflows.tex -s VBFH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_WmH125J.MxAODDetailed.p3015.h015d_Cutflows.tex -s WmH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_WpH125J.MxAODDetailed.p3015.h015d_Cutflows.tex -s WpH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_ZH125J.MxAODDetailed.p3015.h015d_Cutflows.tex -s ZH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.PowhegPy8_ggZH125.MxAODDetailed.p3015.h015d_Cutflows.tex -s ggZH -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.aMCnloPy8_bbH125_yb2.MxAODDetailed.p2908.h015d_Cutflows.tex -s bbH_positive -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.aMCnloPy8_bbH125_ybyt.MxAODDetailed.p2908.h015d_Cutflows.tex -s bbH_negative -m high
python ../../bbyy_cutflowTools/cutflowToNoteBkg.py -i mc15c.aMCnloPy8_ttH125.MxAODDetailed.p2908.h015d_Cutflows.tex -s ttH -m high
