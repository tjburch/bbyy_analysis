"""
Converts output of yybb cutflow maker to 3 tex tables for each region
pass as:
python cutflowToNote.py -i input.tex -m h -o VBFH, ggH, etc.

tyler.james.burch@cern.ch
"""
import argparse

parser = argparse.ArgumentParser('changes cutflows to note format')
parser.add_argument('-i', dest='inputFile', required=True,
                    help='Input files')
parser.add_argument('-s', dest='sampleType', required=True,
                    help='Sample Type')
parser.add_argument('-m', dest='mass', required=True,
                    help='Mass Selection - "high" or "low')
args = parser.parse_args()


sample = args.inputFile
f = open(sample,'r')

if args.sampleType == "hh-yybb":
    f_0tag = open('cutflow-hh-yybb-'+args.mass+'-0tag.tex','w+')
    f_1tag = open('cutflow-hh-yybb-'+args.mass+'-1tag.tex','w+')
    f_2tag = open('cutflow-hh-yybb-'+args.mass+'-2tag.tex','w+')
else:
    f_0tag = open('cutflow-'+args.mass+'-0tag.tex','w+')
    f_1tag = open('cutflow-'+args.mass+'-1tag.tex','w+')
    f_2tag = open('cutflow-'+args.mass+'-2tag.tex','w+')

content = f.readlines()
f.close()
tag = 0
inTable = False
for i,line in enumerate(content):
    if tag ==0 : line = line.strip()
    if line.startswith("\\begin{table}"):
        inTable = True

    if inTable:
        if "\\tiny" in line:
            line = line.replace("\\tiny","\\footnotesize")
        if "\\it{N}_{xAOD}" in line:
            line = line.replace("\\it{N}","N")
        if "\\it{b}" in line:
            line = line.replace("\\it{b}","b")
        if "\\it{bb}" in line:
            line = line.replace("\\it{bb}","bb")
        if "\\it{p}" in line:
            line = line.replace("\\it{p}","p")
        if "\\it{m}" in line:
            line = line.replace("\\it{m}","m")
        if "\\caption{Cutflow for " in line and args.sampleType == "hh-yybb":
            line = line.split("for")[0]+"for hh-yybb, $\hyy$ - %s tag category (%s mass selection)}\n" % (str(tag),args.mass)
        elif "\\caption{Cutflow for " in line:
            line = line.split("for")[0]+"for resonant "+args.mass+"  $\\rightarrow$ hh $\\rightarrow$ yybb - "+str(tag)+" tag category}\n"
        if tag == 0: f_0tag.write("%s\n"% line)
        if tag == 1: f_1tag.write("%s"% line)
        if tag == 2: f_2tag.write("%s"% line)


    if line.startswith("\\end{table}"):
        inTable = False
        tag += 1
f_0tag.close()
f_1tag.close()
f_2tag.close()
