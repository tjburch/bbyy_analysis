class Cut:

    def __init__(self, cut_name, cutflow_text):
        """ for any given parameter, write value for each tag"""
        values = ["", "", ""]
        i = 0
        # Seek through lines for parameter
        for line in cutflow_text:
            if cut_name in line:
                cut_value = line.split("&")[1]
                cut_value = cut_value.replace(" ", "")
                values[i] = cut_value
                i += 1
        self.zero_tag = float(values[0])
        self.one_tag = float(values[1])
        self.two_tag = float(values[2])
        self.cut_name = cut_name


class Cutflow:

    def __init__(self, input_file):

        # Backgrounds
        sample_types = ['_ggH125', '_VBFH125', '_WmH125', '_wpH125', '_ZH125',
                        '_ggZH125', '_bbH125', '_ttH125']
        # Signal
        sample_types += ['_hh_yybb', '_Xhh_m260', '_Xhh_m275', '_Xhh_m300', '_Xhh_m350',
                         '_Xhh_400', '_Xhh_m450', '_Xhh_500', '_Xhh_m750', '_Xhh_m1000']
        # TODO add lambda values 

        self.sample_type = ''
        for i in sample_types:
            if i in input_file:
                self.sample_type = i[1:]
        if self.sample_type == '':
            self.sample_type = 'unknown_sample'

        self.parameters = ['$\it{N}_{xAOD}$',
                           '$\it{N}_{DxAOD}$',
                           '$All\ events$',
                           '$No\ duplicates$',
                           '$GRL$',
                           '$Pass\ trigger$',
                           '$Detector\ DQ$',
                           '$Has\ PV$',
                           '$2\ loose\ photons$',
                           '$e-\gamma\ ambiguity$',
                           '$Trigger\ match$',
                           '$tight\ ID$',
                           '$isolation$',
                           '$rel.\ \it{p}_{T}\ cuts$',
                           '$\it{m}_{\gamma\gamma}\ \in\ [105,160]\ GeV$',
                           '$2\ Cen\ Jets$',
                           '$\it{b}-tagging$',
                           '$\it{b}Jet\ p_{T}\ Cuts$',
                           '$m_{\it{bb}}\ Cut$',
                           '$\gamma\gamma\ SR$']

        f = open(input_file, 'r')
        content = f.readlines()
        f.close()

        self.cutflow_text = content
        self.cuts = []
        for cut_name in self.parameters:
            self.cuts.append(Cut(cut_name, self.cutflow_text))
