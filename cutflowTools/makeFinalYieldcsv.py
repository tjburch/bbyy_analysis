"""
tyler.james.burch@cern.ch
"""

import glob

inputFilesLow={}
inputFilesHigh={}
for name in glob.glob("*.tex"):
    if name.startswith('mc15c.'):
        if 'lowMass' in name: inputFilesLow[name.split(".")[1]+'_low'] = name
        else: inputFilesHigh[name.split(".")[1]] = name


def getYield(inputFileName,YieldType,idx): #idx == 1 for yield, 2 for error
    """ Returns 2,1,0 tag yields for inputFile """

    inFile = open(inputFileName,'r')
    content = inFile.readlines()
    inFile.close()

    values=[]
    for line in content:
        if YieldType in line:
            #print line.split("&")[1].replace(" ","")
            values.append(line.split("&")[idx].replace(" ",""))
    return list(reversed(values))#values.reverse()

ResLine ='$\gamma\gamma\ SR$'
NonResLine ='$m_{\it{bb}}\ Cut$'
ResYieldsLow={}
NonResYieldsLow={}
ResYieldsHigh={}
NonResYieldsHigh={}
ResErrorHigh={}
NonResErrorHigh={}
ResErrorLow={}
NonResErrorLow={}
for key in inputFilesLow:
    ResYieldsLow[key] = getYield(inputFilesLow[key],ResLine,1)
    NonResYieldsLow[key] = getYield(inputFilesLow[key],NonResLine,1)
    ResErrorLow[key] = getYield(inputFilesLow[key],ResLine,2)
    NonResErrorLow[key] = getYield(inputFilesLow[key],NonResLine,2)
for key in inputFilesHigh:
    ResYieldsHigh[key] = getYield(inputFilesHigh[key],ResLine,1)
    NonResYieldsHigh[key] = getYield(inputFilesHigh[key],NonResLine,1)
    ResErrorHigh[key] = getYield(inputFilesHigh[key],ResLine,2)
    NonResErrorHigh[key] = getYield(inputFilesHigh[key],NonResLine,2)

f_out = open('yields.csv','w+')
f_out.write('tight myy cut,,,,,,\n')
f_out.write(',Low,,,High,,\n')
f_out.write(',2-tag,1-tag,0-tag,2-tag,1-tag,0-tag\n')
for keyHigh in sorted(ResYieldsHigh):
    for keyLow in sorted(ResYieldsLow):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            f_out.write(keyHigh+","+ResYieldsLow[keyLow][0]+","+ResYieldsLow[keyLow][1]+","+ResYieldsLow[keyLow][2]+","+ResYieldsHigh[keyHigh][0]+","+ResYieldsHigh[keyHigh][1]+","+ResYieldsHigh[keyHigh][2]+"\n")
for keyLow in sorted(ResYieldsLow):
    if keyLow.startswith("aMcAtNloHwpp_Xhh_m"): # Resonant  signal files
        f_out.write(keyLow+","+ResYieldsLow[keyLow][0]+","+ResYieldsLow[keyLow][1]+","+ResYieldsLow[keyLow][2]+","+ResYieldsLow[keyLow][0]+","+ResYieldsLow[keyLow][1]+","+ResYieldsLow[keyLow][2]+"\n")
for keyHigh in sorted(ResYieldsHigh):
    if keyHigh.startswith("aMcAtNloHwpp_Xhh_m"): # Resonant  signal files
        f_out.write(keyHigh+","+ResYieldsLow[keyLow][0]+","+ResYieldsLow[keyLow][1]+","+ResYieldsLow[keyLow][2]+","+ResYieldsHigh[keyHigh][0]+","+ResYieldsHigh[keyHigh][1]+","+ResYieldsHigh[keyHigh][2]+"\n")

f_out.write('myy error,,,,,,\n')
f_out.write(',Low,,,High,,\n')
f_out.write(',2-tag,1-tag,0-tag,2-tag,1-tag,0-tag\n')
for keyHigh in sorted(ResYieldsHigh):
    for keyLow in sorted(ResYieldsLow):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            f_out.write(keyHigh+","+ResErrorLow[keyLow][0]+","+ResErrorLow[keyLow][1]+","+ResErrorLow[keyLow][2]+","+ResErrorHigh[keyHigh][0]+","+ResErrorHigh[keyHigh][1]+","+ResErrorHigh[keyHigh][2]+"\n")
for keyLow in sorted(ResErrorLow):
    if keyLow.startswith("aMcAtNloHwpp_Xhh_m"): # Resonant  signal files
        f_out.write(keyLow+","+ResErrorLow[keyLow][0]+","+ResErrorLow[keyLow][1]+","+ResErrorLow[keyLow][2]+","+ResErrorLow[keyLow][0]+","+ResErrorLow[keyLow][1]+","+ResErrorLow[keyLow][2]+"\n")
for keyHigh in sorted(ResErrorHigh):
    if keyHigh.startswith("aMcAtNloHwpp_Xhh_m"): # Resonant  signal files
        f_out.write(keyHigh+","+ResErrorLow[keyLow][0]+","+ResErrorLow[keyLow][1]+","+ResErrorLow[keyLow][2]+","+ResErrorHigh[keyHigh][0]+","+ResErrorHigh[keyHigh][1]+","+ResErrorHigh[keyHigh][2]+"\n")

f_out.write('mbb cut,,,,,,\n')
f_out.write(',Low,,,High,,\n')
f_out.write(',2-tag,1-tag,0-tag,2-tag,1-tag,0-tag\n')
for keyHigh in sorted(ResYieldsHigh):
    for keyLow in sorted(ResYieldsLow):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            f_out.write(keyHigh+","+NonResYieldsLow[keyLow][0]+","+NonResYieldsLow[keyLow][1]+","+NonResYieldsLow[keyLow][2]+","+NonResYieldsHigh[keyHigh][0]+","+NonResYieldsHigh[keyHigh][1]+","+NonResYieldsHigh[keyHigh][2]+"\n")
for keyLow in sorted(NonResYieldsLow):
    if keyLow.startswith("aMcAtNloHwpp_Xhh_m"): # NonResonant  signal files
        f_out.write(keyLow+","+NonResYieldsLow[keyLow][0]+","+NonResYieldsLow[keyLow][1]+","+NonResYieldsLow[keyLow][2]+","+NonResYieldsLow[keyLow][0]+","+NonResYieldsLow[keyLow][1]+","+NonResYieldsLow[keyLow][2]+"\n")
for keyHigh in sorted(NonResYieldsHigh):
    if keyHigh.startswith("aMcAtNloHwpp_Xhh_m"): # NonResonant  signal files
        f_out.write(keyHigh+","+NonResYieldsLow[keyLow][0]+","+NonResYieldsLow[keyLow][1]+","+NonResYieldsLow[keyLow][2]+","+NonResYieldsHigh[keyHigh][0]+","+NonResYieldsHigh[keyHigh][1]+","+NonResYieldsHigh[keyHigh][2]+"\n")

f_out.write('mbb error,,,,,,\n')
f_out.write(',Low,,,High,,\n')
f_out.write(',2-tag,1-tag,0-tag,2-tag,1-tag,0-tag\n')
for keyHigh in sorted(ResYieldsHigh):
    for keyLow in sorted(ResYieldsLow):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            f_out.write(keyHigh+","+NonResErrorLow[keyLow][0]+","+NonResErrorLow[keyLow][1]+","+NonResErrorLow[keyLow][2]+","+NonResErrorHigh[keyHigh][0]+","+NonResErrorHigh[keyHigh][1]+","+NonResErrorHigh[keyHigh][2]+"\n")
for keyLow in sorted(NonResErrorLow):
    if keyLow.startswith("aMcAtNloHwpp_Xhh_m"): # NonResonant  signal files
        f_out.write(keyLow+","+NonResErrorLow[keyLow][0]+","+NonResErrorLow[keyLow][1]+","+NonResErrorLow[keyLow][2]+","+NonResErrorLow[keyLow][0]+","+NonResErrorLow[keyLow][1]+","+NonResErrorLow[keyLow][2]+"\n")
for keyHigh in sorted(NonResErrorHigh):
    if keyHigh.startswith("aMcAtNloHwpp_Xhh_m"): # NonResonant  signal files
        f_out.write(keyHigh+","+NonResErrorLow[keyLow][0]+","+NonResErrorLow[keyLow][1]+","+NonResErrorLow[keyLow][2]+","+NonResErrorHigh[keyHigh][0]+","+NonResErrorHigh[keyHigh][1]+","+NonResErrorHigh[keyHigh][2]+"\n")
