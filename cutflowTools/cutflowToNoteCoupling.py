"""
Converts output of yybb cutflow maker to 3 tex tables for each region
pass as:
python cutflowToNoteCoupling.py -i input.tex -l lambda

tyler.james.burch@cern.ch
"""
import argparse

parser = argparse.ArgumentParser('changes cutflows to note format')
parser.add_argument('-i', dest='inputFile', required=True,
                    help='Input files')
parser.add_argument('-l', dest='lambdaValue', required=True,
                    help='lambda value')
args = parser.parse_args()


sample = args.inputFile
if "lowMass" in sample:
    massSelection = "lowMass"
else: massSelection = "highMass"
f = open(sample,'r')

f_0tag = open('cutflow-'+args.lambdaValue+'-0tag.tex','w+')
f_1tag = open('cutflow-'+args.lambdaValue+'-1tag.tex','w+')
f_2tag = open('cutflow-'+args.lambdaValue+'-2tag.tex','w+')


content = f.readlines()
f.close()
tag = 0
inTable = False

# get lambda integer
if "plus" in args.lambdaValue.split("_")[0]:
    lambdaInt = int(args.lambdaValue.split("_")[1])
elif "minus" in args.lambdaValue.split("_")[0]:
    lambdaInt = -int(args.lambdaValue.split("_")[1])
else:
    lambdaInt = 0

for i,line in enumerate(content):
    if tag ==0 : line = line.strip()
    if line.startswith("\\begin{table}"):
        inTable = True

    if inTable:
        if "\\tiny" in line:
            line = line.replace("\\tiny","\\footnotesize")
        if "\\it{N}_" in line:
            line = line.replace("\\it{N}","N")
        if "\\it{b}" in line:
            line = line.replace("\\it{b}","b")
        if "\\it{bb}" in line:
            line = line.replace("\\it{bb}","bb")
        if "\\it{p}" in line:
            line = line.replace("\\it{p}","p")
        if "\\it{m}" in line:
            line = line.replace("\\it{m}","m")
        if "\\caption{Cutflow for " in line:
            line = line.split("for")[0]+"for LO di-Higgs production with $\\lambda/\\lambda_{SM} = "+str(lambdaInt)+"$. "+str(tag)+"-tag category ("+massSelection.strip("Mass")+" mass selection)}\n"
        if tag == 0: f_0tag.write("%s\n"% line)
        if tag == 1: f_1tag.write("%s"% line)
        if tag == 2: f_2tag.write("%s"% line)


    if line.startswith("\\end{table}"):
        inTable = False
        tag += 1
f_0tag.close()
f_1tag.close()
f_2tag.close()
