"""
tyler.james.burch@cern.ch
pass the directory you want
"""
from ROOT import *
from AtlasStyle import *
import glob
import sys
AtlasStyle()

inputFiles={}
for name in glob.glob(sys.argv[1]+"*.tex"):
    if 'aMcAtNloHwpp_Xhh' in name: inputFiles[name] = name

output = TFile("EfficiencyPlots.root","recreate")
massPoints = ['260', '275', '300', '325','350','400','450','500','750', '1000']
NonResEfficiency_ZeroTag,NonResEfficiency_OneTag,NonResEfficiency_TwoTag = [],[],[]
ResEfficiency_ZeroTag,ResEfficiency_OneTag,ResEfficiency_TwoTag = [],[],[]
NonResError_ZeroTag,NonResError_OneTag,NonResError_TwoTag = [],[],[]
ResError_ZeroTag,ResError_OneTag,ResError_TwoTag = [],[],[]

def getYield(inputFileName,YieldType,idx): #idx == 1 for yield, 2 for error, 3 for efficiency
    """ Returns [0,1,2] tag yields for inputFile """
    inFile = open(inputFileName,'r')
    content = inFile.readlines()
    inFile.close()

    values=[]
    for line in content:
        if YieldType in line:
            values.append(line.split("&")[idx].replace(" ","").replace("\\",""))
    return values
def getYieldError(inputFileName,YieldType):
    inFile = open(inputFileName,'r')
    content = inFile.readlines()
    inFile.close()

    nTotalEvents,nTotalEventsError=[],[]
    nPass,nPassError=[],[]
    for line in content:
        if '$\it{N}_{xAOD}$' in line:
            nTotalEvents.append(float(line.split("&")[1].replace(" ","").replace("\\","")))
            nTotalEventsError.append(float(line.split("&")[2].replace(" ","").replace("\\","")))
        if YieldType in line:
            nPass.append(float(line.split("&")[1].replace(" ","").replace("\\","")))
            nPassError.append(float(line.split("&")[2].replace(" ","").replace("\\","")))

    errors=[]
    for i,evt in enumerate(nTotalEvents):
        errors.append( (nPass[i]/nTotalEvents[i]) * TMath.Sqrt( (nPassError[i]/nPass[i])**2 + (nTotalEventsError[i]/nTotalEvents[i])**2 ))
    return errors


for s in massPoints:
    for infile in inputFiles:
        if s in infile:
            # Get Resonant Yield
            nTotalEvents = getYield(infile,'$\it{N}_{xAOD}$',1)
            yields =getYield(infile,'$\gamma\gamma\ SR$',1)
            ResEfficiency_ZeroTag.append(float(yields[0])/float(nTotalEvents[0]))
            ResEfficiency_OneTag.append(float(yields[1])/float(nTotalEvents[1]))
            ResEfficiency_TwoTag.append(float(yields[2])/float(nTotalEvents[2]))
            errors = getYieldError(infile,'$\gamma\gamma\ SR$')
            ResError_ZeroTag.append(errors[0])
            ResError_OneTag.append(errors[1])
            ResError_TwoTag.append(errors[2])

            # Get nonResonant Yield
            yields =getYield(infile,'$m_{\it{bb}}\ Cut$',1)
            NonResEfficiency_ZeroTag.append(float(yields[0])/float(nTotalEvents[0]))
            NonResEfficiency_OneTag.append(float(yields[1])/float(nTotalEvents[1]))
            NonResEfficiency_TwoTag.append(float(yields[2])/float(nTotalEvents[2]))
            errors =getYieldError(infile,'$m_{\it{bb}}\ Cut$')
            NonResError_ZeroTag.append(errors[0])
            NonResError_OneTag.append(errors[1])
            NonResError_TwoTag.append(errors[2])


from array import array
gStyle.SetOptFit(1111111)
gStyle.SetLegendBorderSize(0)
def getValueArrays(massPoints,y,yerr):
    allmass, massLow,massHigh =[],[],[]
    yFloatLow,yFloatHigh = [],[]
    yErrorLow,yErrorHigh = [],[]
    for m in map(float, massPoints):
        allmass.append(m)
        if m <= 500:
            massLow.append(m)
            yFloatLow.append(map(float,y)[allmass.index(m)])
            yErrorLow.append(yerr[allmass.index(m)])
        else:
            massHigh.append(m)
            yFloatHigh.append(map(float,y)[allmass.index(m)])
            yErrorHigh.append(yerr[allmass.index(m)])

    return massLow,massHigh,yFloatLow,yFloatHigh,yErrorLow,yErrorHigh

def zerolistmaker(n):
    listofzeros = [0.0] * n
    return listofzeros

def makeGraph(x,y,yerr,title,tag,mLabel):
    xerr = array('f',zerolistmaker(len(yerr)))
    g = TGraphErrors(len(x),x,y,xerr,yerr)
    c1 = TCanvas()
    g.Fit("pol2")
    g.GetXaxis().SetTitle("Resonant Mass [GeV]")
    g.GetYaxis().SetTitle("Efficiency")
    g.GetFunction("pol2").SetLineColor(2)
    g.SetLineWidth(3)
    g.SetMinimum(0.8 * min(y))
    g.GetYaxis().SetTitleOffset(1.25)
    gStyle.SetStatX(0.9)
    gStyle.SetStatY(.45)
    g.Draw("ap")
    ATLASLabel(0.2, 0.9, "Internal", 1)
    if "Nonresonant" in title: myLabel(0.2,0.86,1,str(tag)+"-tag, "+mLabel+" mass non-resonant selection")
    else: myLabel(0.2,0.86,1,str(tag)+"-tag, "+mLabel+" mass resonant selection")
    c1.Print("efficiencyPlots/"+title+".pdf")
    g.Write()


def generatePlots(massPoints,y,yerr,title,tag):
    xLow,xHigh,yLow,yHigh,yErrorLow,yErrorHigh = getValueArrays(massPoints,y,yerr)
    makeGraph(array("f",xLow),array("f",yLow),array("f",yErrorLow),title+"_Low",tag,"low")
    makeGraph(array("f",xHigh),array("f",yHigh),array("f",yErrorHigh),title+"_High",tag,"high")

generatePlots(massPoints,NonResEfficiency_ZeroTag,NonResError_ZeroTag,"Nonresonant_ZeroTag",0)
generatePlots(massPoints,NonResEfficiency_OneTag ,NonResError_OneTag,"Nonresonant_OneTag",1)
generatePlots(massPoints,NonResEfficiency_TwoTag ,NonResError_TwoTag,"Nonresonant_TwoTag",2)
generatePlots(massPoints,ResEfficiency_ZeroTag ,ResError_ZeroTag,"Resonant_ZeroTag",0)
generatePlots(massPoints,ResEfficiency_OneTag,ResError_OneTag,"Resonant_OneTag",1)
generatePlots(massPoints,ResEfficiency_TwoTag,ResError_TwoTag,"Resonant_TwoTag",2)


for s in massPoints:
    for infile in inputFiles:
        if s in infile:
            # Get Resonant Yield
            yields =getYield(infile,'$\gamma\gamma\ SR$',1)
            ResEfficiency_ZeroTag.append(yields[0].strip('\n'))
            ResEfficiency_OneTag.append(yields[1].strip('\n'))
            ResEfficiency_TwoTag.append(yields[2].strip('\n'))
            # Get nonResonant Yield
            yields =getYield(infile,'$m_{\it{bb}}\ Cut$',1)
            NonResEfficiency_ZeroTag.append(yields[0].strip('\n'))
            NonResEfficiency_OneTag.append(yields[1].strip('\n'))
            NonResEfficiency_TwoTag.append(yields[2].strip('\n'))

def getEffError(x,xerr,N,Nerr):
    return (x/N) * TMath.Sqrt( (xerr/x)**2 + (Nerr/N)**2 )


cutEfficiency_ZeroTag,cutEfficiency_OneTag,cutEfficiency_TwoTag=[],[],[]
cutError_ZeroTag,cutError_OneTag,cutError_TwoTag=[],[],[]
for i,x in enumerate(NonResError_ZeroTag):
    cutEfficiency_ZeroTag.append(float(ResEfficiency_ZeroTag[i])/float(NonResEfficiency_ZeroTag[i]))
    cutEfficiency_OneTag.append(float(ResEfficiency_OneTag[i])/float(NonResEfficiency_OneTag[i]))
    cutEfficiency_TwoTag.append(float(ResEfficiency_TwoTag[i])/float(NonResEfficiency_TwoTag[i]))
    cutError_ZeroTag.append(getEffError(ResEfficiency_ZeroTag[i],ResError_ZeroTag[i],NonResEfficiency_ZeroTag[i],NonResError_ZeroTag[i]))
    cutError_OneTag.append(getEffError(ResEfficiency_OneTag[i],ResError_OneTag[i],NonResEfficiency_OneTag[i],NonResError_OneTag[i]))
    cutError_TwoTag.append(getEffError(ResEfficiency_TwoTag[i],ResError_TwoTag[i],NonResEfficiency_TwoTag[i],NonResError_TwoTag[i]))


def makeMultiGraphComps(x,y,yerr,title,tag,mLabel):
    xerr = array('f',zerolistmaker(len(yerr)))
    g = TGraphErrors(len(x),x,y,xerr,yerr)
    c1 = TCanvas()
    gStyle.SetOptFit(000000)
    g.Fit("pol1")
    g.SetMarkerColor(tag+2)
    g.SetLineColor(tag+2)
    g.GetFunction("pol1").SetLineColor(tag+2)
    g.SetLineWidth(3)
    g.SetTitle(str(tag)+" b-tag category")
    return g

def generateGraphs(massPoints,y,yerr,title,tag):
    xLow,xHigh,yLow,yHigh,yErrorLow,yErrorHigh = getValueArrays(massPoints,y,yerr)
    gL = makeMultiGraphComps(array("f",xLow),array("f",yLow),array("f",yErrorLow),title+"_Low",tag,"low")
    gH = makeMultiGraphComps(array("f",xHigh),array("f",yHigh),array("f",yErrorHigh),title+"_High",tag,"high")
    return gL,gH

g0L,g0H = generateGraphs(massPoints,cutEfficiency_ZeroTag,cutError_ZeroTag,"cutEfficiency_ZeroTag",0)
g1L,g1H = generateGraphs(massPoints,cutEfficiency_OneTag,cutError_OneTag,"cutEfficiency_OneTag",1)
g2L,g2H = generateGraphs(massPoints,cutEfficiency_TwoTag,cutError_TwoTag,"cutEfficiency_TwoTag",2)

mgL = TMultiGraph()
mgL.Add(g0L)
mgL.Add(g1L)
mgL.Add(g2L)
c1 = TCanvas()
mgL.Draw("AP")
mgL.GetYaxis().SetTitle("m_{#gamma#gamma} cut efficiency")
mgL.GetXaxis().SetTitle("M_{X} [GeV]")
mgL.SetMinimum(0.9 * min(cutEfficiency_TwoTag))
#mgL.SetMaximum(1)
mgL.GetYaxis().SetTitleOffset(1.25)

leg = TLegend(0.70,0.2,0.90,0.4);
leg.AddEntry(g0L,"0 b-tag category","pl")
leg.AddEntry(g1L,"1 b-tag category","pl")
leg.AddEntry(g2L,"2 b-tag category","pl")
leg.Draw()
zTagFitParams = [g0L.GetFunction("pol1").GetParameter(0),g0L.GetFunction("pol1").GetParameter(1)]
oTagFitParams = [g1L.GetFunction("pol1").GetParameter(0),g1L.GetFunction("pol1").GetParameter(1)]
tTagFitParams = [g2L.GetFunction("pol1").GetParameter(0),g2L.GetFunction("pol1").GetParameter(1)]
myLabel(0.2, 0.28,1, "#color[2]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (zTagFitParams[0], zTagFitParams[1]))
myLabel(0.2, 0.24,1, "#color[3]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (oTagFitParams[0], oTagFitParams[1]))
myLabel(0.2, 0.20,1, "#color[4]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (tTagFitParams[0], tTagFitParams[1]))

c1.Print("efficiencyLowMass.pdf")
c1.Clear()


mgH = TMultiGraph()
mgH.Add(g0H)
mgH.Add(g1H)
mgH.Add(g2H)
c1.cd()
mgH.Draw("AP")
mgH.GetYaxis().SetTitle("m_{#gamma#gamma} cut efficiency")
mgH.GetXaxis().SetTitle("M_{X} [GeV]")
mgH.SetMinimum(0.9 * min(cutEfficiency_TwoTag))
#mgH.SetMaximum(1)
mgH.GetYaxis().SetTitleOffset(1.25)
leg = TLegend(0.70,0.2,0.90,0.4);
leg.AddEntry(g0H,"0 b-tag category","pl")
leg.AddEntry(g1H,"1 b-tag category","pl")
leg.AddEntry(g2H,"2 b-tag category","pl")
leg.Draw()

zTagFitParams = [g0H.GetFunction("pol1").GetParameter(0),g0H.GetFunction("pol1").GetParameter(1)]
oTagFitParams = [g1H.GetFunction("pol1").GetParameter(0),g1H.GetFunction("pol1").GetParameter(1)]
tTagFitParams = [g2H.GetFunction("pol1").GetParameter(0),g2H.GetFunction("pol1").GetParameter(1)]
myLabel(0.2, 0.28,1, "#color[2]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (zTagFitParams[0], zTagFitParams[1]))
myLabel(0.2, 0.24,1, "#color[3]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (oTagFitParams[0], oTagFitParams[1]))
myLabel(0.2, 0.20,1, "#color[4]{#epsilon_{m_#gamma#gamma} = %0.5f+%0.5f M_{X}}" % (tTagFitParams[0], tTagFitParams[1]))

c1.Print("efficiencyHighMass.pdf")
