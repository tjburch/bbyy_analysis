"""
tyler.james.burch@cern.ch
"""

import glob

inputFilesLow={}
inputFilesHigh={}
for name in glob.glob("*.tex"):
    if name.startswith('mc15c.'):
        if 'lowMass' in name: inputFilesLow[name.split(".")[1]+'_low'] = name
        else: inputFilesHigh[name.split(".")[1]] = name


def getYield(inputFileName):
    """ Returns 2,1,0 tag yields for inputFile """
    inFile = open(inputFileName,'r')
    content = inFile.readlines()
    inFile.close()

    values=[]
    for line in content:
        if '$m_{\it{bb}}\ Cut$' in line:
            #print line.split("&")[1].replace(" ","")
            values.append(line.split("&")[2].replace(" ",""))
    return list(reversed(values))#values.reverse()

yieldsLow={}
yieldsHigh={}
for key in inputFilesLow:
    yieldsLow[key] = getYield(inputFilesLow[key])
for key in inputFilesHigh:
    yieldsHigh[key] = getYield(inputFilesHigh[key])

f_out = open('yields.csv','w+')
f_out.write(',high,,,Low,,\n')
f_out.write(',2-tag,1-tag,0-tag,2-tag,1-tag,0-tag\n')
for keyLow in sorted(yieldsLow):
    for keyHigh in sorted(yieldsHigh):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            f_out.write(keyHigh+","+yieldsLow[keyLow][0]+","+yieldsLow[keyLow][1]+","+yieldsLow[keyLow][2]+","+yieldsHigh[keyHigh][0]+","+yieldsHigh[keyHigh][1]+","+yieldsHigh[keyHigh][2]+"\n")


f_tex = open('yieldsLow.tex','w+')
f_tex.write('\\begin{tabular}{|c|c|c|}\n\\hline\n')
f_tex.write('Sample & number of 0 $b$-tag events & number of 1 $b$-tag events & number of 2 $b$-tag events\n\\hline\n\\hline\n')

for keyLow in sorted(yieldsLow):
    if not "bbH125" in keyHigh: f_tex.write("\\"+keyHigh.split('_')[1][:-3]+","+yieldsLow[keyLow][0]+","+yieldsLow[keyLow][1]+","+yieldsLow[keyLow][2]+"\n")
    elif "bbH125" in keyHigh and float(yieldsLow[keyLow][0]) > 0: f_tex.write("\\"+keyHigh.split('_')[1].replace("125","")+" pos.,"+yieldsLow[keyLow][0]+","+yieldsLow[keyLow][1]+","+yieldsLow[keyLow][2]+"\n")
    elif "bbH125" in keyHigh and float(yieldsLow[keyLow][0]) < 0: f_tex.write("\\"+keyHigh.split('_')[1].replace("125","")+" neg.,"+yieldsLow[keyLow][0]+","+yieldsLow[keyLow][1]+","+yieldsLow[keyLow][2]+"\n")

f_tex.write('\\hline\n\\end{tablular}\n\\end{table}')
f_tex.close()

f_tex = open('yieldsHigh.tex','w+')
f_tex.write('\\begin{tabular}{|c|c|c|}\n\\hline\n')
f_tex.write('Sample & number of 0 $b$-tag events & number of 1 $b$-tag events & number of 2 $b$-tag events\n\\hline\n\\hline\n')

for keyLow in sorted(yieldsLow):
    for keyHigh in sorted(yieldsHigh):
        if keyLow == keyHigh+"_low":       # if we're on the same process (low + high)
            if not "bbH125" in keyHigh: f_tex.write("\\"+keyHigh.split('_')[1][:-3]+","+yieldsHigh[keyHigh][0]+","+yieldsHigh[keyHigh][1]+","+yieldsHigh[keyHigh][2]+"\n")
            elif "bbH125" in keyHigh and float(yieldsLow[keyLow][0]) > 0: f_tex.write("\\"+keyHigh.split('_')[1].replace("125","")+" pos.,"+yieldsHigh[keyHigh][0]+","+yieldsHigh[keyHigh][1]+","+yieldsHigh[keyHigh][2]+"\n")
            elif "bbH125" in keyHigh and float(yieldsLow[keyLow][0]) < 0: f_tex.write("\\"+keyHigh.split('_')[1].replace("125","")+" neg.,"+yieldsHigh[keyHigh][0]+","+yieldsHigh[keyHigh][1]+","+yieldsHigh[keyHigh][2]+"\n")

f_tex.write('\\hline\n\\end{tablular}\n\\end{table}')
f_tex.close()
