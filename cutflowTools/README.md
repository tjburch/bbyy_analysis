# cutflowTools/:
Tools for backend files created from cutflow.tex files
TODO: Refactor most of this code. Improve consistency.

	cutflowShellScripts/ - examples of how to run all the cutflows
	cutflowToCSV.py - turns cutflows to CSV files for easy import to google doc
	cutflowToNoteBkg.py - formats background process cutflows for note (isolates table)
	cutflowToNoteSignal.py - same for signal
	makeEfficiencyPlots.py - makes efficiency plots by parsing cutflows and grabbing numbers