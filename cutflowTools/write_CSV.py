from cutflow_class import Cutflow
import argparse
parser = argparse.ArgumentParser('changes cutflows to note format')
parser.add_argument('-i', dest='inputFile', required=True,
                    help='Input files')
parser.add_argument('-o', dest='outputFile', required=False,
                    default='cutflow', help='Output File Name')
parser.add_argument('-s', dest='sampleOutput', required=False,
                    action='store_true', help='Output File Name')
args = parser.parse_args()

input_cutflow = Cutflow(args.inputFile)


if args.sampleOutput:
    output_name = input_cutflow.sample_type
else:
    output_name = args.outputFile

f_CSV = open(output_name + '.csv', 'w+')

f_CSV.write(',2-tag,1-tag,0-tag \n')

for element in input_cutflow.parameters:
    idx = input_cutflow.parameters.index(element)
    this_cut = input_cutflow.cuts[idx]
    f_CSV.write('%s, %f, %f, %f\n' % (element.replace(',', ' '),
                                      this_cut.two_tag, this_cut.one_tag, this_cut.zero_tag))

f_CSV.close()
