from rootpy.tree import Tree
from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter
from matplotlib import gridspec
import numpy as np

# Make a generic ratio plot definition for rootpy (later move this to github)
def ratio_plot_configuration():

    fig = plt.figure(figsize=(7,8))
    gs = gridspec.GridSpec(nrows=2, ncols=1, height_ratios=[3,1])
    ax0 = plt.subplot(gs[0])
    ax0.get_xaxis().set_visible(False)
    yticks = ax0.yaxis.get_major_ticks()

    yticks[0].set_visible(False)
    ax1 = plt.subplot(gs[1])
    gs.update(wspace=0, hspace=0)
    return fig, [ax0, ax1]


def fill_ratio(h_list):
    # Divide histograms, looks like cloning happens under the hood
    h_ratio = h_list[0].divide(h_list[0], h_list[1]) # This is weird.
    h_ratio.linecolor = 'navy'
    plt.tight_layout()
    rplt.errorbar(h_ratio, markersize=0)
    plt.axhline(y=1, color='black', linestyle=':')


# Define way to fill
def plot_variable(h_list, x_title):
    """ Generates plot for a given variable

    Arguments:
        h_list {list} -- List of input histograms, fixed len 2
        x_title {string} -- Title for x-axis
    """
    bad = False
    for h in h_list:
        if h.Integral() == 0:
            bad = True
    if bad: return

    # generate figure
    fig, ax_list = ratio_plot_configuration()
    plt.sca(ax_list[0])
    for ax in ax_list:
        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())
    # Plan the colormap
    h_list[0].linecolor = 'cornflowerblue'
    h_list[1].linecolor = 'firebrick'

    # things to do to all histograms
    for h in h_list:
        h.Scale(1/h.Integral())
        rplt.errorbar(h, markersize=0)

    leg = plt.legend(ncol=2, fontsize=9)
    plt.ylabel('AU', position=(0, 1),  labelpad=20, fontsize=16)

    # Make ratio plot
    plt.sca(ax_list[1])
    fill_ratio(h_list)
    ax_list[0].get_shared_x_axes().join(ax_list[0], ax_list[1])
    ax_list[1].get_shared_x_axes().join(ax_list[0], ax_list[1])
    plt.ylim(0,2)

    # Label that guy up
    plt.xlabel(x_title, position=(1, 0), va='bottom', ha='right', labelpad=20,  fontsize=14)
    plt.ylabel(r'$\frac{Pythia}{Herwig}$', labelpad=20, fontsize=16)
    # ATLAS Style
    # plt.annotate(s='Tyler James Burch', xy=(.005,.007), xycoords='figure fraction',
    #              textcoords='figure fraction', color='grey',alpha=0.7)

    plt.tight_layout()



# Set some paths
f_pythia = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/HGamHadronisationStudy_pythiaDipole.root'
f_herwig = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/HGamHadronisationStudy_herwig.root'
output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/herwig/'

# Load files into a dictionary
root_files = {
    'Pythia8' : root_open(f_pythia),
    'Herwig' : root_open(f_herwig)
}

# Define variables to look at
m_yy, m_yyjj, m_jj =[], [], []
j1_phi, j2_phi, y1_phi, y2_phi = [], [], [], []
j1_eta, j2_eta, y1_eta, y2_eta = [], [], [], []
pt_j1 , pt_j2 , pt_y1 , pt_y2 = [], [], [], []
njets = []
jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []
deta_maxPtjj =[]
zeppenfeld =[]

# Get some useful info in the title
def make_title(histogram, key):
    nentries = histogram.GetEntries()
    mean = histogram.GetMean()
    return "%s\nEntries = %i\nMean = %.2f" % (key, nentries, mean)

# Fill those variables
for sample in root_files:
    m_yy.append(root_files[sample].m_yy_lowMass.Clone(title=make_title(root_files[sample].m_yy_lowMass, sample)))
    m_yyjj.append(root_files[sample].m_yyjj_lowMass.Clone(title=make_title(root_files[sample].m_yyjj_lowMass, sample)))
    m_jj.append(root_files[sample].m_jj_lowMass.Clone(title=make_title(root_files[sample].m_jj_lowMass, sample)))
    j1_phi.append(root_files[sample].phi_j1_lowMass.Clone(title=make_title(root_files[sample].phi_j1_lowMass, sample)))
    j2_phi.append(root_files[sample].phi_j2_lowMass.Clone(title=make_title(root_files[sample].phi_j2_lowMass, sample)))
    y1_phi.append(root_files[sample].phi_y1_lowMass.Clone(title=make_title(root_files[sample].phi_y1_lowMass, sample)))
    y2_phi.append(root_files[sample].phi_y2_lowMass.Clone(title=make_title(root_files[sample].phi_y2_lowMass, sample)))
    j1_eta.append(root_files[sample].eta_j1_lowMass.Clone(title=make_title(root_files[sample].eta_j1_lowMass, sample)))
    j2_eta.append(root_files[sample].eta_j2_lowMass.Clone(title=make_title(root_files[sample].eta_j2_lowMass, sample)))
    y1_eta.append(root_files[sample].eta_y1_lowMass.Clone(title=make_title(root_files[sample].eta_y1_lowMass, sample)))
    y2_eta.append(root_files[sample].eta_y2_lowMass.Clone(title=make_title(root_files[sample].eta_y2_lowMass, sample)))
    pt_j1.append(root_files[sample].pt_j1_lowMass.Clone(title=make_title(root_files[sample].pt_j1_lowMass, sample)))
    pt_j2.append(root_files[sample].pt_j2_lowMass.Clone(title=make_title(root_files[sample].pt_j2_lowMass, sample)))
    pt_y1.append(root_files[sample].pt_y1_lowMass.Clone(title=make_title(root_files[sample].pt_y1_lowMass, sample)))
    pt_y2.append(root_files[sample].pt_y2_lowMass.Clone(title=make_title(root_files[sample].pt_y2_lowMass, sample)))
    njets.append(root_files[sample].njets_lowMass.Clone(title=make_title(root_files[sample].njets_lowMass, sample)))
    jj_deta.append(root_files[sample].deta_jj_lowMass.Clone(title=make_title(root_files[sample].deta_jj_lowMass, sample)))
    jj_dphi.append(root_files[sample].dphi_jj_lowMass.Clone(title=make_title(root_files[sample].dphi_jj_lowMass, sample)))
    yy_deta.append(root_files[sample].deta_yy_lowMass.Clone(title=make_title(root_files[sample].deta_yy_lowMass, sample)))
    yy_dphi.append(root_files[sample].dphi_yy_lowMass.Clone(title=make_title(root_files[sample].dphi_yy_lowMass, sample)))
    deta_maxPtjj.append(root_files[sample].deta_maxPtjj_lowMass.Clone(title=make_title(root_files[sample].deta_maxPtjj_lowMass, sample)))
    zeppenfeld.append(root_files[sample].zeppenfeld_lowMass.Clone(title=make_title(root_files[sample].deta_maxPtjj_lowMass, sample)))

# Set the plotting styles
style = get_style('ATLAS')
set_style(style)

# Plot all of those (very verbose and repeated, look into shortening (or define/import))
plot_variable(m_yy,"$m_{\gamma\gamma}$ (GeV)")
plt.savefig(output_direct+'m_yy.pdf')
plt.close()

plot_variable(m_yyjj,"$m_{\gamma \gamma jj}$ (GeV)")
plt.text(x=1200, y=0.102, s="$m_{jj}$ fixed to 125 GeV", fontsize=12, ha='right')
plt.savefig(output_direct+'m_yyjj.pdf')
plt.close()

plot_variable(m_jj,"$m_{bb}$ (GeV)")
plt.xlim((80,140))
plt.savefig(output_direct+'m_bb.pdf')
plt.close()

plot_variable(j1_phi,"$\phi_{j1}$")
plt.savefig(output_direct+'j1_phi.pdf')
plot_variable(j2_phi,"$\phi_{j2}$")
plt.savefig(output_direct+'j2_phi.pdf')
plt.close()

plot_variable(j1_eta,"$\eta_{j1}$")
plt.savefig(output_direct+'j1_eta.pdf')
plot_variable(j2_eta,"$\eta_{j2}$")
plt.savefig(output_direct+'j2_eta.pdf')
plt.close()

plot_variable(y1_phi,"$\phi_{\gamma1}$")
plt.savefig(output_direct+'y1_phi.pdf')
plot_variable(y2_phi,"$\phi_{\gamma2}$")
plt.savefig(output_direct+'y2_phi.pdf')
plt.close()

plot_variable(y1_eta,"$\eta_{\gamma1}$")
plt.savefig(output_direct+'y1_eta.pdf')
plot_variable(y2_eta,"$\eta_{\gamma2}$")
plt.savefig(output_direct+'y2_eta.pdf')
plt.close()

plot_variable(njets,"Jet Multiplicity")
plt.savefig(output_direct+'njets.pdf')
plt.close()

plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$")
plt.savefig(output_direct+'jj_deta.pdf')
plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$")
plt.savefig(output_direct+'jj_dphi.pdf')
plt.close()

plot_variable(yy_deta,"$ \Delta \eta_{\gamma\gamma}$")
plt.savefig(output_direct+'yy_deta.pdf')
plot_variable(yy_dphi,"$ \Delta \phi_{\gamma\gamma}$")
plt.savefig(output_direct+'yy_dphi.pdf')
plt.close()

plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$")
plt.savefig(output_direct+'maxPtjj_deta.pdf')
plt.close()

plot_variable(zeppenfeld,"$z_{j3}^{*}$")
plt.savefig(output_direct+'zeppenfeld.pdf')
plt.savefig(output_direct+'zeppenfeld.png')

plt.close()