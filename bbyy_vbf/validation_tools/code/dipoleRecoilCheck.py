from rootpy.tree import Tree
from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import numpy as np
from collections import OrderedDict
# Import generic plotting function from nonresonant-validation
from nonresonant_validation import plot_variable, make_legend
import matplotlib as mpl
mpl.use('PS')

# Set some paths
f_sm = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/HGamHadronisationStudy_l1cvv1cv1.root'
f_dipoleOn = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/HGamHadronisationStudy_dipoleRecoilOn.root'
output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/dipole/'

# Load files into a dictionary
root_files = {
    'dipoleRecoil = Off' : root_open(f_sm),
    'dipoleRecoil = On' : root_open(f_dipoleOn)
}

# Define variables to look at
m_yy, m_yyjj, m_jj, m_vbf =[], [], [], []
j1_phi, j2_phi, y1_phi, y2_phi = [], [], [], []
j1_eta, j2_eta, y1_eta, y2_eta = [], [], [], []
pt_j1 , pt_j2 , pt_y1 , pt_y2 = [], [], [], []
njets = []
jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []
deta_maxPtjj =[]
zeppenfeld =[]

# Get some useful info in the title
def make_title(histogram, key):
    #nentries = histogram.GetEntries()
    #mean = histogram.GetMean()
    #return "%s\nEntries = %i\nMean = %.2f" % (key, nentries, mean)
    return "%s" % (key)

# Fill those variables
for sample in root_files:
    m_yy.append(root_files[sample].m_yy_lowMass.Clone(title=make_title(root_files[sample].m_yy_lowMass, sample)))
    m_yyjj.append(root_files[sample].m_yyjj_lowMass.Clone(title=make_title(root_files[sample].m_yyjj_lowMass, sample)))
    m_jj.append(root_files[sample].m_jj_lowMass.Clone(title=make_title(root_files[sample].m_jj_lowMass, sample)))
    j1_phi.append(root_files[sample].phi_j1_lowMass.Clone(title=make_title(root_files[sample].phi_j1_lowMass, sample)))
    j2_phi.append(root_files[sample].phi_j2_lowMass.Clone(title=make_title(root_files[sample].phi_j2_lowMass, sample)))
    y1_phi.append(root_files[sample].phi_y1_lowMass.Clone(title=make_title(root_files[sample].phi_y1_lowMass, sample)))
    y2_phi.append(root_files[sample].phi_y2_lowMass.Clone(title=make_title(root_files[sample].phi_y2_lowMass, sample)))
    j1_eta.append(root_files[sample].eta_j1_lowMass.Clone(title=make_title(root_files[sample].eta_j1_lowMass, sample)))
    j2_eta.append(root_files[sample].eta_j2_lowMass.Clone(title=make_title(root_files[sample].eta_j2_lowMass, sample)))
    y1_eta.append(root_files[sample].eta_y1_lowMass.Clone(title=make_title(root_files[sample].eta_y1_lowMass, sample)))
    y2_eta.append(root_files[sample].eta_y2_lowMass.Clone(title=make_title(root_files[sample].eta_y2_lowMass, sample)))
    pt_j1.append(root_files[sample].pt_j1_lowMass.Clone(title=make_title(root_files[sample].pt_j1_lowMass, sample)))
    pt_j2.append(root_files[sample].pt_j2_lowMass.Clone(title=make_title(root_files[sample].pt_j2_lowMass, sample)))
    pt_y1.append(root_files[sample].pt_y1_lowMass.Clone(title=make_title(root_files[sample].pt_y1_lowMass, sample)))
    pt_y2.append(root_files[sample].pt_y2_lowMass.Clone(title=make_title(root_files[sample].pt_y2_lowMass, sample)))
    njets.append(root_files[sample].njets_lowMass.Clone(title=make_title(root_files[sample].njets_lowMass, sample)))
    jj_deta.append(root_files[sample].deta_jj_lowMass.Clone(title=make_title(root_files[sample].deta_jj_lowMass, sample)))
    jj_dphi.append(root_files[sample].dphi_jj_lowMass.Clone(title=make_title(root_files[sample].dphi_jj_lowMass, sample)))
    yy_deta.append(root_files[sample].deta_yy_lowMass.Clone(title=make_title(root_files[sample].deta_yy_lowMass, sample)))
    yy_dphi.append(root_files[sample].dphi_yy_lowMass.Clone(title=make_title(root_files[sample].dphi_yy_lowMass, sample)))
    deta_maxPtjj.append(root_files[sample].deta_maxPtjj_lowMass.Clone(title=make_title(root_files[sample].deta_maxPtjj_lowMass, sample)))
    zeppenfeld.append(root_files[sample].zeppenfeld_lowMass.Clone(title=make_title(root_files[sample].deta_maxPtjj_lowMass, sample)))
    m_vbf.append(root_files[sample].m_vbf_lowMass.Clone(title=make_title(root_files[sample].m_vbf_lowMass, sample)))

# Set the plotting styles
style = get_style('ATLAS')
set_style(style)

#nbins0 = m_yyjj[0].GetSize()
#m_yyjj[1].Rebin(nbins0/m_yyjj[1].GetSize())
output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/dipole/'
extension = '.pdf'
# Plot all of those (very verbose and repeated, look into shortening (or define/import))
plot_variable(zeppenfeld,"$z_{j3}^{*}$")
ax=plt.gca(); ax.get_legend().remove()
leg = make_legend(fontsize=14, bbox=(1,0.91),loc='upper right')
plt.savefig(output_direct+'zeppenfeld%s' % extension)
plot_variable(m_yy,"$m_{\gamma\gamma}$ (GeV)")
plt.savefig(output_direct+'m_yy%s' % extension)
plt.close()

plot_variable(m_yyjj,"$m_{\gamma \gamma jj}$ (GeV)")
#plt.text(x=1200, y=0.102, s="$m_{jj}$ fixed to 125 GeV", fontsize=12, ha='right')
plt.annotate('$m_{bb}$ fixed to 125 GeV', xy=(.95,.7), fontsize=12,xycoords='axes fraction',ha='right')
plt.savefig(output_direct+'m_yyjj%s' % extension)
plt.close()

plot_variable(m_jj,"$m_{bb}$ (GeV)")
plt.xlim((80,140))
plt.savefig(output_direct+'m_bb%s' % extension)
plt.close()

plot_variable(m_vbf,"$m_{jj, VBF}$")
plt.savefig(output_direct+'m_vbf%s' % extension)

plot_variable(j1_phi,"\phi_{j1}$")
plt.savefig(output_direct+'j1_phi%s' % extension)
plot_variable(j2_phi,"$\phi_{j2}$")
plt.savefig(output_direct+'j2_phi%s' % extension)
plt.close()

plot_variable(j1_eta,"$\eta_{j1}$")
plt.savefig(output_direct+'j1_eta%s' % extension)
plot_variable(j2_eta,"$\eta_{j2}$")
plt.savefig(output_direct+'j2_eta%s' % extension)
plt.close()

plot_variable(y1_phi,"\phi_{\gamma1}$")
plt.savefig(output_direct+'y1_phi%s' % extension)
plot_variable(y2_phi,"$\phi_{\gamma2}$")
plt.savefig(output_direct+'y2_phi%s' % extension)
plt.close()

plot_variable(y1_eta,"$\eta_{\gamma1}$")
plt.savefig(output_direct+'y1_eta%s' % extension)
plot_variable(y2_eta,"$\eta_{\gamma2}$")
plt.savefig(output_direct+'y2_eta%s' % extension)
plt.close()

plot_variable(njets,"Jet Multiplicity")
plt.savefig(output_direct+'njets%s' % extension)
plt.close()

plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$")
plt.savefig(output_direct+'jj_deta%s' % extension)
plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$")
plt.savefig(output_direct+'jj_dphi%s' % extension)
plt.close()

plot_variable(yy_deta,"$ \Delta \eta_{\gamma\gamma}$")
plt.savefig(output_direct+'yy_deta%s' % extension)
plot_variable(yy_dphi,"$ \Delta \phi_{\gamma\gamma}$")
plt.savefig(output_direct+'yy_dphi%s' % extension)
plt.close()

plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$")
plt.savefig(output_direct+'maxPtjj_deta%s' % extension)
plt.close()

plt.close()