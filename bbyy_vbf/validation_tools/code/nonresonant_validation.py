
from rootpy.tree import Tree
from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
from matplotlib import rc
import numpy as np
from collections import OrderedDict
import glob
hfont = {'fontname':'Helvetica'}

doALL = False
doCVV = True
doCV = True
doKlambda = True
mjj_cut = False

# ******************************************************************************* #
class Input:

    def __init__(self, filename):
        self.rootfile = root_open(filename)

        # String from filename of lambda, c2v, cv
        self.coupling_string = filename.split('/')[-1].replace('.root','').replace('HGamHadronisationStudy_','')

        # should be of form lXcvvYcvZ. Now Isolate X, Y, Z
        self.k_lambda = float(self.coupling_string.split('cvv')[0].replace('l','').replace('p','.'))
        self.cvv = float(self.coupling_string.split('cvv')[1].split('cv')[0].replace('p','.'))
        self.cv = float(self.coupling_string.split('cvv')[1].split('cv')[1].replace('p','.'))



# ******************************************************************************* #

def plot_variable(h_list, x_title,colorchoice=1):
    hfont = {'fontname':'Helvetica'}

    """ Generates plot for a given variable

    Arguments:
        h_list {list} -- List of input histograms
        x_title {string} -- Title for x-axis
    """
    bad = False
    for h in h_list:
        if h.Integral() == 0:
            bad = True
    if bad: return

    # generate figure
    fig = plt.figure(figsize=(7, 5), dpi=100)
    axes = plt.axes()
    axes.xaxis.set_minor_locator(AutoMinorLocator())
    axes.yaxis.set_minor_locator(AutoMinorLocator())
    plt.xlabel(x_title, position=(1, 0), va='bottom', ha='right', labelpad=20, fontsize=14)
    plt.ylabel('Arbitrary Units', position=(0, 1), va='top', ha='right', labelpad=20, fontsize=14)

    # Plan the colormap
    color = np.linspace(0,0.8,len(h_list))
    # things to do to all histograms
    for i, h in zip(color, h_list):
        h.Scale(1/h.Integral())
        if 'HH' in h.GetTitle():
            h.linecolor = "firebrick"
        else:
            if colorchoice == 1: h.linecolor = plt.cm.inferno(i)
            if colorchoice == 2: h.linecolor = plt.cm.jet(i)
            if colorchoice == 3: h.linecolor = plt.cm.rainbow(i)

        #rplt.errorbar(h, markersize=0, linestyle='solid',color=plt.cm.viridis(i)) #use this to connect lines
        rplt.errorbar(h, markersize=0)

    plt.ylim(ymin=0, ymax= max([1.55*h.GetMaximum() for h in h_list]))

    rc('text',usetex=True)
    plt.annotate(r"\textit{\textbf{ATLAS}} Simulation Preliminary", xy=(.02,.91),xycoords='axes fraction', horizontalalignment='left',fontsize=24,  **hfont)
    rc('text',usetex=False)
    #plt.annotate(r"$HH \rightarrow \gamma \gamma b \bar{b}jj$", xy=(.02,.83),xycoords='axes fraction', horizontalalignment='left',fontsize=22,  **hfont)
    if mjj_cut: plt.annotate(r"$m_{jj} > 550$ GeV required", xy=(.02,.83),xycoords='axes fraction', horizontalalignment='left',fontsize=19,  **hfont)
    # plt.annotate(s='Tyler James Burch', xy=(.005,.007), xycoords='figure fraction',
    #             textcoords='figure fraction', color='grey',alpha=0.7)

    #leg = plt.legend(ncol=2, fontsize=8)
    leg = make_legend()

def make_title(Sample):
    return "$\kappa_{\lambda} = %s, c_{2V} = %s, c_{V} = %s$" % (Sample.k_lambda, Sample.cvv, Sample.cv)

def make_legend(location=None, bbox=None, fontsize=13, **kwargs):
    if location:
        leg = plt.legend(fontsize=fontsize, frameon=False, loc=location, **kwargs)
    elif bbox:
        leg = plt.gca().legend(fontsize=fontsize, frameon=False, bbox_to_anchor=bbox, **kwargs)
    else:
        leg = plt.legend(fontsize=fontsize, frameon=False, loc='center right',**kwargs)

    for l in leg.get_texts():
        txt = l.get_text()
        idx = leg.get_texts().index(l)
        if '.0' in txt:
            leg.get_texts()[idx].set_text(txt.replace('.0',''))
    return leg

if __name__ == "__main__":

    # Load in the files as a dictionary
    inputs_dictionary={}
    #for f in glob.glob('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/deta_cut/*l*cvv*cv*'):
    if mjj_cut: dir = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/mjj_cut/*l*cvv*cv*'
    else: dir = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/dec2018_noCut/*l*cvv*cv*'
    for f in glob.glob(dir):
        this_sample = Input(f)
        inputs_dictionary[this_sample.coupling_string] = this_sample


    #output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/nonresonant/deta_cut/'
    if mjj_cut: output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/nonresonant/mjj/'
    else: output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/nonresonant/nominal/'

    m_yy, m_yyjj, m_jj, m_vbf =[], [], [], []
    j1_phi, j2_phi, y1_phi, y2_phi = [], [], [], []
    j1_eta, j2_eta, y1_eta, y2_eta = [], [], [], []
    pt_j1 , pt_j2 , pt_y1 , pt_y2 = [], [], [], []
    njets = []
    jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []

    deta_maxPtjj =[]


    for sample in inputs_dictionary:
        m_yy.append(inputs_dictionary[sample].rootfile.m_yy_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        m_yyjj.append(inputs_dictionary[sample].rootfile.m_yyjj_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        m_jj.append(inputs_dictionary[sample].rootfile.m_jj_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        j1_phi.append(inputs_dictionary[sample].rootfile.phi_j1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        j2_phi.append(inputs_dictionary[sample].rootfile.phi_j2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        y1_phi.append(inputs_dictionary[sample].rootfile.phi_y1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        y2_phi.append(inputs_dictionary[sample].rootfile.phi_y2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        j1_eta.append(inputs_dictionary[sample].rootfile.eta_j1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        j2_eta.append(inputs_dictionary[sample].rootfile.eta_j2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        y1_eta.append(inputs_dictionary[sample].rootfile.eta_y1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        y2_eta.append(inputs_dictionary[sample].rootfile.eta_y2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        pt_j1.append(inputs_dictionary[sample].rootfile.pt_j1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        pt_j2.append(inputs_dictionary[sample].rootfile.pt_j2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        pt_y1.append(inputs_dictionary[sample].rootfile.pt_y1_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        pt_y2.append(inputs_dictionary[sample].rootfile.pt_y2_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        njets.append(inputs_dictionary[sample].rootfile.njets_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        jj_deta .append(inputs_dictionary[sample].rootfile.deta_jj_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        jj_dphi .append(inputs_dictionary[sample].rootfile.dphi_jj_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        yy_deta .append(inputs_dictionary[sample].rootfile.deta_yy_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        yy_dphi .append(inputs_dictionary[sample].rootfile.dphi_yy_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        deta_maxPtjj .append(inputs_dictionary[sample].rootfile.deta_maxPtjj_lowMass.Clone(title=make_title(inputs_dictionary[sample])))
        m_vbf.append(inputs_dictionary[sample].rootfile.m_vbf_lowMass.Clone(title=make_title(inputs_dictionary[sample])))


    for h in m_yyjj: h.Rebin(4)

    style = get_style('ATLAS')
    set_style(style)

    if doALL:
        plot_variable(m_yy,"$m_{\gamma\gamma}$ [GeV]")
        plt.savefig(output_direct+'m_yy.pdf')
        plt.close()

        plot_variable(m_yyjj,"$m_{\gamma \gamma bb}$ [GeV]")
        plt.annotate(r"$m_{b\bar{b}}$ fixed to 125 GeV", xy=(.02,.74),xycoords='axes fraction', horizontalalignment='left',fontsize=20,  **hfont)
        ax=plt.gca(); ax.get_legend().remove()
        leg = make_legend(fontsize=14, bbox=(1,0.74),loc='upper right')
        plt.tight_layout()
        plt.savefig(output_direct+'m_yyjj.pdf')
        plt.close()

        plot_variable(m_jj,"$m_{bb}$ [GeV]")
        plt.xlim((80,140))
        plt.savefig(output_direct+'m_bb.pdf')
        plt.close()

        plot_variable(j1_phi,"\phi_{j1}$")
        plt.savefig(output_direct+'j1_phi.pdf')
        plot_variable(j2_phi,"$\phi_{j2}$")
        plt.savefig(output_direct+'j2_phi.pdf')
        plt.close()

        plot_variable(j1_eta,"$\eta_{j1}$")
        plt.savefig(output_direct+'j1_eta.pdf')
        plot_variable(j2_eta,"$\eta_{j2}$")
        plt.savefig(output_direct+'j2_eta.pdf')
        plt.close()

        #plot_variable(j1_pt,"j1_pt")
        #plt.savefig(output_direct+'j1_pt.pdf')
        #plot_variable(j2_pt,"j2_pt")
        #plt.savefig(output_direct+'j2_pt.pdf')


        plot_variable(y1_phi,"\phi_{\gamma1}$")
        plt.savefig(output_direct+'y1_phi.pdf')
        plot_variable(y2_phi,"$\phi_{\gamma2}$")
        plt.savefig(output_direct+'y2_phi.pdf')
        plt.close()

        plot_variable(y1_eta,"$\eta_{\gamma1}$")
        plt.savefig(output_direct+'y1_eta.pdf')
        plot_variable(y2_eta,"$\eta_{\gamma2}$")
        plt.savefig(output_direct+'y2_eta.pdf')
        plt.close()

        plot_variable(njets,"Jet Multiplicity")
        plt.savefig(output_direct+'njets.pdf')
        plt.close()

        plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$")
        plt.savefig(output_direct+'jj_deta.pdf')

        plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$")
        plt.savefig(output_direct+'jj_dphi.pdf')
        plt.close()

        plot_variable(yy_deta,"$ \Delta \eta_{\gamma\gamma}$")
        plt.savefig(output_direct+'yy_deta.pdf')
        plot_variable(yy_dphi,"$ \Delta \phi_{\gamma\gamma}$")
        plt.savefig(output_direct+'yy_dphi.pdf')
        plt.close()

        plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$")
        plt.savefig(output_direct+'maxPtjj_deta.pdf')
        plt.close()

        plot_variable(m_vbf,"$m_{jj}$ [GeV]")
        plt.savefig(output_direct+'m_jj.pdf')
        plt.close()



    # ***************************************************************** #

    # Make increasing c2v dictionary
    cvv_list = []
    for sample in inputs_dictionary:
        if 'l1' not in inputs_dictionary[sample].coupling_string: continue
        if 'cv1p5' in inputs_dictionary[sample].coupling_string: continue
        if 'cv1' not in inputs_dictionary[sample].coupling_string: continue
        cvv_list.append(inputs_dictionary[sample].cvv)

    # order high to low for pop method
    sorted_cvv = sorted(cvv_list)
    sorted_cvv.reverse()
    cvv_sorted_dict = OrderedDict()
    while len(sorted_cvv) > 0 :
        for sample in inputs_dictionary:
            if 'l1' not in inputs_dictionary[sample].coupling_string: continue
            if 'cv1p5' in inputs_dictionary[sample].coupling_string: continue
            if 'cv1' not in inputs_dictionary[sample].coupling_string: continue

            try:
                if inputs_dictionary[sample].cvv == sorted_cvv[-1]:
                    cvv_sorted_dict[sample] = inputs_dictionary[sample]
                    sorted_cvv.pop()
            except IndexError: continue # kinda ugly, but deals with empty case.


    # ***************************************************************** #
    # Make the plots
    m_yy, m_yyjj, m_jj, m_vbf =[], [], [], []
    jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []
    deta_maxPtjj = []
    njets=[]
    for sample in cvv_sorted_dict:
        m_yy.append(cvv_sorted_dict[sample].rootfile.m_yy_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        m_yyjj.append(cvv_sorted_dict[sample].rootfile.m_yyjj_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        m_jj.append(cvv_sorted_dict[sample].rootfile.m_jj_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        jj_deta.append(cvv_sorted_dict[sample].rootfile.deta_jj_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        jj_dphi.append(cvv_sorted_dict[sample].rootfile.dphi_jj_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        yy_deta.append(cvv_sorted_dict[sample].rootfile.deta_yy_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        yy_dphi.append(cvv_sorted_dict[sample].rootfile.dphi_yy_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        njets .append(cvv_sorted_dict[sample].rootfile.njets_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        deta_maxPtjj.append(cvv_sorted_dict[sample].rootfile.deta_maxPtjj_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))
        m_vbf.append(cvv_sorted_dict[sample].rootfile.m_vbf_lowMass.Clone(title=make_title(cvv_sorted_dict[sample])))

    for h in m_yyjj: h.Rebin(4)

    if doCVV:
        plot_variable(m_yy,"$m_{\gamma\gamma}$ [GeV]", colorchoice=2)
        plt.savefig(output_direct+'m_yy_cvv.pdf')

        plot_variable(m_yyjj,"$m_{\gamma \gamma bb}$ [GeV]", colorchoice=2)
        plt.annotate(r"$m_{b\bar{b}}$ fixed to 125 GeV", xy=(.02,.83),xycoords='axes fraction', horizontalalignment='left',fontsize=20,  **hfont)
        ax=plt.gca(); ax.get_legend().remove()
        leg = make_legend(fontsize=14, bbox=(1,0.83),loc='upper right')
        plt.savefig(output_direct+'m_yyjj_cvv.pdf')

        plot_variable(m_jj,"$m_{bb}$ [GeV]", colorchoice=2)
        plt.xlim((80,140))
        plt.savefig(output_direct+'m_bb_cvv.pdf')

        plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$", colorchoice=2)
        ax=plt.gca(); ax.get_legend().remove()
        if mjj_cut: leg = make_legend(bbox=(-.035,.83), loc='upper left')
        else: leg = make_legend(bbox=(-.035,.91), loc='upper left')
        plt.savefig(output_direct+'jj_deta_cvv.pdf')
        plt.close()

        plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$", colorchoice=2)
        plt.savefig(output_direct+'jj_dphi_cvv.pdf')
        plt.close()

        plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$", colorchoice=2)
        plt.savefig(output_direct+'maxPtjj_deta_cvv.pdf')
        plt.close()

        plot_variable(njets,"Jet Multiplicity", colorchoice=2)
        plt.savefig(output_direct+'njets_cvv.pdf')
        plt.close()

        plot_variable(m_vbf,"$m_{jj}$ [GeV]", colorchoice=2)
        ax=plt.gca(); ax.get_legend().remove()
        leg = make_legend(fontsize=14, bbox=(1,0.86),loc='upper right')
        plt.savefig(output_direct+'m_jj_cvv.pdf')
        plt.close()


    # ***************************************************************** #
    # Repeat for cV


    # Make increasing c2v dictionary
    cv_list = []
    for sample in inputs_dictionary:
        if 'l1' not in inputs_dictionary[sample].coupling_string: continue
        if 'cvv1p5' in inputs_dictionary[sample].coupling_string: continue
        if 'cvv1' not in inputs_dictionary[sample].coupling_string: continue
        cv_list.append(inputs_dictionary[sample].cv)


    # order high to low for pop method
    sorted_cv = sorted(cv_list)
    sorted_cv.reverse()
    cv_sorted_dict = OrderedDict()
    while len(sorted_cv) > 0 :
        for sample in inputs_dictionary:
            if 'l1' not in inputs_dictionary[sample].coupling_string: continue
            if 'cvv1p5' in inputs_dictionary[sample].coupling_string: continue
            if 'cvv1' not in inputs_dictionary[sample].coupling_string: continue

            try:
                if inputs_dictionary[sample].cv == sorted_cv[-1]:
                    cv_sorted_dict[sample] = inputs_dictionary[sample]
                    sorted_cv.pop()
            except IndexError: continue # kinda ugly, but deals with empty case.


    # ***************************************************************** #
    # Make cV plots
    m_yy, m_yyjj, m_jj, m_vbf =[], [], [], []
    jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []
    deta_maxPtjj = []
    njets=[]

    for sample in cv_sorted_dict:
        m_yy.append(cv_sorted_dict[sample].rootfile.m_yy_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        m_yyjj.append(cv_sorted_dict[sample].rootfile.m_yyjj_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        m_jj.append(cv_sorted_dict[sample].rootfile.m_jj_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        jj_deta .append(cv_sorted_dict[sample].rootfile.deta_jj_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        jj_dphi .append(cv_sorted_dict[sample].rootfile.dphi_jj_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        yy_deta .append(cv_sorted_dict[sample].rootfile.deta_yy_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        yy_dphi .append(cv_sorted_dict[sample].rootfile.dphi_yy_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        njets .append(cv_sorted_dict[sample].rootfile.njets_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        deta_maxPtjj.append(cv_sorted_dict[sample].rootfile.deta_maxPtjj_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))
        m_vbf.append(cv_sorted_dict[sample].rootfile.m_vbf_lowMass.Clone(title=make_title(cv_sorted_dict[sample])))

    for h in m_yyjj: h.Rebin(4)

    if doCV:
        plot_variable(m_yy,"$m_{\gamma\gamma}$ [GeV]", colorchoice=3)
        plt.savefig(output_direct+'m_yy_cv.pdf')

        plot_variable(m_yyjj,"$m_{\gamma \gamma bb}$ [GeV]", colorchoice=3)
        plt.annotate(r"$m_{b\bar{b}}$ fixed to 125 GeV", xy=(.02,.83),xycoords='axes fraction', horizontalalignment='left',fontsize=20,  **hfont)
        ax=plt.gca(); ax.get_legend().remove()
        leg = make_legend(fontsize=14, bbox=(1,0.83),loc='upper right')
        plt.savefig(output_direct+'m_yyjj_cv.pdf')

        plot_variable(m_jj,"$m_{bb}$ [GeV]", colorchoice=3)
        plt.xlim((80,140))
        plt.savefig(output_direct+'m_bb_cv.pdf')

        plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$", colorchoice=3)
        ax=plt.gca(); ax.get_legend().remove()
        if mjj_cut: leg = make_legend(bbox=(-.035,.83), loc='upper left')
        else: leg = make_legend(bbox=(-.035,.91), loc='upper left')

        plt.savefig(output_direct+'jj_deta_cv.pdf')

        plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$", colorchoice=3)
        plt.savefig(output_direct+'maxPtjj_deta_cv.pdf')
        plt.close()
        plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$", colorchoice=3)
        plt.savefig(output_direct+'jj_dphi_cv.pdf')
        plt.close()

        plot_variable(njets,"Jet Multiplicity", colorchoice=3)
        plt.savefig(output_direct+'njets_cv.pdf')
        plt.close()

        plot_variable(m_vbf,"$m_{jj}$ [GeV]",colorchoice=3)
        leg = make_legend(fontsize=14, bbox=(1,0.86),loc='upper right')
        plt.savefig(output_direct+'m_jj_cv.pdf')
        plt.close()

    # ***************************************************************** #
    # Repeat for lambda


    # Make increasing c2v dictionary
    klambda_list = []
    for sample in inputs_dictionary:
        if 'cvv1p5' in inputs_dictionary[sample].coupling_string: continue
        if 'cv1p5' in inputs_dictionary[sample].coupling_string: continue
        if 'cvv1' not in inputs_dictionary[sample].coupling_string: continue
        if 'cv1' not in inputs_dictionary[sample].coupling_string: continue
        klambda_list.append(inputs_dictionary[sample].k_lambda)


    # order high to low for pop method
    sorted_klambda = sorted(klambda_list)
    sorted_klambda.reverse()
    klambda_sorted_dict = OrderedDict()
    while len(sorted_klambda) > 0 :
        for sample in inputs_dictionary:
            if 'cvv1p5' in inputs_dictionary[sample].coupling_string: continue
            if 'cv1p5' in inputs_dictionary[sample].coupling_string: continue
            if 'cvv1' not in inputs_dictionary[sample].coupling_string: continue
            if 'cv1' not in inputs_dictionary[sample].coupling_string: continue

            try:
                if inputs_dictionary[sample].k_lambda == sorted_klambda[-1]:
                    klambda_sorted_dict[sample] = inputs_dictionary[sample]
                    sorted_klambda.pop()
            except IndexError: continue # kinda ugly, but deals with empty case.


    # ***************************************************************** #
    # Make klambda plots
    m_yy, m_yyjj, m_jj, m_vbf =[], [], [], []
    jj_deta, jj_dphi, yy_deta, yy_dphi = [], [], [], []
    deta_maxPtjj = []
    njets=[]

    for sample in klambda_sorted_dict:
        m_yy.append(klambda_sorted_dict[sample].rootfile.m_yy_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        m_yyjj.append(klambda_sorted_dict[sample].rootfile.m_yyjj_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        m_jj.append(klambda_sorted_dict[sample].rootfile.m_jj_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        jj_deta .append(klambda_sorted_dict[sample].rootfile.deta_jj_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        jj_dphi .append(klambda_sorted_dict[sample].rootfile.dphi_jj_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        yy_deta .append(klambda_sorted_dict[sample].rootfile.deta_yy_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        yy_dphi .append(klambda_sorted_dict[sample].rootfile.dphi_yy_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        njets .append(klambda_sorted_dict[sample].rootfile.njets_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        deta_maxPtjj.append(klambda_sorted_dict[sample].rootfile.deta_maxPtjj_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))
        m_vbf.append(klambda_sorted_dict[sample].rootfile.m_vbf_lowMass.Clone(title=make_title(klambda_sorted_dict[sample])))

    for h in m_yyjj: h.Rebin(4)

    if doKlambda:
        plot_variable(m_yy,"$m_{\gamma\gamma}$ [GeV]", colorchoice=3)
        plt.savefig(output_direct+'m_yy_klambda.pdf')

        plot_variable(m_yyjj,"$m_{\gamma \gamma bb}$ [GeV]", colorchoice=3)
        plt.annotate(r"$m_{b\bar{b}}$ fixed to 125 GeV", xy=(.02,.83),xycoords='axes fraction', horizontalalignment='left',fontsize=20,  **hfont)
        ax=plt.gca(); ax.get_legend().remove()
        leg = make_legend(fontsize=14, bbox=(1,0.83),loc='upper right')
        plt.savefig(output_direct+'m_yyjj_klambda.pdf')

        plot_variable(m_jj,"$m_{bb}$ [GeV]", colorchoice=3)
        plt.xlim((80,140))
        plt.savefig(output_direct+'m_bb_klambda.pdf')

        plot_variable(jj_deta,"$ \Delta \eta_{Highest\: m_{jj}\: pair}$", colorchoice=3)
        ax=plt.gca(); ax.get_legend().remove()
        if mjj_cut: leg = make_legend(bbox=(-.035,.83), loc='upper left')
        else: leg = make_legend(bbox=(-.035,.91), loc='upper left')
        plt.savefig(output_direct+'jj_deta_klambda.pdf')
        plt.close()
        plot_variable(jj_dphi,"$ \Delta \phi_{Highest\: m_{jj}\: pair}$", colorchoice=3)
        plt.savefig(output_direct+'jj_dphi_klambda.pdf')
        plt.close()


        plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$", colorchoice=3)
        plt.savefig(output_direct+'maxPtjj_deta_klambda.pdf')
        plt.close()

        plot_variable(njets,"Jet Multiplicity", colorchoice=3)
        plt.savefig(output_direct+'njets_klambda.pdf')
        plt.close()

        plot_variable(m_vbf,"$m_{jj}$ [GeV]",colorchoice=3)
        leg = make_legend(fontsize=14, bbox=(1,0.86),loc='upper right')
        plt.savefig(output_direct+'m_jj_klambda.pdf')
        plt.close()
