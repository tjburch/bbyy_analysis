from rootpy.tree import Tree
from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
import numpy as np
from collections import OrderedDict
from rootpy.plotting import Hist2D
from matplotlib import rc
hfont = {'fontname':'Helvetica'}

# set the style
direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/'
masses = [300, 500, 700]
#masses = [ 350, 600, 800]
#masses = [251, 260, 280, 300, 325, 350, 400, 450, 500, 600, 700, 800, 900, 1000]


files =  OrderedDict( (m, root_open(direct+"/ntuples/mjj_cut/HGamHadronisationStudy_{0}.root".format(m))) for m in masses)
files.update({'nonresonant' : root_open(direct+"/ntuples/mjj_cut/HGamHadronisationStudy_l1cvv1cv1.root") })

output_direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/resonant/mjj'

def plot_variable(h_list, x_title):
    """ Generates plot for a given variable

    Arguments:
        h_list {list} -- List of input histograms
        x_title {string} -- Title for x-axis
    """
    bad = False
    for h in h_list:
        if h.Integral() == 0:
            bad = True
    if bad: return

    # generate figure
    fig = plt.figure(figsize=(7, 5), dpi=100)
    axes = plt.axes()

    axes.xaxis.set_minor_locator(AutoMinorLocator())
    axes.yaxis.set_minor_locator(AutoMinorLocator())
    plt.xlabel(x_title, position=(1, 0), va='bottom', ha='right', labelpad=20)
    plt.ylabel('AU', position=(0, 1), va='top', ha='right', labelpad=20)

    # Plan the colormap
    color = np.linspace(0,0.8,len(h_list))
    # things to do to all histograms
    for i, h in zip(color, h_list):
        h.Scale(1/h.Integral())
        if 'HH' in h.GetTitle():
            h.linecolor = "firebrick"
        else:
            h.linecolor = plt.cm.viridis(i)
        #rplt.errorbar(h, markersize=0, linestyle='solid',color=plt.cm.viridis(i)) #use this to connect lines
        rplt.errorbar(h, markersize=0)

    #leg = plt.legend(fontsize=7)
    plt.ylim(ymin=0, ymax= max([1.35*h.GetMaximum() for h in h_list]))

    rc('text',usetex=True)
    plt.annotate(r"\textit{\textbf{ATLAS}} Simulation Preliminary", xy=(.02,.93),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
    rc('text',usetex=False)
    plt.annotate(r"$hh \rightarrow \gamma \gamma b \bar{b}jj$", xy=(.02,.86),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
    plt.annotate(r"$m_{jj} > 550$ Required", xy=(.02,.79),xycoords='axes fraction', horizontalalignment='left',fontsize=18,  **hfont)
    #plt.annotate(s='Tyler James Burch', xy=(.005,.007), xycoords='figure fraction',
    #             textcoords='figure fraction', color='grey',alpha=0.7)
    leg = plt.legend(fontsize=7)



m_yy, m_jj, m_yyjj = [], [], []
j1_phi, j2_phi, y1_phi, y2_phi =[], [], [], []
j1_eta, j2_eta, y1_eta, y2_eta =[], [], [], []
j1_pt, j2_pt, y1_pt, y2_pt =[], [], [], []
njets, nbJets =[], []
jj_deta, jj_dphi = [], []
yy_deta, yy_dphi = [], []
max_deta= []
dR_y1j,dR_y2j =[],[]
pt_leadingJ, pt_subleadingJ  , pt_allJ = [], [], []
eta_leadingJ, eta_subleadingJ, eta_allJ = [], [], []
phi_leadingJ, phi_subleadingJ, phi_allJ = [], [], []
coneTruthType, partonTruthType =[], []
deta_mjj = []
m_vbf_jets = []
deta_maxPtjj =[]


#2d histos
jj_dEta_nJets, jj_dEta_nbJets = [], []

def make_title(hist, mass):
    if type(mass) is int:
        return "mX = "+str(mass)+" GeV ("+str(int(hist.GetEntries()))+" entries)"
    else:
        return "SM $HH \\rightarrow\gamma\gamma b\\bar{b} jj$ ("+str(int(hist.GetEntries()))+" entries)"

for f in files:
    m_yy.append(files[f].m_yy_lowMass.Clone(title=make_title(files[f].m_yy_lowMass, f)))
    m_yyjj.append(files[f].m_yyjj_lowMass.Clone(title=make_title(files[f].m_yy_lowMass, f)))
    m_jj.append(files[f].m_jj_lowMass.Clone(title=make_title(files[f].m_yy_lowMass, f)))

    j1_phi.append(files[f].phi_j1_lowMass.Clone(title=make_title(files[f].phi_j1_lowMass, f)))
    y1_phi.append(files[f].phi_y1_lowMass.Clone(title=make_title(files[f].phi_y1_lowMass, f)))
    j2_phi.append(files[f].phi_j2_lowMass.Clone(title=make_title(files[f].phi_j2_lowMass, f)))
    y2_phi.append(files[f].phi_y2_lowMass.Clone(title=make_title(files[f].phi_y2_lowMass, f)))

    j1_eta.append(files[f].eta_j1_lowMass.Clone(title=make_title(files[f].eta_j1_lowMass, f)))
    j2_eta.append(files[f].eta_j2_lowMass.Clone(title=make_title(files[f].eta_y1_lowMass, f)))
    y1_eta.append(files[f].eta_y1_lowMass.Clone(title=make_title(files[f].eta_j2_lowMass, f)))
    y2_eta.append(files[f].eta_y2_lowMass.Clone(title=make_title(files[f].eta_y2_lowMass, f)))

    j1_pt.append(files[f].pt_j1_lowMass.Clone(title=make_title(files[f].pt_j1_lowMass, f)))
    j2_pt.append(files[f].pt_j2_lowMass.Clone(title=make_title(files[f].pt_y1_lowMass, f)))
    y1_pt.append(files[f].pt_y1_lowMass.Clone(title=make_title(files[f].pt_j2_lowMass, f)))
    y2_pt.append(files[f].pt_y2_lowMass.Clone(title=make_title(files[f].pt_y2_lowMass, f)))

    njets.append(files[f].njets_lowMass.Clone(title=make_title(files[f].njets_lowMass, f)))
    nbJets.append(files[f].nbJets_lowMass.Clone(title=make_title(files[f].nbJets_lowMass, f)))

    jj_deta.append(files[f].deta_jj_lowMass.Clone(title=make_title(files[f].deta_jj_lowMass, f)))
    jj_dphi.append(files[f].dphi_jj_lowMass.Clone(title=make_title(files[f].dphi_jj_lowMass, f)))
    yy_deta.append(files[f].deta_yy_lowMass.Clone(title=make_title(files[f].deta_yy_lowMass, f)))
    yy_dphi.append(files[f].dphi_yy_lowMass.Clone(title=make_title(files[f].dphi_yy_lowMass, f)))

    dR_y1j.append(files[f].dR_y1j_lowMass.Clone(title=make_title(files[f].dR_y1j_lowMass, f)))
    dR_y2j.append(files[f].dR_y2j_lowMass.Clone(title=make_title(files[f].dR_y2j_lowMass, f)))

    max_deta.append(files[f].max_lightJet_eta_lowMass.Clone(title=make_title(files[f].max_lightJet_eta_lowMass, f)))

    pt_leadingJ.append(files[f].pt_leadingJ_lowMass.Clone(title=make_title(files[f].pt_leadingJ_lowMass, f)))
    pt_subleadingJ.append(files[f].pt_subleadingJ_lowMass.Clone(title=make_title(files[f].pt_subleadingJ_lowMass, f)))
    pt_allJ.append(files[f].pt_allJ_noSelection.Clone(title=make_title(files[f].pt_allJ_noSelection, f)))

    eta_leadingJ.append(files[f].eta_leadingJ_lowMass.Clone(title=make_title(files[f].eta_leadingJ_lowMass, f)))
    eta_subleadingJ.append(files[f].eta_subleadingJ_lowMass.Clone(title=make_title(files[f].eta_subleadingJ_lowMass, f)))
    eta_allJ.append(files[f].eta_allJ_noSelection.Clone(title=make_title(files[f].eta_allJ_noSelection, f)))

    phi_leadingJ.append(files[f].phi_leadingJ_lowMass.Clone(title=make_title(files[f].phi_leadingJ_lowMass, f)))
    phi_subleadingJ.append(files[f].phi_subleadingJ_lowMass.Clone(title=make_title(files[f].phi_subleadingJ_lowMass, f)))
    phi_allJ.append(files[f].phi_allJ_noSelection.Clone(title=make_title(files[f].phi_allJ_noSelection, f)))

    coneTruthType.append(files[f].coneTruthType.Clone(title=make_title(files[f].coneTruthType, f)))
    partonTruthType.append(files[f].partonTruthType.Clone(title=make_title(files[f].partonTruthType, f)))

    jj_dEta_nJets.append(files[f].jj_dEta_nJets_lowMass.Clone(title=make_title(files[f].jj_dEta_nJets_lowMass, f)))
    jj_dEta_nbJets.append(files[f].jj_dEta_nbJets_lowMass.Clone(title=make_title(files[f].jj_dEta_nbJets_lowMass, f)))
    deta_mjj.append(files[f].jj_dEta_mjj_lowMass.Clone(title=make_title(files[f].jj_dEta_mjj_lowMass, f)))
    m_vbf_jets.append(files[f].m_vbf_lowMass.Clone(title=make_title(files[f].m_vbf_lowMass, f)))

    deta_maxPtjj .append(files[f].deta_maxPtjj_lowMass.Clone(title=make_title(files[f].deta_maxPtjj_lowMass, f)))

style = get_style('ATLAS')
set_style(style)

plot_variable(m_yy,"$m_{\gamma\gamma}$ (GeV)")
plt.savefig(output_direct+'/m_yy.pdf')
plt.close()

plot_variable(m_yyjj,"$m_{\gamma \gamma b \\bar{b}}$ (GeV)")
plt.ylim(ymin=0)
plt.annotate('$m_{b\\bar{b}}$ fixed to 125 GeV', xy=(0.98,.75), fontsize=14,xycoords='axes fraction',ha='right')
plt.savefig(output_direct+'/m_yyjj.pdf')
plt.close()

def rebin_histos(hist_list, factor=5):
    for h in hist_list:
        h.Rebin(factor)

plot_variable(m_jj,"$m_{bb}$ (GeV)")
plt.xlim((80,140))
plt.savefig(output_direct+'/m_bb.pdf')
plt.close()

plot_variable(j1_phi,"$\phi_{j1}$")
plt.savefig(output_direct+'/j1_phi.pdf')
plot_variable(j2_phi,"$\phi_{j2}$")
plt.savefig(output_direct+'/j2_phi.pdf')
plt.close()

plot_variable(j1_eta,"$\eta_{j1}$")
plt.savefig(output_direct+'/j1_eta.pdf')
plot_variable(j2_eta,"$\eta_{j2}$")
plt.savefig(output_direct+'/j2_eta.pdf')
plt.close()

#plot_variable(j1_pt,"j1_pt")
#plt.savefig(output_direct+'/j1_pt.pdf')
#plot_variable(j2_pt,"j2_pt")
#plt.savefig(output_direct+'/j2_pt.pdf')


plot_variable(y1_phi,"$\phi_{\gamma1}$")
plt.savefig(output_direct+'/y1_phi.pdf')
plot_variable(y2_phi,"$\phi_{\gamma2}$")
plt.savefig(output_direct+'/y2_phi.pdf')
plt.close()

plot_variable(y1_eta,"$\eta_{\gamma1}$")
plt.savefig(output_direct+'/y1_eta.pdf')
plot_variable(y2_eta,"$\eta_{\gamma2}$")
plt.savefig(output_direct+'/y2_eta.pdf')
plt.close()

plot_variable(njets,"Jet Multiplicity")
plt.savefig(output_direct+'/njets.pdf')
plt.close()

plot_variable(nbJets,"b-Jet Multiplicity")
plt.savefig(output_direct+'/nbJets.pdf')
plt.close()

plot_variable(jj_deta,"$\Delta \eta_{Highest\: m_{jj}\: pair}$")
plt.ylim(ymin=0)
plt.savefig(output_direct+'/jj_deta.pdf')
plot_variable(jj_dphi,"$\Delta \phi_{Highest\: m_{jj}\: pair}$")
plt.savefig(output_direct+'/jj_dphi.pdf')
plt.close()

plot_variable(yy_deta,"$\Delta \eta_{\gamma\gamma}$")
plt.savefig(output_direct+'/yy_deta.pdf')
plt.close()
plot_variable(yy_dphi,"$\Delta \phi_{\gamma\gamma}$")
plt.savefig(output_direct+'/yy_dphi.pdf')
plt.close()

plot_variable(max_deta,"$|\eta_{j}^{max}|$")
plt.savefig(output_direct+'/deta_jmax.pdf')
plt.close()

plot_variable(dR_y1j,"$\Delta R_{\gamma_{1} j}$")
plt.xlim((0, 0.005))
plt.ylim(ymin=0)
plt.savefig(output_direct+'/dR_y1j.pdf')
plt.close()

plot_variable(dR_y2j,"$\Delta R_{\gamma_{2} j}$")
plt.xlim((0, 0.005))
plt.ylim(ymin=0)
plt.savefig(output_direct+'/dR_y2j.pdf')
plt.close()



rebin_histos(pt_leadingJ)
plot_variable(pt_leadingJ,"$pT_{leading\: jet}$")
plt.xlim((0, 800))
plt.savefig(output_direct+'/pt_leadingJ.pdf')
plt.close()

rebin_histos(pt_subleadingJ)
plot_variable(pt_subleadingJ,"$pT_{subleading\: jet}$")
plt.xlim((0, 800))
plt.savefig(output_direct+'/pt_subleadingJ.pdf')
plt.close()

rebin_histos(pt_allJ)
plot_variable(pt_subleadingJ,"$pT_{all\: jet}$")
plt.xlim((0, 800))
plt.savefig(output_direct+'/pt_allJ.pdf')
plt.close()

plot_variable(eta_leadingJ,"$\eta_{leading\: jet}$")
plt.savefig(output_direct+'/eta_leadingJ.pdf')
plt.close()
plot_variable(eta_subleadingJ,"$\eta_{subleading\: jet}$")
plt.savefig(output_direct+'/eta_subleadingJ.pdf')
plt.close()
plot_variable(eta_subleadingJ,"$\eta_{all\: jet}$")
plt.savefig(output_direct+'/eta_allJ.pdf')
plt.close()

plot_variable(phi_leadingJ,"$\phi_{leading\: jet}$")
plt.savefig(output_direct+'/phi_leadingJ.pdf')
plt.close()
plot_variable(phi_subleadingJ,"$\phi_{subleading\: jet}$")
plt.savefig(output_direct+'/phi_subleadingJ.pdf')
plt.close()
plot_variable(phi_subleadingJ,"$\phi_{all\: jet}$")
plt.savefig(output_direct+'/phi_allJ.pdf')
plt.close()

plot_variable(coneTruthType,"Truth Type (Cone)")
plt.xlim((0, 10))
plt.annotate(s='b-jets', xy=(5.2,0.01))
plt.savefig(output_direct+'/coneTruthType.pdf')
plt.close()

plot_variable(partonTruthType,"Truth Type (Parton)")
plt.xlim((0, 22))
plt.annotate(s='b-jets', xy=(5.1,0.01), fontsize=6)
plt.savefig(output_direct+'/partonTruthType.pdf')
plt.close()

#rebin_histos(m_vbf_jets, 10)
plot_variable(m_vbf_jets,"$m_{jj,vbf}$")
#plt.xlim((0, 2000))
plt.savefig(output_direct+'/m_jj_vbf.pdf')
plt.close()

plot_variable(deta_maxPtjj,"$ \Delta \eta_{j_{1}j_{2}}$")
plt.savefig(output_direct+'maxPtjj_deta.pdf')
plt.close()

def plot_2d(h_list, x_title, y_title):
    """ Generates figure with subplots for each mass point

    TODO: Extrapolate for varible nPlots

    Arguments:
        h_list {list} -- List of input histograms
        x_title {string} -- Title for x-axis
    """
    bad = False
    for h in h_list:
        if h.Integral() == 0:
            bad = True
    if bad: return

    n_subplots = len(h_list)

    # generate figure
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, figsize=(7, 7), dpi=100)
    ax = fig.add_subplot(111, frameon=False)
    axes=[ax1, ax2, ax3, ax4]

    #plt.xlabel(x_title, position=(1, 0), va='bottom', ha='right', labelpad=20)
    #    plt.ylabel('AU', position=(0, 1), va='top', ha='right', labelpad=20)

    # things to do to all histograms
    for i, h in zip(axes, h_list):
        h.Scale(1/h.Integral())
        rplt.hist2d(h, axes=i)
        i.set_title(h.GetTitle())

    ax.set_xlabel(x_title,labelpad=20)
    ax.set_ylabel(y_title,labelpad=20)
    plt.xticks([])
    plt.yticks([])


    plt.annotate(s='Tyler James Burch', xy=(.005,.007), xycoords='figure fraction',
                 textcoords='figure fraction', color='grey',alpha=0.7)


# fig = plt.figure(figsize=(7, 5), dpi=100)
# axes = plt.axes()
# rplt.hist2d(jj_dEta_nJets[0], axes=axes)
# plt.savefig(output_direct+'/jj_dEta_njets.pdf')

plot_2d(jj_dEta_nJets,"$\Delta \eta$", "nJets")
plt.savefig(output_direct+'/jj_dEta_njets.pdf')
plt.close()

plot_2d(jj_dEta_nbJets,"$\Delta \eta$", "n b-Jets")
plt.savefig(output_direct+'/jj_dEta_nbJets.pdf')
plt.close()

plot_2d(deta_mjj,"$\Delta \eta$", "$m_{jj}$ of highest dijet mass pair")
plt.savefig(output_direct+'/jj_dEta_mjj.pdf')
plt.close()
