import glob
from rootpy.io import root_open
import csv
import os

# Match up files 
def file_match(input_file, other_list):
    match_string = input_file.split('HGamHadronisationStudy')[1]
    for f in other_list:
        if match_string in f:
            return f



if __name__=="__main__":
    mjj_cut_files = glob.glob('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/mjj_cut/*.root')
    standard_files = glob.glob('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/ntuples/standard/*.root')
    output_csv = open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/validation_tools/run/comparsion.csv', mode='w+')
    writer = csv.writer(output_csv )
    writer.writerow(['fFile', 'nEvents (mjj > 550)', 'Total nEvents', 'Fraction Retained', 'Fraction Rejected'])

    for filename in sorted(mjj_cut_files):
        matched_file = file_match(filename, standard_files)
        
        with root_open(filename) as f_mjj_cut:
            with root_open(matched_file) as f_standard:
                integral_mjj_cut = f_mjj_cut.deta_jj_lowMass.Integral()
                integral_standard = f_standard.deta_jj_lowMass.Integral()
                fraction_retained = integral_mjj_cut/integral_standard
                fraction_reject = 1-fraction_retained
                
                shortfile = filename.split('HGamHadronisationStudy_')[1].replace('.root','')
                write_list = [shortfile, integral_mjj_cut, integral_standard, fraction_retained, fraction_reject ]
                writer.writerow(write_list)                
    

    output_csv.close()
