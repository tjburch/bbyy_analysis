# Data classes
# ---------------------------------------------------
import ROOT
from math import fabs, pi


class Jet:
    def __init__(self, pt, eta, phi, m, btag, candidate):
        self.four_vector = ROOT.TLorentzVector()
        self.four_vector.SetPtEtaPhiM(pt, eta, phi, m)
        self.b_tagged = btag
        self.is_candidate = candidate


class VBF_jets:
    def __init__(self, jet_collection):
        self.idx1, self.idx2 = -1, -1
        self.jet_pair = -2
        self.deta, self.dphi, self.m = -99,-99, -99
        self.four_vector = ROOT.TLorentzVector()
        # Skip candidate jets
        VBFconsidered_jets = [j for j in jet_collection if j.is_candidate == False]
        # If 2 extra jets, fill object
        if len(VBFconsidered_jets) < 2:
            return
        self.jet_pair = -1
        max_mass = -1
        for j in VBFconsidered_jets:
            for i in VBFconsidered_jets:
                if i == j:
                    continue
                else:
                    iPair = j.four_vector + i.four_vector
                    if iPair.M() > max_mass:
                        self.jet_pair = iPair
                        self.idx1 = jet_collection.index(i)
                        self.idx2 = jet_collection.index(j)
                        self.deta = fabs(i.four_vector.Eta() - j.four_vector.Eta())
                        self.dphi = fabs(i.four_vector.Phi() - j.four_vector.Phi())
                        if self.dphi > pi:
                            self.dphi = 2*pi - self.dphi
                        self.m = iPair.M()
                        self.four_vector = iPair
                        max_mass = iPair.M()