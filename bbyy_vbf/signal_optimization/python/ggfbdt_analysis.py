import numpy as np
import matplotlib.pyplot as plt
import pdb
from utils import *

def get_sample_dict():
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    samples = {}
    vbf_files = [sourcedir + fname for fname in  ["bdtscore_vbf_mc16a.npy","bdtscore_vbf_mc16d.npy","bdtscore_vbf_mc16e.npy"]]
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    samples["arr_vbf_test"] = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)

    ## ggF samples
    ggf_files = [sourcedir + fname for fname in  ["bdtscore_ggf_mc16a.npy","bdtscore_ggf_mc16d.npy","bdtscore_ggf_mc16e.npy"]]
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)        
    samples["arr_ggf_test"] = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)

    ## yy samples
    yy_files = [sourcedir + fname for fname in  ["bdtscore_yy_mc16a.npy","bdtscore_yy_mc16d.npy","bdtscore_yy_mc16e.npy"]]
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    samples["arr_yy_test"] = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)

    # NTNI samples
    #ntni_files = [sourcedir + fname for fname in  ["bdtscore_ntni_mc16a.npy", "bdtscore_ntni_mc16d.npy", "bdtscore_ntni_mc16e.npy"]]
    #arr_ntni_test, ntni_eventweight_scalefactor_test = load_samples(ntni_files, ggf_preselection=True, test=True, myyCut=False)
    #yy_scaled_weight = samples["arr_yy_test"]["eventweight"].sum()
    ## Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    #total_continuum_yield = yy_scaled_weight / 0.84
    #ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni_test["eventweight"].sum()
    #samples["arr_ntni_test"] = rescale_eventweight(arr_ntni_test, ntni_fractional_reweight)

    # Single Higgs
    tth_files = [sourcedir + fname for fname in  ["bdtscore_TTH_mc16a.npy","bdtscore_TTH_mc16d.npy","bdtscore_TTH_mc16e.npy"]]
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    samples["arr_tth_test"] = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)

    ggh_files = [sourcedir + fname for fname in  ["bdtscore_GGH_mc16a.npy","bdtscore_GGH_mc16d.npy","bdtscore_GGH_mc16e.npy"]]
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
    samples["arr_ggh_test"] = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)

    zh_files = [sourcedir + fname for fname in  ["bdtscore_ZH_mc16a.npy","bdtscore_ZH_mc16d.npy","bdtscore_ZH_mc16e.npy"]]
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
    samples["arr_zh_test"] = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)

    #vbfh_files = [sourcedir + fname for fname in  ["bdtscore_VBFH_mc16a.npy","bdtscore_VBFH_mc16d.npy","bdtscore_VBFH_mc16e.npy"]]
    #arr_vbfh_test, vbfh_eventweight_scalefactor_test = load_samples(vbfh_files, ggf_preselection=True, test=True)
    #samples["arr_vbfh_test"] = rescale_eventweight(arr_vbfh_test, vbfh_eventweight_scalefactor_test)            
    return samples


def plot_categories(sample):
    # Add 2 to high mass categories, replace nans
    bdt_cat_adjust_highmass = sample["bdtcat_HH"] + 2
    bdt_cat_adjust_highmass[np.isnan(bdt_cat_adjust_highmass)] = 0
    # Fix low mass
    bdt_cat_adjust_lowmass = sample["bdtcat_HH_lowmass"]
    bdt_cat_adjust_lowmass[np.isnan(bdt_cat_adjust_lowmass)] = 0

    # add
    bdt_cat_array = bdt_cat_adjust_highmass + bdt_cat_adjust_lowmass
    valid_bdt_cat = bdt_cat_array>0
    bdt_cat_array = bdt_cat_array[valid_bdt_cat]
    ew = sample[valid_bdt_cat]["eventweight"]
    plt.hist(bdt_cat_array, weights=ew, histtype='step',bins=4, label="BDT")


    cutCategories = sample["cutCategory"]
    cutCategories = cutCategories[cutCategories > 0]
    ewcat = sample[sample["cutCategory"]>0]["eventweight"]
    plt.hist(cutCategories, weights=ewcat, color="firebrick",histtype='step',bins=4, label="Cuts")

    bottom,top =plt.ylim()
    plt.ylim(bottom, top*1.3)
    plt.legend(frameon=False, loc='best')
    plt.xlabel("Category")
    plt.ylabel("Events")

    print("BDT: " + str(ew.sum()) +  "  Cuts: "+ str(ewcat.sum()))


def main():

    sample_dictionary = get_sample_dict()
    arr_signals = np.concatenate([sample_dictionary[s] for s in ["arr_vbf_test", "arr_ggf_test"]])
    arr_backgrounds = np.concatenate([sample_dictionary[s] for s in ["arr_yy_test",  "arr_tth_test", "arr_ggh_test", "arr_zh_test"]])

    # Print checks
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % sample_dictionary["arr_vbf_test"]["eventweight"].sum())
    print("Total ggF yield: %.4f" % sample_dictionary["arr_ggf_test"]["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % sample_dictionary["arr_yy_test"]["eventweight"].sum())
    print("-----Single Higgs--------")
    print("Total ttH yield: %.4f" % sample_dictionary["arr_tth_test"]["eventweight"].sum())
    print("Total ggH yield: %.4f" % sample_dictionary["arr_ggh_test"]["eventweight"].sum())
    print("Total ZH yield: %.4f" % sample_dictionary["arr_zh_test"]["eventweight"].sum())
    #print("Total VBFH yield: %.4f" % sample_dictionary["arr_vbfh_test"]["eventweight"].sum())

    category_significance = []
    for col in ["bdtcat_HH", "bdtcat_HH_lowmass"]:
        for val in [1,2]:
            inCategory_sig = arr_signals[arr_signals[col] == val]
            inCategory_bkg = arr_backgrounds[arr_backgrounds[col] == val]
            category_significance.append(asimov_significance(
                s=inCategory_sig["eventweight"].sum(),
                b=inCategory_bkg["eventweight"].sum(),
                ))
    baseline_significance = sqrt(sum([x ** 2 for x in category_significance]))
    print("Initial Significance: {0}".format(baseline_significance))


    plot_categories(sample_dictionary["arr_vbf_test"])
    plt.title("VBF HH Sample")
    plt.tight_layout()
    plt.savefig('run/bdtcat_plots/vbf_categories')
    plt.close()
    plot_categories(sample_dictionary["arr_yy_test"])
    plt.title("$\gamma\gamma$-Continuum Sample")
    plt.tight_layout()
    plt.savefig('run/bdtcat_plots/yy_categories')
    plt.close()

if __name__ == "__main__":
    main()

