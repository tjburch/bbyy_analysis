from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
import matplotlib.pyplot as plt
import pickle
from xgboost import XGBClassifier, DMatrix
from sklearn.preprocessing import StandardScaler
from utils import *
import pdb 
import argparse
from classifySet import load_classifier_set
from tensorflow.keras.models import load_model

parser = argparse.ArgumentParser()
# These options exist for stepwise optimization
parser.add_argument("--stepwiseVariablePop", help="Variable index to pop for N-1 test", type=int)
parser.add_argument("--classifier", help="pass custom classifier", type=str, default="saved_classifiers/xgb_multiclass.dat")
parser.add_argument("-m","--model",help="Model type to run (BDT or NN)", default="BDT")
args = parser.parse_args()

lumi_scaling = 100

def add_cols(arr, classifier, scaler=None):
    """ Adds predictions and baseline passing """
    names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
        "aplanority",
        "aplanarity",
        "sphericity",
        "spherocity",
        "sphericityT",
        "planarity",
        "circularity",
        "planarFlow",
        "ht",
        "mjjb1",
        "mjjb2",
        "mjj_w1",
        "mjj_w2",
        "ptyy_myy_ratio",
        "pt_balance",
        "pt_qqbb",
        "pt_qq_bb_ratio",
        #"bdt_score"
    ]
    if args.stepwiseVariablePop:
        print("Omitting Variable: {0}".format(names.pop(args.stepwiseVariablePop)))

    
    # Predictions BDT
    if not scaler:   
        predictions =  classifier.predict_proba(make_clf_compliant(arr, names=names))
    if scaler:
        clf_arr = make_clf_compliant(arr)
        clf_arr = scaler.transform(clf_arr)
        predictions =  classifier.predict(clf_arr)

    
    vbfProb = predictions[:,0]
    ggfProb = predictions[:,1]
    yyProb = predictions[:,2]
    tthProb = predictions[:,3]
    arr = append_fields(
        arr, 
        data=[vbfProb, ggfProb, yyProb, tthProb],
        names=["vbfProb", "ggfProb", "yyProb", "tthProb"]
    )

    # Qualifiers
    ggfPassed = arr["cutCategory"] > 0
    vbfPassed = arr["jj_deta"] >= 0
    arr = append_fields(
        arr, 
        data=[ggfPassed, vbfPassed],
        names=["ggfPassed", "vbfPassed"],
    )
    return arr


def main():

    # Load Samples
    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    print("Loading Samples")
    # Setup VBF samples, common between both signal and background
    vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test * lumi_scaling)

    ## ggF samples
    ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)        
    arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test * lumi_scaling)

    ## yy samples
    yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test * lumi_scaling)

    # NTNI samples
    ntni_files = [sourcedir + fname for fname in  ["forcuts_ntni_mc16a.npy", "forcuts_ntni_mc16d.npy", "forcuts_ntni_mc16e.npy"]]
    arr_ntni_test, ntni_eventweight_scalefactor_test = load_samples(ntni_files, ggf_preselection=True, test=True, myyCut=False)
    yy_scaled_weight = arr_yy_test["eventweight"].sum()
    # Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    total_continuum_yield = yy_scaled_weight / 0.84
    ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni_test["eventweight"].sum()
    arr_ntni_test = rescale_eventweight(arr_ntni_test, ntni_fractional_reweight * lumi_scaling)

    # Single Higgs
    tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test * lumi_scaling)

    ggh_files = [sourcedir + fname for fname in  ["forcuts_GGH_mc16a.npy","forcuts_GGH_mc16d.npy","forcuts_GGH_mc16e.npy"]]
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
    arr_ggh_test = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test * lumi_scaling)

    zh_files = [sourcedir + fname for fname in  ["forcuts_ZH_mc16a.npy","forcuts_ZH_mc16d.npy","forcuts_ZH_mc16e.npy"]]
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
    arr_zh_test = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test * lumi_scaling)

    vbfh_files = [sourcedir + fname for fname in  ["forcuts_VBFH_mc16a.npy","forcuts_VBFH_mc16d.npy","forcuts_VBFH_mc16e.npy"]]
    arr_vbfh_test, vbfh_eventweight_scalefactor_test = load_samples(vbfh_files, ggf_preselection=True, test=True)
    arr_vbfh_test = rescale_eventweight(arr_vbfh_test, vbfh_eventweight_scalefactor_test * lumi_scaling)

    # Print checks
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % arr_vbf_test["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf_test["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % arr_yy_test["eventweight"].sum())
    print("Rescaled NTNI Bkg yield: %.4f" % arr_ntni_test["eventweight"].sum())
    print("-----Single Higgs--------")
    print("Total ttH yield: %.4f" % arr_tth_test["eventweight"].sum())
    print("Total ggH yield: %.4f" % arr_ggh_test["eventweight"].sum())
    print("Total ZH yield: %.4f" % arr_zh_test["eventweight"].sum())
    print("Total VBFH yield: %.4f" % arr_vbfh_test["eventweight"].sum())

    # Join Signal and Background
    arr_signals = np.concatenate([arr_vbf_test, arr_ggf_test])
    arr_backgrounds = np.concatenate([arr_yy_test, arr_ntni_test, arr_tth_test, arr_ggh_test, arr_zh_test, arr_vbfh_test])

    # Get original Significance
    category_significance = []       
    for cat in range(1, 5):
        inCategory_sig = arr_signals[arr_signals["cutCategory"] == cat]
        inCategory_bkg = arr_backgrounds[arr_backgrounds["cutCategory"] == cat]
        category_significance.append(asimov_significance(
            s=inCategory_sig["eventweight"].sum(),
            b=inCategory_bkg["eventweight"].sum(),
            ))
    baseline_significance = sqrt(sum([x ** 2 for x in category_significance]))
    print("Initial Significance: {0}".format(baseline_significance))


    # Load Classifer, apply
    print("Loading Classifier")
    if args.model == "BDT":
        xgb_multiclassifier = pickle.load(open(args.classifier, "rb"))
        print("Applying BDT")    
        arr_signals = add_cols(arr_signals, xgb_multiclassifier)
        arr_backgrounds = add_cols(arr_backgrounds, xgb_multiclassifier)
    if args.model == "NN":
        classifier_set = load_classifier_set()
        scaler = classifier_set.rescale_features_to_unity()
        nn = load_model("saved_classifiers/nn.h5")
        print("Applying NN")    
        arr_signals = add_cols(arr_signals, nn, scaler)
        arr_backgrounds = add_cols(arr_backgrounds, nn, scaler)




    # 4D Scan
    print("Beginning Scan")
    granularity = 0.1
    maxval = 1.0
    nVals = (maxval/granularity)**4
    #pbar = tqdm(total=nVals)
    best_significance = 0
    cuts = [0,0,0,0]
    for cut0 in np.arange(0, maxval, granularity):
        print (str((cut0 / maxval) *100) + " Percent done")
        for cut1 in np.arange(0, maxval, granularity):
            for cut2 in np.arange(0, maxval, granularity):
                for cut3 in np.arange(0, maxval, granularity):
                    # Generate VBF category
                    #pdb.set_trace()
                    vbfcat_bool_signal = np.logical_and.reduce([
                        arr_signals["vbfPassed"],
                        arr_signals["ggfPassed"],
                        arr_signals["vbfProb"] > cut0,
                        arr_signals["ggfProb"] < cut1,
                        arr_signals["yyProb"] < cut2,
                        arr_signals["tthProb"] < cut3,]
                    )
                    vbfcat_bool_background = np.logical_and.reduce([
                        arr_backgrounds["vbfPassed"],
                        arr_backgrounds["ggfPassed"],
                        arr_backgrounds["vbfProb"] > cut0,
                        arr_backgrounds["ggfProb"] < cut1,
                        arr_backgrounds["yyProb"] < cut2,
                        arr_backgrounds["tthProb"] < cut3,]
                    )
                    # Calculate Significance of that category
                    vbf_category_significance = asimov_significance(
                        s =arr_signals[vbfcat_bool_signal]["eventweight"].sum(),
                        b =arr_backgrounds[vbfcat_bool_background]["eventweight"].sum(),                        
                        )
                    
                    # Generate ggF Categories
                    ggfcat_bool_signal = np.logical_and(
                        ~vbfcat_bool_signal,
                        arr_signals["ggfPassed"],
                    )
                    ggfcat_bool_background = np.logical_and(
                        ~vbfcat_bool_background,
                        arr_backgrounds["ggfPassed"],
                    )
                    
                    category_significance = []
                    for cat in range(1, 5):
                        inCategory_sig = arr_signals[np.logical_and(ggfcat_bool_signal, arr_signals["cutCategory"] == cat)]
                        inCategory_bkg = arr_backgrounds[np.logical_and(ggfcat_bool_background, arr_backgrounds["cutCategory"] == cat)]
                        category_significance.append(asimov_significance(
                            s=inCategory_sig["eventweight"].sum(),
                            b=inCategory_bkg["eventweight"].sum(),
                            ))


                    if vbf_category_significance != 0:
                        category_significance.append(vbf_category_significance)
                    if len(category_significance) > 0:
                        this_significance = sqrt(sum([x ** 2 for x in category_significance]))
                    else:
                        this_significance = 0

                    if this_significance > best_significance:
                        best_significance = this_significance
                        cuts = [cut0, cut1, cut2, cut3]
                    #pbar.update(1)


    print("Original significance: {0}".format(round(baseline_significance,3)))
    print("Best significance: {0}".format(round(best_significance,3)))
    print("Cuts: {0}".format(cuts))
    print("Percent Improvement: {0}".format(round(100*(best_significance-baseline_significance)/baseline_significance, 3)))

if __name__ == "__main__":
    main()