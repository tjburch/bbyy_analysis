from xgboost import XGBClassifier, plot_importance, DMatrix, plot_tree
import xgboost as xgb
from numpy_optimizer import join_signal_regions
from bdt_optimizer import rescale_eventweight
from numpy.lib.recfunctions import append_fields, drop_fields
import numpy as np
import matplotlib.pyplot as plt
import argparse
import pandas as pd
import pickle
from sklearn.metrics import accuracy_score
from utils import *
import logging
logging.basicConfig(level=logging.ERROR)
import pdb
from sklearn.preprocessing import StandardScaler


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--signal", help="Do signal BDT", action="store_true")
parser.add_argument("-b", "--background", help="Do background BDT", action="store_true")
parser.add_argument("-d", "--depth", help="Depth")
parser.add_argument("-t", "--nTrees", help="Number of trees")
parser.add_argument("--optimizeDepth", help="Optimize depth parameter", action="store_true")
parser.add_argument("--optimizeTrees", help="Optimize trees parameter", action="store_true")
parser.add_argument("--noWeight", help="Don't use Weights", action="store_true")
args = parser.parse_args()

def print_usage():
    if not args.signal and not args.background:
        print("Not doing anything.")
    elif args.signal:
        print("Doing Signal BDT")
    elif args.background:
        print("Doing Background BDT")

    if not args.depth and not args.optimizeDepth:
        args.depth = 6
        print("Using default value of depth: {0}".format(args.depth))
    if not args.optimizeTrees and not args.nTrees:
        args.nTrees = 200
        print("Using default value {0} for n_estimators".format(args.nTrees))

names=[
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
]

output_directory = "run/classifier_evaluation/"

def plot_test_data(classifier, X_signal, X_background, w_signal, w_background):
    signal_predictions = classifier.predict_proba(X_signal)[:,1]
    bkg_predictions = classifier.predict_proba(X_background)[:,1]

    # plot signal and background separately
    plt.figure();
    plt.hist(signal_predictions,bins=np.linspace(0,1,50),
            histtype='step',color='midnightblue',label='signal', density=True, weights=w_signal);
    plt.hist(bkg_predictions,bins=np.linspace(0,1,50),
            histtype='step',color='firebrick',label='background',density=True, weights=w_background);
    # make the plot readable
    plt.xlabel('Prediction from BDT',fontsize=12);
    plt.ylabel('Events (Normalized to 1)',fontsize=12);
    plt.legend(frameon=False);


def plot_training_metrics(classifier, name):
    results = classifier.evals_result()
    epochs = len(results['validation_0']['error'])
    x_axis = range(0, epochs)
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
    ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
    ax.legend()
    plt.ylabel('Log Loss')
    plt.title('XGBoost Log Loss')
    plt.savefig("run/xgb/{0}_loss".format(name))
    plt.close()
    # plot classification error
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['error'], label='Train')
    ax.plot(x_axis, results['validation_1']['error'], label='Test')
    ax.legend()
    plt.ylabel('Classification Error')
    plt.title('XGBoost Classification Error')
    plt.savefig("run/xgb/{0}_error".format(name))
    plt.close()

def create_classifier(X_train, y_train, X_test, y_test, w_train=None, w_test=None, names=names, bdt_kwargdict={}, task_kwargdict={}):
    
    # Make Classifier
    param = {}
    # BDT parameters
    bdt_param={}
    bdt_param['learning_rate']      = 0.1 # learning rate
    bdt_param['max_depth']        = 3  # maximum depth of a tree
    bdt_param['subsample']        = 0.8 # fraction of events to train tree on
    bdt_param['colsample_bytree'] = 0.8 # fraction of features to train tree on
    bdt_param['objective']   = 'binary:logistic' # objective function
    bdt_param['n_estimators']   = 200 
    bdt_param["feature_names"] = names

    # Learning task parameters
    task_param={}
    task_param['eval_metric'] = ['logloss', 'error']           # evaluation metric for cross validation
    task_param['early_stopping_rounds'] = 30
    task_param['eval_set'] = [(X_train, y_train), (X_test,y_test)]
    if w_test is not None and w_train is not None:
        task_param['sample_weight'] = np.abs(w_train)
        task_param['sample_weight_eval_set'] = [np.abs(w_train), np.abs(w_test)]

    # Manual override default values
    bdt_param.update(bdt_kwargdict)
    task_param.update(task_kwargdict)

    # Classifier method
    classifier = XGBClassifier(**bdt_param)
    
    #pdb.set_trace()
    classifier.fit(X_train, y_train, verbose=False, **task_param)
    return classifier


class ClassifySet:
    # Object to house all the samples for passing to classifier, includes routines for joining signal and background oncce set
    # Also includes method for training and various optimizations

    def __init__(self):
        self.X_train_signal, self.X_test_signal, self.y_train_signal, self.y_test_signal, self.w_train_signal, self.w_test_signal = None, None, None, None, None, None
        self.X_train_bkg, self.X_test_bkg, self.y_train_bkg, self.y_test_bkg, self.w_train_bkg, self.w_test_bkg = None, None, None, None, None, None

        self.signal_samples = [self.X_train_signal, self.X_test_signal, self.y_train_signal, self.y_test_signal]
        self.bkg_samples = [self.X_train_bkg, self.X_test_bkg, self.y_train_bkg, self.y_test_bkg]

    def set_signal_samples(self, X_train_signal, X_test_signal, w_train_signal=None, w_test_signal=None):
        self.X_train_signal = X_train_signal
        self.X_test_signal = X_test_signal
        self.y_train_signal = np.ones(X_train_signal.shape[0])
        self.y_test_signal = np.ones(X_test_signal.shape[0])
        self.signal_samples = [self.X_train_signal, self.X_test_signal, self.y_train_signal, self.y_test_signal]
        self.w_train_signal = w_train_signal
        self.w_test_signal = w_test_signal


    def set_bkg_samples(self, X_train_bkg, X_test_bkg, w_train_bkg=None, w_test_bkg=None):
        self.X_train_bkg = X_train_bkg
        self.X_test_bkg = X_test_bkg
        self.y_train_bkg = np.zeros(X_train_bkg.shape[0])
        self.y_test_bkg = np.zeros(X_test_bkg.shape[0])
        self.bkg_samples = [self.X_train_bkg, self.X_test_bkg, self.y_train_bkg, self.y_test_bkg]   
        self.w_train_bkg = w_train_bkg
        self.w_test_bkg = w_test_bkg

    def rescale_features_to_unity(self):
        scaler = StandardScaler()
        all_X = np.concatenate((self.X_train, self.X_test))
        scaler.fit(all_X)
        self.X_train_signal = scaler.transform(self.X_train_signal)
        self.X_test_signal = scaler.transform(self.X_test_signal)
        self.X_train_bkg = scaler.transform(self.X_train_bkg)
        self.X_test_bkg = scaler.transform(self.X_test_bkg)
        self.X_train = scaler.transform(self.X_train)
        self.X_test = scaler.transform(self.X_test)

    def join_samples(self):

        if not all([a.any() for a in self.signal_samples]):
            raise ValueError("Set signal before joining")
        elif not any([a.any() for a in self.bkg_samples]):
            raise ValueError("Set background before joining")
        
        self.X_train = np.concatenate((self.X_train_signal, self.X_train_bkg))
        self.X_test = np.concatenate((self.X_test_signal, self.X_test_bkg))
        self.y_train = np.concatenate((self.y_train_signal, self.y_train_bkg))
        self.y_test = np.concatenate((self.y_test_signal, self.y_test_bkg))
        #self.rescale_features_to_unity()

        if self.w_train_signal is not None and self.w_train_bkg is not None: 
            self.w_train = np.concatenate((self.w_train_signal, self.w_train_bkg))
        else:
            self.w_train = None
        if self.w_test_signal is not None and self.w_test_bkg is not None: 
            self.w_test = np.concatenate((self.w_test_signal, self.w_test_bkg))
        else:
            self.w_test = None
        
    def optimize_depth(self, max_depth=20):
        print("Optimizing Depth")
        depth_scores_train=[]
        depth_scores_test=[]
        depth_range = np.arange(1, max_depth)
        for depth in depth_range:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict={"max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, bdt_kwargdict={"max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            depth_scores_train.append(accuracy_score(ypred_train,self.y_train))
            depth_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Depth {0} completed".format(depth))

        plt.plot(depth_range,depth_scores_train)
        plt.plot(depth_range,depth_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('Depth')
        plt.ylabel('Accuracy')
        plt.xticks(depth_range)
        best_depth = np.argmax(depth_scores_test)+1 #offset by one because rage starts at 1
        print("Best depth: {0}".format(best_depth))
        return best_depth
     
    def train(self, bdt_kwargdict=None):
        print("Training parameters: {0}".format(bdt_kwargdict))
        if self.w_train is not None and self.w_test is not None:
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names, bdt_kwargdict=bdt_kwargdict)
        else:
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict=bdt_kwargdict)


    def optimize_trees(self, to_test=[25, 50, 100, 200, 300], depth=6):
        print("Optimizing Trees")
        tree_scores_train=[]
        tree_scores_test=[]
        for ntrees in to_test:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            tree_scores_train.append(accuracy_score(ypred_train,self.y_train))
            tree_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Tested at {0} trees completed".format(ntrees))

        plt.plot(to_test, tree_scores_train)
        plt.plot(to_test, tree_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('nTrees')
        plt.ylabel('Accuracy')
        best_trees = to_test[np.argmax(tree_scores_test)] #offset by one because rage starts at 1
        print("Best trees: {0}".format(best_trees))
        return best_trees
         


def main():
    print_usage()

    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    # Setup VBF samples, common between both signal and background
    vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples(vbf_files, ggf_preselection=True, test=False)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    #arr_vbf_train = arr_vbf_train[arr_vbf_train["jj_deta"] >= 0]
    #arr_vbf_test = arr_vbf_test[arr_vbf_test["jj_deta"] >= 0]

    ## Rescale to accomodate for lost weights
    arr_vbf_train = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train) 
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    ## Reshape to xgb inputs
    X_train_signal = make_clf_compliant(arr_vbf_train)
    X_test_signal = make_clf_compliant(arr_vbf_test)
    ## Set individual eventweights to be the average of the whole
    logging.info("Original train VBF weight = {0:.3}".format(arr_vbf_train["eventweight"].sum()))
    logging.info("Original test VBF weight = {0:.3}".format(arr_vbf_test["eventweight"].sum()))
    w_train_signal = np.ones(arr_vbf_train.shape[0]) * arr_vbf_train["eventweight"].sum() / arr_vbf_train["eventweight"].shape[0]
    w_test_signal = np.ones(arr_vbf_test.shape[0]) * arr_vbf_test["eventweight"].sum() / arr_vbf_test["eventweight"].shape[0]
    logging.info("Rescaled to average train VBF weight = {0:.3}".format(w_train_signal.sum()))
    logging.info("Original test VBF weight = {0:.3}".format(w_test_signal.sum()))

    if args.signal:

        # Get ggF File
        ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
        arr_ggf_train, ggf_eventweight_scalefactor_train = load_samples(ggf_files, ggf_preselection=True, test=False)
        arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)
        ## Rescale to accomodate for lost weights
        arr_ggf_train = rescale_eventweight(arr_ggf_train, ggf_eventweight_scalefactor_train)
        arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)
        #arr_ggf_train = arr_ggf_train[arr_ggf_train["jj_deta"] >= 0]
        #arr_ggf_test = arr_ggf_test[arr_ggf_test["jj_deta"] >= 0]
        X_train_bkg = make_clf_compliant(arr_ggf_train)
        X_test_bkg = make_clf_compliant(arr_ggf_test)
        logging.info("Original train ggF weight = {0:.3}".format(arr_ggf_train["eventweight"].sum()))
        logging.info("Original test ggF weight = {0:.3}".format(arr_ggf_test["eventweight"].sum()))        
        w_train_bkg = np.ones(arr_ggf_train.shape[0]) * arr_ggf_train["eventweight"].sum() / arr_ggf_train["eventweight"].shape[0]
        w_test_bkg = np.ones(arr_ggf_test.shape[0]) * arr_ggf_test["eventweight"].sum() / arr_ggf_test["eventweight"].shape[0]
        logging.info("Rescaled to average train ggF weight = {0:.3}".format(w_train_bkg.sum()))
        logging.info("Original test ggF weight = {0:.3}".format(w_test_bkg.sum()))

        # Rebalance Classes
        w_train_signal = w_train_signal * (w_train_signal.shape[0] / w_train_signal.sum())
        w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())
        w_test_signal = w_test_signal * (w_test_signal.shape[0] / w_test_signal.sum())
        w_test_bkg = w_test_bkg * (w_test_signal.shape[0] / w_test_bkg.sum())
        
        # Set samples to Classifier Set object
        signalBDT_set = ClassifySet()
        if args.noWeight:
            signalBDT_set.set_signal_samples(X_train_signal, X_test_signal)
            signalBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg)
        else:            
            signalBDT_set.set_signal_samples(X_train_signal, X_test_signal, w_train_signal, w_test_signal)
            signalBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg, w_train_bkg, w_test_bkg)
        # Finalize object
        signalBDT_set.join_samples()

        if args.optimizeDepth:
            best_depth = signalBDT_set.optimize_depth()        
            plt.savefig("run/xgb/ggf_optimized_depth")
            plt.close()
            # Train also at best depth
            classifier = signalBDT_set.train({"max_depth":best_depth})
        elif args.optimizeTrees:
            best_trees = signalBDT_set.optimize_trees(depth=args.depth)
            plt.savefig("run/xgb/ggf_optimized_trees")
            plt.close()             
            print("Training final model at {0} trees".format(best_trees))
            classifier = signalBDT_set.train({"n_estimators":best_trees})             
        else:
            classifier = signalBDT_set.train({"max_depth": args.depth, "n_estimators":args.nTrees}) 

        # Save output
        output_classifier = 'saved_classifiers/ggf_xgb.dat'
        print("Saving classifier: {0}".format(output_classifier))
        pickle.dump(classifier, open(output_classifier,'wb'))            

        # Plot 
        plot_test_data(classifier, signalBDT_set.X_test_signal, signalBDT_set.X_test_bkg, signalBDT_set.w_test_signal,  signalBDT_set.w_test_bkg)
        plt.savefig('run/xgb/ggf_xgb_scores')
        plt.close()
        plot_training_metrics(classifier, "ggf")
        classifier.get_booster().feature_names = names
        plot_importance(classifier)
        plt.savefig("run/xgb/ggf_importance")
        plt.close()



    if args.background:
        yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
        tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
        ggh_files = [sourcedir + fname for fname in  ["forcuts_GGH_mc16a.npy","forcuts_GGH_mc16d.npy","forcuts_GGH_mc16e.npy"]]
        zh_files = [sourcedir + fname for fname in  ["forcuts_ZH_mc16a.npy","forcuts_ZH_mc16d.npy","forcuts_ZH_mc16e.npy"]]

        arr_yy_train, yy_eventweight_scalefactor_train = load_samples(yy_files, ggf_preselection=True, test=False, myyCut=False)
        arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
        #arr_yy_train = arr_yy_train[arr_yy_train["jj_deta"] >= 0]
        #arr_yy_test = arr_yy_test[arr_yy_test["jj_deta"] >= 0]
        arr_yy_train = rescale_eventweight(arr_yy_train, yy_eventweight_scalefactor_train)
        arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)
        w_train_yy = np.ones(arr_yy_train.shape[0]) * arr_yy_train["eventweight"].sum() / arr_yy_train["eventweight"].shape[0]
        w_test_yy = np.ones(arr_yy_test.shape[0]) * arr_yy_test["eventweight"].sum() / arr_yy_test["eventweight"].shape[0]

        # Single Higgs
        arr_tth_train, tth_eventweight_scalefactor_train = load_samples(tth_files, ggf_preselection=True, test=False)
        arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
        #arr_tth_train = arr_tth_train[arr_tth_train["jj_deta"] >= 0]
        #arr_tth_test = arr_tth_test[arr_tth_test["jj_deta"] >= 0]        
        arr_tth_train = rescale_eventweight(arr_tth_train, tth_eventweight_scalefactor_train)
        arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)
        w_train_tth = ( np.ones(arr_tth_train.shape[0]) * arr_tth_train["eventweight"].sum() / arr_tth_train["eventweight"].shape[0] )
        w_test_tth = ( np.ones(arr_tth_test.shape[0]) * arr_tth_test["eventweight"].sum() / arr_tth_test["eventweight"].shape[0] )
        arr_ggh_train, ggh_eventweight_scalefactor_train = load_samples(ggh_files, ggf_preselection=True, test=False)
        arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
        #arr_ggh_train = arr_ggh_train[arr_ggh_train["jj_deta"] >= 0]
        #arr_ggh_test = arr_ggh_test[arr_ggh_test["jj_deta"] >= 0]        
        arr_ggh_train = rescale_eventweight(arr_ggh_train, ggh_eventweight_scalefactor_train)
        arr_ggh_test = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)
        w_train_ggh = np.ones(arr_ggh_train.shape[0]) * arr_ggh_train["eventweight"].sum() / arr_ggh_train["eventweight"].shape[0]
        w_test_ggh = np.ones(arr_ggh_test.shape[0]) * arr_ggh_test["eventweight"].sum() / arr_ggh_test["eventweight"].shape[0]        
        arr_zh_train, zh_eventweight_scalefactor_train = load_samples(zh_files, ggf_preselection=True, test=False)
        arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
        #arr_zh_train = arr_zh_train[arr_zh_train["jj_deta"] >= 0]
        #arr_zh_test = arr_zh_test[arr_zh_test["jj_deta"] >= 0]                
        arr_zh_train = rescale_eventweight(arr_zh_train, zh_eventweight_scalefactor_train)
        arr_zh_test = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)   
        w_train_zh = np.ones(arr_zh_train.shape[0]) * arr_zh_train["eventweight"].sum() / arr_zh_train["eventweight"].shape[0]
        w_test_zh = np.ones(arr_zh_test.shape[0]) * arr_zh_test["eventweight"].sum() / arr_zh_test["eventweight"].shape[0] 




        logging.info("Background training weights:\n  yy={0:.3}\n  ttH={1:.3}\n  ggH={2:.3}\n  ZH={3:.3}".format(w_train_yy.sum(), w_train_tth.sum(), w_train_ggh.sum(), w_train_zh.sum()))

        joined_backgrounds_train = np.concatenate([arr_yy_train, arr_tth_train, arr_ggh_train, arr_zh_train])
        joined_backgrounds_test = np.concatenate([arr_yy_test, arr_tth_test, arr_ggh_test, arr_zh_test])

        X_train_bkg = make_clf_compliant(joined_backgrounds_train)
        X_test_bkg = make_clf_compliant(joined_backgrounds_test)
        w_train_bkg = np.abs(np.concatenate([w_train_yy, w_train_tth, w_train_ggh, w_train_zh]))
        w_test_bkg = np.abs(np.concatenate([w_test_yy, w_test_tth, w_test_ggh, w_test_zh]))

        w_train_signal = w_train_signal * (w_train_signal.shape[0] / w_train_signal.sum())
        w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())
        w_test_signal = w_test_signal * (w_test_signal.shape[0] / w_test_signal.sum())
        w_test_bkg = w_test_bkg * (w_test_signal.shape[0] / w_test_bkg.sum())        

        # Set samples to Classifier Set object
        backgroundBDT_set = ClassifySet()
        if args.noWeight:
            backgroundBDT_set.set_signal_samples(X_train_signal, X_test_signal)
            backgroundBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg)
        else:
            backgroundBDT_set.set_signal_samples(X_train_signal, X_test_signal, w_train_signal, w_test_signal)
            backgroundBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg, w_train_bkg, w_test_bkg)
        backgroundBDT_set.join_samples()
        
        if args.optimizeDepth:
            best_depth = backgroundBDT_set.optimize_depth()        
            plt.savefig("run/xgb/yy_optimized_depth")
            plt.close()
            # Train also at best depth
            classifier = backgroundBDT_set.train({"max_depth":best_depth})
        elif args.optimizeTrees:
            best_trees = backgroundBDT_set.optimize_trees(depth=args.depth)
            plt.savefig("run/xgb/yy_optimized_trees")
            plt.close()             
            print("Training final model at {0} trees".format(best_trees))
            classifier = backgroundBDT_set.train({"n_estimators":best_trees})
        else:
            classifier = backgroundBDT_set.train({"max_depth":args.depth, "n_estimators":args.nTrees})

        # Save output
        output_classifier = 'saved_classifiers/yy_xgb.dat'
        print("Saving classifier: {0}".format(output_classifier))
        pickle.dump(classifier, open(output_classifier,'wb'))

        # Plot 
        plot_test_data(classifier, backgroundBDT_set.X_test_signal, backgroundBDT_set.X_test_bkg, backgroundBDT_set.w_test_signal,  backgroundBDT_set.w_test_bkg)
        plt.savefig('run/xgb/yy_xgb_scores')
        plt.close()
        plot_training_metrics(classifier,"yy")
        classifier.get_booster().feature_names = names
        plot_importance(classifier)
        plt.tight_layout()
        plt.savefig("run/xgb/yy_importance")
        plt.close()


if __name__ == "__main__":
    main()
