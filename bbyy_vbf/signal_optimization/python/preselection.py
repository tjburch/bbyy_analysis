import ROOT
import sys
from vbf_classes import *
from preselection_helpers import *
from array import array
import numpy as np
import argparse

doROOT = False

try:
    if sys.argv[3] == "ntni":
        ntni = True
        print "Doing NTNI photons"
    else:
        ntni = False
except IndexError: ntni = False

if len(sys.argv) < 3:
    print("Usage: python preselection.py inputfile.root outputfile.root (lumi in pb)")
    sys.exit(1)


# Define input and output
print("Loading Files")
# ---------------------------------------------------
f_in = ROOT.TFile(sys.argv[1])
output_tree = ROOT.TTree("mini", "mini")


# Propagate cutflow to file
print("Getting Cutflow")
# ---------------------------------------------------
for key in f_in.GetListOfKeys():
    if "noDalitz_weighted" in key.GetName():
        cutflow_name = key.GetName()
    elif "_data" in sys.argv[1]:
        cutflow_name = "data"
if cutflow_name is None:
    raise RuntimeError("Unable to get cutflow!")
    sys.exit(1)
else:
    if cutflow_name is not "data":
        cutflow = f_in.Get(cutflow_name)

# Initialize xAOD Framework
print("Initializing xAOD Framework")
# ---------------------------------------------------
ROOT.xAOD.Init()


def char2bool(char):
    """Convert char to bool."""
    return {"\x00": False, "\x01": True}[char]


def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value

# Load Tree
print("Loading up xAOD tree to iterate on")
# ---------------------------------------------------
input_tree = ROOT.xAOD.MakeTransientTree(f_in, "CollectionTree")
entries = input_tree.GetEntries()
print("%s Entries" % str(entries))


# Output Arrays
# ---------------------------------------------------
if not doROOT:
    np_m_yy = np.array([])
    np_jj_deta = np.array([])
    np_jj_dphi = np.array([])
    np_mjj = np.array([])
    np_n_jet = np.array([])
    np_eventweight = np.array([])
    np_tagregion = np.array([])
    np_cutCategory = np.array([])
    np_myyjj = np.array([])
    np_dR_yy = np.array([])
    np_dR_jj = np.array([])
    np_dR_yyjj = np.array([])
    np_j1_tagbin = np.array([])
    np_j2_tagbin = np.array([])
    np_nbtag70 =np.array([])
    np_y1_isTight = np.array([])
    np_y2_isTight = np.array([])
    np_y1_isIsoFixedCutLoose = np.array([])
    np_y2_isIsoFixedCutLoose = np.array([])
    np_y1_isIsoFixedCutTight = np.array([])
    np_y2_isIsoFixedCutTight = np.array([])
    np_y1_pt = np.array([])
    np_y2_pt = np.array([])
    np_passing = np.array([])
    np_eventNumber = np.array([])
    np_ptyy_myy_ratio = np.array([])
    np_pt_balance = np.array([])
    np_pt_qqbb = np.array([])
    np_pt_qq_bb_ratio = np.array([])
    # Event shape Variables
    np_aplanority = np.array([])
    np_aplanarity = np.array([])
    np_sphericity = np.array([])
    np_spherocity = np.array([])
    np_sphericityT = np.array([])
    np_planarity = np.array([])
    np_circularity = np.array([])
    np_planarFlow = np.array([])
    # w-mass varaiables
    np_ht = np.array([])
    np_mjjb1 = np.array([])
    np_mjjb2 = np.array([])
    np_mjj_w1 = np.array([])
    np_mjj_w2 = np.array([])

    np_pt_j3 = np.array([])

# Get Luminosity
print("Getting Luminosity and weights")
# ---------------------------------------------------

if "mc16a" in sys.argv[1]:
    lumi = 36215.0
elif "mc16d" in sys.argv[1]:
    lumi = 44307.4
elif "mc16e" in sys.argv[1]:
    lumi = 58450.1
elif "_data" in sys.argv[1]:
    lumi = 9999999 #fill dummy value
else:
    raise IOError("Lumi can't be figured out by filename")

if cutflow_name != "data":
    sum_weight = (
        cutflow.GetBinContent(1) / cutflow.GetBinContent(2)
    ) * cutflow.GetBinContent(3)
else:
    sum_weight = 1



# Event Loop
print("Beginning Event Loop")
# ---------------------------------------------------
for ientry in range(input_tree.GetEntries()):
    # Reset TTree values
    m_yy, jj_deta, jj_dphi, mjj, n_jet, pt_qqbb, pt_qq_bb_ratio = -99, -99, -99, -99, -99, -99, -99
    # Print Status
    if ientry % 10000 == 0:
        print(
            "ientry = %d out of %d, percent done = %.3f"
            % (
                ientry,
                input_tree.GetEntries(),
                1.0 * ientry / input_tree.GetEntries() * 100,
            )
        )
    input_tree.GetEntry(ientry)

    m_yy = read(input_tree.HGamEventInfo, "float", "m_yy") * 0.001

    # Apply Preselection
    passing = False

    # Normal Selection
    if ntni:

        # NTNI Selection
        if (
            not read(input_tree.HGamEventInfo, "char", "isPassed")
            and read(input_tree.HGamEventInfo, "int", "N_lep") == 0
            and read(input_tree.HGamEventInfo, "int", "N_j_central") < 6
            and read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") > 70.0
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj")
            <= 180.0
        ):
            passing = True

    else:
        # Normal Selection
        if (
            read(input_tree.HGamEventInfo, "char", "isPassed")
            and read(input_tree.HGamEventInfo, "int", "N_lep") == 0
            and read(input_tree.HGamEventInfo, "int", "N_j_central") < 6
            and read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") > 70.0
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj")
            <= 180.0
        ):
            passing = True


    if not passing:
        continue

    # Get the tag region
    tagregion = 0
    for j in input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT:
        bin = read(j, "int", "MV2c10bin")
        if bin >= 1:
            tagregion += 1

    # Blind - NOT NEEDED FOR MC
    # if (tagregion == 2 and m_yy > 120 and m_yy < 130): continue

    # Get weights
    if cutflow_name != "data":
        weight = (
            read(input_tree.HGamEventInfo, "float", "crossSectionBRfilterEff")
            * read(input_tree.HGamEventInfo, "float", "weight")
            * read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_weight")
            * float(lumi)
            / float(sum_weight)
        )
    else: weight = 1


    # Define Jets
    # ------------------------------------------------------------------------------------------------
    # Read
    jets = []
    nbtag70 = 0
    for j in input_tree.HGamAntiKt4EMTopoJets:
        # determine if candidate
        candiate_jet = False
        for cand in input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT:
            if (
                j.eta() < cand.eta() + 0.01
                and j.eta() > cand.eta() - 0.01
                and j.phi() < cand.phi() + 0.01
                and j.phi() > cand.phi() - 0.01
            ):
                candiate_jet = True
        thisJet = Jet(
            j.pt() * 0.001,
            j.eta(),
            j.phi(),
            j.m() * 0.001,
            btag=(read(j, "char", "MV2c10_FixedCutBEff_85")),
            candidate=candiate_jet,
        )
        jets.append(thisJet)
        if read(j, "char", "MV2c10_FixedCutBEff_70"):
            nbtag70 += 1

    # Photon Quantities
    # ------------------------------------------------------------------------------------------------
    y1, y2 = input_tree.HGamPhotons[0], input_tree.HGamPhotons[1]
    lv_y1 = ROOT.TLorentzVector()
    lv_y2 = ROOT.TLorentzVector()
    lv_y1.SetPtEtaPhiM(y1.pt(),y1.eta(),y1.phi(),0)
    lv_y2.SetPtEtaPhiM(y2.pt(),y2.eta(),y2.phi(),0)

    np_y1_isTight = np.append(np_y1_isTight, True if read(y1, "char", "isTight") else False)
    np_y2_isTight = np.append(np_y2_isTight, True if read(y2, "char", "isTight") else False)
    np_y1_isIsoFixedCutLoose = np.append(np_y1_isIsoFixedCutLoose, True if read(y1, "char", "isIsoFixedCutLoose") else False)
    np_y2_isIsoFixedCutLoose = np.append(np_y2_isIsoFixedCutLoose, True if read(y2, "char", "isIsoFixedCutLoose") else False)
    np_y1_isIsoFixedCutTight = np.append(np_y1_isIsoFixedCutTight, True if read(y1, "char", "isIsoFixedCutTight") else False)
    np_y2_isIsoFixedCutTight = np.append(np_y2_isIsoFixedCutTight, True if read(y2, "char", "isIsoFixedCutTight") else False)

    np_y1_pt = np.append(np_y1_pt, read(input_tree.HGamEventInfo, "float", "pT_y1")*.001)
    np_y2_pt = np.append(np_y2_pt, read(input_tree.HGamEventInfo, "float", "pT_y2")*.001)    

    np_ptyy_myy_ratio = np.append(np_ptyy_myy_ratio, read(input_tree.HGamEventInfo, "float", "pT_yy")*0.001 / m_yy )


    # Shape Variables
    # ------------------------------------------------------------------------------------------------
    sorted_jets = sorted(input_tree.HGamAntiKt4EMTopoJets, key=lambda x: x.pt(), reverse=True)
    lv_j1 = ROOT.TLorentzVector()
    lv_j2 = ROOT.TLorentzVector()
    lv_j1.SetPtEtaPhiM(sorted_jets[0].pt(),sorted_jets[0].eta(),sorted_jets[0].phi(),sorted_jets[0].m())
    lv_j2.SetPtEtaPhiM(sorted_jets[1].pt(),sorted_jets[1].eta(),sorted_jets[1].phi(),sorted_jets[1].m())

    shape_dictionary = shape_variables(sorted_jets[0], sorted_jets[1], input_tree.HGamPhotons[0], input_tree.HGamPhotons[1])
    np_aplanority = np.append(np_aplanority, shape_dictionary["aplanority"])
    np_aplanarity = np.append(np_aplanarity, shape_dictionary["aplanarity"])
    np_sphericity = np.append(np_sphericity, shape_dictionary["sphericity"])
    np_spherocity = np.append(np_spherocity, shape_dictionary["spherocity"])
    np_sphericityT = np.append(np_sphericityT, shape_dictionary["sphericityT"])
    np_planarity = np.append(np_planarity,  shape_dictionary["planarity"])
    np_circularity = np.append(np_circularity, shape_dictionary["circularity"])
    np_planarFlow = np.append(np_planarFlow, shape_dictionary["planarFlow"])
    this_pt_balance = (lv_j1 + lv_j2 + lv_y1 + lv_y2).Pt() / (lv_j1.Pt() + lv_j2.Pt() + lv_y1.Pt() + lv_y2.Pt() )
    np_pt_balance = np.append(np_pt_balance, this_pt_balance)

    # Other ttH Variables
    # ------------------------------------------------------------------------------------------------
    b1 = input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[0]
    b2 = input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[1]
    ht, mjjb1, mjjb2, mjj_w1, mjj_w2 = tth_variables(input_tree.HGamAntiKt4EMTopoJets, b1, b2)
    np_ht = np.append(np_ht, ht)
    np_mjjb1 = np.append(np_mjjb1, mjjb1)
    np_mjjb2 = np.append(np_mjjb2, mjjb2)
    np_mjj_w1 = np.append(np_mjj_w1, mjj_w1)
    np_mjj_w2 = np.append(np_mjj_w2, mjj_w2)

    # Set VBF Variables
    # ------------------------------------------------------------------------------------------------
    leading_b = Jet(
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[0].pt() * 0.001,
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[0].eta(),
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[0].phi(),
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[0].m() * 0.001,
            btag=True,
            candidate=True,
        )
    subleading_b = Jet(
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[1].pt() * 0.001,
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[1].eta(),
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[1].phi(),
            input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[1].m() * 0.001,
            btag=True,
            candidate=True,
        )
    bb_pair = leading_b.four_vector + subleading_b.four_vector
    
    n_jet = read(input_tree.HGamEventInfo, "int", "N_j")
    vbf_jets = None
    if n_jet >= 4:
        vbf_jets = VBF_jets(jets)
        if vbf_jets.deta == -99 or vbf_jets.m == -99:
            ("Problem in Dijet!!!!")

        jj_deta = vbf_jets.deta
        jj_dphi = vbf_jets.dphi
        mjj = vbf_jets.m
        pt_qqbb = (vbf_jets.four_vector + bb_pair).Pt() 
        pt_qq_bb_ratio = (float(vbf_jets.four_vector.Pt())) / (bb_pair.Pt())

    j_pts = [-99]
    if n_jet > 4:
        for j in jets:
            if j.is_candidate: continue
            if vbf_jets:
                if jets.index(j) == vbf_jets.idx1: continue
                if jets.index(j) == vbf_jets.idx2: continue
            j_pts.append(j.four_vector.Pt())
    np_pt_j3 = np.append(np_pt_j3, max(j_pts))

    # Fill Numpy arrays
    np_m_yy = np.append(np_m_yy, m_yy)
    np_jj_deta = np.append(np_jj_deta, jj_deta)
    np_jj_dphi = np.append(np_jj_dphi, jj_dphi)
    np_mjj = np.append(np_mjj, mjj)
    np_n_jet = np.append(np_n_jet, float(n_jet))
    np_eventweight = np.append(np_eventweight, float(weight))
    np_tagregion = np.append(np_tagregion, tagregion)
    np_nbtag70 = np.append(np_nbtag70, nbtag70)
    np_pt_qqbb = np.append(np_pt_qqbb,pt_qqbb)
    np_pt_qq_bb_ratio = np.append(np_pt_qq_bb_ratio, pt_qq_bb_ratio)

    np_cutCategory = np.append(np_cutCategory, read(input_tree.HGamEventInfo, "int", "yybb_nonRes_cutBased_discreteMV2c10pT_Cat"))
    np_myyjj = np.append(np_myyjj, read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_yyjj_tilde"))
    np_dR_yy = np.append(np_dR_yy, read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_deltaR_yy"))
    np_dR_jj = np.append(np_dR_jj, read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_deltaR_jj"))
    np_dR_yyjj = np.append(np_dR_yyjj, read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_deltaR_yyjj"))
    np_eventNumber = np.append(np_eventNumber, input_tree.EventInfo.eventNumber())

    for idx in range(0, len(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT)):
        tagbin = read(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[idx], "int", "MV2c10bin")
        if idx == 0:
            np_j1_tagbin = np.append(np_j1_tagbin, float(tagbin))
        elif idx == 1:
            np_j2_tagbin = np.append(np_j2_tagbin, float(tagbin))
        else:
            print("something has gone terribly wrong")


ROOT.xAOD.ClearTransientTrees()
f_in.Close()

# Create Structured array
nparrays = [
    np_m_yy,
    np_jj_deta,
    np_jj_dphi,
    np_mjj,
    np_n_jet,
    np_eventweight,
    np_tagregion,
    np_cutCategory,
    np_myyjj,
    np_dR_yy,
    np_dR_jj,
    np_dR_yyjj,
    np_j1_tagbin,
    np_j2_tagbin,
    np_nbtag70,
    np_y2_isTight,
    np_y1_isTight,
    np_y1_isIsoFixedCutLoose,
    np_y2_isIsoFixedCutLoose,
    np_y1_isIsoFixedCutTight,
    np_y2_isIsoFixedCutTight,
    np_y1_pt,
    np_y2_pt,
    np_ptyy_myy_ratio,
    np_pt_balance,
    np_pt_qqbb,
    np_pt_qq_bb_ratio,
    np_eventNumber,
    np_aplanority,
    np_aplanarity,
    np_sphericity,
    np_spherocity,
    np_sphericityT,
    np_planarity,
    np_circularity,
    np_planarFlow,
    np_ht,
    np_mjjb1,
    np_mjjb2,
    np_mjj_w1,
    np_mjj_w2,
    np_pt_j3
]

names = [
    "m_yy",
    "jj_deta",
    "jj_dphi",
    "mjj",
    "n_jet",
    "eventweight",
    "tagregion",
    "cutCategory",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "n_btag70",
    "y2_isTight",
    "y1_isTight",
    "y1_isIsoFixedCutLoose",
    "y2_isIsoFixedCutLoose",
    "y1_isIsoFixedCutTight",
    "y2_isIsoFixedCutTight",
    "y1_pt",
    "y2_pt",
    "ptyy_myy_ratio",
    "pt_balance",
    "pt_qqbb",
    "pt_qq_bb_ratio",
    "eventNumber",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    "pt_j3"
]

recarray = np.core.records.fromarrays(nparrays, names=names)

#if args.root:

#else:
np.save(sys.argv[2].replace(".root", ".npy"), recarray)
