import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from utils import *
import os
import pdb
import argparse

doVBF = True
doGGF = True
doYY = False
doNTNI = False
doTTH = False

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--coupling", help="Varied Coupling to use", type=str)
args = parser.parse_args()

# Create coupling directory if necessary
cwd = os.getcwd()
if args.coupling:
    outputdir = "{0}/run/{1}/".format(cwd, args.coupling)
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)
else:
    outputdir = "{0}/run/".format(cwd)


sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
if not args.coupling:
    vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
    vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
    vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"
else:
    couplingdir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/coupling_ntuples/"

    vbf_mc16a = couplingdir+"mc16a_"+args.coupling+".npy"
    vbf_mc16d = couplingdir+"mc16d_"+args.coupling+".npy"
    vbf_mc16e = couplingdir+"mc16e_"+args.coupling+".npy"


ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
yy_mc16e = sourcedir + "forcuts_yy_mc16e.npy"

ntni1516 = sourcedir + "forcuts_ntni_mc16a.npy"
ntni17 = sourcedir + "forcuts_ntni_mc16d.npy"
ntni18 = sourcedir + "forcuts_ntni_mc16e.npy"

tth_mc16a = sourcedir + "forcuts_TTH_mc16a.npy"
tth_mc16d = sourcedir + "forcuts_TTH_mc16d.npy"
tth_mc16e = sourcedir + "forcuts_TTH_mc16e.npy"


# Load samples
arr_vbf = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])
arr_ggf = join_signal_regions([ggf_mc16a, ggf_mc16d, ggf_mc16e])
arr_yy = join_signal_regions([yy_mc16a, yy_mc16d, yy_mc16e], tagReq=True, ggf_preselection=False)
arr_ntni = join_signal_regions([ntni1516, ntni17, ntni18], tagReq=True, ggf_preselection=False)
arr_ntni = ntni_photons(arr_ntni)
arr_tth = join_signal_regions([tth_mc16a, tth_mc16d, tth_mc16e])

# Get rid of invalid events
valid_vbf = arr_vbf[arr_vbf["jj_deta"] >= 0]  # Works as proxy, -99 for invalid events
valid_ggf = arr_ggf[arr_ggf["jj_deta"] >= 0]  # Works as proxy, -99 for invalid events
valid_yy = arr_yy[arr_yy["jj_deta"] >= 0]  # Works as proxy, -99 for invalid events
valid_ntni = arr_ntni[arr_ntni["jj_deta"] >= 0]
valid_tth = arr_tth[arr_tth["jj_deta"] >= 0]


# Renormalize to 1
arr_vbf = rescale_eventweight(valid_vbf, 1. / valid_vbf["eventweight"].sum())
arr_ggf = rescale_eventweight(valid_ggf, 1. / valid_ggf["eventweight"].sum())
arr_yy = rescale_eventweight(valid_yy, 1. / valid_yy["eventweight"].sum())
arr_ntni = rescale_eventweight(valid_ntni, 1. / valid_ntni["eventweight"].sum())
arr_tth = rescale_eventweight(valid_tth, 1. / valid_tth["eventweight"].sum())

names = [
    "m_yy",
    "jj_deta",
    "mjj",
    "n_jet",
    "cutCategory",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "tagregion",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    "ptyy_myy_ratio",
    "pt_qqbb",
    "pt_qq_bb_ratio",    
    "pt_balance",
    "pt_j3"
    ]
axes_titles = [
    "$m_{\gamma\gamma}$ [GeV]",
    "$\Delta\eta_{jj}$",
    "$m_{jj}$",
    "Jet Multiplicity",
    "Cut Category",
    "$m_{\gamma\gamma bb}$ [GeV]",
    "$\Delta R_{\gamma,\gamma}$",
    "$\Delta R_{b,b}$",
    "$\Delta R_{\gamma\gamma,bb}$",
    "$j_{1}$ Tagbin",
    "$j_{2}$ Tagbin",
    "tagregion",
    "aplanarity",
    "aplanority",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "$H_{T}$ [GeV]",
    "$m_{jj_{W}b_{1}}$ [GeV]",
    "$m_{jj_{W}b_{2}}$ [GeV]",
    "$mjj_{W1}$ [GeV]",
    "$mjj_{W2}$ [GeV]",
    "$pT_{\gamma\gamma}/m_{\gamma\gamma}$",
    "$pT_{qqbb}$ [GeV]", 
    "$pT_{qq}/pT_{bb}$",
    "pT Balance",
    "$p_{T,j3}$"
    ]


bin_dict = {
    "n_jet": np.arange(1.5, 10.5, 1),
    "m_yyjj": np.linspace(start=0, stop=1500, num=60),
    "mjj": np.linspace(start=0, stop=3000, num=60),
    "dR_jj": np.arange(0, 5, 0.25),
    "dR_yy": np.arange(0, 4, 0.25),
    "dR_yyjj": np.arange(0, 8, 0.5),
    "jj_deta": np.arange(0, 10, 0.5),
    "j1_tagbin": np.arange(1, 6, 1),
    "j2_tagbin": np.arange(1, 6, 1),
    "tagregion": np.arange(0,5,1),
    "aplanarity": np.linspace(0,1, 35),
    "aplanority": np.linspace(0,1, 35),
    "sphericity": np.linspace(0,1, 35),
    "spherocity": np.linspace(0,1, 35),
    "sphericityT": np.linspace(0,1, 35),
    "planarity": np.linspace(0,1, 35),
    "circularity": np.linspace(0,1, 35),
    "planarFlow": np.linspace(0,1, 35),
    "ht" : np.linspace(0, 1000, 35),
    "mjjb1" : np.linspace(0, 1000, 35),
    "mjjb2" : np.linspace(0, 1000, 35),
    "mjj_w1" : np.linspace(0, 160, 35),
    "mjj_w2" : np.linspace(0, 160, 35),
    "pt_qqbb" : np.linspace(0,800,50),
    "pt_qq_bb_ratio": np.linspace(0,10,35),
    "ptyy_myy_ratio" : np.linspace(0,5, 35),
    "pt_balance" : np.linspace(0,1,35),
    "pt_j3" : np.linspace(0,800,50),
}


for distribution, title in zip(names, axes_titles):

    if distribution in ["mjjb1","mjjb2","mjj_w1","mjj_w2"]:
        arr_vbf_draw = arr_vbf[arr_vbf[distribution]>50]
        arr_ggf_draw = arr_ggf[arr_ggf[distribution]>50]
        arr_yy_draw = arr_yy[arr_yy[distribution]>50]
        arr_tth_draw = arr_tth[arr_tth[distribution]>50]        
    else:
        arr_vbf_draw = arr_vbf
        arr_ggf_draw = arr_ggf
        arr_yy_draw = arr_yy
        arr_tth_draw = arr_tth

    #arr_yy_draw["ptyy_myy_ratio"] = arr_yy_draw["ptyy_myy_ratio"] * .001
    #arr_tth_draw["ptyy_myy_ratio"] = arr_tth_draw["ptyy_myy_ratio"] * .001

    fig = plt.figure()

    if distribution in bin_dict:
        bins = bin_dict[distribution]
    else:
        all_arrs = np.concatenate(
            [arr_vbf[distribution], arr_ggf[distribution]]
            #[arr_vbf[distribution], arr_ggf[distribution], arr_yy[distribution], arr_ntni[distribution]]
        )
        bins = np.arange(all_arrs.min(), all_arrs.max())

    yvals =[]
    if doVBF:
        y1, x1, _1 = plt.hist(
            arr_vbf_draw[distribution],
            weights=arr_vbf_draw["eventweight"],
            bins=bins,
            histtype="step",
            color="firebrick",
            label="VBF HH",
            density=True
        )
        yvals.append(y1)
    if doGGF:
        y2, x2, _2 = plt.hist(
            arr_ggf_draw[distribution],
            weights=arr_ggf_draw["eventweight"],
            bins=bins,
            histtype="step",
            color="cornflowerblue",
            label="ggF HH",
            density=True
        )
        yvals.append(y2)

    if doYY:
        y3, x3, _2 = plt.hist(
            arr_yy_draw[distribution],
            weights=arr_yy_draw["eventweight"],
            bins=bins,
            histtype="step",
            color="indigo",
            label="$\gamma\gamma$+jets",
            density=True
        )
        yvals.append(y3)

    if doNTNI:
        y4, x4, _4 = plt.hist(
            arr_ntni[distribution],
            weights=arr_ntni["eventweight"],
            bins=bins,
            histtype="step",
            color="darkcyan",
            label="NTNI",
            density=True
        )
        yvals.append(y4)

    if doTTH:
        y5, x5, _5 = plt.hist(
            arr_tth_draw[distribution],
            weights=arr_tth_draw["eventweight"],
            bins=bins,
            histtype="step",
            color="forestgreen",
            label="ttH",
            density=True
        )
        yvals.append(y5)

    plt.ylim(ymin=0, ymax=1.3 * max([v.max(0) for v in yvals]))
    plt.ylabel("A.U.", horizontalalignment="right", y=1.0, fontsize=16)
    plt.xlabel(title, horizontalalignment="right", x=1.0, fontsize=16)
    plt.legend(frameon=False)
    plt.tight_layout()
    plt.savefig(outputdir + distribution)
    plt.close()


variables = [
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    ]


#### correlations of event shapes
tth_df = pd.DataFrame(arr_tth[variables])
corr = tth_df.corr()
sns.heatmap(corr, 
        xticklabels=corr.columns.values,
        yticklabels=corr.columns.values,
        linewidths=.5,
        cmap="coolwarm",
        vmin=-1,
        vmax=1
        )
plt.tight_layout()
plt.savefig(outputdir + "/tth_corr.png")
plt.close()
vbf_df = pd.DataFrame(arr_vbf[variables])
corr = vbf_df.corr()
sns.heatmap(corr, 
        xticklabels=corr.columns.values,
        yticklabels=corr.columns.values,
        linewidths=.5,
        cmap="coolwarm",
        vmin=-1,
        vmax=1        
        )
plt.tight_layout()
plt.savefig(outputdir + "/vbf_corr.png")