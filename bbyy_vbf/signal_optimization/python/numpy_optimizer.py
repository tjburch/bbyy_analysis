from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
#from tqdm import tqdm
import matplotlib.pyplot as plt
from utils import *

# number of splits in segmenting ggF/VBF
n_deta_splits = 20
n_mjj_splits = 32

# number of splits when optimizing VBF vs background
category_deta_splits = 10
category_mjj_splits = 32
category_myyjj_splits = 10

# Define custom bounds to override min/max
custom_mjj = (400, 1200)
custom_deta = (3, 8)
custom_myyjj = (300, 800)

# Global define sample names
sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/2019_cut_optimization/ntuples/"
vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"



def apply_cut(arr, mjj_val, deta_val, m_yyjj_val=None):
    """Apply cut on array at input values
    
    Arguments:
        arr {np structured array} -- Array to Cut
        mjj_val {[float]} -- mjj > mjj_val
        deta_val {[float]} -- jj_deta > deta_val
    Returns:
        [np structured arrays] -- pass_cut: passes all applied cuts. fail_cut: all other events
    """
    if m_yyjj_val:
        cut_boolean_array = (
            (arr["jj_deta"] > deta_val)
            * (arr["mjj"] > mjj_val)
            * (arr["m_yyjj"] > m_yyjj_val)
        )
    else:
        cut_boolean_array = (arr["jj_deta"] > deta_val) * (arr["mjj"] > mjj_val)
    pass_cut = arr[cut_boolean_array]
    fail_cut = arr[np.logical_not(cut_boolean_array)]
    return pass_cut, fail_cut


def find_optimized_vbf_category(arr_vbf, arr_yy):
    """Once events are partitioned off, finds best s/b in that category.    
    Arguments:
        arr_vbf {np.array} -- VBF category events from VBF signal
        arr_yy {[type]} -- VBF category events from yy background
    """

    best_significance = namedtuple("optCategory", ["value", "mjj", "jj_deta", "m_yyjj"])

    # Set new bounds just for this run
    try:
        if custom_mjj:
            mjj_bounds = custom_mjj
        else:
            mjj_bounds = (
                min([np.amin(arr_vbf["mjj"]), np.amin(arr_ggf["mjj"])]),
                max([np.amax(arr_vbf["mjj"]), np.amax(arr_ggf["mjj"])]),
            )
        if custom_deta:
            deta_bounds = custom_deta
        else:
            deta_bounds = (
                min([np.amin(arr_vbf["jj_deta"]), np.amin(arr_ggf["jj_deta"])]),
                max([np.amax(arr_vbf["jj_deta"]), np.amax(arr_ggf["jj_deta"])]),
            )
        if custom_myyjj:
            m_yyjj_bounds = custom_myyjj
        else:
            m_yyjj_bounds = (
                min([np.amin(arr_vbf["m_yyjj"]), np.amin(arr_ggf["m_yyjj"])]),
                max([np.amax(arr_vbf["m_yyjj"]), np.amax(arr_ggf["m_yyjj"])]),
            )
    except ValueError:
        best_significance.value = 0
        return best_significance

    # apriori apply dR Cuts at 2.5
    dr_vbf = arr_vbf[(arr_vbf["dR_jj"] < 2.5) * (arr_vbf["dR_yy"] < 2.5)]
    dr_yy = arr_vbf[(arr_vbf["dR_jj"] < 2.5) * (arr_vbf["dR_yy"] < 2.5)]

    best_significance.value = -1
    # Do 3d optimization on myyjj, jj_deta, mjj
    for mjj_cut in np.linspace(start=0, stop=mjj_bounds[1], num=n_mjj_splits):
        for deta_cut in np.linspace(start=0, stop=deta_bounds[1], num=n_deta_splits):
            for m_yyjj_cut in np.linspace(
                start=0, stop=deta_bounds[1], num=n_deta_splits
            ):

                passcut_array_vbf = apply_cut(dr_vbf, mjj_cut, deta_cut, m_yyjj_cut)[0]
                passcut_array_yy = apply_cut(dr_yy, mjj_cut, deta_cut, m_yyjj_cut)[0]

                this_cut_signficance = asimov_significance(
                    passcut_array_vbf["eventweight"].sum(),
                    passcut_array_yy["eventweight"].sum(),
                )
                if this_cut_signficance > best_significance.value:
                    best_significance.value = this_cut_signficance
                    best_significance.mjj = mjj_cut
                    best_significance.jj_deta = deta_cut
                    best_significance.m_yyjj = m_yyjj_cut

    return best_significance


def full_categorized_significance(
    arr_ggf, arr_yy_ggfcat, arr_vbf=None, arr_yy_vbfcat=None
):
    """Finds full significance for all categories. Takes into account ggF cut-based optimization    
    Arguments:
        arr_ggf {np.array} -- signal ggF array failing VBF selection
        arr_yy_ggfcat {np.array} -- yy continuum array failing VBF selection    
    Keyword Arguments (done as kwargs to evaluate without VBF selection):
        arr_vbf {np.array} -- signal VBF array passing VBF selection (default: {None})
        arr_yy_vbfcat {np.array} -- yy continuum array failing VBF selection (default: {None})
    Returns:
        [float] -- significance of all categories (all ggF + VBF if passed) added in quadrature
    """

    # Find ggF significance for cuts-based optimization for each category
    eachCategory_significance = []
    for cat in range(1, 5):
        inCategory_ggf = arr_ggf[arr_ggf["cutCategory"] == cat]
        inCategory_yy = arr_yy[arr_yy["cutCategory"] == cat]
        eachCategory_significance.append(
            asimov_significance(
                inCategory_ggf["eventweight"].sum(), inCategory_yy["eventweight"].sum()
            )
        )
    # If VBF category is passed, add it
    if arr_vbf is not None and arr_yy_vbfcat is not None:
        vbfCat_significance = find_optimized_vbf_category(
            arr_vbf, arr_yy_vbfcat
        )  # returns significance object with cuts
        eachCategory_significance.append(vbfCat_significance.value)

    cuts_sig_squared = [x ** 2 for x in eachCategory_significance]
    total_sig = sqrt(sum(cuts_sig_squared))

    return total_sig


def plot_all_significances(significance_list, mjj_list, deta_list):
    iteration = range(0, len(significance_list))
    fig = plt.plot(iteration, significance_list, ".", markersize=2)
    plt.xlabel("Iteration")
    plt.ylabel("Significance")
    plt.savefig("significances.png")
    plt.close()

    plt.figure()
    ax = plt.axes()
    tpc = ax.tripcolor(
        mjj_list, deta_list, significance_list, cmap="coolwarm", shading="gouraud"
    )
    ax.set_xlim(0, 1200)
    ax.set_ylim(0, 10)
    plt.colorbar(tpc)
    plt.xlabel("$m_{jj}$ cut")
    plt.ylabel("$\Delta\eta$ cut")
    plt.tight_layout()
    plt.savefig("significance3d.pdf")


def scan_vals(arr_vbf, arr_ggf, arr_yy):
    # Drop invalid elements
    valid_vbf = arr_vbf[
        arr_vbf["jj_deta"] >= 0
    ]  # Works as proxy, -99 for invalid events
    valid_ggf = arr_ggf[
        arr_ggf["jj_deta"] >= 0
    ]  # Works as proxy, -99 for invalid events

    # TODO: Generalize this if adding more variables
    if custom_mjj:
        mjj_bounds = custom_mjj
    else:
        mjj_bounds = (
            min([np.amin(valid_vbf["mjj"]), np.amin(valid_ggf["mjj"])]),
            max([np.amax(valid_vbf["mjj"]), np.amax(valid_ggf["mjj"])]),
        )
    if custom_deta:
        deta_bounds = custom_deta
    else:
        deta_bounds = (
            min([np.amin(valid_vbf["jj_deta"]), np.amin(valid_ggf["jj_deta"])]),
            max([np.amax(valid_vbf["jj_deta"]), np.amax(valid_ggf["jj_deta"])]),
        )

    best_significance = namedtuple("SigPoint", ["value", "mjj", "jj_deta"])
    best_significance.value = -1

    print("Starting scan. Bounds: ")
    print("mjj from %.2f to %.2f" % (mjj_bounds[0], mjj_bounds[1]))
    print("deta from %.2f to %.2f" % (deta_bounds[0], deta_bounds[1]))
    pbar = tqdm(total=n_mjj_splits * n_deta_splits)
    all_significance_values = []
    deta_vals = []
    mjj_vals = []
    for mjj_cut in np.linspace(start=0, stop=mjj_bounds[1], num=n_mjj_splits):
        for deta_cut in np.linspace(start=0, stop=deta_bounds[1], num=n_deta_splits):
            # Switch to "valid" - might save time?
            passcut_array_vbf, failcut_array_vbf = apply_cut(arr_vbf, mjj_cut, deta_cut)
            passcut_array_ggf, failcut_array_ggf = apply_cut(arr_ggf, mjj_cut, deta_cut)
            passcut_array_yy, failcut_array_yy = apply_cut(arr_yy, mjj_cut, deta_cut)
            this_cut_signficance = full_categorized_significance(
                failcut_array_ggf, failcut_array_yy, passcut_array_vbf, passcut_array_yy
            )
            if this_cut_signficance > best_significance.value:
                best_significance.value = this_cut_signficance
                best_significance.mjj = mjj_cut
                best_significance.jj_deta = deta_cut
            all_significance_values.append(this_cut_signficance)
            deta_vals.append(deta_cut)
            mjj_vals.append(mjj_cut)
            pbar.update(1)

    plot_all_significances(all_significance_values, mjj_vals, deta_vals)
    return best_significance





if __name__ == "__main__":

    # Load samples
    # VBF (only mc16a now TODO: add mc16d)
    arr_vbf = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])

    # GGF
    arr_ggf = join_signal_regions([ggf_mc16a, ggf_mc16d, ggf_mc16e])

    # Sherpa yy continuum
    arr_yy = join_signal_regions([yy_mc16a, yy_mc16d])

    # Renormalize to data set
    arr_vbf = rescale_eventweight(arr_vbf, 1)
    arr_ggf = rescale_eventweight(arr_ggf, 1)
    arr_yy = rescale_eventweight(arr_yy, 138972.5 / (36215.0 + 44307.4))

    # Print some safety checks
    print("Original Values - ")
    print("Total VBF yield: %.4f" % arr_vbf["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf["eventweight"].sum())
    print("Total Continuum Bkg yield: %.4f" % arr_yy["eventweight"].sum())

    # Find ggF-only Cuts significance
    original_significance = full_categorized_significance(arr_ggf, arr_yy)
    print(
        "Significance of ggF only, Cuts-based signal region: %.3f"
        % original_significance
    )

    # Do the Value Scanning
    optimization = scan_vals(arr_vbf, arr_ggf, arr_yy)

    # Print out Results
    print("\nOptimization Finished - Results:")
    print("Optimized Significance %.3f" % optimization.value)
    print(
        "Achieved at dEta > %.3f and mjj > %.3f"
        % (optimization.jj_deta, optimization.mjj)
    )
