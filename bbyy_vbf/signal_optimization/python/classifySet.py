from xgboost import XGBClassifier, plot_importance, DMatrix, plot_tree
import xgboost as xgb
from numpy.lib.recfunctions import append_fields, drop_fields
import numpy as np
import matplotlib.pyplot as plt
import argparse
import pandas as pd
import pickle
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
from collections import OrderedDict
from utils import *
import logging
logging.basicConfig(level=logging.ERROR)

# Neural Network packages
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
#from keras.utils.vis_utils import plot_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping
from keras.utils import normalize, to_categorical

from keras.engine.input_layer import Input
from keras.wrappers.scikit_learn import KerasClassifier
from time import time
from tensorflow.python.framework import ops
from tensorflow.keras.models import load_model
from keras.metrics import categorical_accuracy

from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline

import h5py

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--depth", help="Depth", type=int)
parser.add_argument("-t", "--nTrees", help="Number of trees", type=int)
parser.add_argument("--jetRequirement", help="Require 4 jets", action="store_true")
parser.add_argument("--optimizeDepth", help="Optimize depth parameter", action="store_true")
parser.add_argument("--optimizeTrees", help="Optimize trees parameter", action="store_true")
parser.add_argument("--optimizeGamma", help="Optimize gamma parameter", action="store_true")
parser.add_argument("--noWeight", help="Don't use Weights", action="store_true")
# These options exist for stepwise optimization
parser.add_argument("--stepwiseVariablePop", help="Variable index to pop for N-1 test", type=int)
parser.add_argument("--outputDir", help="output directory", type=str, default="saved_classifiers/")
# Additions for NN 
parser.add_argument("-m","--model",help="Model type to run (BDT or NN)", default="BDT")
args = parser.parse_args()

names=[
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    "ptyy_myy_ratio",
    "pt_balance",
    "pt_qqbb",
    "pt_qq_bb_ratio",
    #"bdt_score"
]
if args.stepwiseVariablePop:
    print("Omitting Variable: {0}".format(names.pop(args.stepwiseVariablePop)))

output_directory = "run/classifier_evaluation/"


# BDT training functions/class
### ------------------------------------------------------------------------------------------
def create_classifier(X_train, y_train, X_test, y_test, w_train=None, w_test=None, multiclass=False, names=names, bdt_kwargdict={}, task_kwargdict={}):
    """ Classifier explicitly created and trained here. Hyperparameters set withing this function.
    X_train/test are the features, y_train/test are the classification labels
    w's are weights. If none given, weights are not used (find this to be better for training)
    names -> names of features
    bdt_kwargdict -> Hyperparameters affecting the BDT that you'd like to manually override (e.g. depth)
    task_kwargdict -> Hyperparameters affecting the training task that you'd like to manually override (e.g. early_stopping_rounds)
    """
    # Make Classifier
    param = {}
    # BDT parameters
    bdt_param={}
    bdt_param['learning_rate']      = 0.1 # learning rate
    bdt_param['max_depth']        = 3  # maximum depth of a tree
    bdt_param['subsample']        = 0.8 # fraction of events to train tree on
    bdt_param['colsample_bytree'] = 0.8 # fraction of features to train tree on
    bdt_param['objective']   = 'multi:softmax' # objective function

    bdt_param['n_estimators']   = 200 
    bdt_param["feature_names"] = names

    # Learning task parameters
    task_param={}
    task_param['eval_metric'] = ['mlogloss', 'merror']           # evaluation metric for cross validation
    task_param['early_stopping_rounds'] = 30
    task_param['eval_set'] = [(X_train, y_train), (X_test,y_test)]
    if w_test is not None and w_train is not None:
        task_param['sample_weight'] = np.abs(w_train)
        task_param['sample_weight_eval_set'] = [np.abs(w_train), np.abs(w_test)]

    # Manual override default values
    bdt_param.update(bdt_kwargdict)
    task_param.update(task_kwargdict)

    # Classifier method
    classifier = XGBClassifier(**bdt_param)
    
    #pdb.set_trace()
    classifier.fit(X_train, y_train, verbose=False, **task_param)
    return classifier

def create_nn(X_train, y_train, X_test, y_test, w_train=None, w_test=None, names=names, task_kwargdict={}, nn_kwargdict={}):

    # Construct the model
    n_features = X_train.shape[1]
    y_binary_train = to_categorical(y_train)
    y_binary_test = to_categorical(y_test)
    print("Number of features: {0}".format(n_features))

    model = Sequential()
    model.add(Dense(round(n_features*1.25), input_dim=n_features, activation="relu")) # Geometric size decrease
    model.add(Dropout(0.2))
    model.add(Dense(round(n_features*.7), activation="relu")) # Geometric size decrease
    model.add(Dropout(0.2))
    model.add(Dense(4, activation="softmax")) # Output = number of classes
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=[categorical_accuracy])
    model.summary()

    # Add some Callbacks
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
    early_stopping = EarlyStopping(
        monitor="val_loss",
        min_delta=0,
        patience=10,
        mode="auto",
        restore_best_weights=True
    )

    # Fit model
    if w_train is not None and w_test is not None:
        print("training with using weights")
        history = model.fit(
            X_train,
            y_binary_train,
            sample_weight=w_train,
            epochs=1000, # Set high for early stopping
            batch_size=64,
            validation_data=(X_test, y_binary_test, w_test),
            callbacks=[tensorboard, early_stopping]
        )
    else:
        print("training without using weights")
        history = model.fit(
            X_train,
            y_binary_train,
            epochs=1000, # Set high for early stopping
            batch_size=64,
            validation_data=(X_test, y_binary_test),
            callbacks=[tensorboard, early_stopping]
        )


    # Plot training
    plt.plot(history.history['categorical_accuracy'])
    plt.plot(history.history['val_categorical_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('run/NN/accuracy.pdf')
    plt.close()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('run/NN/loss.pdf')
    plt.close()



    return model


class ClassifySet:
    """ Object to house all the samples for passing to classifier, includes routines for joining signal and background oncce set
    Also includes method for training and various optimizations
    """

    def __init__(self):
        """Initialized to None, then set by sample"""
        self.X_train, self.X_test, self.y_train, self.y_test, self.w_train, self.w_test = None, None, None, None, None, None
        self.X_train, self.X_test, self.y_train, self.y_test, self.w_train, self.w_test = None, None, None, None, None, None


    def add_sample(self, X_train, X_test, thisClass, w_train=None, w_test=None):
        """Routine to add a single sample to X, y, w matrices"""
        self.X_train = np.concatenate([self.X_train, X_train]) if self.X_train is not None else X_train
        self.X_test = np.concatenate([self.X_test, X_test]) if self.X_test is not None else X_test
        self.y_train = np.concatenate([self.y_train,  np.ones(X_train.shape[0]) * thisClass]) if self.y_train is not None else  np.ones(X_train.shape[0]) * thisClass
        self.y_test =  np.concatenate([self.y_test,  np.ones(X_test.shape[0]) * thisClass]) if self.y_test is not None else  np.ones(X_test.shape[0]) * thisClass
        self.w_train = np.concatenate([self.w_train, w_train]) if self.w_train is not None else w_train
        self.w_test = np.concatenate([self.w_test, w_test]) if self.w_test is not None else w_test
            
    def rescale_features_to_unity(self):
        """ Option to rescale to bounded 0-1"""
        scaler = MinMaxScaler()
        all_X = np.concatenate((self.X_train, self.X_test))
        scaler.fit(all_X)
        self.X_train = scaler.transform(self.X_train)
        self.X_test = scaler.transform(self.X_test)
        return scaler

    def train_bdt(self, bdt_kwargdict=None):
        """Trains the BDT        
        Keyword Arguments:
            bdt_kwargdict {dict} -- BDT hyperparameter override (default: {None})        
        Returns:
            XGBclassifier trained
        """
        print("Training parameters: {0}".format(bdt_kwargdict))
        if self.w_train is not None and self.w_test is not None:
            # TODO : add task_kwargdict
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, multiclass=True, bdt_kwargdict=bdt_kwargdict)
        else:
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, multiclass=True, names=names, bdt_kwargdict=bdt_kwargdict)

    def train_nn(self, nn_kwargdict=None):
        """Trains the NN        
        Keyword Arguments:
            nn_kwargdict {dict} -- NN hyperparameter override (default: {None})        
        Returns:
            keras NN trained
        """
        if nn_kwargdict:
            print("Training parameters: {0}".format(nn_kwargdict))
        
        self.rescale_features_to_unity()

        if self.w_train is not None and self.w_test is not None:
            return create_nn(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, nn_kwargdict=nn_kwargdict)
        else:
            return create_nn(self.X_train, self.y_train, self.X_test, self.y_test, names=names, nn_kwargdict=nn_kwargdict)

    # HP Optimization functions
    ### --------------------------------------------
    def optimize_depth(self, max_depth=20):
        """Routine to optimize tree depth. Starts at 0 and works up to max_depth
        Also generates a plot showing accuracy for train and test at each step
        Returns:
            best depth (int) - best depth based on testing sample
        """
        print("Optimizing Depth")
        depth_scores_train=[]
        depth_scores_test=[]
        depth_range = np.arange(1, max_depth)
        for depth in depth_range:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict={"max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, bdt_kwargdict={"max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            depth_scores_train.append(accuracy_score(ypred_train,self.y_train))
            depth_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Depth {0} completed".format(depth))

        plt.plot(depth_range,depth_scores_train)
        plt.plot(depth_range,depth_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('Depth')
        plt.ylabel('Accuracy')
        plt.xticks(depth_range)
        best_depth = np.argmax(depth_scores_test)+1 #offset by one because rage starts at 1
        print("Best depth: {0}".format(best_depth))
        return best_depth


    def optimize_trees(self, to_test=[25, 50, 100, 200, 300], depth=15):
        """Routine to optimize nTrees        
        Keyword Arguments:
            to_test {list} -- list of values to test for nTrees (default: {[25, 50, 100, 200, 300]})
            depth {int} -- depth to use (default: {15})
        Returns:
            [int] -- best number nTrees based on testing accuracy. Also plots as you proceed.
        """
        print("Optimizing Trees")
        tree_scores_train=[]
        tree_scores_test=[]
        for ntrees in to_test:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, multiclass=True, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, multiclass=True, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            tree_scores_train.append(accuracy_score(ypred_train,self.y_train))
            tree_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Tested at {0} trees completed".format(ntrees))

        plt.plot(to_test, tree_scores_train)
        plt.plot(to_test, tree_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('nTrees')
        plt.ylabel('Accuracy')
        best_trees = to_test[np.argmax(tree_scores_test)] #offset by one because rage starts at 1
        print("Best trees: {0}".format(best_trees))
        return best_trees


    def optimize_gamma(self, to_test=[0, 1, 2, 5, 10, 15, 20], depth=15):
        """Routine to optimize gamma parameter
        # TODO: merge this with other optimization routines        
        Keyword Arguments:
            to_test {list} -- values to test (default: {[0, 1, 2, 5, 10, 15, 20]})
            depth {int} -- depth to use (default: {15})        
        Returns:
            [int] -- Best value for gamma (have found it to be 0 in most cases)
        """

        gamma_scores_train=[]
        gamma_scores_test=[]
        for igamma in to_test:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, multiclass=True, names=names, bdt_kwargdict={"gamma":igamma, "max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, multiclass=True, names=names, bdt_kwargdict={"gamma":igamma, "max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            gamma_scores_train.append(accuracy_score(ypred_train,self.y_train))
            gamma_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Tested at gamma {0} completed".format(igamma))

        plt.plot(to_test, gamma_scores_train)
        plt.plot(to_test, gamma_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('Gamma')
        plt.ylabel('Accuracy')
        best_gamma = to_test[np.argmax(gamma_scores_test)] #offset by one because rage starts at 1
        print("Best gamma: {0}".format(best_gamma))
        return best_gamma 




def load_classifier_set():

    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    # Setup VBF samples, split into training and testing
    #vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    vbf_files = [sourcedir + fname for fname in  ["bdtscore_vbf_mc16a.npy","bdtscore_vbf_mc16d.npy","bdtscore_vbf_mc16e.npy"]]
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples(vbf_files, ggf_preselection=True, test=False)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)

    ## Rescale weights for train/test split
    arr_vbf_train = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train) 
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    if args.jetRequirement:
        arr_vbf_train = arr_vbf_train[arr_vbf_train["jj_deta"] > 0]
        arr_vbf_test = arr_vbf_test[arr_vbf_test["jj_deta"] > 0]

    ## Reshape to xgb inputs ([event,features] dimensional matrix)
    X_train_vbf = make_clf_compliant(arr_vbf_train, names=names)
    X_test_vbf = make_clf_compliant(arr_vbf_test, names=names)
    ## Set individual eventweights to be the average of the whole (found helps)
    logging.info("Original train VBF weight = {0:.3}".format(arr_vbf_train["eventweight"].sum()))
    logging.info("Original test VBF weight = {0:.3}".format(arr_vbf_test["eventweight"].sum()))
    w_train_vbf = np.ones(arr_vbf_train.shape[0]) * arr_vbf_train["eventweight"].sum() / arr_vbf_train["eventweight"].shape[0]
    w_test_vbf = np.ones(arr_vbf_test.shape[0]) * arr_vbf_test["eventweight"].sum() / arr_vbf_test["eventweight"].shape[0]
    logging.info("Rescaled to average train VBF weight = {0:.3}".format(w_train_vbf.sum()))
    logging.info("Original test VBF weight = {0:.3}".format(w_test_vbf.sum()))

    ## ggF samples - Same routine as above
    #ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
    ggf_files = [sourcedir + fname for fname in  ["bdtscore_ggf_mc16a.npy","bdtscore_ggf_mc16d.npy","bdtscore_ggf_mc16e.npy"]]
    arr_ggf_train, ggf_eventweight_scalefactor_train = load_samples(ggf_files, ggf_preselection=True, test=False)
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)        
    arr_ggf_train = rescale_eventweight(arr_ggf_train, ggf_eventweight_scalefactor_train)
    arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)  
    if args.jetRequirement:
        arr_ggf_train = arr_ggf_train[arr_ggf_train["jj_deta"] > 0]
        arr_ggf_test = arr_ggf_test[arr_ggf_test["jj_deta"] > 0]

    w_train_ggf = np.ones(arr_ggf_train.shape[0]) * arr_ggf_train["eventweight"].sum() / arr_ggf_train["eventweight"].shape[0]
    w_test_ggf = np.ones(arr_ggf_test.shape[0]) * arr_ggf_test["eventweight"].sum() / arr_ggf_test["eventweight"].shape[0]
    X_train_ggf = make_clf_compliant(arr_ggf_train, names=names)
    X_test_ggf = make_clf_compliant(arr_ggf_test, names=names)

    ## yy samples - Same routine as above
    #yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
    yy_files = [sourcedir + fname for fname in  ["bdtscore_yy_mc16a.npy","bdtscore_yy_mc16d.npy","bdtscore_yy_mc16e.npy"]]
    arr_yy_train, yy_eventweight_scalefactor_train = load_samples(yy_files, ggf_preselection=True, test=False, myyCut=False)
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)    
    arr_yy_train = rescale_eventweight(arr_yy_train, yy_eventweight_scalefactor_train)
    arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)      
    if args.jetRequirement:
        arr_yy_train = arr_yy_train[arr_yy_train["jj_deta"] > 0]
        arr_yy_test = arr_yy_test[arr_yy_test["jj_deta"] > 0]

    w_train_yy = np.ones(arr_yy_train.shape[0]) * arr_yy_train["eventweight"].sum() / arr_yy_train["eventweight"].shape[0]
    w_test_yy = np.ones(arr_yy_test.shape[0]) * arr_yy_test["eventweight"].sum() / arr_yy_test["eventweight"].shape[0]
    X_train_yy = make_clf_compliant(arr_yy_train, names=names)
    X_test_yy = make_clf_compliant(arr_yy_test, names=names)

    # Single Higgs - Same routine as above
    #tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
    tth_files = [sourcedir + fname for fname in  ["bdtscore_TTH_mc16a.npy","bdtscore_TTH_mc16d.npy","bdtscore_TTH_mc16e.npy"]]
    arr_tth_train, tth_eventweight_scalefactor_train = load_samples(tth_files, ggf_preselection=True, test=False)
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    arr_tth_train = rescale_eventweight(arr_tth_train, tth_eventweight_scalefactor_train)
    arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)     
    if args.jetRequirement:
        arr_tth_train = arr_tth_train[arr_tth_train["jj_deta"] > 0]
        arr_tth_test = arr_tth_test[arr_tth_test["jj_deta"] > 0]        
    w_train_tth = ( np.ones(arr_tth_train.shape[0]) * arr_tth_train["eventweight"].sum() / arr_tth_train["eventweight"].shape[0] )
    w_test_tth = ( np.ones(arr_tth_test.shape[0]) * arr_tth_test["eventweight"].sum() / arr_tth_test["eventweight"].shape[0] )
    X_train_tth = make_clf_compliant(arr_tth_train, names=names)
    X_test_tth = make_clf_compliant(arr_tth_test, names=names)

    # Balance weights between signal and each background sample, both train and test
    w_train_vbf = w_train_vbf * (w_train_vbf.shape[0] / w_train_vbf.sum())
    w_train_ggf = w_train_ggf * (w_train_vbf.shape[0] / w_train_ggf.sum())
    w_train_yy = w_train_yy * (w_train_vbf.shape[0] / w_train_yy.sum())
    w_train_tth = w_train_tth * (w_train_vbf.shape[0] / w_train_tth.sum())

    w_test_vbf = w_test_vbf * (w_test_vbf.shape[0] / w_test_vbf.sum())
    w_test_ggf = w_test_ggf * (w_test_vbf.shape[0] / w_test_ggf.sum())
    w_test_yy = w_test_yy * (w_test_vbf.shape[0] / w_test_yy.sum())
    w_test_tth = w_test_tth * (w_test_vbf.shape[0] / w_test_tth.sum())

    # Create the ClassifySet object and load all the samples into it
    classifier_set = ClassifySet()
    if args.noWeight:
        classifier_set.add_sample(X_train_vbf, X_test_vbf, thisClass=0)
        classifier_set.add_sample(X_train_ggf, X_test_ggf, thisClass=1)
        classifier_set.add_sample(X_train_yy, X_test_yy, thisClass=2)
        classifier_set.add_sample(X_train_tth, X_test_tth, thisClass=3)
    else:
        classifier_set.add_sample(X_train_vbf, X_test_vbf, thisClass=0, w_train=w_train_vbf, w_test=w_test_vbf)
        classifier_set.add_sample(X_train_ggf, X_test_ggf, thisClass=1, w_train=w_train_ggf, w_test=w_test_ggf)
        classifier_set.add_sample(X_train_yy, X_test_yy, thisClass=2, w_train=w_train_yy, w_test=w_test_yy)
        classifier_set.add_sample(X_train_tth, X_test_tth, thisClass=3, w_train=w_train_tth, w_test=w_test_tth)

    return classifier_set


