from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
from tqdm import tqdm
import matplotlib.pyplot as plt
import pickle
from xgboost import XGBClassifier, DMatrix
from sklearn.preprocessing import StandardScaler

from utils import *

import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-z", "--zeroTag", help="Do Zero Tag", action="store_true")
args = parser.parse_args()

step_size = 0.03
"""
names = [
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "planarFlow"
]
"""
names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
        "aplanority",
        "aplanarity",
        "sphericity",
        "spherocity",
        "sphericityT",
        "planarity",
        "circularity",
        "planarFlow",
        "ht",
        "mjjb1",
        "mjjb2",
        "mjj_w1",
        "mjj_w2",
    ],

csv_file = open("optimization.csv","w+")
csv_file.write("Signal_BDT,Bkg_BDT,Significance\n")

def full_categorized_significance(
    thisVBFcategorized_sig_array,
    thisGGFcategorized_sig_array,
    thisVBFcategorized_bkg_array,
    thisGGFcategorized_bkg_array,
    sig_clf_cut,
):
    """Finds full significance for all categories. Takes into account ggF cut-based optimization    
    Arguments:
        arr_ggf {np.array} -- signal ggF array failing VBF selection
        arr_yy_ggfcat {np.array} -- yy continuum array failing VBF selection    
    Keyword Arguments (done as kwargs to evaluate without VBF selection):
        arr_vbf {np.array} -- signal VBF array passing VBF selection (default: {None})
        arr_yy_vbfcat {np.array} -- yy continuum array failing VBF selection (default: {None})
    Returns:
        [float] -- significance of all categories (all ggF + VBF if passed) added in quadrature
    TODO : Fix comment string here
    """

    # Evaluate all VBF by classifier 2
    to_predict_sig = make_clf_compliant(thisVBFcategorized_sig_array)
    to_predict_bkg = make_clf_compliant(thisVBFcategorized_bkg_array)
    
    #bkg_scaler = StandardScaler()
    #try:    
    #    bkg_scaler.fit(np.concatenate([to_predict_sig,to_predict_bkg]))    
    #except ValueError:
    #    return 0, 0
    #to_predict_sig = bkg_scaler.transform(to_predict_sig)
    #to_predict_bkg = bkg_scaler.transform(to_predict_bkg)

    if to_predict_sig is None or to_predict_bkg is None:
        return 0, 0 
    predict_sig = vbf_yy_clf.predict_proba(make_clf_compliant(thisVBFcategorized_sig_array))[:,1]
    predict_bkg = vbf_yy_clf.predict_proba(make_clf_compliant(thisVBFcategorized_bkg_array))[:,1]

    best_significance = 0
    bkg_clf_cut = None
    scanrange = np.arange(0, 1, step_size)
    iSignificance = []
    for probability_cut in scanrange:
        logfile.write("     Cutting background BDT at %.2f \n" % probability_cut)

        sig_passing_cut = thisVBFcategorized_sig_array[predict_sig > probability_cut]
        bkg_passing_cut = thisVBFcategorized_bkg_array[predict_bkg > probability_cut]

        this_cut_vbf_category_significance = asimov_significance(
            sig_passing_cut["eventweight"].sum(), bkg_passing_cut["eventweight"].sum()
        )

        # Reclaim ggF that fail this categorization
        eachCategory_significance = []
        sig_failing_cut = thisVBFcategorized_sig_array[
            np.logical_not(predict_sig > probability_cut)
        ]
        bkg_failing_cut = thisVBFcategorized_bkg_array[
            np.logical_not(predict_bkg > probability_cut)
        ]

        # Build whole ggF sample
        all_ggf_categorized_sig = np.concatenate(
            [thisGGFcategorized_sig_array, sig_failing_cut]
        )
        all_ggf_categorized_bkg = np.concatenate(
            [thisGGFcategorized_bkg_array, bkg_failing_cut]
        )

        # Find ggF significance for cuts-based optimization for each category
        for cat in range(1, 5):
            inCategory_sig = all_ggf_categorized_sig[
                all_ggf_categorized_sig["cutCategory"] == cat
            ]
            inCategory_bkg = all_ggf_categorized_bkg[
                all_ggf_categorized_bkg["cutCategory"] == cat
            ]
            eachCategory_significance.append(
                asimov_significance(
                    inCategory_sig["eventweight"].sum(),
                    inCategory_bkg["eventweight"].sum(),
                )
            )

        if this_cut_vbf_category_significance != 0:
            eachCategory_significance.append(this_cut_vbf_category_significance)
        if len(eachCategory_significance) > 0:
            this_cut_vbf_category_significance = sqrt(
                sum([x ** 2 for x in eachCategory_significance])
            )
        else:
            this_cut_vbf_category_significance = 0
        """
        logfile.write(
            "        nEvents VBF: %.2f  nEvents ggF:  %.2f    total:  %.2f\n"
            % (
                sig_passing_cut["eventweight"].sum(),
                all_ggf_categorized_events["eventweight"].sum(),
                sig_passing_cut["eventweight"].sum()
                + all_ggf_categorized_events["eventweight"].sum(),
            )
        )
        logfile.write(
            "        bkg VBF: %.2f  bkg ggF:  %.2f    total:  %.2f\n"
            % (
                bkg_passing_cut["eventweight"].sum(),
                all_ggf_categorized_bkg["eventweight"].sum(),
                bkg_passing_cut["eventweight"].sum()
                + all_ggf_categorized_bkg["eventweight"].sum(),
            )
        )
        """

        csv_file.write("{0},{1},{2}".format(sig_clf_cut,probability_cut,this_cut_vbf_category_significance))
        csv_file.write("     bkgVBF entries: {0}\n".format(bkg_passing_cut.shape[0]))

        iSignificance.append(this_cut_vbf_category_significance)
        if args.zeroTag:
            threshold = 10000
        else: threshold = 0
        if this_cut_vbf_category_significance > best_significance and bkg_passing_cut.shape[0] > 10000:
            best_significance = this_cut_vbf_category_significance
            bkg_clf_cut = probability_cut


    return best_significance, bkg_clf_cut


def scan_vals(all_signal, all_background):


    # Create boolean arrays for what falls into VBF category.
    # Start by only considering 4 jet events. Failures of this have -99 jj_deta.
    sig_sample_bool_array = all_signal["jj_deta"] >= 0
    bkg_sample_bool_array = all_background["jj_deta"] >= 0

    # Reshape original arrays to fit into classifier
    for_clf_sig = make_clf_compliant(all_signal)
    for_clf_bkg = make_clf_compliant(all_background)
    #signal_scaler = StandardScaler()
    #signal_scaler.fit(np.concatenate([for_clf_sig,for_clf_bkg]))
    #for_clf_sig = signal_scaler.transform(for_clf_sig)
    #for_clf_bkg = signal_scaler.transform(for_clf_bkg)

    predict_sig = vbf_ggf_clf.predict_proba(for_clf_sig)[:,1]
    predict_bkg = vbf_ggf_clf.predict_proba(for_clf_bkg)[:,1]

    best_significance = namedtuple("SigPoint", ["value", "ggf_clf_cut", "bkg_clf_cut"])
    best_significance.value = -1

    scan_range = np.arange(0, 1, step_size)
    pbar = tqdm(total=len(scan_range))
    print("Starting scan. Bounds: %.2f to %.2f " % (scan_range.min(), scan_range.max()))
    all_significance_values = []

    for probability_cut in scan_range:
        # Signal Classification
        sig_pass = predict_sig > probability_cut
        signal_boolean = np.logical_and(sig_pass, sig_sample_bool_array)        
        thisVBFcategorized_sig_array = all_signal[signal_boolean]
        thisGGFcategorized_sig_array = all_signal[np.logical_not(signal_boolean)]

        # background classification
        bkg_boolean_array = bkg_sample_bool_array * predict_bkg > probability_cut
        thisVBFcategorized_bkg_array = all_background[bkg_boolean_array]
        thisGGFcategorized_bkg_array = all_background[np.logical_not(bkg_boolean_array)]

        # Find significance after applying VBF BDT
        this_cut_signficance, bkg_clf_cut = full_categorized_significance(
            thisVBFcategorized_sig_array,
            thisGGFcategorized_sig_array,
            thisVBFcategorized_bkg_array,
            thisGGFcategorized_bkg_array,
            probability_cut,
        )
        logfile.write("Cutting signal BDT at %.2f \n" % probability_cut)
        if this_cut_signficance > best_significance.value:
            best_significance.value = this_cut_signficance
            best_significance.bkg_clf_cut = bkg_clf_cut
            best_significance.ggf_clf_cut = probability_cut
        all_significance_values.append(this_cut_signficance)
        # print "Cut signal BDT at %.2f, best significance %.4f\n" %(probability_cut, this_cut_signficance)
        pbar.update(1)

    # plot_all_significances(all_significance_values, mjj_vals, deta_vals)
    return best_significance



if __name__ == "__main__":

    # Global define sample names
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
    vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
    vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

    ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
    ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
    ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

    yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
    yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
    yy_mc16e = sourcedir + "forcuts_yy_mc16e.npy"

    ntni1516 = sourcedir + "forcuts_ntni_mc16a.npy"
    ntni17 = sourcedir + "forcuts_ntni_mc16d.npy"
    ntni18 = sourcedir + "forcuts_ntni_mc16e.npy"

    tth_mc16a = sourcedir + "forcuts_TTH_mc16a.npy"
    tth_mc16d = sourcedir + "forcuts_TTH_mc16d.npy"
    tth_mc16e = sourcedir + "forcuts_TTH_mc16e.npy"

    ggh_mc16a = sourcedir + "forcuts_GGH_mc16a.npy"
    ggh_mc16d = sourcedir + "forcuts_GGH_mc16d.npy"
    ggh_mc16e = sourcedir + "forcuts_GGH_mc16e.npy"

    zh_mc16a = sourcedir + "forcuts_ZH_mc16a.npy"
    zh_mc16d = sourcedir + "forcuts_ZH_mc16d.npy"
    zh_mc16e = sourcedir + "forcuts_ZH_mc16e.npy"

    vbfh_mc16a = sourcedir + "forcuts_VBFH_mc16a.npy"
    vbfh_mc16d = sourcedir + "forcuts_VBFH_mc16d.npy"
    vbfh_mc16e = sourcedir + "forcuts_VBFH_mc16e.npy"

    # Load BDTs
    vbf_ggf_classifier_name = "saved_classifiers/ggf_xgb.dat"
    vbf_yy_classifier_name = "saved_classifiers/yy_xgb.dat"

    vbf_ggf_clf = pickle.load(open(vbf_ggf_classifier_name, "rb"))
    vbf_yy_clf = pickle.load(open(vbf_yy_classifier_name, "rb"))
    logfile = open("optimization.log", "w+")

    clf1_vals = []
    clf2_vals = []
    significance_vals = []

    # Load samples
    # VBF
    arr_vbf, vbf_eventweight_scalefactor = load_samples([vbf_mc16a, vbf_mc16d, vbf_mc16e], ggf_preselection=True)

    # GGF
    arr_ggf, ggf_eventweight_scalefactor = load_samples([ggf_mc16a, ggf_mc16d, ggf_mc16e], ggf_preselection=True)

    # Sherpa yy continuum
    arr_yy, yy_eventweight_scalefactor = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], myyCut=False, ggf_preselection=True)

    # NTNI photons
    arr_ntni, ntni_eventweight_scalefactor = load_samples([ntni1516, ntni17, ntni18], myyCut=False, ggf_preselection=True)

    # Single Higgs
    arr_tth, tth_eventweight_scalefactor = load_samples([tth_mc16a, tth_mc16d, tth_mc16e], ggf_preselection=True)
    arr_ggh, ggh_eventweight_scalefactor = load_samples([ggh_mc16a, ggh_mc16d, ggh_mc16e], ggf_preselection=True)
    arr_zh, zh_eventweight_scalefactor = load_samples([zh_mc16a, zh_mc16d, zh_mc16e], ggf_preselection=True)
    arr_vbfh, vbfh_eventweight_scalefactor = load_samples([vbfh_mc16a, vbfh_mc16d, vbfh_mc16e], ggf_preselection=True)

    # Get background weight and do transformations for methods to improve bkg stats
    arr_yy = rescale_eventweight(arr_yy, yy_eventweight_scalefactor)
    yy_scaled_weight = arr_yy["eventweight"].sum()

    # 0-tag data
    if args.zeroTag:
        arr_yy0tag, yy0tag_eventweight_scalefactor = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], ggf_preselection=False, myyCut=False, tagReq=0)
        arr_yy0tag = rand_sample_tagbin(model_array=arr_yy, replace_array=arr_yy0tag, plot=True)
        categories = reconstruct_categories(arr_yy0tag, btagging=False)
        arr_yy0tag["cutCategory"] = categories
        arr_yy0tag = arr_yy0tag[arr_yy0tag["cutCategory"] > 0]

        yy_tag_reweight =  yy_scaled_weight / arr_yy0tag["eventweight"].sum()
        arr_yy0tag = rescale_eventweight(arr_yy0tag,  yy_tag_reweight)
        arr_yy = rebalance_categories(arr_yy, arr_yy0tag)

    # Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    total_continuum_yield = yy_scaled_weight / 0.84
    ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni["eventweight"].sum()

    # Renormalize the rest to data set
    arr_vbf = rescale_eventweight(arr_vbf, vbf_eventweight_scalefactor)
    arr_ggf = rescale_eventweight(arr_ggf, ggf_eventweight_scalefactor)
    arr_ntni = rescale_eventweight(arr_ntni, ntni_fractional_reweight)
    arr_tth = rescale_eventweight(arr_tth, tth_eventweight_scalefactor)
    arr_ggh = rescale_eventweight(arr_ggh, ggh_eventweight_scalefactor)
    arr_zh = rescale_eventweight(arr_zh, zh_eventweight_scalefactor)
    arr_vbfh = rescale_eventweight(arr_vbfh, vbfh_eventweight_scalefactor)

    # Make single higgs array where type doesn't matter
    arr_singleHiggs = np.concatenate([arr_tth, arr_ggh, arr_zh, arr_vbfh])

    all_signal = np.concatenate([arr_ggf, arr_vbf])
    all_signal = arr_vbf
    all_background = np.concatenate([arr_yy, arr_ntni, arr_singleHiggs])

    # Print some safety checks
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % arr_vbf["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % arr_yy["eventweight"].sum())
    print("Rescaled NTNI Bkg yield: %.4f" % arr_ntni["eventweight"].sum())
    print("Net yy+ntni yield: %.4f" % (arr_yy["eventweight"].sum()+arr_ntni["eventweight"].sum()))
    print("-----Single Higgs--------" % arr_ggf["eventweight"].sum())
    print("Total ttH yield: %.4f" % arr_tth["eventweight"].sum())
    print("Total ggH yield: %.4f" % arr_ggh["eventweight"].sum())
    print("Total ZH yield: %.4f" % arr_zh["eventweight"].sum())
    print("Total VBFH yield: %.4f" % arr_vbfh["eventweight"].sum())



    def find_in_category(arr, cat):
        array = arr[arr["cutCategory"] == cat]
        return array["eventweight"].sum()

    # Find ggF-only Cuts significance
    eachCategory_significance = []
    all_signal = np.concatenate([arr_ggf, arr_vbf])
    all_background = np.concatenate([arr_yy, arr_ntni, arr_singleHiggs])
    for cat in range(1, 5):
        inCategory_ggf = arr_ggf[arr_ggf["cutCategory"] == cat]
        inCategory_vbf = arr_vbf[arr_vbf["cutCategory"] == cat]
        inCategory_yy = arr_yy[arr_yy["cutCategory"] == cat]
        inCategory_ntni = arr_ntni[arr_ntni["cutCategory"] == cat]

        inCategory_signal = all_signal[all_signal["cutCategory"] == cat]
        inCategory_background = all_background[all_background["cutCategory"] == cat]

        eachCategory_significance.append(
            asimov_significance(
                inCategory_signal["eventweight"].sum(),
                inCategory_background["eventweight"].sum(),
            )
        )
        print(
            "Category: "
            + str(cat)
            + " significance: "
            + str(round(eachCategory_significance[cat - 1],4))
            + "  VBF events: "
            + str(round(inCategory_vbf["eventweight"].sum(),4))
            + "  ggF events: "
            + str(round(inCategory_ggf["eventweight"].sum(),4))
            + "  yy events: "
            + str(round(inCategory_yy["eventweight"].sum(),4))
            + "  ntni events: "
            + str(round(inCategory_ntni["eventweight"].sum(),4))
        )

    original_significance = sqrt(sum([x ** 2 for x in eachCategory_significance]))
    print(
        "Significance with no VBF region, Cuts-based signal region: %.3f"
        % original_significance
    )

    # Do the Value Scanning
    optimization = scan_vals(all_signal, all_background)

    # Print out Results
    print("\nOptimization Finished - Results:")
    print("Optimized Significance %.7f" % optimization.value)
    try:
        print(
            "Achieved by cutting signal BDT at: %.2f , bkg BDT at: %.2f"
            % (optimization.ggf_clf_cut, optimization.bkg_clf_cut)
        )
    except TypeError:
        print(optimization.ggf_clf_cut)
        print(optimization.bkg_clf_cut)
