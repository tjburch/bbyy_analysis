# Tyler James Burch <tyler.james.burch@cern.ch> - Northern Illinois University
# This script supercedes quick-histograms.py, for documentation reasons.
# Builds general auxiliary plots, namely:
# - Histogram plots for BDT inputs
# - Correlation Matrices

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from utils import *
import os
import pdb
import argparse
from rootpy.plotting import set_style
from collections import namedtuple, OrderedDict
from xgb_multiclassifier_analysis import get_sample_dict
set_style('ATLAS')

doVBF = True
doGGF = True
doYY = True
doNTNI = False
doTTH = True

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--histograms", help="Do Histogram Plots", action="store_true")
parser.add_argument("-c", "--corr", help="Do Correlation Plots", action="store_true")
args = parser.parse_args()
# If no arguments passed, do all 
if not any(args.__dict__.values()) == True:
    for k, v in args.__dict__.items():
        setattr(args, k, True)

# Global definitions
sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
cwd = os.getcwd()
outputdir = "{0}/run/variable_prune/".format(cwd)


Variable = namedtuple('Variable', 'name title binning')
all_variables =[]
all_variables.append(Variable(name="m_yy", title="$m_{\gamma\gamma}$ [GeV]", binning=None))
all_variables.append(Variable(name="jj_deta", title="$\Delta\eta_{jj}$", binning=np.arange(0, 10, 0.5)))
all_variables.append(Variable(name="mjj", title="$m_{jj}$", binning=np.linspace(start=0, stop=3000, num=60)))
all_variables.append(Variable(name="n_jet", title="Jet Multiplicity", binning=np.arange(1.5, 10.5, 1)))
all_variables.append(Variable(name="cutCategory", title="Cut Category", binning=np.arange(0,6)))
all_variables.append(Variable(name="m_yyjj", title="$m_{\gamma\gamma bb}$ [GeV]", binning=np.linspace(start=0, stop=1500, num=60)))
all_variables.append(Variable(name="dR_yy", title="$\Delta R_{\gamma,\gamma}$", binning=np.arange(0, 4, 0.25)))
all_variables.append(Variable(name="dR_jj", title="$\Delta R_{b,b}$", binning=np.arange(0, 5, 0.25)))
all_variables.append(Variable(name="dR_yyjj", title="$\Delta R_{\gamma\gamma,bb}$", binning=np.arange(0, 8, 0.5)))
all_variables.append(Variable(name="j1_tagbin", title="$j_{1}$ Tagbin", binning=np.arange(1, 6, 1)))
all_variables.append(Variable(name="j2_tagbin", title="$j_{2}$ Tagbin", binning=np.arange(1, 6, 1)))
all_variables.append(Variable(name="tagregion", title="tagregion", binning=np.arange(0,5,1)))
all_variables.append(Variable(name="aplanority", title="aplanarity", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="aplanarity", title="aplanority", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="sphericity", title="sphericity", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="spherocity", title="spherocity", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="sphericityT", title="sphericityT", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="planarity", title="planarity", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="circularity", title="circularity", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="planarFlow", title="planarFlow", binning=np.linspace(0,1, 35)))
all_variables.append(Variable(name="ht", title="$H_{T}$ [GeV]", binning=np.linspace(0, 1000, 35)))
all_variables.append(Variable(name="mjjb1", title="$m_{jj_{W}b_{1}}$ [GeV]", binning=np.linspace(0, 1000, 35)))
all_variables.append(Variable(name="mjjb2", title="$m_{jj_{W}b_{2}}$ [GeV]", binning=np.linspace(0, 1000, 35)))
all_variables.append(Variable(name="mjj_w1", title="$mjj_{W1}$ [GeV]", binning=np.linspace(0, 160, 35)))
all_variables.append(Variable(name="mjj_w2", title="$mjj_{W2}$ [GeV]", binning=np.linspace(0, 160, 35)))
all_variables.append(Variable(name="ptyy_myy_ratio", title="$pT_{\gamma\gamma}/m_{\gamma\gamma}$", binning=np.linspace(0,5, 35)))
all_variables.append(Variable(name="pt_qqbb", title="$pT_{jjbb}$ [GeV]", binning=np.linspace(0,800,50)))
all_variables.append(Variable(name="pt_qq_bb_ratio", title="$pT_{jj}/pT_{bb}$", binning=np.linspace(0,10,35)))
all_variables.append(Variable(name="pt_balance", title="pT Balance", binning=np.linspace(0,1,35)))




def histogram_plotting(array_dict):

    def rename(title):
        if title in ["m_yyjj","dR_jj"]: return title.replace("jj","bb")
        else: return title

    for var in all_variables:
        print("doing {0}".format(var.name))
        if var.name in ["mjjb1","mjjb2","mjj_w1","mjj_w2"]:
            array_draw = {k:arr[arr[var.name]>50] for k,arr in array_dict.items()}
        else:
            array_draw = array_dict

        color_iter, i = ["firebrick","cornflowerblue","indigo","forestgreen","darkcyan"], 0

        fig = plt.figure()
        yvals =[]
        for t, array in array_draw.items():
            y, x, _ = plt.hist(
                array[var.name],
                weights=array["eventweight"],
                bins=var.binning,
                histtype="step",
                color=color_iter[i],
                label=t,
                density=True
            )

            yvals.append(y)
            i +=1

        plt.ylim(ymin=0, ymax=1.3 * max([v.max(0) for v in yvals]))
        plt.ylabel("A.U.", horizontalalignment="right", y=1.0, fontsize=16)
        plt.xlabel(var.title, horizontalalignment="right", x=1.0, fontsize=16)
        atlas_label("Internal",x=0.78, y=0.9, ha='right')
        plt.legend(frameon=False)
        plt.tight_layout()
        plt.savefig(outputdir + rename(var.name))
        plt.close()



def corrplot(sample, title,t=None):
    """ Generates correlation plot for global defined variables
    Arguments:
        sample {np.array} -- sample to check correlations of
        title {str} -- Save title
    Keyword Arguments:
        t {str} -- Title for plot (default: {None})
    """
    df = pd.DataFrame(sample)
    corr = df.corr()
    sns.heatmap(corr, 
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values,
            linewidths=.5,
            cmap="coolwarm",
            vmin=-1,
            vmax=1,
            annot=True,
            fmt=".2f"
            )
    if t: plt.title(t)
    plt.tight_layout()
    plt.savefig(outputdir + "/" +title )
    plt.close()



if __name__ == "__main__":

    # Slimmed list of variables to use
    variables = [
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "aplanarity",
    "sphericityT",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    "pt_balance",
    "pt_qq_bb_ratio",        
    ]
    sample_dict = get_sample_dict()    
    #sample_set = load_classifier_set(jetRequirement=True) # plot only 4 jet events

    jet_req = lambda arr : arr[arr["jj_deta"]>=0]
    arrays_to_plot = OrderedDict()
    if doVBF: arrays_to_plot["VBF HH"] = jet_req(sample_dict["arr_vbf_test"])
    if doGGF: arrays_to_plot["ggF HH"] = jet_req(sample_dict["arr_ggf_test"])
    if doYY: arrays_to_plot["yy"] = jet_req(sample_dict["arr_yy_test"])
    if doTTH: arrays_to_plot["ttH"] = jet_req(sample_dict["arr_tth_test"])
    if doNTNI: pass # to implement 

    # Histograms
    if args.histograms:
        histogram_plotting(arrays_to_plot)

    if args.corr:
        corrplot(arrays_to_plot["ttH"][variables], "other_tth_corr","tth")
        plt
        corrplot(arrays_to_plot["VBF HH"][variables], "other_vbf_corr","vbf")
        corrplot(arrays_to_plot["ggF HH"][variables], "other_ggf_corr","ggf")
        corrplot(arrays_to_plot["yy"][variables], "other_yy_corr","yy")
        all_samples = np.concatenate([a for a in arrays_to_plot.values()])
        corrplot(all_samples, "other_all_corr","all")