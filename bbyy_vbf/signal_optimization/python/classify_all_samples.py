import numpy as np
import matplotlib.pyplot as plt
from numpy_optimizer import rescale_eventweight, join_signal_regions
from bdt_optimizer import split_by_index, make_clf_compliant
from joblib import load
from collections import OrderedDict

# Load Files
sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/2019_cut_optimization/ntuples/"
file_dictionary = {
    "vbf_mc16a": sourcedir + "forcuts_vbf_mc16a.npy",
    "vbf_mc16d": sourcedir + "forcuts_vbf_mc16d.npy",
    "vbf_mc16e": sourcedir + "forcuts_vbf_mc16e.npy",
    "ggf_mc16d": sourcedir + "forcuts_ggf_mc16d.npy",
    "ggf_mc16a": sourcedir + "forcuts_ggf_mc16a.npy",
    "ggf_mc16e": sourcedir + "forcuts_ggf_mc16e.npy",
    "yy_mc16a": sourcedir + "forcuts_yy_mc16a.npy",
    "yy_mc16d": sourcedir + "forcuts_yy_mc16d.npy",
    "tth_mc16a": sourcedir + "forcuts_TTH_mc16a.npy",
    "zh_mc16a": sourcedir + "forcuts_ZH_mc16a.npy",
    "ggh_mc16a": sourcedir + "forcuts_GGH_mc16a.npy",
}

# Load Classifiers
vbf_ggf_classifier_name = "saved_classifiers/ggf_GradientBoosting.joblib"
vbf_yy_classifier_name = "saved_classifiers/continuum_GradientBoosting.joblib"

vbf_ggf_clf = load(vbf_ggf_classifier_name, "r")
vbf_yy_clf = load(vbf_yy_classifier_name, "r")


# Load samples
# VBF

arr_vbf = join_signal_regions(
    [value for key, value in file_dictionary.items() if "vbf" in key]
)
vbf_original_eventweight = arr_vbf["eventweight"].sum()
arr_vbf = split_by_index(arr_vbf)[1]
vbf_eventweight_scalefactor = vbf_original_eventweight / arr_vbf["eventweight"].sum()

# GGF
arr_ggf = join_signal_regions(
    [value for key, value in file_dictionary.items() if "ggf" in key]
)
ggf_original_eventweight = arr_ggf["eventweight"].sum()
arr_ggf = split_by_index(arr_ggf)[1]
ggf_eventweight_scalefactor = ggf_original_eventweight / arr_ggf["eventweight"].sum()

# Sherpa yy continuum
arr_yy = join_signal_regions(
    [value for key, value in file_dictionary.items() if "yy" in key]
)
yy_original_eventweight = arr_yy["eventweight"].sum()
arr_yy = split_by_index(arr_yy)[1]
yy_eventweight_scalefactor = yy_original_eventweight / arr_yy["eventweight"].sum()

# Single Higgs
## tth
arr_tth = join_signal_regions([file_dictionary["tth_mc16a"]])
tth_original_eventweight = arr_tth["eventweight"].sum()
arr_tth = split_by_index(arr_tth)[1]
tth_eventweight_scalefactor = tth_original_eventweight / arr_tth["eventweight"].sum()
# zh
arr_zh = join_signal_regions([file_dictionary["zh_mc16a"]])
zh_original_eventweight = arr_zh["eventweight"].sum()
arr_zh = split_by_index(arr_zh)[1]
zh_eventweight_scalefactor = zh_original_eventweight / arr_zh["eventweight"].sum()
# ggh
arr_ggh = join_signal_regions([file_dictionary["ggh_mc16a"]])
ggh_original_eventweight = arr_ggh["eventweight"].sum()
arr_ggh = split_by_index(arr_ggh)[1]
ggh_eventweight_scalefactor = ggh_original_eventweight / arr_ggh["eventweight"].sum()

# Renormalize to data set
intended_luminosity = 140459.56
arr_vbf = rescale_eventweight(arr_vbf, vbf_eventweight_scalefactor)
arr_ggf = rescale_eventweight(arr_ggf, ggf_eventweight_scalefactor)
arr_yy = rescale_eventweight(
    arr_yy, yy_eventweight_scalefactor * intended_luminosity / (36215.0 + 44307.4)
)
arr_tth = rescale_eventweight(
    arr_tth, tth_eventweight_scalefactor * intended_luminosity / (36215.0)
)
arr_zh = rescale_eventweight(
    arr_zh, zh_eventweight_scalefactor * intended_luminosity / (36215.0)
)
arr_ggh = rescale_eventweight(
    arr_ggh, ggh_eventweight_scalefactor * intended_luminosity / (36215.0)
)

# Reshape original arrays to fit into classifier
for_clf_vbf = make_clf_compliant(arr_vbf)
for_clf_ggf = make_clf_compliant(arr_ggf)
for_clf_yy = make_clf_compliant(arr_yy)
for_clf_tth = make_clf_compliant(arr_tth)
for_clf_zh = make_clf_compliant(arr_zh)
for_clf_ggh = make_clf_compliant(arr_ggh)


# "predict_proba" returns array with list [P(bkg), P(sig)]
# Signal Classifier
predict1_vbf = vbf_ggf_clf.predict_proba(for_clf_vbf)[:, 1]
predict1_ggf = vbf_ggf_clf.predict_proba(for_clf_ggf)[:, 1]
predict1_yy = vbf_ggf_clf.predict_proba(for_clf_yy)[:, 1]
predict1_tth = vbf_ggf_clf.predict_proba(for_clf_tth)[:, 1]
predict1_zh = vbf_ggf_clf.predict_proba(for_clf_zh)[:, 1]
predict1_ggh = vbf_ggf_clf.predict_proba(for_clf_ggh)[:, 1]
decisions1 = OrderedDict()
decisions1["vbf"] = predict1_vbf
decisions1["ggf"] = predict1_ggf
decisions1["yy"] = predict1_yy
decisions1["tth"] = predict1_tth
decisions1["zh"] = predict1_zh
decisions1["ggh"] = predict1_ggh


# Background Classifier
predict2_vbf = vbf_yy_clf.predict_proba(for_clf_vbf)[:, 1]
predict2_ggf = vbf_yy_clf.predict_proba(for_clf_ggf)[:, 1]
predict2_yy = vbf_yy_clf.predict_proba(for_clf_yy)[:, 1]
predict2_tth = vbf_yy_clf.predict_proba(for_clf_tth)[:, 1]
predict2_zh = vbf_yy_clf.predict_proba(for_clf_zh)[:, 1]
predict2_ggh = vbf_yy_clf.predict_proba(for_clf_ggh)[:, 1]
decisions2 = OrderedDict()
decisions2["ggf"] = predict2_ggf
decisions2["vbf"] = predict2_vbf
decisions2["yy"] = predict2_yy
decisions2["tth"] = predict2_tth
decisions2["zh"] = predict2_zh
decisions2["ggh"] = predict2_ggh


weights = OrderedDict(
    {
        "vbf": arr_vbf["eventweight"],
        "ggf": arr_ggf["eventweight"],
        "yy": arr_yy["eventweight"],
        "tth": arr_tth["eventweight"],
        "zh": arr_zh["eventweight"],
        "ggh": arr_ggh["eventweight"],
    }
)


def plot(sample_dict, weights_dict, savename):
    fig = plt.figure()
    ax = plt.gca()

    color_wheel = {
        "vbf": "firebrick",
        "ggf": "cornflowerblue",
        "yy": "forestgreen",
        "tth": "peru",
        "zh": "darkmagenta",
        "ggh": "lightseagreen",
    }

    label_style = {
        "vbf": "VBF HH",
        "ggf": "ggF HH",
        "yy": "$\gamma\gamma$ Continuum",
        "tth": "ttH",
        "zh": "ZH",
        "ggh": "ggH",
    }
    # Plot
    for s in sample_dict:
        bin_array = np.arange(0, 1, 0.04)
        plt.hist(
            sample_dict[s],
            bins=bin_array,
            density=True,
            weights=weights_dict[s],
            color=color_wheel[s],
            label=label_style[s],
            histtype="step",
        )

    # Label
    plt.xlabel("BDT Output")
    plt.ylabel("Arbitrary Units")
    plt.legend(loc="upper center", frameon=False)
    plt.xlim(0, 1)
    plt.ylim(ymin=0)
    plt.tight_layout()
    plt.savefig(savename)
    plt.close()


plot(decisions1, weights, "run/signal_decisions_clf_noSig.png")
plot(decisions2, weights, "run/bkg_decisions_clf_noSig.png")
