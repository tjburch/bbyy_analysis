from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import classification_report, roc_auc_score, roc_curve, auc
from sklearn.model_selection import train_test_split, RandomizedSearchCV, GridSearchCV
from numpy_optimizer import join_signal_regions
from bdt_optimizer import rescale_eventweight
from numpy.lib.recfunctions import append_fields, drop_fields
import numpy as np
import matplotlib.pyplot as plt
from joblib import dump
from timeit import default_timer as timer
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-s", "--signal", help="Do signal BDT", action="store_true")
parser.add_argument("-b", "--background", help="Do background BDT", action="store_true")
args = parser.parse_args()

if not args.signal and not args.background:
    print("Not doing anything.")
elif args.signal:
    print("Doing Signal BDT")
elif args.background:
    print("Doing Background BDT")

# classifiers_to_run = ['AdaBoost',  'GradientBoosting']
classifiers_to_run = ["GradientBoosting"]
do_hyperparameter_scan = False

output_directory = "run/classifier_evaluation/"

search_params = {
    "n_estimators": [10, 40, 80, 100, 200, 400, 800, 1200],
    "max_depth": [2, 3, 4, 5, 10, 20],
    "min_samples_leaf": [1, 2, 4],
    "min_samples_split": [2, 5, 10],
    "max_features": [None, "auto", "sqrt"],
}


# Define all classifiers
# -------------------------------------------------------------
def train_AdaBoost(X_train, y_train, w_train, dt):
    print("Training AdaBoost")
    start = timer()
    bdt = AdaBoostClassifier(dt, algorithm="SAMME", n_estimators=200, learning_rate=0.5)
    clf = bdt.fit(X_train, y_train, sample_weight=w_train)
    end = timer()
    print("Trained in {0} seconds".format(end - start))
    return clf


def train_GradientBoosting(X_train, y_train, w_train, **kwargs):
    """ Builds grad boost bdt. Pass hyperparameters as kwargs """
    print("Training GradBoost")
    start = timer()
    bdt = GradientBoostingClassifier(**kwargs)
    clf = bdt.fit(X_train, y_train, sample_weight=w_train)
    end = timer()
    print("Trained in {0} seconds".format(end - start))
    return clf


# -------------------------------------------------------------


def compare_train_test(
    clf,
    X_train,
    y_train,
    X_test,
    y_test,
    w_train,
    w_test,
    labels=["S", "B"],
    bins=40,
    title="overtraining.png",
):
    decisions, weights = [], []
    for X, y, w in ((X_train, y_train, w_train), (X_test, y_test, w_test)):
        d1 = clf.predict_proba(X[y > 0.5])[:, 1]
        w1 = w[y > 0.5]
        d2 = clf.predict_proba(X[y < 0.5])[:, 1]
        w2 = w[y < 0.5]
        decisions += [d1, d2]
        weights += [w1, w2]

    low = min(np.min(d) for d in decisions)
    high = max(np.max(d) for d in decisions)
    low_high = (low, high)

    plt.hist(
        decisions[0],
        weights=weights[0],
        color="r",
        alpha=0.5,
        range=low_high,
        bins=bins,
        histtype="stepfilled",
        normed=True,
        label="%s (train)" % labels[0],
    )
    plt.hist(
        decisions[1],
        weights=weights[1],
        color="b",
        alpha=0.5,
        range=low_high,
        bins=bins,
        histtype="stepfilled",
        normed=True,
        label="%s (train)" % labels[1],
    )

    hist, bins = np.histogram(
        decisions[2], weights=weights[2], bins=bins, range=low_high, normed=True
    )
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    width = bins[1] - bins[0]
    center = (bins[:-1] + bins[1:]) / 2
    plt.errorbar(center, hist, yerr=err, fmt="o", c="r", label="%s (test)" % labels[0])

    hist, bins = np.histogram(
        decisions[3], weights=weights[3], bins=bins, range=low_high, normed=True
    )
    scale = len(decisions[2]) / sum(hist)
    err = np.sqrt(hist * scale) / scale

    plt.errorbar(center, hist, yerr=err, fmt="o", c="b", label="%s (test)" % labels[1])
    plt.gca().set_ylim(bottom=0)
    # plt.gca().set_xlim(-1,1)
    plt.xlabel("BDT output")
    plt.ylabel("Arbitrary units")
    plt.legend(loc="best")
    plt.savefig(title)
    plt.close()


def draw_roc_curve(y_test, y_pred, filename):
    fpr, tpr, thresholds = roc_curve(y_test, y_pred)
    thisauc = auc(fpr, tpr)
    # Make ROC plots
    print("Making ROC Plots")
    plt.figure(1)
    plt.plot([1, 0], [0, 1], "k--")
    plt.plot(tpr, 1 - fpr, label="ROCAUC = {:.3f}".format(thisauc))
    plt.xlabel("True Positive Rate")
    plt.ylabel("1-False Positive Rate")
    plt.title("ROC curve")
    plt.legend(loc="best")
    plt.savefig(filename)
    plt.close()


def evaluate_classifier(
    clf, X_train, X_test, y_train, y_test, w_train, w_test, labels, ensemble_name
):
    y_predict = clf.predict(X_test)
    # Make Classification Report
    print("Evaluating Classifier {0}".format(ensemble_name))
    print(
        classification_report(y_test, y_predict, target_names=["background", "signal"])
    )

    # Make ROC
    test_prob = clf.predict_proba(X_test)[:, 1]
    print(
        "AUC ROC: %.3f" % (roc_auc_score(y_test, test_prob > 0.5, sample_weight=w_test))
    )
    roc_save_name = "ROC_{0}_{1}".format(
        labels[1], ensemble_name
    )  # Use background label as naming since it's different between samples
    print("Saving ROC curve as ", roc_save_name)
    draw_roc_curve(y_test, test_prob > 0.5, output_directory + roc_save_name)

    # Make overtraining plot
    overtraining_name = "response_{0}_{1}".format(labels[1], ensemble_name)
    compare_train_test(
        clf,
        X_train,
        y_train,
        X_test,
        y_test,
        w_train,
        w_test,
        labels=labels,
        title=output_directory + overtraining_name,
    )


def split_by_index(arr, ew=False):

    index_col = np.arange(0, arr.shape[0])
    arr = append_fields(arr, "index", index_col)
    train = arr[np.logical_or(arr["index"] % 4 == 0, arr["index"] % 4 == 1)]
    test = arr[arr["index"] % 4 == 2]
    train = drop_fields(train, "index")
    test = drop_fields(test, "index")
    if ew:
        train = train["f0"]
        test = test["f0"]
    return train, test


if __name__ == "__main__":

    # Load Files
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/2019_cut_optimization/ntuples/"
    vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
    vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
    vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

    ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
    ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
    ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

    # Make array over signal region
    arr_vbf_0 = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])
    arr_ggf_0 = join_signal_regions([ggf_mc16a, ggf_mc16d, ggf_mc16e])

    # Join arrays
    # Renormalize to data set
    intended_luminosity = 140459.56
    arr_vbf_0 = rescale_eventweight(arr_vbf_0, intended_luminosity)
    arr_ggf_0 = rescale_eventweight(arr_ggf_0, intended_luminosity)

    if args.signal:
        print("doing Signal BDT")

        # slim to only variables we want
        names = [
            "jj_deta",
            "mjj",
            "n_jet",
            "m_yyjj",
            "dR_yy",
            "dR_jj",
            "dR_yyjj",
            "j1_tagbin",
            "j2_tagbin",
        ]
        arr_vbf = arr_vbf_0[:][names]
        w_vbf = arr_vbf_0[:]["eventweight"]
        arr_ggf = arr_ggf_0[:][names]
        w_ggf = arr_ggf_0[:]["eventweight"]

        arr_vbf_train, arr_vbf_test = split_by_index(arr_vbf)
        w_train_signal, w_test_signal = split_by_index(w_vbf, ew=True)
        arr_ggf_train, arr_ggf_test = split_by_index(arr_ggf)
        w_train_bkg, w_test_bkg = split_by_index(w_ggf, ew=True)

        # Destructure
        X_train_signal = arr_vbf_train.view(np.float64).reshape(
            arr_vbf_train.shape + (-1,)
        )
        X_test_signal = arr_vbf_test.view(np.float64).reshape(
            arr_vbf_test.shape + (-1,)
        )
        X_train_bkg = arr_ggf_train.view(np.float64).reshape(
            arr_ggf_train.shape + (-1,)
        )
        X_test_bkg = arr_ggf_test.view(np.float64).reshape(arr_ggf_test.shape + (-1,))

        y_train_signal = np.ones(arr_vbf_train.shape[0])
        y_test_signal = np.ones(arr_vbf_test.shape[0])
        y_train_bkg = np.zeros(arr_ggf_train.shape[0])
        y_test_bkg = np.zeros(arr_ggf_test.shape[0])

        # Renormalize, emulating logic here: https://root.cern.ch/doc/v610/DataSetFactory_8cxx_source.html#1530
        w_train_signal = w_train_signal * (
            w_train_signal.shape[0] / w_train_signal.sum()
        )
        w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())

        # Join with S/B labels
        print("Joining signal and background")
        X_train = np.concatenate((X_train_signal, X_train_bkg))
        X_test = np.concatenate((X_test_signal, X_test_bkg))
        y_train = np.concatenate((y_train_signal, y_train_bkg))
        y_test = np.concatenate((y_test_signal, y_test_bkg))
        w_train = np.concatenate((w_train_signal, w_train_bkg))
        w_test = np.concatenate((w_test_signal, w_test_bkg))

        # Make All Classifiers
        trained_classifiers = {}
        if "AdaBoost" in classifiers_to_run:
            trained_classifiers["AdaBoost"] = train_AdaBoost(
                X_train, y_train, w_train, DecisionTreeClassifier(max_depth=3)
            )
        if "GradientBoosting" in classifiers_to_run:
            if do_hyperparameter_scan is False:
                keywords = {
                    "max_features": "sqrt",
                    "min_samples_split": 2,
                    "n_estimators": 200,
                    "max_depth": 5,
                    "min_samples_leaf": 2,
                }
                trained_classifiers["GradientBoosting"] = train_GradientBoosting(
                    X_train, y_train, w_train, **keywords
                )
            else:
                print("Doing hyperparameter search (grid)")
                bdt = GradientBoostingClassifier()
                grid_search = GridSearchCV(
                    estimator=bdt, param_grid=search_params, n_jobs=-1, verbose=2
                )
                grid_search.fit(X_train, y_train, w_train)
                print("Best Parameters: ")
                print(grid_search.best_params_)

        # Check performance
        for classifier in trained_classifiers:
            evaluate_classifier(
                trained_classifiers[classifier],
                X_train,
                X_test,
                y_train,
                y_test,
                w_train,
                w_test,
                labels=["VBF", "ggF"],
                ensemble_name=classifier,
            )
            dump(
                trained_classifiers[classifier],
                "saved_classifiers/ggf_%s.joblib" % classifier,
            )

    ### -----------------------------
    ## Now Do vbf vs bkg
    ### -----------------------------

    if args.background:
        yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
        yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
        arr_yy_0 = join_signal_regions([yy_mc16a, yy_mc16d])
        arr_yy_0 = rescale_eventweight(arr_yy_0, intended_luminosity)

        arr_vbf_0 = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])
        arr_vbf_0 = rescale_eventweight(arr_vbf_0, 1)

        # slim to only variables we want
        names = [
            "jj_deta",
            "mjj",
            "n_jet",
            "m_yyjj",
            "dR_yy",
            "dR_jj",
            "dR_yyjj",
            "j1_tagbin",
            "j2_tagbin",
        ]
        arr_vbf = arr_vbf_0[:][names]
        w_vbf = arr_vbf_0[:]["eventweight"]
        arr_yy = arr_yy_0[:][names]
        w_yy = arr_yy_0[:]["eventweight"]

        arr_vbf_train, arr_vbf_test = split_by_index(arr_vbf)
        w_train_signal, w_test_signal = split_by_index(w_vbf, ew=True)
        arr_yy_train, arr_yy_test = split_by_index(arr_yy)
        w_train_bkg, w_test_bkg = split_by_index(w_yy, ew=True)
        # Destructure
        X_train_signal = arr_vbf_train.view(np.float64).reshape(
            arr_vbf_train.shape + (-1,)
        )
        X_test_signal = arr_vbf_test.view(np.float64).reshape(
            arr_vbf_test.shape + (-1,)
        )
        X_train_bkg = arr_yy_train.view(np.float64).reshape(arr_yy_train.shape + (-1,))
        X_test_bkg = arr_yy_test.view(np.float64).reshape(arr_yy_test.shape + (-1,))

        y_train_signal = np.ones(arr_vbf_train.shape[0])
        y_test_signal = np.ones(arr_vbf_test.shape[0])
        y_train_bkg = np.zeros(arr_yy_train.shape[0])
        y_test_bkg = np.zeros(arr_yy_test.shape[0])

        # Renormalize, emulating logic here: https://root.cern.ch/doc/v610/DataSetFactory_8cxx_source.html#1530
        w_train_signal = w_train_signal * (
            w_train_signal.shape[0] / w_train_signal.sum()
        )
        w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())

        X_train = np.concatenate((X_train_signal, X_train_bkg))
        X_test = np.concatenate((X_test_signal, X_test_bkg))
        y_train = np.concatenate((y_train_signal, y_train_bkg))
        y_test = np.concatenate((y_test_signal, y_test_bkg))
        w_train = np.concatenate((w_train_signal, w_train_bkg))
        w_test = np.concatenate((w_test_signal, w_test_bkg))

        # Make All Classifiers
        trained_bkg_classifiers = {}
        if "AdaBoost" in classifiers_to_run:
            trained_bkg_classifiers["AdaBoost"] = train_AdaBoost(
                X_train, y_train, w_train, DecisionTreeClassifier(max_depth=3)
            )
        if "GradientBoosting" in classifiers_to_run:
            if do_hyperparameter_scan is False:
                keywords = {
                    "max_features": None,
                    "min_samples_split": 10,
                    "n_estimators": 80,
                    "max_depth": 5,
                    "min_samples_leaf": 4,
                }
                trained_bkg_classifiers["GradientBoosting"] = train_GradientBoosting(
                    X_train, y_train, w_train, **keywords
                )
            else:
                print("Doing hyperparameter search (grid)")
                bdt = GradientBoostingClassifier()
                grid_search = GridSearchCV(
                    estimator=bdt, param_grid=search_params, n_jobs=-1, verbose=2
                )
                grid_search.fit(X_train, y_train, w_train)
                print("Best Parameters: ")
                print(grid_search.best_params_)

        # Check performance
        for classifier in trained_bkg_classifiers:
            evaluate_classifier(
                trained_bkg_classifiers[classifier],
                X_train,
                X_test,
                y_train,
                y_test,
                w_train,
                w_test,
                labels=["VBF", "continuum"],
                ensemble_name=classifier,
            )
            savename = "saved_classifiers/continuum_%s.joblib" % classifier
            print("Saving as: ", savename)
            dump(trained_bkg_classifiers[classifier], savename)
