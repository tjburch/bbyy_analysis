import ROOT

def shape_variables(j1, j2, p1, p2):
    shape_dictionary = {}

    photon1 = ROOT.TLorentzVector()
    photon2 = ROOT.TLorentzVector()
    jet1 = ROOT.TLorentzVector()
    jet2 = ROOT.TLorentzVector()
    jj = ROOT.TLorentzVector()
    jjC = ROOT.TLorentzVector()
    yy = ROOT.TLorentzVector()
    yyjj = ROOT.TLorentzVector()
    yyjjC = ROOT.TLorentzVector()


    jet1.SetPtEtaPhiM(j1.pt(),j1.eta(),j1.phi(),j1.m())
    jet2.SetPtEtaPhiM(j2.pt(),j2.eta(),j2.phi(),j2.m())
    
    photon1.SetPtEtaPhiM(p1.pt(),p1.eta(),p1.phi(),0)
    photon2.SetPtEtaPhiM(p2.pt(),p2.eta(),p2.phi(),0)

    yy = photon1+photon2
    jj = jet1+jet2
    jjC = jet1+jet2
    jjm = jj.M()
    jjC = jjC * (125./jjm)
    yyjj = yy+jj
    yyjjC = yy+jjC


    MomentumTensor = ROOT.TMatrixDSym(3)
    MomentumTensorT = ROOT.TMatrixDSym(3)
    MomentumTensorO = ROOT.TMatrixDSym(3)

    Sxx=0.0; Sxy=0.0; Sxz=0.0; Syy=0.0; Syz=0.0; Szz=0.0; normal=0.0
    Oxx=0.0; Oxy=0.0; Oxz=0.0; Oyy=0.0; Oyz=0.0; Ozz=0.0; Onormal=0.0

    Sxx += jet1.Px()*jet1.Px(); Sxx += jet2.Px()*jet2.Px()
    Sxx += photon1.Px()*photon1.Px(); Sxx += photon2.Px()*photon2.Px()
    Sxy += jet1.Px()*jet1.Py(); Sxy += jet2.Px()*jet2.Py()
    Sxy += photon1.Px()*photon1.Py(); Sxy += photon2.Px()*photon2.Py()
    Sxz += jet1.Px()*jet1.Pz(); Sxz += jet2.Px()*jet2.Pz()
    Sxz += photon1.Px()*photon1.Pz(); Sxz += photon2.Px()*photon2.Pz()
    Syy += jet1.Py()*jet1.Py(); Syy += jet2.Py()*jet2.Py()
    Syy += photon1.Py()*photon1.Py(); Syy += photon2.Py()*photon2.Py()
    Syz += jet1.Py()*jet1.Pz(); Syz += jet2.Py()*jet2.Pz()
    Syz += photon1.Py()*photon1.Pz(); Syz += photon2.Py()*photon2.Pz()
    Szz += jet1.Pz()*jet1.Pz(); Szz += jet2.Pz()*jet2.Pz()
    Szz += photon1.Pz()*photon1.Pz(); Szz += photon2.Pz()*photon2.Pz()

    normal += jet1.P()*jet1.P(); normal += jet2.P()*jet2.P()
    normal += photon1.P()*photon1.P(); normal += photon2.P()*photon2.P()

    Oxx += jet1.Px()*jet1.Px()/jet1.P(); Oxx += jet2.Px()*jet2.Px()/jet2.P()
    Oxx += photon1.Px()*photon1.Px()/photon1.P(); Oxx += photon2.Px()*photon2.Px()/photon2.P()
    Oxy += jet1.Px()*jet1.Py()/jet1.P(); Oxy += jet2.Px()*jet2.Py()/jet2.P()
    Oxy += photon1.Px()*photon1.Py()/photon1.P(); Oxy += photon2.Px()*photon2.Py()/photon2.P()
    Oxz += jet1.Px()*jet1.Pz()/jet1.P(); Oxz += jet2.Px()*jet2.Pz()/jet2.P()
    Oxz += photon1.Px()*photon1.Pz()/photon1.P(); Oxz += photon2.Px()*photon2.Pz()/photon2.P()
    Oyy += jet1.Py()*jet1.Py()/jet1.P(); Oyy += jet2.Py()*jet2.Py()/jet2.P()
    Oyy += photon1.Py()*photon1.Py()/photon1.P(); Oyy += photon2.Py()*photon2.Py()/photon2.P()
    Oyz += jet1.Py()*jet1.Pz()/jet1.P(); Oyz += jet2.Py()*jet2.Pz()/jet2.P()
    Oyz += photon1.Py()*photon1.Pz()/photon1.P(); Oyz += photon2.Py()*photon2.Pz()/photon2.P()
    Ozz += jet1.Pz()*jet1.Pz()/jet1.P(); Ozz += jet2.Pz()*jet2.Pz()/jet2.P()
    Ozz += photon1.Pz()*photon1.Pz()/photon1.P(); Ozz += photon2.Pz()*photon2.Pz()/photon2.P()

    Onormal += jet1.P(); Onormal += jet2.P()
    Onormal += photon1.P(); Onormal += photon2.P()

            
    MomentumTensor[0][0] = Sxx/normal
    MomentumTensor[0][1] = Sxy/normal
    MomentumTensor[0][2] = Sxz/normal
    MomentumTensor[1][0] = MomentumTensor[0][1]
    MomentumTensor[1][1] = Syy/normal
    MomentumTensor[1][2] = Syz/normal
    MomentumTensor[2][0] = MomentumTensor[0][2]
    MomentumTensor[2][1] = MomentumTensor[1][2]
    MomentumTensor[2][2] = Szz/normal

    MomentumTensorT[0][0] = MomentumTensor[0][0]
    MomentumTensorT[0][1] = MomentumTensor[0][1]
    MomentumTensorT[1][1] = MomentumTensor[1][1]
    MomentumTensorT[1][0] = MomentumTensor[1][0]


    MomentumTensorO[0][0] = Oxx/Onormal
    MomentumTensorO[0][1] = Oxy/Onormal
    MomentumTensorO[0][2] = Oxz/Onormal
    MomentumTensorO[1][0] = MomentumTensorO[0][1]
    MomentumTensorO[1][1] = Oyy/Onormal
    MomentumTensorO[1][2] = Oyz/Onormal
    MomentumTensorO[2][0] = MomentumTensorO[0][2]
    MomentumTensorO[2][1] = MomentumTensorO[1][2]
    MomentumTensorO[2][2] = Ozz/Onormal

    EigenValues = ROOT.TMatrixDSymEigen(MomentumTensor)
    EigenValuesT = ROOT.TMatrixDSymEigen(MomentumTensorT)
    EigenValuesO = ROOT.TMatrixDSymEigen(MomentumTensorO)

    eigenVec = EigenValues.GetEigenValues()
    eigenVecT = EigenValuesT.GetEigenValues()
    eigenVecO = EigenValuesO.GetEigenValues()

    shape_dictionary["aplanarity"] = 1.5*eigenVec[2]
    shape_dictionary["aplanority"] = 1.5*eigenVecO[2]
    shape_dictionary["sphericity"] = 1.5*(eigenVec[1]+eigenVec[2])
    shape_dictionary["spherocity"] = 1.5*(eigenVecO[1]+eigenVecO[2])
    shape_dictionary["sphericityT"] = 2.0*eigenVecT[1]/(eigenVecT[0]+eigenVecT[1])
    shape_dictionary["planarity"] = eigenVec[1] - eigenVec[2]
    varC = 3.0*(eigenVec[0] * eigenVec[1] + eigenVec[0] * eigenVec[2] + eigenVec[1] * eigenVec[2])
    varD = 27.0*(eigenVec[0] * eigenVec[1] * eigenVec[2])
    if ((eigenVec[0] + eigenVec [1]) != 0):
        shape_dictionary["circularity"] = 2.0*eigenVec[1]/(eigenVec[0] + eigenVec [1])
    else:
        shape_dictionary["circularity"] = -99
    if (((eigenVec[0] + eigenVec [1]) * (eigenVec[0] + eigenVec [1]) ) != 0):
        shape_dictionary["planarFlow"] = 4.0*eigenVec[0]*eigenVec[1]/((eigenVec[0] + eigenVec [1]) * (eigenVec[0] + eigenVec [1]))
    else:
        shape_dictionary["planarFlow"]  = -99
    return shape_dictionary

def tth_variables(jet_collection, b1_jet, b2_jet):
    # Initializations
    # -------------------
    wmass = 80.38
    ht = 0

    alljet_pts = [j.pt() for j in jet_collection]
    alljet_etas = [j.eta() for j in jet_collection]
    alljet_phis = [j.phi() for j in jet_collection]
    alljet_ms = [j.m() for j in jet_collection]

    b1 = ROOT.TLorentzVector()
    b1.SetPtEtaPhiM(b1_jet.pt()*0.001, b1_jet.eta(), b1_jet.phi(), b1_jet.m()*0.001)
    b2 = ROOT.TLorentzVector()
    b2.SetPtEtaPhiM(b2_jet.pt()*0.001, b2_jet.eta(), b2_jet.phi(), b2_jet.m()*0.001)

    wjj1 = ROOT.TLorentzVector()
    wjj2 = ROOT.TLorentzVector()

    # -------------------
    for ij in range(len(alljet_pts)):
        tempvector = ROOT.TLorentzVector()
        tempvector.SetPtEtaPhiM(0.001*alljet_pts[ij], alljet_etas[ij], alljet_phis[ij], alljet_ms[ij]*0.001)
        if (tempvector.DeltaR(b1) < 0.1): 
            continue ### don't pick up same jet as b
        if (tempvector.DeltaR(b2) < 0.1): 
            continue ### don't pick up same jet as b
        ht += tempvector.Pt()

        for jj in range(ij+1,len(alljet_pts)):
            tempvector2 = ROOT.TLorentzVector()
            tempvector2.SetPtEtaPhiM(0.001*alljet_pts[jj], alljet_etas[jj], alljet_phis[jj], alljet_ms[jj]*0.001)
            thisjj = tempvector + tempvector2
            if (abs(thisjj.M() - wmass) < abs(wjj1.M() - wmass)):
                wjj2.SetPtEtaPhiM(wjj1.Pt(),wjj1.Eta(),wjj1.Phi(),wjj1.M())
                wjj1.SetPtEtaPhiM(thisjj.Pt(),thisjj.Eta(),thisjj.Phi(),thisjj.M())
            elif (abs(thisjj.M() - wmass) < abs(wjj2.M() - wmass)):
                wjj2.SetPtEtaPhiM(thisjj.Pt(),thisjj.Eta(),thisjj.Phi(),thisjj.M())

    # Set the mjj w1/w2
    if (wjj1.M() < 1e10): 
        mjj_w1 = wjj1.M()
    else: 
        mjj_w1 = 0

    if (wjj2.M() < 1e10): 
        mjj_w2 = wjj2.M()
    else:
         mjj_w2 = 0

    if (wjj2.M() < 1e10): ### found two extra jets
        if (wjj1.DeltaR(b1) < wjj1.DeltaR(b2)):
            # Mass with closest b
            jjb = wjj1+b1
            mjjb1 = jjb.M()
            jjb = wjj2+b2
            mjjb2 = jjb.M()
        else:
            jjb = wjj1+b2
            mjjb1 = jjb.M()
            jjb = wjj2+b1
            mjjb2 = jjb.M()
    elif (wjj1.M() < 1e10): ### found one extra jet
        if (wjj1.DeltaR(b1) < wjj1.DeltaR(b2)):
            jjb = wjj1+b1
            mjjb1 = jjb.M()
            mjjb2 = 0
        else:
            jjb = wjj1+b2
            mjjb1 = jjb.M()
            mjjb2 = 0
    else: ### no extra jets
        mjjb1 = 0
        mjjb2 = 0

    return ht, mjjb1, mjjb2, mjj_w1, mjj_w2