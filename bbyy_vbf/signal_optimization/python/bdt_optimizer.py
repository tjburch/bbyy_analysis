from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
#from tqdm import tqdm
import matplotlib.pyplot as plt
from joblib import load
from sklearn.model_selection import train_test_split
from utils import *


def make_clf_compliant(
    arr,
    names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
    ],
):
    """Reshapes structured numpy array to fit in classifier.
    Arguments:
        arr {np.recarray} -- array to restructure    
    Keyword Arguments:
        names {list} -- features necessary for clf (default: {['jj_deta','mjj','n_jet','m_yyjj','dR_yy','dR_jj','dR_yyjj']})
    Returns:
        [np.array] -- ready for predict
    """
    arr = arr[:][names]
    arr = arr.view(np.float64).reshape(arr.shape + (-1,))
    return arr


def plot_all_significances(significance_list, mjj_list, deta_list):
    iteration = range(0, len(significance_list))
    fig = plt.plot(iteration, significance_list, ".", markersize=2)
    plt.xlabel("Iteration")
    plt.ylabel("Significance")
    plt.savefig("significances.png")
    plt.close()

    plt.figure()
    ax = plt.axes()
    tpc = ax.tripcolor(
        mjj_list, deta_list, significance_list, cmap="coolwarm", shading="gouraud"
    )
    ax.set_xlim(0, 1200)
    ax.set_ylim(0, 10)
    plt.colorbar(tpc)
    plt.xlabel("$m_{jj}$ cut")
    plt.ylabel("$\Delta\eta$ cut")
    plt.tight_layout()
    plt.savefig("significance3d.pdf")


def full_categorized_significance(
    thisVBFcategorized_ggf_array,
    thisGGFcategorized_ggf_array,
    thisVBFcategorized_vbf_array,
    thisGGFcategorized_vbf_array,
    thisVBFcategorized_yy_array,
    thisGGFcategorized_yy_array,
    sig_clf_cut,
):
    """Finds full significance for all categories. Takes into account ggF cut-based optimization    
    Arguments:
        arr_ggf {np.array} -- signal ggF array failing VBF selection
        arr_yy_ggfcat {np.array} -- yy continuum array failing VBF selection    
    Keyword Arguments (done as kwargs to evaluate without VBF selection):
        arr_vbf {np.array} -- signal VBF array passing VBF selection (default: {None})
        arr_yy_vbfcat {np.array} -- yy continuum array failing VBF selection (default: {None})
    Returns:
        [float] -- significance of all categories (all ggF + VBF if passed) added in quadrature
    TODO : Fix comment string here
    """

    # Evaluate all VBF by classifier 2
    predict_vbf = vbf_yy_clf.predict_proba(
        make_clf_compliant(thisVBFcategorized_vbf_array)
    )[:, 1]
    predict_yy = vbf_yy_clf.predict_proba(
        make_clf_compliant(thisVBFcategorized_yy_array)
    )[:, 1]
    predict_ggf = vbf_yy_clf.predict_proba(
        make_clf_compliant(thisVBFcategorized_ggf_array)
    )[:, 1]

    best_significance = 0
    bkg_clf_cut = None
    if "Gradient" in vbf_yy_classifier_name:
        scanrange = np.arange(0, 1, 0.01)
    else:
        scanrange = np.arange(-1, 1, 0.05)

    for probability_cut in scanrange:
        logfile.write("     Cutting background BDT at %.2f \n" % probability_cut)

        vbf_passing_cut = thisVBFcategorized_vbf_array[predict_vbf > probability_cut]
        ggf_passing_cut = thisVBFcategorized_ggf_array[predict_ggf > probability_cut]
        signal_passing = np.concatenate([vbf_passing_cut, ggf_passing_cut])
        yy_passing_cut = thisVBFcategorized_yy_array[predict_yy > probability_cut]

        this_cut_vbf_category_significance = asimov_significance(
            signal_passing["eventweight"].sum(), yy_passing_cut["eventweight"].sum()
        )

        # Reclaim ggF that fail this categorization
        eachCategory_significance = []
        vbf_failing_cut = thisVBFcategorized_vbf_array[
            np.logical_not(predict_vbf > probability_cut)
        ]
        ggf_failing_cut = thisVBFcategorized_ggf_array[
            np.logical_not(predict_ggf > probability_cut)
        ]
        signal_failing = np.concatenate([vbf_failing_cut, ggf_failing_cut])
        yy_failing_cut = thisVBFcategorized_yy_array[
            np.logical_not(predict_yy > probability_cut)
        ]

        # Build whole ggF sample
        all_ggf_categorized_events = np.concatenate(
            [thisGGFcategorized_ggf_array, thisGGFcategorized_vbf_array, signal_failing]
        )
        all_ggf_categorized_yy_background = np.concatenate(
            [thisGGFcategorized_yy_array, yy_failing_cut]
        )
        eachCategory_ggf = []
        eachCategory_yy = []
        # Find ggF significance for cuts-based optimization for each category
        for cat in range(1, 5):
            inCategory_ggf = all_ggf_categorized_events[
                all_ggf_categorized_events["cutCategory"] == cat
            ]
            inCategory_yy = all_ggf_categorized_yy_background[
                all_ggf_categorized_yy_background["cutCategory"] == cat
            ]
            eachCategory_ggf.append(inCategory_ggf["eventweight"].sum())
            eachCategory_yy.append(inCategory_yy["eventweight"].sum())
            eachCategory_significance.append(
                asimov_significance(
                    inCategory_ggf["eventweight"].sum(),
                    inCategory_yy["eventweight"].sum(),
                )
            )

        if this_cut_vbf_category_significance != 0:
            eachCategory_significance.append(this_cut_vbf_category_significance)
        if len(eachCategory_significance) > 0:
            this_cut_vbf_category_significance = sqrt(
                sum([x ** 2 for x in eachCategory_significance])
            )
        else:
            this_cut_vbf_category_significance = 0
        logfile.write(
            "        nEvents VBF: %.2f  nEvents ggF:  %.2f    total:  %.2f\n"
            % (
                signal_passing["eventweight"].sum(),
                all_ggf_categorized_events["eventweight"].sum(),
                signal_passing["eventweight"].sum()
                + all_ggf_categorized_events["eventweight"].sum(),
            )
        )

        if this_cut_vbf_category_significance > best_significance:
            best_significance = this_cut_vbf_category_significance
            bkg_clf_cut = probability_cut

        ggf_string = ",".join(str(float(v)) for v in eachCategory_ggf)
        yy_string = ",".join(str(float(v)) for v in eachCategory_yy)
        csv_file.write(
            "%f,%f," % (sig_clf_cut, probability_cut)
            + ggf_string
            + ","
            + str(signal_passing["eventweight"].sum())
            + ","
            + yy_string
            + ","
            + str(yy_passing_cut["eventweight"].sum())
            + ","
            + str(this_cut_vbf_category_significance)
            + "\n"
        )
        # print "     Cut background BDT at %.2f, significance %.4f" %(probability_cut, this_cut_vbf_category_significance)

        clf1_vals.append

    return best_significance, bkg_clf_cut


def scan_vals(arr_vbf, arr_ggf, arr_yy):
    # Create boolean arrays for what falls into VBF category.
    # Start by only considering 4 jet events. Failures of this have -99 jj_deta.
    vbf_sample_bool_array = arr_vbf["jj_deta"] >= 0
    ggf_sample_bool_array = arr_ggf["jj_deta"] >= 0
    yy_sample_bool_array = arr_yy["jj_deta"] >= 0

    # Reshape original arrays to fit into classifier
    for_clf_vbf = make_clf_compliant(arr_vbf)
    for_clf_ggf = make_clf_compliant(arr_ggf)
    for_clf_yy = make_clf_compliant(arr_yy)

    # "predict_proba" returns array with list [P(bkg), P(sig)]
    # Later just cut on signal probability, so only take first element
    predict_vbf = vbf_ggf_clf.predict_proba(for_clf_vbf)[:, 1]
    predict_ggf = vbf_ggf_clf.predict_proba(for_clf_ggf)[:, 1]
    predict_yy = vbf_ggf_clf.predict_proba(for_clf_yy)[:, 1]

    best_significance = namedtuple("SigPoint", ["value", "ggf_clf_cut", "bkg_clf_cut"])
    best_significance.value = -1

    if "Gradient" in vbf_ggf_classifier_name:
        scan_range = np.arange(0, 1, 0.01)
    else:
        scan_range = np.arange(-1, 1, 0.05)
    pbar = tqdm(total=len(scan_range))
    all_significance_values = []
    print("Starting scan. Bounds: %.2f to %.2f " % (scan_range.min(), scan_range.max()))
    all_significance_values = []

    for probability_cut in scan_range:
        # VBF
        vbf_boolean_array = vbf_sample_bool_array * predict_vbf > probability_cut
        thisVBFcategorized_vbf_array = arr_vbf[vbf_boolean_array]
        thisGGFcategorized_vbf_array = arr_vbf[np.logical_not(vbf_boolean_array)]

        # ggF
        ggf_boolean_array = ggf_sample_bool_array * predict_ggf > probability_cut
        thisVBFcategorized_ggf_array = arr_ggf[ggf_boolean_array]
        thisGGFcategorized_ggf_array = arr_ggf[np.logical_not(ggf_boolean_array)]

        # background classification
        yy_boolean_array = yy_sample_bool_array * predict_yy > probability_cut
        thisVBFcategorized_yy_array = arr_yy[yy_boolean_array]
        thisGGFcategorized_yy_array = arr_yy[np.logical_not(yy_boolean_array)]

        # Find significance after applying VBF BDT
        this_cut_signficance, bkg_clf_cut = full_categorized_significance(
            thisVBFcategorized_ggf_array,
            thisGGFcategorized_ggf_array,
            thisVBFcategorized_vbf_array,
            thisGGFcategorized_vbf_array,
            thisVBFcategorized_yy_array,
            thisGGFcategorized_yy_array,
            probability_cut,
        )
        logfile.write("Cutting signal BDT at %.2f \n" % probability_cut)
        if this_cut_signficance > best_significance.value:
            best_significance.value = this_cut_signficance
            best_significance.bkg_clf_cut = bkg_clf_cut
            best_significance.ggf_clf_cut = probability_cut
        all_significance_values.append(this_cut_signficance)
        # print "Cut signal BDT at %.2f, best significance %.4f\n" %(probability_cut, this_cut_signficance)
        pbar.update(1)

    # plot_all_significances(all_significance_values, mjj_vals, deta_vals)
    return best_significance


def rescale_eventweight(array, sf):
    """Rescales the eventweight column to input luminosity
    Arguments:
        array {np structured array} -- array to rescale
    Returns:
        [np structured array] -- rescaled array
    """
    # Scale eventweight
    scaled_eventweight = array["eventweight"] * sf
    # Drop old eventweight
    return_array = drop_fields(array, "eventweight")
    # Append new weight and return
    return append_fields(return_array, "eventweight", scaled_eventweight, usemask=False)




if __name__ == "__main__":

    # Global define sample names
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/2019_cut_optimization/ntuples/"
    vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
    vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
    vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

    ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
    ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
    ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

    yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
    yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"

    # Load BDTs
    vbf_ggf_classifier_name = "saved_classifiers/ggf_GradientBoosting.joblib"
    vbf_yy_classifier_name = "saved_classifiers/continuum_GradientBoosting.joblib"
    # vbf_ggf_classifer_name = 'saved_classifiers/ggf_AdaBoost.joblib'
    # vbf_yy_classifier_name = 'saved_classifiers/continuum_AdaBoost.joblib'

    vbf_ggf_clf = load(vbf_ggf_classifier_name, "r")
    vbf_yy_clf = load(vbf_yy_classifier_name, "r")
    logfile = open("optimization.log", "w+")
    csv_file = open("optimization.csv", "w+")


    clf1_vals = []
    clf2_vals = []
    significance_vals = []


    # Header of CSV
    csv_file.write("sig_cut,bkg_cut,ggf1,ggf2,ggf3,ggf4,vbf,yy1,yy2,yy3,yy4,yy_vbf,significance\n")

    # Load samples
    # VBF (only mc16a now TODO: add mc16d)
    arr_vbf = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])
    vbf_original_eventweight = arr_vbf["eventweight"].sum()
    arr_vbf = split_by_index(arr_vbf)[1]
    vbf_eventweight_scalefactor = (
        vbf_original_eventweight / arr_vbf["eventweight"].sum()
    )
    # GGF
    arr_ggf = join_signal_regions([ggf_mc16a, ggf_mc16d, ggf_mc16e])
    ggf_original_eventweight = arr_ggf["eventweight"].sum()
    arr_ggf = split_by_index(arr_ggf)[1]
    ggf_eventweight_scalefactor = (
        ggf_original_eventweight / arr_ggf["eventweight"].sum()
    )

    # Sherpa yy continuum
    arr_yy = join_signal_regions([yy_mc16a, yy_mc16d])
    yy_original_eventweight = arr_yy["eventweight"].sum()
    arr_yy = split_by_index(arr_yy)[1]
    yy_eventweight_scalefactor = yy_original_eventweight / arr_yy["eventweight"].sum()

    # Renormalize to data set
    intended_luminosity = 140459.56
    arr_vbf = rescale_eventweight(arr_vbf, vbf_eventweight_scalefactor)
    arr_ggf = rescale_eventweight(arr_ggf, ggf_eventweight_scalefactor)
    arr_yy = rescale_eventweight(
        arr_yy, yy_eventweight_scalefactor * intended_luminosity / (36215.0 + 44307.4)
    )

    # Print some safety checks
    print("Original Values - ")
    print("Total VBF yield: %.4f" % arr_vbf["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf["eventweight"].sum())
    print("Total Continuum Bkg yield: %.4f" % arr_yy["eventweight"].sum())

    def find_in_category(arr, cat):
        array = arr[arr["cutCategory"] == cat]
        return array["eventweight"].sum()

    # Find ggF-only Cuts significance
    eachCategory_ggf = []
    eachCategory_yy = []
    eachCategory_significance = []
    all_signal = np.concatenate([arr_ggf, arr_vbf])
    for cat in range(1, 5):
        inCategory_ggf = arr_ggf[arr_ggf["cutCategory"] == cat]
        inCategory_vbf = arr_vbf[arr_vbf["cutCategory"] == cat]

        inCategory_signal = all_signal[all_signal["cutCategory"] == cat]
        inCategory_yy = arr_yy[arr_yy["cutCategory"] == cat]
        eachCategory_ggf.append(inCategory_signal["eventweight"].sum())
        eachCategory_yy.append(inCategory_yy["eventweight"].sum())
        eachCategory_significance.append(
            asimov_significance(
                inCategory_signal["eventweight"].sum(),
                inCategory_yy["eventweight"].sum(),
            )
        )
        print(
            "Category: "
            + str(cat)
            + " significance: "
            + str(eachCategory_significance[cat - 1])
            + "  ggF events: "
            + str(inCategory_ggf["eventweight"].sum())
            + "  yy events: "
            + str(inCategory_yy["eventweight"].sum())
        )

        print(
            "cat ",
            cat,
            " ggf:",
            find_in_category(arr_ggf, cat),
            " vbf:",
            find_in_category(arr_vbf, cat),
            " yy:",
            find_in_category(arr_yy, cat),
        )
    print(arr_vbf[arr_vbf["cutCategory"] < 1]["eventweight"].sum())
    print(arr_ggf[arr_ggf["cutCategory"] < 1]["eventweight"].sum())
    print(arr_yy[arr_yy["cutCategory"] < 1]["eventweight"].sum())

    original_significance = sqrt(sum([x ** 2 for x in eachCategory_significance]))
    print(
        "Significance with no VBF region, Cuts-based signal region: %.3f"
        % original_significance
    )
    ggf_string = ",".join(str(float(v)) for v in eachCategory_ggf)
    yy_string = ",".join(str(float(v)) for v in eachCategory_yy)
    csv_file.write("0,0," + ggf_string + ",0," + yy_string + ",0,"+str(original_significance)+"\n")

    # Do the Value Scanning
    optimization = scan_vals(arr_vbf, arr_ggf, arr_yy)

    # Print out Results
    print("\nOptimization Finished - Results:")
    print("Optimized Significance %.3f" % optimization.value)
    try:
        print(
            "Achieved by cutting signal BDT at: %.2f , bkg BDT at: %.2f"
            % (optimization.ggf_clf_cut, optimization.bkg_clf_cut)
        )
    except TypeError:
        print(optimization.ggf_clf_cut)
        print(optimization.bkg_clf_cut)
        # print('Achieved by cutting signal BDT at: %.2f , bkg BDT at: %.2f' % (optimization.ggf_clf_cut, optimization.bkg_clf_cut))
