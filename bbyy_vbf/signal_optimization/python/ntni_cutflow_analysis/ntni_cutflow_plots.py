import numpy as np
import matplotlib.pyplot as plt
ntni1516 = np.load("/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/ntni_cutflow1516.npy")
ntni17 = np.load("/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/ntni_cutflow17.npy")
ntni18 = np.load("/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/ntni_cutflow18.npy")
arr_ntni = sum([ntni1516, ntni17, ntni18])


print(arr_ntni)
#plt.plot(x=list(range(0,10)), y=arr_ntni)

plt.errorbar(x=list(range(0,9)), y=arr_ntni, xerr=.5, linestyle="none")

#(y, bins, lol) = plt.hist(arr_ntni["np_personal_cutflow"])
#bins = bins[:-1]
#for yvalue, bin_no in zip(y, bins):
#    plt.annotate(yvalue, xy=(bin_no+.5, y.max()+500), xycoords="data")

for i in range(0,9):
    plt.gca().axvline(i-0.5, color="gray",alpha=0.5)

labels = [
    "Events\n(skimmed sample)",
    "NTNI Photons", 
    "Cutflow >= 11",
    "2 Cen Jets",
    "N_lep == 0",
    "<6 Cen Jets",
    "<3 b-tagged",
    "$70 < m_{bb} < 180$",
    "$105 < m_{\gamma\gamma} < 160$"
]
plt.gca().set_xticks(np.arange(-0.5,8.5,1))
plt.gca().set_xticklabels(labels, rotation=50)
plt.xlim(left=-0.5,right=8.5)


plt.tight_layout()

plt.savefig("ntni_hist.png")

