import sys
sys.path.append("/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/python/")

import ROOT
import sys
from vbf_classes import *
from array import array
import numpy as np
import argparse


if len(sys.argv) < 3:
    print("Usage: python preselection.py inputfile.root outputfile.root (lumi in pb)")
    sys.exit(1)

# Define input and output
print("Loading Files")
# ---------------------------------------------------
f_in = ROOT.TFile(sys.argv[1])
output_tree = ROOT.TTree("mini", "mini")


# Initialize xAOD Framework
print("Initializing xAOD Framework")
# ---------------------------------------------------
ROOT.xAOD.Init()


def char2bool(char):
    """Convert char to bool."""
    return {"\x00": False, "\x01": True}[char]


def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value




# Load Tree
print("Loading up xAOD tree to iterate on")
# ---------------------------------------------------
input_tree = ROOT.xAOD.MakeTransientTree(f_in, "CollectionTree")
entries = input_tree.GetEntries()
print("%s Entries" % str(entries))

# Output Arrays
# ---------------------------------------------------
np_personal_cutflow = np.full(shape=(9,), fill_value=input_tree.GetEntries())
np_passing = []

# Event Loop
print("Beginning Event Loop")
# ---------------------------------------------------
for ientry in range(input_tree.GetEntries()):
    # Print Status
    if ientry % 10000 == 0:
        print(
            "ientry = %d out of %d, percent done = %.3f"
            % (
                ientry,
                input_tree.GetEntries(),
                1.0 * ientry / input_tree.GetEntries() * 100,
            )
        )
    input_tree.GetEntry(ientry)

    m_yy = read(input_tree.HGamEventInfo, "float", "m_yy") * 0.001


    # Apply Preselection
    passing = False
    # NTNI Selection
    y1_tightiso, y2_tightiso  = False, False
    if (read(input_tree.HGamPhotons[0], "char", "isTight") and read(input_tree.HGamPhotons[0], "char", "isIsoFixedCutLoose")):
        y1_tightiso = True
    if (read(input_tree.HGamPhotons[1], "char", "isTight") and read(input_tree.HGamPhotons[1], "char", "isIsoFixedCutLoose")):
        y2_tightiso = True
    if (y1_tightiso and y2_tightiso):
        np_personal_cutflow[1:] -=1
    elif not (read(input_tree.HGamEventInfo, "int", "cutFlow") > 11):
        np_personal_cutflow[2:] -=1
    elif not read(input_tree.HGamEventInfo, "int", "N_j_central") >= 2: 
        np_personal_cutflow[3:] -=1
    elif not read(input_tree.HGamEventInfo, "int", "N_lep") == 0:
        np_personal_cutflow[4:] -=1
    elif not read(input_tree.HGamEventInfo, "int", "N_j_central") < 6:
        np_personal_cutflow[5:] -=1
    elif not read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3:
        np_personal_cutflow[6:] -=1
    elif not (
        read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") > 70.0 
        and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") <= 180.0
    ):
        np_personal_cutflow[7:] -=1
    elif not (m_yy < 160 or m_yy > 105):
        np_personal_cutflow[8:] -=1    
    else:
        np_passing.append(True)

# Create Structured array
nparrays = [
    np_personal_cutflow,
    np.array(np_passing)
]

names = [
    "np_personal_cutflow",
    "np_passing",
]
#$recarray = np.core.records.fromarrays(nparrays, names=names)

# Create npy file. Would be nice to do h5 but t3 doesn't work happy with this.
#np.save(sys.argv[2], recarray)
np.save(sys.argv[2], np_personal_cutflow)
