import numpy as np
import matplotlib.pyplot as plt
from utils import *
# Load Samples
sourcedir = (
    "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
)
yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
yy_mc16e = sourcedir + "forcuts_yy_mc16e.npy"

arr_yy_2tag, yy_eventweight_scalefactor = load_samples(
    [yy_mc16a, yy_mc16d, yy_mc16e], myyCut=False, ggf_preselection=True)
arr_yy_2tag = rescale_eventweight(arr_yy_2tag, yy_eventweight_scalefactor)

arr_yy_0tag, yy_0tag_eventweight_scalefactor = load_samples(
    [yy_mc16a, yy_mc16d, yy_mc16e],
    ggf_preselection=False,
    myyCut=False,
    tagReq=0)
arr_yy_0tag = rand_sample_tagbin(model_array=arr_yy_2tag,
                                replace_array=arr_yy_0tag,
                                plot=True)
categories = reconstruct_categories(arr_yy_0tag, btagging=False)
arr_yy_0tag["cutCategory"] = categories
arr_yy_0tag = arr_yy_0tag[arr_yy_0tag["cutCategory"] > 0]
arr_yy_0tag = rebalance_categories(arr_yy_2tag, arr_yy_0tag)

arr_yy_2tag = arr_yy_2tag[arr_yy_2tag["jj_deta"] >= 0]
arr_yy_0tag = arr_yy_0tag[arr_yy_0tag["jj_deta"] >= 0]
arr_yy_2tag = arr_yy_2tag[arr_yy_2tag["n_jet"] ==5]
arr_yy_0tag = arr_yy_0tag[arr_yy_0tag["n_jet"] ==5]

# Predefined stuff
# //---------------
names = [
    "m_yy",
    "jj_deta",
    "mjj",
    "n_jet",
    "tagregion",
    "cutCategory",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
]
axes_titles = [
    "$m_{\gamma\gamma}$ [GeV]",
    "$\Delta\eta_{jj}$",
    "$m_{jj}$",
    "Jet Multiplicity",
    "Tag Region",
    "Cut Category",
    "$m_{\gamma\gamma jj}$ [GeV]",
    "$\Delta R_{\gamma,\gamma}$",
    "$\Delta R_{j,j}$",
    "$\Delta R_{\gamma\gamma,jj}$",
    "$j_{1}$ Tagbin",
    "$j_{2}$ Tagbin",
]

bin_dict = {
    "n_jet": np.arange(1.5, 10.5, 1),
    "m_yyjj": np.linspace(start=0, stop=1500, num=35),
    "mjj": np.linspace(start=0, stop=3000, num=35),
    "dR_jj": np.arange(0, 5, 0.25),
    "dR_yy": np.arange(0, 4, 0.25),
    "dR_yyjj": np.arange(0, 8, 0.5),
    "jj_deta": np.arange(0, 10, 0.5),
    "cutCategory": np.arange(1, 6, 1),
    "j1_tagbin": np.arange(1, 6, 1),
    "j2_tagbin": np.arange(1, 6, 1),
}


# Main Function
# //---------------
for distribution, title in zip(names, axes_titles):



    fig = plt.figure()

    if distribution in bin_dict:
        bins = bin_dict[distribution]
    else:
        bins = np.linspace(
            start=min([arr_yy_2tag[distribution].min(), arr_yy_0tag[distribution].min()]),
            stop=max([arr_yy_2tag[distribution].max(), arr_yy_0tag[distribution].max()]),
            num=25,
        )
        # take care of single value error case
        if len(np.unique(np.array(bins))) == 1:
            v = np.unique(np.array(bins))[0]
            bins = [v - 1, v, v + 1, v + 2]

    y1, x1, _1 = plt.hist(
        arr_yy_2tag[distribution],
        weights=arr_yy_2tag["eventweight"],
        bins=bins,
        histtype="step",
        color="tab:blue",
        label="2-tag $\gamma\gamma$ ({0} Entries)".format(
            arr_yy_2tag.shape[0]),
        density=True,
    )
    y2, x2, _2 = plt.hist(
        arr_yy_0tag[distribution],
        weights=arr_yy_0tag["eventweight"],
        bins=bins,
        histtype="step",
        color="tab:red",
        label="0-tag $\gamma\gamma$ ({0} Entries)".format(
            arr_yy_0tag.shape[0]),
        density=True,
    )
    try:
        plt.ylim(ymin=0, ymax=1.35 * max([y1.max(), y2.max()]))
    except ValueError:
        pass
    plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
    plt.xlabel(title, horizontalalignment="right", x=1.0)
    plt.legend(frameon=False, fontsize=12)

    plt.tight_layout()
    plt.savefig(sourcedir + "../run/checks/tagCheck/"+distribution)
    plt.close()
"""
colors = ["r","g","b","cyan"]

for distribution, title in zip(names, axes_titles):
    bins = np.linspace(
        start=arr_yy_2tag[distribution].min(),
        stop=arr_yy_2tag[distribution].max(),
        num=17,
    )

    for tag in range(4,7):
        if tag == 4: plt.close()

        iarr_yy_2tag = arr_yy_2tag[arr_yy_2tag["n_jet"] == tag]


        y1, x1, _1 = plt.hist(
            iarr_yy_2tag[distribution],
            weights=iarr_yy_2tag["eventweight"],
            bins=bins,
            histtype="step",
            color=colors[tag-4],
            label="nJet = {0}".format(
                tag),
            density=True,
        )

    if tag == 6:
        plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
        plt.xlabel(title, horizontalalignment="right", x=1.0)
        plt.legend(frameon=False, fontsize=12)
        plt.tight_layout()
        plt.savefig(sourcedir + "../run/checks/tagCheck/2t_nj_"+distribution)
        plt.close()
"""

"""
for distribution, title in zip(names, axes_titles):
    bins = np.linspace(
        start=arr_yy_0tag[distribution].min(),
        stop=arr_yy_0tag[distribution].max(),
        num=17,
    )

    for tag in range(4,7):
        if tag == 4: plt.close()

        iarr_yy_0tag = arr_yy_0tag[arr_yy_0tag["n_jet"] == tag]


        y1, x1, _1 = plt.hist(
            iarr_yy_0tag[distribution],
            weights=iarr_yy_0tag["eventweight"],
            bins=bins,
            histtype="step",
            color=colors[tag-4],
            label="nJet = {0}".format(
                tag),
            density=True,
        )


    if tag == 6:
        plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
        plt.xlabel(title, horizontalalignment="right", x=1.0)
        plt.legend(frameon=False, fontsize=12)
        plt.tight_layout()
        plt.savefig(sourcedir + "../run/checks/tagCheck/0t_nj_"+distribution)
        plt.close()       
"""         