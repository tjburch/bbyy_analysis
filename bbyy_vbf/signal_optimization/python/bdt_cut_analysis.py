from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from joblib import load
from numpy_optimizer import join_signal_regions
from bdt_optimizer import split_by_index, asimov_significance
from numpy_optimizer import rescale_eventweight
import matplotlib.style as style
import pandas as pd
from collections import namedtuple, OrderedDict
from utils import reconstruct_categories

sig_clf_score = 0.74
bkg_clf_score = 0.60
# rec sig = 0.95 bkg 0.97
# Flag to use the scores above or not
hand_picked_scores = True 

# Global define sample names
sourcedir = (
    "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
)
vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"

# Load BDTs
vbf_ggf_classifier_name = "saved_classifiers/ggf_GradientBoosting.joblib"
vbf_yy_classifier_name = "saved_classifiers/continuum_GradientBoosting.joblib"


vbf_ggf_clf = load(vbf_ggf_classifier_name, "r")
vbf_yy_clf = load(vbf_yy_classifier_name, "r")


def make_clf_compliant(
    arr,
    names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
    ],
):
    """Reshapes structured numpy array to fit in classifier.
    Arguments:
        arr {np.recarray} -- array to restructure
    Keyword Arguments:
        names {list} -- features necessary for clf (default: {['jj_deta','mjj','n_jet','m_yyjj','dR_yy','dR_jj','dR_yyjj']})
    Returns:
        [np.array] -- ready for predict
    """
    arr = arr[:][names]
    arr = arr.view(np.float64).reshape(arr.shape + (-1,))
    return arr


# Load samples
# VBF
arr_vbf = join_signal_regions([vbf_mc16a, vbf_mc16d, vbf_mc16e])  # Joins Samples
vbf_original_eventweight = arr_vbf["eventweight"].sum()  # Gets original Event weights
arr_vbf = split_by_index(arr_vbf)[1]  # Splits into train[0] or test[1]
# restore eventweight
vbf_eventweight_scalefactor = vbf_original_eventweight / arr_vbf["eventweight"].sum()
# GGF
arr_ggf = join_signal_regions([ggf_mc16a, ggf_mc16d, ggf_mc16e])
ggf_original_eventweight = arr_ggf["eventweight"].sum()
arr_ggf = split_by_index(arr_ggf)[1]
ggf_eventweight_scalefactor = ggf_original_eventweight / arr_ggf["eventweight"].sum()

# Sherpa yy continuum
arr_yy = join_signal_regions([yy_mc16a, yy_mc16d], tagReq=False, ggf_preselection=False)
high_mass_requirments = (arr_yy['m_yyjj'] >= 350.0) & (arr_yy['dR_yy'] < 2.0) & (arr_yy['dR_jj'] < 2.0) & (arr_yy['dR_yyjj'] < 3.4)
low_mass_requirements= (arr_yy['m_yyjj'] < 350.0)
arr_yy = arr_yy[np.logical_or(high_mass_requirments, low_mass_requirements)]

yy_original_eventweight = arr_yy["eventweight"].sum()
arr_yy = split_by_index(arr_yy)[1]
yy_eventweight_scalefactor = yy_original_eventweight / arr_yy["eventweight"].sum()

intended_luminosity = 140459.56
arr_vbf = rescale_eventweight(arr_vbf, vbf_eventweight_scalefactor)
arr_ggf = rescale_eventweight(arr_ggf, ggf_eventweight_scalefactor)
arr_yy = rescale_eventweight(
    arr_yy, yy_eventweight_scalefactor * intended_luminosity / (36215.0 + 44307.4)
)


def classify(array, sig_clf_score, bkg_clf_score):
    # Pass both the classifiers
    logic_vbfsample = np.logical_and(
        vbf_ggf_clf.predict_proba(make_clf_compliant(array))[:, 1] > sig_clf_score,
        vbf_yy_clf.predict_proba(make_clf_compliant(array))[:, 1] > bkg_clf_score,
    )
    # and have at least 4 jets
    return_array = np.logical_and(logic_vbfsample, array["n_jet"] >= 4)
    return return_array


vbf_logic_vbfsample = classify(arr_vbf, sig_clf_score, bkg_clf_score)
vbf_logic_ggfsample = classify(arr_ggf, sig_clf_score, bkg_clf_score)
vbf_logic_yysample = classify(arr_yy, sig_clf_score, bkg_clf_score)

vbf_arrays = {
    "vbf": arr_vbf[vbf_logic_vbfsample],
    "ggf": arr_ggf[vbf_logic_ggfsample],
    "yy": arr_yy[vbf_logic_yysample],
}
vbf_weights = {
    "vbf": arr_vbf[vbf_logic_vbfsample]["eventweight"],
    "ggf": arr_ggf[vbf_logic_ggfsample]["eventweight"],
    "yy": arr_yy[vbf_logic_yysample]["eventweight"],
}

ggf_arrays = {
    "vbf": arr_vbf[np.logical_not(vbf_logic_vbfsample)],
    "ggf": arr_ggf[np.logical_not(vbf_logic_ggfsample)],
    "yy": arr_yy[np.logical_not(vbf_logic_yysample)],
}
ggf_weights = {
    "vbf": arr_vbf[np.logical_not(vbf_logic_vbfsample)]["eventweight"],
    "ggf": arr_ggf[np.logical_not(vbf_logic_ggfsample)]["eventweight"],
    "yy": arr_yy[np.logical_not(vbf_logic_yysample)]["eventweight"],
}
"""
for arr in vbf_arrays:
    print("VBF cat, ", arr, " sample count: ", vbf_arrays[arr].shape[0])
for arr in ggf_arrays:
    print("GGF cat, ", arr, " sample count: ", vbf_arrays[arr].shape[0])
"""

# Presuppositions up until this point. Major routines follow
# ------------------------------------------------------------------------
def plot_signficance_scaling():
    """ This routine scales the vbf cross section and evaluates the significance
        Better done by reoptimizing.
    """
    x_ax = []
    y = []
    for val in np.arange(1, 25):

        vbf_ew = val * vbf_arrays["vbf"]["eventweight"]
        ggf_ew = val * ggf_arrays["vbf"]["eventweight"]
        eachCategory_significance = []
        for cat in range(1, 5):
            all_ggf_categorized_events = np.concatenate(
                [ggf_arrays["vbf"], ggf_arrays["ggf"]]
            )
            inCategory_ggf = all_ggf_categorized_events[
                all_ggf_categorized_events["cutCategory"] == cat
            ]
            inCategory_yy = ggf_arrays["yy"][ggf_arrays["yy"]["cutCategory"] == cat]
            eachCategory_significance.append(
                asimov_significance(
                    inCategory_ggf["eventweight"].sum(),
                    inCategory_yy["eventweight"].sum(),
                )
            )

        signal_passing = np.concatenate([vbf_ew, ggf_ew])
        eachCategory_significance.append(
            asimov_significance(
                signal_passing.sum(), vbf_arrays["yy"]["eventweight"].sum()
            )
        )
        this_cut_vbf_category_significance = sqrt(
            sum([x ** 2 for x in eachCategory_significance])
        )

        print(
            "VBF x-sec "
            + str(val)
            + " gives sig "
            + str(this_cut_vbf_category_significance)
        )
        x_ax.append(val)
        y.append(this_cut_vbf_category_significance)

    plt.plot(x_ax, y, "o")
    plt.ylabel("Asimov Significance")
    plt.xlabel("VBF Ehancement (multiples of SM)")
    plt.tight_layout()
    plt.savefig("crossection_scale.png")


def plot_histograms(array_collection, weight_collection, category):
    """ This routine plots histograms after classification"""

    # Predefined stuff
    # //---------------
    names = [
        "m_yy",
        "jj_deta",
        "mjj",
        "n_jet",
        "tagregion",
        "cutCategory",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
    ]
    axes_titles = [
        "$m_{\gamma\gamma}$ [GeV]",
        "$\Delta\eta_{jj}$",
        "$m_{jj}$",
        "Jet Multiplicity",
        "Tag Region",
        "Cut Category",
        "$m_{\gamma\gamma jj}$ [GeV]",
        "$\Delta R_{\gamma,\gamma}$",
        "$\Delta R_{j,j}$",
        "$\Delta R_{\gamma\gamma,jj}$",
        "$j_{1}$ Tagbin",
        "$j_{2}$ Tagbin",
    ]

    bin_dict = {
        "n_jet": np.arange(1.5, 10.5, 1),
        "m_yyjj": np.linspace(start=0, stop=1500, num=35),
        "mjj": np.linspace(start=0, stop=3000, num=35),
        "dR_jj": np.arange(0, 5, 0.25),
        "dR_yy": np.arange(0, 4, 0.25),
        "dR_yyjj": np.arange(0, 8, 0.5),
        "jj_deta": np.arange(0, 10, 0.5),
        "cutCategory": np.arange(1, 6, 1),
        "j1_tagbin": np.arange(1, 6, 1),
        "j2_tagbin": np.arange(1, 6, 1),
    }
    if category == "vbf":
        label_dict = {
            "vbf": "VBF HH Classified",
            "ggf": "ggF HH Misclassified",
            "yy": "$\gamma\gamma$-continuum",
        }
        annotation = "VBF Category"
    elif category == "ggf":
        label_dict = {
            "vbf": "VBF HH Misclassified",
            "ggf": "ggF HH Classified",
            "yy": "$\gamma\gamma$-continuum",
        }
        annotation = "ggF Category"

    # Main Function
    # //---------------
    for distribution, title in zip(names, axes_titles):

        fig = plt.figure()

        if distribution in bin_dict:
            bins = bin_dict[distribution]
        else:
            bins = np.linspace(
                start=min(
                    [
                        array_collection["vbf"][distribution].min(),
                        array_collection["ggf"][distribution].min(),
                        array_collection["yy"][distribution].min(),
                    ]
                ),
                stop=max(
                    [
                        array_collection["vbf"][distribution].max(),
                        array_collection["ggf"][distribution].max(),
                        array_collection["yy"][distribution].max(),
                    ]
                ),
                num=25,
            )
            # take care of single value error case
            if len(np.unique(np.array(bins))) == 1:
                v = np.unique(np.array(bins))[0]
                bins = [v - 1, v, v + 1, v + 2]

        y1, x1, _1 = plt.hist(
            array_collection["vbf"][distribution],
            weights=weight_collection["vbf"],
            bins=bins,
            histtype="step",
            color="firebrick",
            label=label_dict["vbf"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["vbf"].sum(),
                array_collection["vbf"][distribution].shape[0],
            ),
            density=True,
        )
        y2, x2, _2 = plt.hist(
            array_collection["ggf"][distribution],
            weights=weight_collection["ggf"],
            bins=bins,
            histtype="step",
            color="cornflowerblue",
            label=label_dict["ggf"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["ggf"].sum(),
                array_collection["ggf"][distribution].shape[0],
            ),
            density=True,
        )
        y3, x3, _2 = plt.hist(
            array_collection["yy"][distribution],
            weights=weight_collection["yy"],
            bins=bins,
            histtype="step",
            color="indigo",
            label=label_dict["yy"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["yy"].sum(),
                array_collection["yy"][distribution].shape[0],
            ),
            density=True,
        )
        plt.ylim(ymin=0, ymax=1.35 * max([y1.max(), y2.max(), y3.max()]))
        plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
        plt.xlabel(title, horizontalalignment="right", x=1.0)
        plt.legend(frameon=False, fontsize=10)
        plt.annotate(
            annotation,
            xy=(0.02, 0.93),
            xycoords="axes fraction",
            ha="left",
            fontsize=16,
        )
        plt.tight_layout()
        plt.savefig(sourcedir + "../run/" + category + "_" + distribution)
        plt.close()


def generate_cutflow(array_collection, category, sig_score, bkg_score):
    if category == "vbf":
        annotation = "VBF HH Sample"
    elif category == "ggf":
        annotation = "ggF HH Sample"
    elif category == "yy":
        annotation = "$\gamma\gamma$-Continuum"

    cutflow_values = []

    # Initial Values
    cutflow_values.append(array_collection["eventweight"].sum())

    # Apply 4 Jet Cut
    cut1_array = array_collection[array_collection["n_jet"] >= 4]
    cutflow_values.append(cut1_array["eventweight"].sum())

    # Pass Signal Classifier
    cut2_logic = (
        vbf_ggf_clf.predict_proba(make_clf_compliant(cut1_array))[:, 1] > sig_score
    )
    cut2_array = cut1_array[cut2_logic]
    cutflow_values.append(cut2_array["eventweight"].sum())

    # Pass Background Classifier
    cut3_logic = (
        vbf_yy_clf.predict_proba(make_clf_compliant(cut2_array))[:, 1] > bkg_score
    )
    cut3_array = cut2_array[cut3_logic]
    cutflow_values.append(cut3_array["eventweight"].sum())

    x = range(0, len(cutflow_values))
    bin_names = [
        "Initial",
        "nJet $\geq$ 4",
        "Signal BDT > {0}".format(sig_score),
        "Bkg BDT > {0}".format(bkg_score),
    ]
    fig = plt.figure()
    ax = plt.gca()
    plt.bar(x=x, height=cutflow_values)
    plt.xticks(x, bin_names, rotation=30)
    plt.xlabel("Cut", fontsize=16)
    plt.ylabel("Events", fontsize=16)
    plt.annotate(
        annotation, xy=(0.98, 0.93), xycoords="axes fraction", ha="right", fontsize=16
    )

    labels = [
        "$\epsilon_{abs}=$%.3f" % (cut / cutflow_values[0]) for cut in cutflow_values
    ]
    relative_cuts = [1]
    for i in range(len(cutflow_values) - 1):
        relative_cuts.append(float(cutflow_values[i + 1]) / cutflow_values[i])
        rel_labels = ["$\epsilon_{rel}=$%.3f" % cut for cut in relative_cuts]

    rects = ax.patches
    for rect, label, rel_label in zip(rects, labels, rel_labels):
        if rects.index(rect) == 0:
            continue
        height = rect.get_height()
        ax.text(
            rect.get_x() + rect.get_width() / 2,
            ax.get_ylim()[1] * 0.01 + (height),
            label,
            ha="center",
            va="bottom",
        )
        ax.text(
            rect.get_x() + rect.get_width() / 2,
            ax.get_ylim()[1] * 0.05 + (height),
            rel_label,
            ha="center",
            va="bottom",
        )

    plt.tight_layout()
    plt.savefig(sourcedir + "../run/" + category + "_cutflow")
    plt.close()


def make_response_curve(arr, category):
    if category == "vbf":
        annotation = "VBF HH Sample"
    elif category == "ggf":
        annotation = "ggF HH Sample"
    elif category == "yy":
        annotation = "$\gamma\gamma$-Continuum"

    sig_probabilities = vbf_ggf_clf.predict_proba(make_clf_compliant(arr))[:, 1]
    bkg_probabilities = vbf_yy_clf.predict_proba(make_clf_compliant(arr))[:, 1]

    all_probabilities = np.concatenate([sig_probabilities, bkg_probabilities])
    bins = np.arange(all_probabilities.min(), all_probabilities.max(), 0.05)

    fig = plt.figure()
    ax = plt.gca()
    y_sig, x_sig, _sig = plt.hist(
        sig_probabilities,
        weights=arr["eventweight"],
        bins=bins,
        histtype="step",
        color="cornflowerblue",
        label="Signal BDT",
        density=True,
    )

    y_bkg, x_bkg, _bkg = plt.hist(
        bkg_probabilities,
        weights=arr["eventweight"],
        bins=bins,
        histtype="step",
        color="firebrick",
        label="Background BDT",
        density=True,
    )

    plt.ylim(ymin=0, ymax=1.3 * max([y_sig.max(), y_bkg.max()]))
    plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
    plt.xlabel("Classifier Score", horizontalalignment="right", x=1.0)
    plt.annotate(
        annotation, xy=(0.02, 0.80), xycoords="axes fraction", ha="left", fontsize=16
    )
    # Draw line at cuts
    if bkg_clf_score == sig_clf_score:
        ax.axvline(bkg_clf_score, color="forestgreen", linestyle="--", label="BDT Cuts")
    else:
        ax.axvline(
            sig_clf_score,
            color="cornflowerblue",
            linestyle="--",
            label="Signal BDT Cut",
        )  # match sig color
        ax.axvline(
            bkg_clf_score, color="firebrick", linestyle="--", label="Bkg BDT Cut"
        )  # match bkg color

    plt.legend(loc="best", ncol=2)
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles=reversed(handles), labels=reversed(labels), loc="best", ncol=2)
    plt.tight_layout()
    plt.savefig(sourcedir + "../run/" + category + "_classifierScore")
    plt.close()


def build_event_dataframes(ggfonly_event_object, vbfcategorized_event_object):

    d_ggf = OrderedDict(
        {
            "Initial": ggfonly_event_object.ggf,
            "After Cuts": vbfcategorized_event_object.ggf,
        }
    )

    d_vbf = OrderedDict(
        {
            "Initial": ggfonly_event_object.vbf,
            "After Cuts": vbfcategorized_event_object.vbf,
        }
    )

    d_yy = OrderedDict(
        {
            "Initial": ggfonly_event_object.yy,
            "After Cuts": vbfcategorized_event_object.yy,
        }
    )
    df_ggf = pd.DataFrame(d_ggf)
    df_vbf = pd.DataFrame(d_vbf)
    df_yy = pd.DataFrame(d_yy)

    for df in [df_ggf, df_vbf, df_yy]:
        df["Change"] = df["Initial"] - df["After Cuts"]
        df["% Change"] = 100 * (df["After Cuts"] - df["Initial"]) / df["Initial"]

    df_ggf = df_ggf.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )
    df_vbf = df_vbf.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )
    df_yy = df_yy.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )

    for df in [df_ggf, df_vbf, df_yy]:
        df["Category"] = [
            "Category 1",
            "Category 2",
            "Category 3",
            "Category 4",
            "VBF Category",
        ]
        df = df.set_index("Category", inplace=True)

    return df_ggf, df_vbf, df_yy


def post_cut_significance(
    arr_vbf,
    arr_ggf,
    arr_yy,
    sig_clf_score,
    bkg_clf_score,
    to_screen=False,
    table_name=False,
):
    # Evaluate
    vbf_passing_cut = arr_vbf[classify(arr_vbf, sig_clf_score, bkg_clf_score)]
    ggf_passing_cut = arr_ggf[classify(arr_ggf, sig_clf_score, bkg_clf_score)]
    signal_passing = np.concatenate([vbf_passing_cut, ggf_passing_cut])
    yy_passing_cut = arr_yy[classify(arr_yy, sig_clf_score, bkg_clf_score)]

    vbf_failing_cut = arr_vbf[
        np.logical_not(classify(arr_vbf, sig_clf_score, bkg_clf_score))
    ]
    ggf_failing_cut = arr_ggf[
        np.logical_not(classify(arr_ggf, sig_clf_score, bkg_clf_score))
    ]
    yy_failing_cut = arr_yy[
        np.logical_not(classify(arr_yy, sig_clf_score, bkg_clf_score))
    ]

    # Find ggF-only Cuts significance
    eachCategory_significance = []
    ggf_sample_events = []
    vbf_sample_events = []
    yy_sample_events = []

    for cat in range(1, 5):
        inCategory_ggf = ggf_failing_cut[ggf_failing_cut["cutCategory"] == cat]
        inCategory_yy = yy_failing_cut[yy_failing_cut["cutCategory"] == cat]
        inCategory_vbf = vbf_failing_cut[vbf_failing_cut["cutCategory"] == cat]
        inCategory_signal = np.concatenate([inCategory_ggf, inCategory_vbf])
        eachCategory_significance.append(
            asimov_significance(
                inCategory_signal["eventweight"].sum(),
                inCategory_yy["eventweight"].sum(),
            )
        )
        if to_screen:
            print(
                "Category: "
                + str(cat)
                + " significance: "
                + str(eachCategory_significance[cat - 1])
                + "  ggF events: "
                + str(inCategory_ggf["eventweight"].sum())
                + "  VBF events: "
                + str(inCategory_vbf["eventweight"].sum())
                + "  yy events: "
                + str(inCategory_yy["eventweight"].sum())
            )
        # Append to lists for named tuple
        ggf_sample_events.append(inCategory_ggf["eventweight"].sum())
        vbf_sample_events.append(inCategory_vbf["eventweight"].sum())
        yy_sample_events.append(inCategory_yy["eventweight"].sum())

    # Calculate VBF category significance
    vbf_category_significance = asimov_significance(
        signal_passing["eventweight"].sum(), yy_passing_cut["eventweight"].sum()
    )
    if to_screen:
        print(
            "VBF Category: "
            + " significance: "
            + str(vbf_category_significance)
            + "  ggF events: "
            + str(ggf_passing_cut["eventweight"].sum())
            + "  VBF events: "
            + str(vbf_passing_cut["eventweight"].sum())
            + "  yy events: "
            + str(yy_passing_cut["eventweight"].sum())
        )
    ggf_sample_events.append(ggf_passing_cut["eventweight"].sum())
    vbf_sample_events.append(vbf_passing_cut["eventweight"].sum())
    yy_sample_events.append(yy_passing_cut["eventweight"].sum())

    # Construct Named tuple
    info_object = namedtuple("info_object", ["ggf", "vbf", "yy", "sig_cut", "bkg_cut"])
    info_object.ggf = ggf_sample_events
    info_object.vbf = vbf_sample_events
    info_object.yy = yy_sample_events
    info_object.sig_cut = sig_clf_score
    info_object.bkg_cut = bkg_clf_score

    eachCategory_significance.append(vbf_category_significance)
    significance = sqrt(sum([x ** 2 for x in eachCategory_significance]))
    return significance, info_object


def get_cut_for_rejection(background_array, bdt, bkg_rej):
    """ Function to get the BDT cut size for given background rejection """

    probabilities = bdt.predict_proba(make_clf_compliant(background_array))[:, 1]

    # Iterate over cuts on BDT
    cut_values = []
    rejection_values = []
    for cut_value in np.arange(probabilities.min(), probabilities.max(), step=0.01):
        # Evaluate background rejection at this point

        entries_pass_this_cut = background_array[probabilities > cut_value]
        events_pass_this_cut = entries_pass_this_cut["eventweight"].sum()
        all_events = background_array["eventweight"].sum()

        percent_rejection = 1 - events_pass_this_cut / all_events
        rejection_values.append(percent_rejection)
        cut_values.append(cut_value)

    # Find closest value
    subtracted_values = [abs(x - bkg_rej) for x in rejection_values]
    closest_arg = np.argmin(subtracted_values)
    associated_cut = cut_values[closest_arg]

    return associated_cut


def cuts_with_preselection(arr_ggf, arr_yy, bkg_rejection, vbf_ggf_clf, vbf_yy_clf):
    """ Implements cutflow appropriately then passes on classifier score """

    # Apply njets cut to ggF
    ggf_passing_njets = arr_ggf[arr_ggf["n_jet"] >= 4]
    # Find x percent of those
    evaluated_ggfclf_score = round(
        get_cut_for_rejection(ggf_passing_njets, vbf_ggf_clf, bkg_rejection), 2
    )

    # Apply both clf score and njets cut to yy sample
    b_njets_yy = arr_yy["n_jet"] >= 4
    b_ggfclf_yy = (
        vbf_ggf_clf.predict_proba(make_clf_compliant(arr_yy))[:, 1]
        > evaluated_ggfclf_score
    )
    yy_passing_previous_cuts = arr_yy[np.logical_and(b_njets_yy, b_ggfclf_yy)]
    evaluated_yyclf_score = round(
        get_cut_for_rejection(yy_passing_previous_cuts, vbf_yy_clf, bkg_rejection), 2
    )

    return evaluated_ggfclf_score, evaluated_yyclf_score


def build_cuts_table(arr_vbf, arr_ggf, arr_yy, vbf_ggf_clf, vbf_yy_clf, filename):
    """ Basically does all the functions in this script in an iterative fashion
        Build pandas dataframe and then output as latex or CSV
    """
    # Define a bunch of lists to be added to a dictionary later
    background_rejections = []
    ggf_scores = []
    yy_scores = []
    significance = []

    # Iterate over some background rejection levels
    for background_rejection in np.arange(0.7, 1.0, step=0.05):

        # Evaluate
        evaluated_ggfclf_score, evaluated_yyclf_score = cuts_with_preselection(
            arr_ggf, arr_yy, background_rejection, vbf_ggf_clf, vbf_yy_clf
        )
        sig = post_cut_significance(
            arr_vbf, arr_ggf, arr_yy, evaluated_ggfclf_score, evaluated_yyclf_score
        )[0]

        # Fill Lists
        background_rejections.append(background_rejection)
        ggf_scores.append(evaluated_ggfclf_score)
        yy_scores.append(evaluated_yyclf_score)
        significance.append(sig)

    # Build Dictionary
    d = {
        "Bkg Rejection": background_rejections,
        "Significance": [round(x, 3) for x in significance],
        "Signal BDT Cut": ggf_scores,
        "Bkg BDT Cut": yy_scores,
    }

    # Make Pandas Dataframe
    df = pd.DataFrame(d)
    df = df[["Bkg Rejection", "Significance", "Signal BDT Cut", "Bkg BDT Cut"]]
    if ".tex" in filename:
        with open(filename, "w+") as f:
            df.to_latex(buf=f, index=False)
    elif ".csv" in filename:
        df.to_csv(filename)

    df.plot(
        y="Significance",
        x="Bkg Rejection",
        linestyle="None",
        marker="o",
        color="firebrick",
        legend=None,
        figsize=(12, 4),
    )
    plt.xlabel("Background Rejection", fontsize=18)
    plt.ylabel("Asimov Significance", fontsize=18)
    plt.gca().tick_params(axis="both", labelsize=12)
    plt.tight_layout()
    plt.savefig("run/significance_scaling.pdf")


if __name__ == "__main__":

    # TODO: Update these to work with configured cuts
    plot_histograms(vbf_arrays, vbf_weights, "vbf")
    plot_histograms(ggf_arrays, ggf_weights, "ggf")
    plot_signficance_scaling()

    make_response_curve(arr_vbf, "vbf")
    make_response_curve(arr_ggf, "ggf")
    make_response_curve(arr_yy, "yy")

    # Percentage evaluation
    bkg_rejection = 0.85

    evaluated_ggfclf_score, evaluated_yyclf_score = cuts_with_preselection(
        arr_ggf, arr_yy, bkg_rejection, vbf_ggf_clf, vbf_yy_clf
    )
    print(
        "For {0} bkg rejection on ggF Classifier, cut at {1:.2f}".format(
            bkg_rejection, evaluated_ggfclf_score
        )
    )
    print(
        "For {0} bkg rejection on yy Classifier, cut at {1:.2f}".format(
            bkg_rejection, evaluated_yyclf_score
        )
    )

    if hand_picked_scores:
        print("Using user input scores of {0} signal cut, {1} background".format(sig_clf_score, bkg_clf_score))
        evaluated_ggfclf_score = sig_clf_score
        evaluated_yyclf_score = bkg_clf_score

    generate_cutflow(arr_vbf, "vbf", evaluated_ggfclf_score, evaluated_yyclf_score)
    generate_cutflow(arr_ggf, "ggf", evaluated_ggfclf_score, evaluated_yyclf_score)
    generate_cutflow(arr_yy, "yy", evaluated_ggfclf_score, evaluated_yyclf_score)

    print("No Cuts: ")
    no_cut_sig, no_cut_event_breakdown = post_cut_significance(
        arr_vbf,
        arr_ggf,
        arr_yy,
        1.0,
        1.0,  # Effectively don't cut on either
        to_screen=True,
    )
    print("Yields significance: {0:.3f}".format(no_cut_sig))

    print(
        "Cuts at {0} signal clf and {1} bkg clf ".format(
            evaluated_ggfclf_score, evaluated_yyclf_score
        )
    )
    sig, event_breakdown = post_cut_significance(
        arr_vbf,
        arr_ggf,
        arr_yy,
        evaluated_ggfclf_score,
        evaluated_yyclf_score,
        to_screen=True,
    )
    print("Yields significance: {0:.3f}".format(sig))

    # Make Event Change tables
    df_ggf, df_vbf, df_yy = build_event_dataframes(
        no_cut_event_breakdown, event_breakdown
    )
    with open("run/events_tables.tex", "w+") as f:
        f.write("ggF Table\n")
        df_ggf.to_latex(buf=f)
        f.write("\nVBF Table\n")
        df_vbf.to_latex(buf=f)
        f.write("\nyy Table\n")
        df_yy.to_latex(buf=f)

    # Clean up table
    # TODO: Streamline this better...
    with open("run/events_tables.tex", "r") as f:
        filedata = f.readlines()

    with open("run/events_tables.tex", "w") as f:
        for l in filedata:
            if "begin{tabular}" in l:
                l += "\multicolumn{5}{c}{TYPE Sample}\\\\ \n"
            if "inf" in l:
                l = l.replace("inf", "")
            if "00000" in l:
                l = l.replace("00000", "")
            f.write(l)

    table_file = "run/cuts_table.tex"
    print("Building Cuts Table in File: {0}".format(table_file))
    build_cuts_table(arr_vbf, arr_ggf, arr_yy, vbf_ggf_clf, vbf_yy_clf, table_file)
