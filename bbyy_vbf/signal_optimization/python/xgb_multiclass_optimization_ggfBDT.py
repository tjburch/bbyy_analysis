from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
from tqdm import tqdm
import matplotlib.pyplot as plt
import pickle
from xgboost import XGBClassifier, DMatrix
from sklearn.preprocessing import StandardScaler
from utils import *
import pdb 

def add_cols(arr, classifier):
    """ Adds predictions and baseline passing """
    # Predictions
    predictions =  classifier.predict_proba(make_clf_compliant(arr))
    vbfProb = predictions[:,0]
    ggfProb = predictions[:,1]
    yyProb = predictions[:,2]
    tthProb = predictions[:,3]
    arr = append_fields(
        arr, 
        data=[vbfProb, ggfProb, yyProb, tthProb],
        names=["vbfProb", "ggfProb", "yyProb", "tthProb"]
    )

    # Qualifiers
    ggfPassed = arr["cutCategory"] > 0
    ggfPassed = np.logical_or.reduce(
        [
        arr["cutCategory"] > 0,
        arr["bdtcat_HH"] > 0,
        arr["bdtcat_HH_lowmass"] > 0,
        ]
    )    
    vbfPassed = arr["jj_deta"] >= 0
    arr = append_fields(
        arr, 
        data=[ggfPassed, vbfPassed],
        names=["ggfPassed", "vbfPassed"],
    )
    return arr


def main():

    # Load Samples
    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    print("Loading Samples")
    # Setup VBF samples, common between both signal and background
    vbf_files = [sourcedir + fname for fname in  ["bdtscore_vbf_mc16a.npy","bdtscore_vbf_mc16d.npy","bdtscore_vbf_mc16e.npy"]]
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)

    ## ggF samples
    ggf_files = [sourcedir + fname for fname in  ["bdtscore_ggf_mc16a.npy","bdtscore_ggf_mc16d.npy","bdtscore_ggf_mc16e.npy"]]
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)        
    arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)

    ## yy samples
    yy_files = [sourcedir + fname for fname in  ["bdtscore_yy_mc16a.npy","bdtscore_yy_mc16d.npy","bdtscore_yy_mc16e.npy"]]
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)

    # NTNI samples
    #ntni_files = [sourcedir + fname for fname in  ["bdtscore_ntni_mc16a.npy", "bdtscore_ntni_mc16d.npy", "bdtscore_ntni_mc16e.npy"]]
    #arr_ntni_test, ntni_eventweight_scalefactor_test = load_samples(ntni_files, ggf_preselection=True, test=True, myyCut=False)
    #yy_scaled_weight = arr_yy_test["eventweight"].sum()
    ## Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    #total_continuum_yield = yy_scaled_weight / 0.84
    #ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni_test["eventweight"].sum()
    #arr_ntni_test = rescale_eventweight(arr_ntni_test, ntni_fractional_reweight)

    # Single Higgs
    tth_files = [sourcedir + fname for fname in  ["bdtscore_TTH_mc16a.npy","bdtscore_TTH_mc16d.npy","bdtscore_TTH_mc16e.npy"]]
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)

    ggh_files = [sourcedir + fname for fname in  ["bdtscore_GGH_mc16a.npy","bdtscore_GGH_mc16d.npy","bdtscore_GGH_mc16e.npy"]]
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
    arr_ggh_test = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)

    zh_files = [sourcedir + fname for fname in  ["bdtscore_ZH_mc16a.npy","bdtscore_ZH_mc16d.npy","bdtscore_ZH_mc16e.npy"]]
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
    arr_zh_test = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)

    # Print checks
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % arr_vbf_test["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf_test["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % arr_yy_test["eventweight"].sum())
    #print("Rescaled NTNI Bkg yield: %.4f" % arr_ntni_test["eventweight"].sum())
    print("-----Single Higgs--------")
    print("Total ttH yield: %.4f" % arr_tth_test["eventweight"].sum())
    print("Total ggH yield: %.4f" % arr_ggh_test["eventweight"].sum())
    print("Total ZH yield: %.4f" % arr_zh_test["eventweight"].sum())

    # Join Signal and Background
    arr_signals = np.concatenate([arr_vbf_test, arr_ggf_test])
    arr_backgrounds = np.concatenate([arr_yy_test, arr_tth_test, arr_ggh_test, arr_zh_test])

    # Get original Significance
    category_significance = []
    for col in ["bdtcat_HH", "bdtcat_HH_lowmass"]:
        for val in [1,2]:
            inCategory_sig = arr_signals[arr_signals[col] == val]
            inCategory_bkg = arr_backgrounds[arr_backgrounds[col] == val]
            category_significance.append(asimov_significance(
                s=inCategory_sig["eventweight"].sum(),
                b=inCategory_bkg["eventweight"].sum(),
                ))
    baseline_significance = sqrt(sum([x ** 2 for x in category_significance]))
    print("Initial Significance: {0}".format(baseline_significance))


    # Load Classifer, apply
    print("Loading Classifier")
    xgb_multiclassifier = pickle.load(open("saved_classifiers/xgb_multiclass.dat", "rb"))
    print("Applying Classifier")
    arr_signals = add_cols(arr_signals, xgb_multiclassifier)
    arr_backgrounds = add_cols(arr_backgrounds, xgb_multiclassifier)

    # scale luminosity
    arr_signals["eventweight"] = arr_signals["eventweight"] * 10
    arr_backgrounds["eventweight"] = arr_backgrounds["eventweight"] * 10

    # 4D Scan
    print("Beginning Scan")
    granularity = 0.1
    maxval = 1.0
    nVals = (maxval/granularity)**4
    pbar = tqdm(total=nVals)
    best_significance = 0
    cuts = [0,0,0,0]
    for cut0 in np.arange(0, maxval, granularity):
        for cut1 in np.arange(0, maxval, granularity):
            for cut2 in np.arange(0, maxval, granularity):
                for cut3 in np.arange(0, maxval, granularity):
                    # Generate VBF category
                    #pdb.set_trace()
                    vbfcat_bool_signal = np.logical_and.reduce([
                        arr_signals["vbfPassed"],
                        arr_signals["ggfPassed"],
                        arr_signals["vbfProb"] > cut0,
                        arr_signals["ggfProb"] < cut1,
                        arr_signals["yyProb"] < cut2,
                        arr_signals["tthProb"] < cut3,]
                    )
                    vbfcat_bool_background = np.logical_and.reduce([
                        arr_backgrounds["vbfPassed"],
                        arr_backgrounds["ggfPassed"],
                        arr_backgrounds["vbfProb"] > cut0,
                        arr_backgrounds["ggfProb"] < cut1,
                        arr_backgrounds["yyProb"] < cut2,
                        arr_backgrounds["tthProb"] < cut3,]
                    )
                    # Calculate Significance of that category
                    vbf_category_significance = asimov_significance(
                        s =arr_signals[vbfcat_bool_signal]["eventweight"].sum(),
                        b =arr_backgrounds[vbfcat_bool_background]["eventweight"].sum(),                        
                        )
                    
                    # Generate ggF Categories
                    ggfcat_bool_signal = np.logical_and(
                        ~vbfcat_bool_signal,
                        arr_signals["ggfPassed"],
                    )
                    ggfcat_bool_background = np.logical_and(
                        ~vbfcat_bool_background,
                        arr_backgrounds["ggfPassed"],
                    )
                    
                    category_significance = []  
                    for col in ["bdtcat_HH", "bdtcat_HH_lowmass"]:
                        for val in [1,2]:                  
                            inCategory_sig = arr_signals[np.logical_and(ggfcat_bool_signal, arr_signals[col] == val)]
                            inCategory_bkg = arr_backgrounds[np.logical_and(ggfcat_bool_background, arr_backgrounds[col] == val)]
                            category_significance.append(asimov_significance(
                                s=inCategory_sig["eventweight"].sum(),
                                b=inCategory_bkg["eventweight"].sum(),
                                ))


                    if vbf_category_significance != 0:
                        category_significance.append(vbf_category_significance)
                    if len(category_significance) > 0:
                        this_significance = sqrt(sum([x ** 2 for x in category_significance]))
                    else:
                        this_significance = 0

                    if this_significance > best_significance:
                        best_significance = this_significance
                        cuts = [cut0, cut1, cut2, cut3]
                    pbar.update(1)


    print("Original significance: {0}".format(round(baseline_significance,3)))
    print("Best significance: {0}".format(round(best_significance,3)))
    print("Cuts: {0}".format(cuts))
    print("Percent Improvement: {0}".format(round(100*(best_significance-baseline_significance)/baseline_significance, 3)))

if __name__ == "__main__":
    main()