from math import sqrt
import numpy as np
import matplotlib.pyplot as plt
from joblib import load
from utils import rescale_eventweight
import matplotlib.style as style
import pandas as pd
from collections import namedtuple, OrderedDict
from utils import reconstruct_categories
from xgboost import XGBClassifier, DMatrix
import pickle
from utils import *

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-z", "--zeroTag", help="Do Zero Tag", action="store_true")
args = parser.parse_args()

sig_clf_score = 0.72
bkg_clf_score = 0.39

names = [
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "planarFlow"
]


# Global define sample names
sourcedir = (
    "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
)
vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
yy_mc16e = sourcedir + "forcuts_yy_mc16e.npy"

ntni1516 = sourcedir + "forcuts_ntni_mc16a.npy"
ntni17 = sourcedir + "forcuts_ntni_mc16d.npy"
ntni18 = sourcedir + "forcuts_ntni_mc16e.npy"

tth_mc16a = sourcedir + "forcuts_TTH_mc16a.npy"
tth_mc16d = sourcedir + "forcuts_TTH_mc16d.npy"
tth_mc16e = sourcedir + "forcuts_TTH_mc16e.npy"

ggh_mc16a = sourcedir + "forcuts_GGH_mc16a.npy"
ggh_mc16d = sourcedir + "forcuts_GGH_mc16d.npy"
ggh_mc16e = sourcedir + "forcuts_GGH_mc16e.npy"

zh_mc16a = sourcedir + "forcuts_ZH_mc16a.npy"
zh_mc16d = sourcedir + "forcuts_ZH_mc16d.npy"
zh_mc16e = sourcedir + "forcuts_ZH_mc16e.npy"

vbfh_mc16a = sourcedir + "forcuts_VBFH_mc16a.npy"
vbfh_mc16d = sourcedir + "forcuts_VBFH_mc16d.npy"
vbfh_mc16e = sourcedir + "forcuts_VBFH_mc16e.npy"

# Load BDTs
vbf_ggf_classifier_name = "saved_classifiers/ggf_xgb.dat"
vbf_yy_classifier_name = "saved_classifiers/yy_xgb.dat"

vbf_ggf_clf = pickle.load(open(vbf_ggf_classifier_name, "rb"))
vbf_yy_clf = pickle.load(open(vbf_yy_classifier_name, "rb"))


# Load samples
# VBF
arr_vbf, vbf_eventweight_scalefactor = load_samples([vbf_mc16a, vbf_mc16d, vbf_mc16e], ggf_preselection=True)

# GGF
arr_ggf, ggf_eventweight_scalefactor = load_samples([ggf_mc16a, ggf_mc16d, ggf_mc16e], ggf_preselection=True)

# Sherpa yy continuum
arr_yy, yy_eventweight_scalefactor = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], myyCut=False, ggf_preselection=True)

# NTNI photons
arr_ntni, ntni_eventweight_scalefactor = load_samples([ntni1516, ntni17, ntni18], myyCut=False, ggf_preselection=True)

# Single Higgs
arr_tth, tth_eventweight_scalefactor = load_samples([tth_mc16a, tth_mc16d, tth_mc16e], ggf_preselection=True)
arr_ggh, ggh_eventweight_scalefactor = load_samples([ggh_mc16a, ggh_mc16d, ggh_mc16e], ggf_preselection=True)
arr_zh, zh_eventweight_scalefactor = load_samples([zh_mc16a, zh_mc16d, zh_mc16e], ggf_preselection=True)
arr_vbfh, vbfh_eventweight_scalefactor = load_samples([vbfh_mc16a, vbfh_mc16d, vbfh_mc16e], ggf_preselection=True)

# Get background weight and do transformations for methods to improve bkg stats
arr_yy = rescale_eventweight(arr_yy, yy_eventweight_scalefactor)
yy_scaled_weight = arr_yy["eventweight"].sum()

# 0-tag data
if args.zeroTag:
    arr_yy0tag, yy0tag_eventweight_scalefactor = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], ggf_preselection=False, myyCut=False, tagReq=0)
    arr_yy0tag = rand_sample_tagbin(model_array=arr_yy, replace_array=arr_yy0tag, plot=True)
    categories = reconstruct_categories(arr_yy0tag, btagging=False)
    arr_yy0tag["cutCategory"] = categories
    arr_yy0tag = arr_yy0tag[arr_yy0tag["cutCategory"] > 0]

    yy_tag_reweight =  yy_scaled_weight / arr_yy0tag["eventweight"].sum()
    arr_yy0tag = rescale_eventweight(arr_yy0tag,  yy_tag_reweight)
    arr_yy = rebalance_categories(arr_yy, arr_yy0tag)

# Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
total_continuum_yield = yy_scaled_weight / 0.84
ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni["eventweight"].sum()



arr_vbf = rescale_eventweight(arr_vbf, vbf_eventweight_scalefactor)
arr_ggf = rescale_eventweight(arr_ggf, ggf_eventweight_scalefactor)
arr_ntni = rescale_eventweight(arr_ntni, ntni_fractional_reweight)
arr_tth = rescale_eventweight(arr_tth, tth_eventweight_scalefactor)
arr_ggh = rescale_eventweight(arr_ggh, ggh_eventweight_scalefactor)
arr_zh = rescale_eventweight(arr_zh, zh_eventweight_scalefactor)
arr_vbfh = rescale_eventweight(arr_vbfh, vbfh_eventweight_scalefactor)
# Make single higgs array where type doesn't matter
arr_singleHiggs = np.concatenate([arr_tth, arr_ggh, arr_zh, arr_vbfh])

all_signal = np.concatenate([arr_ggf, arr_vbf])
all_background = np.concatenate([arr_yy, arr_ntni, arr_singleHiggs])

def classify(array, sig_clf_score, bkg_clf_score):
    # Pass both the classifiers
    formatted_array = make_clf_compliant(array)

    logic_vbfsample = np.logical_and(
        vbf_ggf_clf.predict_proba(formatted_array)[:,1] > sig_clf_score,
        vbf_yy_clf.predict_proba(formatted_array)[:,1] > bkg_clf_score,
    )
    # and have at least 4 jets
    #return_array = np.logical_and(logic_vbfsample, array["n_jet"] >= 4)
    return_array = np.logical_and(logic_vbfsample, array["jj_deta"] >= 0)
    return return_array


vbf_logic_vbfsample = classify(arr_vbf, sig_clf_score, bkg_clf_score)
vbf_logic_ggfsample = classify(arr_ggf, sig_clf_score, bkg_clf_score)
vbf_logic_yysample = classify(arr_yy, sig_clf_score, bkg_clf_score)
vbf_logic_ntnisample =classify(arr_ntni, sig_clf_score, bkg_clf_score)

vbf_arrays = {
    "vbf": arr_vbf[vbf_logic_vbfsample],
    "ggf": arr_ggf[vbf_logic_ggfsample],
    "yy": arr_yy[vbf_logic_yysample],
    "ntni": arr_ntni[vbf_logic_ntnisample],
}
vbf_weights = {
    "vbf": arr_vbf[vbf_logic_vbfsample]["eventweight"],
    "ggf": arr_ggf[vbf_logic_ggfsample]["eventweight"],
    "yy": arr_yy[vbf_logic_yysample]["eventweight"],
    "ntni": arr_ntni[vbf_logic_ntnisample]["eventweight"],
}

ggf_arrays = {
    "vbf": arr_vbf[np.logical_not(vbf_logic_vbfsample)],
    "ggf": arr_ggf[np.logical_not(vbf_logic_ggfsample)],
    "yy": arr_yy[np.logical_not(vbf_logic_yysample)],
    "ntni": arr_ntni[np.logical_not(vbf_logic_ntnisample)],
}
ggf_weights = {
    "vbf": arr_vbf[np.logical_not(vbf_logic_vbfsample)]["eventweight"],
    "ggf": arr_ggf[np.logical_not(vbf_logic_ggfsample)]["eventweight"],
    "yy": arr_yy[np.logical_not(vbf_logic_yysample)]["eventweight"],
    "ntni": arr_ntni[np.logical_not(vbf_logic_ntnisample)]["eventweight"],
}


# Presuppositions up until this point. Major routines follow
# ------------------------------------------------------------------------
def plot_signficance_scaling():
    """ This routine scales the vbf cross section and evaluates the significance
        Better done by reoptimizing.
    """
    x_ax = []
    y = []
    for val in np.arange(1, 25):

        vbf_ew = val * vbf_arrays["vbf"]["eventweight"]
        ggf_ew = val * ggf_arrays["vbf"]["eventweight"]
        eachCategory_significance = []
        for cat in range(1, 5):
            all_ggf_categorized_events = np.concatenate(
                [ggf_arrays["vbf"], ggf_arrays["ggf"]]
            )
            inCategory_ggf = all_ggf_categorized_events[
                all_ggf_categorized_events["cutCategory"] == cat
            ]
            inCategory_yy = ggf_arrays["yy"][ggf_arrays["yy"]["cutCategory"] == cat]
            eachCategory_significance.append(
                asimov_significance(
                    inCategory_ggf["eventweight"].sum(),
                    inCategory_yy["eventweight"].sum() * yy_eventweight_scalefactor,
                )
            )

        signal_passing = np.concatenate([vbf_ew, ggf_ew])
        eachCategory_significance.append(
            asimov_significance(
                signal_passing.sum(), vbf_arrays["yy"]["eventweight"].sum()* yy_eventweight_scalefactor
            )
        )
        this_cut_vbf_category_significance = sqrt(
            sum([x ** 2 for x in eachCategory_significance])
        )

        print(
            "VBF x-sec "
            + str(val)
            + " gives sig "
            + str(this_cut_vbf_category_significance)
        )
        x_ax.append(val)
        y.append(this_cut_vbf_category_significance)

    plt.plot(x_ax, y, "o")
    plt.ylabel("Asimov Significance")
    plt.xlabel("VBF Ehancement (multiples of SM)")
    plt.tight_layout()
    plt.savefig("crossection_scale.png")


def plot_histograms(array_collection, weight_collection, category):
    """ This routine plots histograms after classification"""

    # Predefined stuff
    # //---------------
    names = [
        "m_yy",
        "jj_deta",
        "mjj",
        "n_jet",
        "tagregion",
        "cutCategory",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
    ]
    axes_titles = [
        "$m_{\gamma\gamma}$ [GeV]",
        "$\Delta\eta_{jj}$",
        "$m_{jj}$",
        "Jet Multiplicity",
        "Tag Region",
        "Cut Category",
        "$m_{\gamma\gamma jj}$ [GeV]",
        "$\Delta R_{\gamma,\gamma}$",
        "$\Delta R_{j,j}$",
        "$\Delta R_{\gamma\gamma,jj}$",
        "$j_{1}$ Tagbin",
        "$j_{2}$ Tagbin",
    ]

    bin_dict = {
        "n_jet": np.arange(1.5, 10.5, 1),
        "m_yyjj": np.linspace(start=0, stop=1500, num=35),
        "mjj": np.linspace(start=0, stop=3000, num=35),
        "dR_jj": np.arange(0, 5, 0.25),
        "dR_yy": np.arange(0, 4, 0.25),
        "dR_yyjj": np.arange(0, 8, 0.5),
        "jj_deta": np.arange(0, 10, 0.5),
        "cutCategory": np.arange(1, 6, 1),
        "j1_tagbin": np.arange(1, 6, 1),
        "j2_tagbin": np.arange(1, 6, 1),
    }
    if category == "vbf":
        label_dict = {
            "vbf": "VBF HH Classified",
            "ggf": "ggF HH Misclassified",
            "yy": "$\gamma\gamma$-continuum",
            "ntni": "NTNI Data",
        }
        annotation = "VBF Category"
    elif category == "ggf":
        label_dict = {
            "vbf": "VBF HH Misclassified",
            "ggf": "ggF HH Classified",
            "yy": "$\gamma\gamma$-continuum",
            "ntni": "NTNI Data",
        }
        annotation = "ggF Category"
    if args.zeroTag:
        label_dict["yy"] = "$\gamma\gamma$-continuum (0-tag)"

    # Main Function
    # //---------------
    for distribution, title in zip(names, axes_titles):

        fig = plt.figure()

        if distribution in bin_dict:
            bins = bin_dict[distribution]
        else:
            bins = np.linspace(
                start=min(
                    [
                        array_collection["vbf"][distribution].min(),
                        array_collection["ggf"][distribution].min(),
                        array_collection["yy"][distribution].min(),
                        array_collection["ntni"][distribution].min(),
                    ]
                ),
                stop=max(
                    [
                        array_collection["vbf"][distribution].max(),
                        array_collection["ggf"][distribution].max(),
                        array_collection["yy"][distribution].max(),
                        array_collection["ntni"][distribution].max(),
                    ]
                ),
                num=25,
            )
            # take care of single value error case
            if len(np.unique(np.array(bins))) == 1:
                v = np.unique(np.array(bins))[0]
                bins = [v - 1, v, v + 1, v + 2]

        y1, x1, _1 = plt.hist(
            array_collection["vbf"][distribution],
            weights=weight_collection["vbf"],
            bins=bins,
            histtype="step",
            color="tab:red",
            label=label_dict["vbf"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["vbf"].sum(),
                array_collection["vbf"][distribution].shape[0],
            ),
            density=True,
        )
        y2, x2, _2 = plt.hist(
            array_collection["ggf"][distribution],
            weights=weight_collection["ggf"],
            bins=bins,
            histtype="step",
            color="tab:blue",
            label=label_dict["ggf"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["ggf"].sum(),
                array_collection["ggf"][distribution].shape[0],
            ),
            density=True,
        )
        y3, x3, _3 = plt.hist(
            array_collection["yy"][distribution],
            weights=weight_collection["yy"],
            bins=bins,
            histtype="step",
            color="tab:purple",
            label=label_dict["yy"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["yy"].sum(),
                array_collection["yy"][distribution].shape[0],
            ),
            density=True,
        )
        y4, x4, _3 = plt.hist(
            array_collection["ntni"][distribution],
            weights=weight_collection["ntni"],
            bins=bins,
            histtype="step",
            color="tab:green",
            label=label_dict["ntni"]
            + "\n{0:.3f} Events ({1} Entries)".format(
                weight_collection["ntni"].sum(),
                array_collection["ntni"][distribution].shape[0],
            ),
            density=True,
        )        
        plt.ylim(ymin=0, ymax=1.35 * max([y1.max(), y2.max(), y3.max(), y4.max()]))
        plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
        plt.xlabel(title, horizontalalignment="right", x=1.0)
        plt.legend(frameon=False, fontsize=8, ncol=2)
        plt.annotate(
            annotation,
            xy=(0.02, 0.96),
            xycoords="axes fraction",
            ha="left",
            fontsize=10,
        )
        plt.tight_layout()
        plt.savefig(sourcedir + "../run/xgb/" + category + "_" + distribution)
        plt.close()


def generate_cutflow(array_collection, category, sig_score, bkg_score):
    if category == "vbf":
        annotation = "VBF HH Sample"
    elif category == "ggf":
        annotation = "ggF HH Sample"
    elif category == "yy" and not args.zeroTag:
        annotation = "$\gamma\gamma$-Continuum"
    elif category == "yy" and args.zeroTag:
        annotation = "$\gamma\gamma$-Continuum (0-tag)"                       
    elif category == "NTNI":
        annotation = "NTNI Data"
    else:
        annotation = category

    cutflow_values = []

    # Initial Values
    cutflow_values.append(array_collection["eventweight"].sum())

    # Apply 4 Jet Cut
    cut1_array = array_collection[array_collection["n_jet"] >= 4]
    cutflow_values.append(cut1_array["eventweight"].sum())

    # Pass Signal Classifier
    c1_array =make_clf_compliant(cut1_array)

    cut2_logic = vbf_ggf_clf.predict_proba(c1_array)[:,1] > sig_score
    cut2_array = cut1_array[cut2_logic]
    cutflow_values.append(cut2_array["eventweight"].sum())

    # Pass Background Classifier
    c2_array = make_clf_compliant(cut2_array)
    cut3_logic = vbf_yy_clf.predict_proba(c2_array)[:,1] > bkg_score
    cut3_array = cut2_array[cut3_logic]
    cutflow_values.append(cut3_array["eventweight"].sum())

    x = range(0, len(cutflow_values))
    bin_names = [
        "Initial",
        "nJet $\geq$ 4",
        "Signal BDT > {0}".format(sig_score),
        "Bkg BDT > {0}".format(bkg_score),
    ]
    fig = plt.figure()
    ax = plt.gca()
    plt.bar(x=x, height=cutflow_values)
    plt.xticks(x, bin_names, rotation=30)
    plt.xlabel("Cut", fontsize=16)
    plt.ylabel("Events", fontsize=16)
    plt.annotate(
        annotation, xy=(0.98, 0.93), xycoords="axes fraction", ha="right", fontsize=16
    )

    labels = [
        "$\epsilon_{abs}=$%.3f" % (float(cut) / cutflow_values[0]) for cut in cutflow_values
    ]
    relative_cuts = [1]
    for i in range(len(cutflow_values) - 1):
        relative_cuts.append(float(cutflow_values[i + 1]) / cutflow_values[i])
        rel_labels = ["$\epsilon_{rel}=$%.3f" % cut for cut in relative_cuts]

    rects = ax.patches
    for rect, label, rel_label in zip(rects, labels, rel_labels):
        if rects.index(rect) == 0:
            continue
        height = rect.get_height()
        ax.text(
            rect.get_x() + rect.get_width() / 2,
            ax.get_ylim()[1] * 0.01 + (height),
            label,
            ha="center",
            va="bottom",
        )
        ax.text(
            rect.get_x() + rect.get_width() / 2,
            ax.get_ylim()[1] * 0.05 + (height),
            rel_label,
            ha="center",
            va="bottom",
        )

    plt.tight_layout()
    plt.savefig(sourcedir + "../run/xgb/" + category + "_cutflow")
    plt.close()


def make_response_curve(arr, category):
    if category == "vbf":
        annotation = "VBF HH Sample"
    elif category == "ggf":
        annotation = "ggF HH Sample"
    elif category == "yy" and not args.zeroTag:
        annotation = "$\gamma\gamma$-Continuum"
    elif category == "yy" and args.zeroTag:
        annotation = "$\gamma\gamma$-Continuum (0-tag)"  
    elif category == "NTNI":
        annotation = "NTNI Data"
    else:
        annotation = category

    formatted_arr =  make_clf_compliant(arr)

    sig_probabilities = vbf_ggf_clf.predict_proba(formatted_arr)[:,1]
    bkg_probabilities = vbf_yy_clf.predict_proba(formatted_arr)[:,1]

    all_probabilities = np.concatenate([sig_probabilities, bkg_probabilities])
    bins = np.arange(all_probabilities.min(), all_probabilities.max(), 0.03)

    fig = plt.figure()
    ax = plt.gca()
    y_sig, x_sig, _sig = plt.hist(
        sig_probabilities,
        weights=arr["eventweight"],
        bins=bins,
        histtype="step",
        color="cornflowerblue",
        label="Signal BDT",
        density=True,
    )

    y_bkg, x_bkg, _bkg = plt.hist(
        bkg_probabilities,
        weights=arr["eventweight"],
        bins=bins,
        histtype="step",
        color="firebrick",
        label="Background BDT",
        density=True,
    )

    plt.ylim(ymin=0, ymax=1.3 * max([y_sig.max(), y_bkg.max()]))
    plt.ylabel("A.U.", horizontalalignment="right", y=1.0)
    plt.xlabel("Classifier Score", horizontalalignment="right", x=1.0)
    plt.annotate(
        annotation, xy=(0.98, 0.80), xycoords="axes fraction", ha="right", fontsize=16
    )
    # Draw line at cuts
    if bkg_clf_score == sig_clf_score:
        ax.axvline(bkg_clf_score, color="forestgreen", linestyle="--", label="BDT Cuts")
    else:
        ax.axvline(
            sig_clf_score,
            color="cornflowerblue",
            linestyle="--",
            label="Signal BDT Cut",
        )  # match sig color
        ax.axvline(
            bkg_clf_score, color="firebrick", linestyle="--", label="Bkg BDT Cut"
        )  # match bkg color

    plt.legend(loc="best", ncol=2)
    handles, labels = ax.get_legend_handles_labels()
    plt.legend(handles=reversed(handles), labels=reversed(labels), loc="best", ncol=2)
    plt.tight_layout()
    plt.savefig(sourcedir + "../run/xgb/" + category + "_classifierScore")
    plt.close()


def build_event_dataframes(ggfonly_event_object, vbfcategorized_event_object):

    d_ggf = OrderedDict(
        {
            "Initial": ggfonly_event_object.ggf,
            "After Cuts": vbfcategorized_event_object.ggf,
        }
    )

    d_vbf = OrderedDict(
        {
            "Initial": ggfonly_event_object.vbf,
            "After Cuts": vbfcategorized_event_object.vbf,
        }
    )

    d_yy = OrderedDict(
        {
            "Initial": ggfonly_event_object.yy,
            "After Cuts": vbfcategorized_event_object.yy,
        }
    )
    df_ggf = pd.DataFrame(d_ggf)
    df_vbf = pd.DataFrame(d_vbf)
    df_yy = pd.DataFrame(d_yy)

    for df in [df_ggf, df_vbf, df_yy]:
        df["Change"] = df["Initial"] - df["After Cuts"]
        df["% Change"] = 100 * (df["After Cuts"] - df["Initial"]) / df["Initial"]

    df_ggf = df_ggf.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )
    df_vbf = df_vbf.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )
    df_yy = df_yy.round(
        decimals=pd.Series(
            [3, 3, 4, 1], index=["Initial", "After Cuts", "Change", "% Change"]
        )
    )

    for df in [df_ggf, df_vbf, df_yy]:
        df["Category"] = [
            "Category 1",
            "Category 2",
            "Category 3",
            "Category 4",
            "VBF Category",
        ]
        df = df.set_index("Category", inplace=True)

    return df_ggf, df_vbf, df_yy


def apply_bdt_cuts(arr, sig_score, bkg_score):
    """ Returns np array passing and failing given cuts """
    passing = arr[classify(arr, sig_score, bkg_score)]
    failing = arr[np.logical_not(classify(arr, sig_score, bkg_score))]
    return passing, failing
    
def post_cut_significance(
    arr_vbf,
    arr_ggf,
    arr_yy,
    arr_ntni,
    arr_tth,
    arr_ggh,
    arr_zh,  
    arr_vbfh,  
    sig_clf_score,
    bkg_clf_score,
    to_screen=False,
    table_name=False,
    no_model=False,
    ):
   
    # Evaluate
    vbf_passing_cut, vbf_failing_cut = apply_bdt_cuts(arr_vbf, sig_clf_score, bkg_clf_score)
    ggf_passing_cut, ggf_failing_cut = apply_bdt_cuts(arr_ggf, sig_clf_score, bkg_clf_score)
    signal_passing = np.concatenate([vbf_passing_cut, ggf_passing_cut])
    signal_failing = np.concatenate([vbf_failing_cut, ggf_failing_cut])

    yy_passing_cut, yy_failing_cut = apply_bdt_cuts(arr_yy, sig_clf_score, bkg_clf_score)
    ntni_passing_cut, ntni_failing_cut = apply_bdt_cuts(arr_ntni, sig_clf_score, bkg_clf_score)
    tth_passing_cut, tth_failing_cut = apply_bdt_cuts(arr_tth, sig_clf_score, bkg_clf_score)
    ggh_passing_cut, ggh_failing_cut = apply_bdt_cuts(arr_ggh, sig_clf_score, bkg_clf_score)
    zh_passing_cut, zh_failing_cut = apply_bdt_cuts(arr_zh, sig_clf_score, bkg_clf_score)
    vbfh_passing_cut, vbfh_failing_cut = apply_bdt_cuts(arr_vbfh, sig_clf_score, bkg_clf_score)

    background_passing = np.concatenate([yy_passing_cut, ntni_passing_cut, tth_passing_cut, ggh_passing_cut, zh_passing_cut, vbfh_passing_cut])
    background_failing = np.concatenate([yy_failing_cut, ntni_failing_cut, tth_failing_cut, ggh_failing_cut, zh_failing_cut, vbfh_failing_cut])

    print("vbf passing: {0:.4f}  Failing: {1:.4f}".format(vbf_passing_cut["eventweight"].sum(), vbf_failing_cut["eventweight"].sum()))
    print("ggf passing: {0:.4f}  Failing: {1:.4f}".format(ggf_passing_cut["eventweight"].sum(), ggf_failing_cut["eventweight"].sum()))
    print("yy passing: {0:.4f}  Failing: {1:.4f}".format(yy_passing_cut["eventweight"].sum(), yy_failing_cut["eventweight"].sum()))
    print("ntni passing: {0:.4f}  Failing: {1:.4f}".format(ntni_passing_cut["eventweight"].sum(), ntni_failing_cut["eventweight"].sum()))
    print("tth passing: {0:.4f}  Failing: {1:.4f}".format(tth_passing_cut["eventweight"].sum(), tth_failing_cut["eventweight"].sum()))
    print("ggh passing: {0:.4f}  Failing: {1:.4f}".format(ggh_passing_cut["eventweight"].sum(), ggh_failing_cut["eventweight"].sum()))
    print("zh passing: {0:.4f}  Failing: {1:.4f}".format(zh_passing_cut["eventweight"].sum(), zh_failing_cut["eventweight"].sum()))
    print("vbfh passing: {0:.4f}  Failing: {1:.4f}".format(vbfh_passing_cut["eventweight"].sum(), vbfh_failing_cut["eventweight"].sum()))


    if no_model:
        # Find no VBF Model significance
        eachCategory_significance = []
        for cat in range(1, 5):
            inCategory_ggf = ggf_failing_cut[ggf_failing_cut["cutCategory"] == cat]
            inCategory_yy = yy_failing_cut[yy_failing_cut["cutCategory"] == cat]
            inCategory_ntni = ntni_failing_cut[ntni_failing_cut["cutCategory"] == cat]

            inCategory_signal = signal_failing[signal_failing["cutCategory"]==cat]
            inCategory_background = background_failing[background_failing["cutCategory"]==cat]
            eachCategory_significance.append(
                asimov_significance(
                    inCategory_ggf["eventweight"].sum(),
                    inCategory_background["eventweight"].sum()
                )
            )
            if to_screen:
                print(
                    "Category: "
                    + str(cat)
                    + " significance: "
                    + str(round(eachCategory_significance[cat - 1],4))
                    + "  ggF events: "
                    + str(round(inCategory_ggf["eventweight"].sum(),4))
                    + "  yy events: "
                    + str(round(inCategory_yy["eventweight"].sum(),4))
                    + "  ntni events: "
                    + str(round(inCategory_ntni["eventweight"].sum(),4))
                )
        significance = sqrt(sum([x ** 2 for x in eachCategory_significance]))
        print("no model significance: {0}".format(significance))


    eachCategory_significance = []
    ggf_sample_events = []
    vbf_sample_events = []
    yy_sample_events = []
    ntni_sample_events =[]
    tth_sample_events =[]
    ggh_sample_events =[]
    zh_sample_events =[]
    vbfh_sample_events =[]

    for cat in range(1, 5):
        inCategory_ggf = ggf_failing_cut[ggf_failing_cut["cutCategory"] == cat]
        inCategory_yy = yy_failing_cut[yy_failing_cut["cutCategory"] == cat]
        inCategory_vbf = vbf_failing_cut[vbf_failing_cut["cutCategory"] == cat]
        inCategory_ntni = ntni_failing_cut[ntni_failing_cut["cutCategory"] == cat]
        inCategory_tth = tth_failing_cut[tth_failing_cut["cutCategory"] == cat]
        inCategory_ggh = ggh_failing_cut[ggh_failing_cut["cutCategory"] == cat]
        inCategory_zh = zh_failing_cut[zh_failing_cut["cutCategory"] == cat]
        inCategory_vbfh = vbfh_failing_cut[vbfh_failing_cut["cutCategory"] == cat]

        inCategory_signal = signal_failing[signal_failing["cutCategory"] == cat]
        inCategory_background = background_failing[background_failing["cutCategory"] == cat]

        eachCategory_significance.append(
            asimov_significance(
                inCategory_signal["eventweight"].sum(),
                inCategory_background["eventweight"].sum(),
            )
        )
        if to_screen:
            print(
                "Category: "
                + str(cat)
                + " significance: "
                + str(eachCategory_significance[cat - 1])
                + "  ggF events: "
                + str(round(inCategory_ggf["eventweight"].sum(),4))
                + "  VBF events: "
                + str(round(inCategory_vbf["eventweight"].sum(),4))
                + "  yy events: "
                + str(round(inCategory_yy["eventweight"].sum(),4))
                + "  ntni events: "
                + str(round(inCategory_ntni["eventweight"].sum(),4))
                + "  bkg events: "
                + str(round(
                    inCategory_ntni["eventweight"].sum()+inCategory_yy["eventweight"].sum()+inCategory_tth["eventweight"].sum()
                    +inCategory_zh["eventweight"].sum()+inCategory_ggh["eventweight"].sum(),
                    4)
                )
            )
        # Append to lists for named tuple
        ggf_sample_events.append(inCategory_ggf["eventweight"].sum())
        vbf_sample_events.append(inCategory_vbf["eventweight"].sum())
        yy_sample_events.append(inCategory_yy["eventweight"].sum())
        ntni_sample_events.append(inCategory_ntni["eventweight"].sum())
        tth_sample_events.append(inCategory_tth["eventweight"].sum())
        ggh_sample_events.append(inCategory_ggh["eventweight"].sum())
        zh_sample_events.append(inCategory_zh["eventweight"].sum())
        vbfh_sample_events.append(inCategory_vbfh["eventweight"].sum())


    # Calculate VBF category significance
    vbf_category_significance = asimov_significance(
        signal_passing["eventweight"].sum(), background_passing["eventweight"].sum()
    )
    if to_screen:
        print(
            "VBF Category: "
            + " significance: "
            + str(vbf_category_significance)
            + "  ggF events: "
            + str(round(ggf_passing_cut["eventweight"].sum(),4))
            + "  VBF events: "
            + str(round(vbf_passing_cut["eventweight"].sum(),4))
            + "  yy events: "
            + str(round(yy_passing_cut["eventweight"].sum(),4))
            + "  ntni events: "
            + str(round(ntni_passing_cut["eventweight"].sum(),4))
        )
    ggf_sample_events.append(ggf_passing_cut["eventweight"].sum())
    vbf_sample_events.append(vbf_passing_cut["eventweight"].sum())
    yy_sample_events.append(yy_passing_cut["eventweight"].sum())
    ntni_sample_events.append(ntni_passing_cut["eventweight"].sum())
    tth_sample_events.append(tth_passing_cut["eventweight"].sum())
    ggh_sample_events.append(ggh_passing_cut["eventweight"].sum())
    zh_sample_events.append(zh_passing_cut["eventweight"].sum())

    # Construct Named tuple
    info_object = namedtuple("info_object", ["ggf", "vbf", "yy", "ntni","tth","ggh","zh","vbfh", "sig_cut", "bkg_cut"])
    info_object.ggf = ggf_sample_events
    info_object.vbf = vbf_sample_events
    info_object.yy = yy_sample_events
    info_object.ntni = ntni_sample_events
    info_object.tth = tth_sample_events
    info_object.ggh =ggh_sample_events
    info_object.zh = zh_sample_events
    info_object.vbfh = vbfh_sample_events

    info_object.sig_cut = sig_clf_score
    info_object.bkg_cut = bkg_clf_score

    eachCategory_significance.append(vbf_category_significance)
    significance = sqrt(sum([x ** 2 for x in eachCategory_significance]))
    return significance, info_object


def get_cut_for_rejection(background_array, bdt, bkg_rej):
    """ Function to get the BDT cut size for given background rejection """

    formatted_arr = make_clf_compliant(background_array)
    probabilities = bdt.predict_proba(formatted_arr)[:,1]

    # Iterate over cuts on BDT
    cut_values = []
    rejection_values = []
    for cut_value in np.arange(probabilities.min(), probabilities.max(), step=0.01):
        # Evaluate background rejection at this point

        entries_pass_this_cut = background_array[probabilities > cut_value]
        events_pass_this_cut = entries_pass_this_cut["eventweight"].sum()
        all_events = background_array["eventweight"].sum()

        percent_rejection = 1 - events_pass_this_cut / all_events
        rejection_values.append(percent_rejection)
        cut_values.append(cut_value)

    # Find closest value
    subtracted_values = [abs(x - bkg_rej) for x in rejection_values]
    closest_arg = np.argmin(subtracted_values)
    associated_cut = cut_values[closest_arg]

    return associated_cut


def cuts_with_preselection(arr_ggf, arr_yy, bkg_rejection, vbf_ggf_clf, vbf_yy_clf):
    """ Implements cutflow appropriately then passes on classifier score """

    # Apply njets cut to ggF
    ggf_passing_njets = arr_ggf[arr_ggf["n_jet"] >= 4]
    # Find x percent of those
    evaluated_ggfclf_score = round(
        get_cut_for_rejection(ggf_passing_njets, vbf_ggf_clf, bkg_rejection), 2
    )

    # Apply both clf score and njets cut to yy sample
    b_njets_yy = arr_yy["n_jet"] >= 4

    ggf_dmatrix = DMatrix(data=make_clf_compliant(arr_yy), feature_names=names)

    b_ggfclf_yy = vbf_ggf_clf.predict(ggf_dmatrix) > evaluated_ggfclf_score

    yy_passing_previous_cuts = arr_yy[np.logical_and(b_njets_yy, b_ggfclf_yy)]
    evaluated_yyclf_score = round(
        get_cut_for_rejection(yy_passing_previous_cuts, vbf_yy_clf, bkg_rejection), 2
    )

    return evaluated_ggfclf_score, evaluated_yyclf_score


def build_cuts_table(arr_vbf, arr_ggf, arr_yy, vbf_ggf_clf, vbf_yy_clf, filename):
    """ Basically does all the functions in this script in an iterative fashion
        Build pandas dataframe and then output as latex or CSV
    """
    # Define a bunch of lists to be added to a dictionary later
    background_rejections = []
    ggf_scores = []
    yy_scores = []
    significance = []

    # Iterate over some background rejection levels
    for background_rejection in np.arange(0.7, 1.0, step=0.05):

        # Evaluate
        evaluated_ggfclf_score, evaluated_yyclf_score = cuts_with_preselection(
            arr_ggf, arr_yy, background_rejection, vbf_ggf_clf, vbf_yy_clf
        )
        sig = post_cut_significance(
            arr_vbf, arr_ggf, arr_yy, evaluated_ggfclf_score, evaluated_yyclf_score
        )[0]

        # Fill Lists
        background_rejections.append(background_rejection)
        ggf_scores.append(evaluated_ggfclf_score)
        yy_scores.append(evaluated_yyclf_score)
        significance.append(sig)

    # Build Dictionary
    d = {
        "Bkg Rejection": background_rejections,
        "Significance": [round(x, 3) for x in significance],
        "Signal BDT Cut": ggf_scores,
        "Bkg BDT Cut": yy_scores,
    }

    # Make Pandas Dataframe
    df = pd.DataFrame(d)
    df = df[["Bkg Rejection", "Significance", "Signal BDT Cut", "Bkg BDT Cut"]]
    if ".tex" in filename:
        with open(filename, "w+") as f:
            df.to_latex(buf=f, index=False)
    elif ".csv" in filename:
        df.to_csv(filename)

    df.plot(
        y="Significance",
        x="Bkg Rejection",
        linestyle="None",
        marker="o",
        color="firebrick",
        legend=None,
        figsize=(12, 4),
    )
    plt.xlabel("Background Rejection", fontsize=18)
    plt.ylabel("Asimov Significance", fontsize=18)
    plt.gca().tick_params(axis="both", labelsize=12)
    plt.tight_layout()
    plt.savefig("run/significance_scaling.pdf")


if __name__ == "__main__":

    # TODO: Update these to work with configured cuts
    #plot_histograms(vbf_arrays, vbf_weights, "vbf")
    #plot_histograms(ggf_arrays, ggf_weights, "ggf")
    #plot_signficance_scaling()
    
    make_response_curve(arr_vbf, "vbf")
    make_response_curve(arr_ggf, "ggf")
    make_response_curve(arr_yy, "yy")
    make_response_curve(arr_ntni, "NTNI")
    make_response_curve(arr_ggh, "ggH")
    make_response_curve(arr_tth, "ttH")
    make_response_curve(arr_zh, "ZH")
    make_response_curve(arr_vbfh, "VBFH")

    """
    print("Using BDT scores of {0} signal, {1} bkg".format(sig_clf_score, bkg_clf_score))
    generate_cutflow(arr_vbf, "vbf", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_ggf, "ggf", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_yy, "yy", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_ntni, "NTNI", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_tth, "ttH", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_ggh, "ggH", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_zh, "ZH", sig_clf_score, bkg_clf_score)
    generate_cutflow(arr_vbfh, "VBFH", sig_clf_score, bkg_clf_score)
    """
    # Find ggF-only Cuts significance
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % arr_vbf["eventweight"].sum())
    print("Total ggF yield: %.4f" % arr_ggf["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % arr_yy["eventweight"].sum())
    print("Rescaled NTNI Bkg yield: %.4f" % arr_ntni["eventweight"].sum())
    print("Net yy+ntni yield: %.4f" % (arr_yy["eventweight"].sum()+arr_ntni["eventweight"].sum()))
    print("-----Single Higgs--------" % arr_ggf["eventweight"].sum())
    print("Total ttH yield: %.4f" % arr_tth["eventweight"].sum())
    print("Total ggH yield: %.4f" % arr_ggh["eventweight"].sum())
    print("Total ZH yield: %.4f" % arr_zh["eventweight"].sum())
    print("Total VBFH yield: %.4f" % arr_vbfh["eventweight"].sum())

    print("No Cuts: ")
    no_cut_sig, no_cut_event_breakdown = post_cut_significance(
        arr_vbf,
        arr_ggf,
        arr_yy,
        arr_ntni,
        arr_tth,
        arr_ggh,
        arr_zh,
        arr_vbfh,
        1.0,
        1.0,  # Effectively don't cut on either
        to_screen=True,
        no_model=True,
    )
    print("Yields significance: {0:.6f}".format(no_cut_sig))

    print("Cuts at {0} signal clf and {1} bkg clf ".format(sig_clf_score, bkg_clf_score))
    sig, event_breakdown = post_cut_significance(
        arr_vbf,
        arr_ggf,
        arr_yy,
        arr_ntni,
        arr_tth,
        arr_ggh,
        arr_zh,
        arr_vbfh,
        sig_clf_score,
        bkg_clf_score,
        to_screen=True,
    )
    print("Yields significance: {0:.3f}".format(sig))

    # Make Event Change tables
    df_ggf, df_vbf, df_yy = build_event_dataframes(no_cut_event_breakdown, event_breakdown)

    with open("run/events_tables.tex", "w+") as f:
        f.write("ggF Table\n")
        df_ggf.to_latex(buf=f)
        f.write("\nVBF Table\n")
        df_vbf.to_latex(buf=f)
        f.write("\nyy Table\n")
        df_yy.to_latex(buf=f)

    # Clean up table
    # TODO: Streamline this better...
    with open("run/events_tables.tex", "r") as f:
        filedata = f.readlines()

    with open("run/events_tables.tex", "w") as f:
        for l in filedata:
            if "begin{tabular}" in l:
                l += "\multicolumn{5}{c}{TYPE Sample}\\\\ \n"
            if "inf" in l:
                l = l.replace("inf", "")
            if "00000" in l:
                l = l.replace("00000", "")
            f.write(l)

    table_file = "run/cuts_table.tex"
    print("Building Cuts Table in File: {0}".format(table_file))
    #build_cuts_table(arr_vbf, arr_ggf, arr_yy, vbf_ggf_clf, vbf_yy_clf, table_file)
