#!/bin/bash

#NEEDED
export HOME=$(pwd)
export PROOFANADIR=$(pwd)
export PYTHONPATH="/cvmfs/sft.cern.ch/lcg/views/LCG_96/x86_64-slc6-gcc8-opt/lib:/cvmfs/sft.cern.ch/lcg/views/LCG_96/x86_64-slc6-gcc8-opt/lib/python2.7/site-packages"
#Setup Tools
source /home/tburch/ml_setup.sh

printf "Start time: "; /bin/date
printf "Job is running on node: "; /bin/hostname
printf "Job running as user: "; /usr/bin/id
printf "Job is running in directory: "; /bin/pwd
printf "TJB ls output: "; ls

echo Args: ${1}
#mkdir saved_classifiers/
#printf "Directory made. ls output: "; ls

/cvmfs/sft.cern.ch/lcg/views/LCG_96/x86_64-slc6-gcc8-opt/bin/python xgb_multiclassifier_train.py -d 15 --stepwiseVariablePop ${1} --outputDir .

printf "Trained. ls output: "; ls

/cvmfs/sft.cern.ch/lcg/views/LCG_96/x86_64-slc6-gcc8-opt/bin/python xgb_multiclassifier_optimization.py --stepwiseVariablePop ${1} --classifier xgb_multiclass.dat


