from math import log, sqrt
import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields
from collections import namedtuple
from tqdm import tqdm
import matplotlib.pyplot as plt
import pickle
from xgboost import XGBClassifier, DMatrix
from xgb_multiclassifier_optimization import add_cols
from utils import *


cuts = [0.0, 0.30, 0.55, 0.95]
titlemap = { "vbf": "VBF HH", "ggf": "ggF HH", "yy": "$\gamma\gamma$-Continuum","tth": "ttH"} # globalized to be used anywhere

# Functions to manipulate data
def add_cols(arr, classifier):
    """ Adds predictions and baseline passing """
    # Predictions

    predictions =  classifier.predict_proba(make_clf_compliant(arr))
    vbfProb = predictions[:,0]
    ggfProb = predictions[:,1]
    yyProb = predictions[:,2]
    tthProb = predictions[:,3]
    arr = append_fields(
        arr, 
        data=[vbfProb, ggfProb, yyProb, tthProb],
        names=["vbfProb", "ggfProb", "yyProb", "tthProb"]
    )
    # Qualifiers
    ggfPassed = arr["cutCategory"] > 0
    vbfPassed = np.logical_and.reduce([
        arr["jj_deta"] >= 0,
        arr["vbfProb"] > cuts[0],
        arr["ggfProb"] < cuts[1],
        arr["yyProb"] < cuts[2],
        arr["tthProb"] < cuts[3],
    ])
    arr = append_fields(
        arr, 
        data=[ggfPassed, vbfPassed],
        names=["ggfPassed", "vbfPassed"],
    )
    return arr


def get_sample_dict():
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    samples = {}
    vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    samples["arr_vbf_test"] = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)

    ## ggF samples
    ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)        
    samples["arr_ggf_test"] = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)

    ## yy samples
    yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    samples["arr_yy_test"] = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)

    # NTNI samples
    ntni_files = [sourcedir + fname for fname in  ["forcuts_ntni_mc16a.npy", "forcuts_ntni_mc16d.npy", "forcuts_ntni_mc16e.npy"]]
    arr_ntni_test, ntni_eventweight_scalefactor_test = load_samples(ntni_files, ggf_preselection=True, test=True, myyCut=False)
    yy_scaled_weight = samples["arr_yy_test"]["eventweight"].sum()
    # Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    total_continuum_yield = yy_scaled_weight / 0.84
    ntni_fractional_reweight = 0.16 * total_continuum_yield / arr_ntni_test["eventweight"].sum()
    samples["arr_ntni_test"] = rescale_eventweight(arr_ntni_test, ntni_fractional_reweight)

    # Single Higgs
    tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    samples["arr_tth_test"] = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)

    ggh_files = [sourcedir + fname for fname in  ["forcuts_GGH_mc16a.npy","forcuts_GGH_mc16d.npy","forcuts_GGH_mc16e.npy"]]
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
    samples["arr_ggh_test"] = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)

    zh_files = [sourcedir + fname for fname in  ["forcuts_ZH_mc16a.npy","forcuts_ZH_mc16d.npy","forcuts_ZH_mc16e.npy"]]
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
    samples["arr_zh_test"] = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)

    vbfh_files = [sourcedir + fname for fname in  ["forcuts_VBFH_mc16a.npy","forcuts_VBFH_mc16d.npy","forcuts_VBFH_mc16e.npy"]]
    arr_vbfh_test, vbfh_eventweight_scalefactor_test = load_samples(vbfh_files, ggf_preselection=True, test=True)
    samples["arr_vbfh_test"] = rescale_eventweight(arr_vbfh_test, vbfh_eventweight_scalefactor_test)            
    return samples


# plotting functions
def bdt_score_plot(samples, category):
    """plots the bdt score for a given category
    
    Arguments:
        samples {dict} -- dictionary of samples with associated names
        category {string} -- which class to plot. Good values: vbfProb, ggFProb, tthProb, yyProb
    """
    colors = ["midnightblue","firebrick","forestgreen","teal"]

    for i, sample in enumerate(samples):
        sample_to_plot = samples[sample]
        to_plot = sample_to_plot[sample_to_plot["jj_deta"]>0]
        plt.hist(to_plot[category], bins=np.arange(0,1,0.03), weights=to_plot["eventweight"]/to_plot["eventweight"].sum(), 
        histtype="step", color=colors[i], label=titlemap[sample])
    plt.legend(frameon=False)
    plt.ylim(bottom=0)
    plt.ylabel("AU")
    plt.xlabel("BDT Score")



def main():

    sample_dictionary = get_sample_dict()
    arr_signals = np.concatenate([sample_dictionary[s] for s in ["arr_vbf_test", "arr_ggf_test"]])
    arr_backgrounds = np.concatenate([sample_dictionary[s] for s in ["arr_yy_test", "arr_ntni_test", "arr_tth_test", "arr_ggh_test", "arr_zh_test", "arr_vbfh_test"]])

    # Print checks
    print("Original Values - ")
    print("------Signals-------")
    print("Total VBF yield: %.4f" % sample_dictionary["arr_vbf_test"]["eventweight"].sum())
    print("Total ggF yield: %.4f" % sample_dictionary["arr_ggf_test"]["eventweight"].sum())
    print("------Continuum-------")
    print("Rescaled Continuum Bkg yield: %.4f" % sample_dictionary["arr_yy_test"]["eventweight"].sum())
    print("Rescaled NTNI Bkg yield: %.4f" % sample_dictionary["arr_ntni_test"]["eventweight"].sum())
    print("-----Single Higgs--------")
    print("Total ttH yield: %.4f" % sample_dictionary["arr_tth_test"]["eventweight"].sum())
    print("Total ggH yield: %.4f" % sample_dictionary["arr_ggh_test"]["eventweight"].sum())
    print("Total ZH yield: %.4f" % sample_dictionary["arr_zh_test"]["eventweight"].sum())
    print("Total VBFH yield: %.4f" % sample_dictionary["arr_vbfh_test"]["eventweight"].sum())


    # Get original Significance
    # ---------------------------------
    category_significance = []                    
    for cat in range(1, 5):
        inCategory_sig = arr_signals[arr_signals["cutCategory"] == cat]
        inCategory_bkg = arr_backgrounds[arr_backgrounds["cutCategory"] == cat]
        category_significance.append(asimov_significance(
            s=inCategory_sig["eventweight"].sum(),
            b=inCategory_bkg["eventweight"].sum(),
            ))
    baseline_significance = sqrt(sum([x ** 2 for x in category_significance]))

    # load Classifier
    print("Loading Classifier")
    xgb_multiclassifier = pickle.load(open("saved_classifiers/xgb_multiclass.dat", "rb"))
    arr_signals = add_cols(arr_signals, xgb_multiclassifier)
    arr_backgrounds = add_cols(arr_backgrounds, xgb_multiclassifier)
    evaluated_sample_dictionary = {key:add_cols(value, xgb_multiclassifier) for key,value in sample_dictionary.items()}

    # Evaluate significance as a check
    # ---------------------------------
    category_significance = []
    vbf_category_significance = asimov_significance(
        s =arr_signals[arr_signals["vbfPassed"]]["eventweight"].sum(),
        b =arr_backgrounds[arr_backgrounds["vbfPassed"]]["eventweight"].sum(),                        
        )
    category_significance.append(vbf_category_significance)

    for cat in range(1, 5):
        inCategory_sig = arr_signals[np.logical_and(~arr_signals["vbfPassed"], arr_signals["cutCategory"] == cat)]
        inCategory_bkg = arr_backgrounds[np.logical_and(~arr_backgrounds["vbfPassed"], arr_backgrounds["cutCategory"] == cat)]
        category_significance.append(asimov_significance(
            s=inCategory_sig["eventweight"].sum(),
            b=inCategory_bkg["eventweight"].sum(),
            ))
    this_significance = sqrt(sum([x ** 2 for x in category_significance]))
    print("Baseline Significance {0}".format(baseline_significance))
    print("Updated Significance {0}".format(this_significance))


    # Plot BDT Scores
    # ---------------------------------
    print("Plotting BDT scores.")
    samples_for_plotting = {
        "vbf": evaluated_sample_dictionary["arr_vbf_test"],
        "ggf": evaluated_sample_dictionary["arr_ggf_test"],
        "yy": evaluated_sample_dictionary["arr_yy_test"],
        "tth": evaluated_sample_dictionary["arr_tth_test"],
        }


    for i in ["vbf", "ggf","yy","tth"]:
        bdt_score_plot(samples_for_plotting,"{0}Prob".format(i))
        plt.title(titlemap[i]+" Class")
        plt.tight_layout()
        plt.savefig("run/xgb_multiclass_plots/{0}_scores".format(i))
        plt.close()

if __name__ == "__main__":
    main()