from xgboost import XGBClassifier, plot_importance, DMatrix, plot_tree
import xgboost as xgb
from numpy.lib.recfunctions import append_fields, drop_fields
import numpy as np
import matplotlib.pyplot as plt
import argparse
import pandas as pd
import pickle
from sklearn.metrics import accuracy_score
from collections import OrderedDict
from utils import *
import logging
logging.basicConfig(level=logging.ERROR)

# Neural Network packages
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Dropout
#from keras.utils.vis_utils import plot_model
from tensorflow.keras.utils import plot_model
from tensorflow.keras.callbacks import TensorBoard, EarlyStopping
from keras.utils import normalize, to_categorical

from keras.engine.input_layer import Input
from keras.wrappers.scikit_learn import KerasClassifier
from time import time
from tensorflow.python.framework import ops
from tensorflow.keras.models import load_model
from keras.metrics import categorical_accuracy

from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sklearn.utils import resample
import seaborn as sns

import h5py
from IPython.display import Image
from classifySet import ClassifySet

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--depth", help="Depth", type=int)
parser.add_argument("-t", "--nTrees", help="Number of trees", type=int)
parser.add_argument("--jetRequirement", help="Require 4 jets", action="store_true")
parser.add_argument("--optimizeDepth", help="Optimize depth parameter", action="store_true")
parser.add_argument("--optimizeTrees", help="Optimize trees parameter", action="store_true")
parser.add_argument("--optimizeGamma", help="Optimize gamma parameter", action="store_true")
parser.add_argument("--noWeight", help="Don't use Weights", action="store_true")
#parser.add_argument("--rebalance", help="Balance out classes", action="store_true")

# These options exist for stepwise optimization
parser.add_argument("--stepwiseVariablePop", help="Variable index to pop for N-1 test", type=int)
parser.add_argument("--outputDir", help="output directory", type=str, default="saved_classifiers/")
# Additions for NN 
parser.add_argument("-m","--model",help="Model type to run (BDT or NN)", default="BDT")
args = parser.parse_args()

# Set Defaults, protect against bad things
if args.model not in ["BDT","NN"]:
    print("Not a valid model type! Defaulting to BDT")

if args.model == "BDT":
    if not args.depth and not args.optimizeDepth:
        args.depth = 6
        print("Using default value of depth: {0}".format(args.depth))
    if not args.optimizeTrees and not args.nTrees:
        args.nTrees = 200
        print("Using default value {0} for n_estimators".format(args.nTrees))

names=[
    "jj_deta",
    "mjj",
    "n_jet",
    "m_yyjj",
    "dR_yy",
    "dR_jj",
    "dR_yyjj",
    "j1_tagbin",
    "j2_tagbin",
    "aplanority",
    "aplanarity",
    "sphericity",
    "spherocity",
    "sphericityT",
    "planarity",
    "circularity",
    "planarFlow",
    "ht",
    "mjjb1",
    "mjjb2",
    "mjj_w1",
    "mjj_w2",
    "ptyy_myy_ratio",
    "pt_balance",
    "pt_qqbb",
    "pt_qq_bb_ratio",
    "bdt_score"
]
if args.stepwiseVariablePop:
    print("Omitting Variable: {0}".format(names.pop(args.stepwiseVariablePop)))

output_directory = "run/classifier_evaluation/"


# BDT training functions/class
### ------------------------------------------------------------------------------------------
def create_classifier(X_train, y_train, X_test, y_test, w_train=None, w_test=None, multiclass=False, names=names, bdt_kwargdict={}, task_kwargdict={}):
    """ Classifier explicitly created and trained here. Hyperparameters set withing this function.
    X_train/test are the features, y_train/test are the classification labels
    w's are weights. If none given, weights are not used (find this to be better for training)
    names -> names of features
    bdt_kwargdict -> Hyperparameters affecting the BDT that you'd like to manually override (e.g. depth)
    task_kwargdict -> Hyperparameters affecting the training task that you'd like to manually override (e.g. early_stopping_rounds)
    """
    # Make Classifier
    param = {}
    # BDT parameters
    bdt_param={}
    bdt_param['learning_rate']      = 0.1 # learning rate
    bdt_param['max_depth']        = 3  # maximum depth of a tree
    bdt_param['subsample']        = 0.8 # fraction of events to train tree on
    bdt_param['colsample_bytree'] = 0.8 # fraction of features to train tree on
    bdt_param['objective']   = 'multi:softmax' # objective function

    bdt_param['n_estimators']   = 200 
    bdt_param["feature_names"] = names

    # Learning task parameters
    task_param={}
    task_param['eval_metric'] = ['mlogloss', 'merror']           # evaluation metric for cross validation
    task_param['early_stopping_rounds'] = 30
    task_param['eval_set'] = [(X_train, y_train), (X_test,y_test)]
    if w_test is not None and w_train is not None:
        task_param['sample_weight'] = np.abs(w_train)
        task_param['sample_weight_eval_set'] = [np.abs(w_train), np.abs(w_test)]

    # Manual override default values
    bdt_param.update(bdt_kwargdict)
    task_param.update(task_kwargdict)

    # Classifier method
    classifier = XGBClassifier(**bdt_param)
    
    #pdb.set_trace()
    print (X_train.shape)
    classifier.fit(X_train, y_train, verbose=True, **task_param)
    return classifier

def create_nn(X_train, y_train, X_test, y_test, w_train=None, w_test=None, names=names, task_kwargdict={}, nn_kwargdict={}):

    # Construct the model
    n_features = X_train.shape[1]
    y_binary_train = to_categorical(y_train)
    y_binary_test = to_categorical(y_test)
    print("Number of features: {0}".format(n_features))
    # Try soft sign
    # Require < 4 jets
    model = Sequential()
    model.add(Dense(round(n_features*1.5), input_dim=n_features, activation="relu")) 
    model.add(Dropout(0.2))
    model.add(Dense(round(n_features*0.7), activation="relu"))
    model.add(Dropout(0.2))
    model.add(Dense(4, activation="softmax")) # Output = number of classes
    model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=[categorical_accuracy])
    model.summary()

    # Add some Callbacks
    tensorboard = TensorBoard(log_dir="logs/{}".format(time()))
    early_stopping = EarlyStopping(
        monitor="val_loss",
        min_delta=0,
        patience=10,
        mode="auto",
        restore_best_weights=True
    )

    # Fit model
    if w_train is not None and w_test is not None:
        history = model.fit(
            X_train,
            y_binary_train,
            sample_weight=w_train,
            epochs=1000, # Set high for early stopping
            batch_size=64,
            validation_data=(X_test, y_binary_test, w_test),
            callbacks=[tensorboard, early_stopping]
        )
    else:
        history = model.fit(
            X_train,
            y_binary_train,
            sample_weight=w_train,
            epochs=1000, # Set high for early stopping
            batch_size=64,
            validation_data=(X_test, y_binary_test, w_test),
            callbacks=[tensorboard, early_stopping]
        )


    # Plot training
    plt.plot(history.history['categorical_accuracy'])
    plt.plot(history.history['val_categorical_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('run/NN/accuracy.pdf')
    plt.close()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('run/NN/loss.pdf')
    plt.close()



    return model



# Plotting functions
### ------------------------------------------------------------------------------------------
def plot_test_data(classifier, samples, weights, theClass):
    """Plotting bdt score for trained classifier. This implementation was quick and not ideal, see the call for how it works.
    Arguments:
        classifier {XGBclassifier} -- Classifier that has been trained
        samples {OrderedDict} -- Dictionary of samples to evaluate (names used as labels)
        weights {OrderedDict} -- Dictionary of wieghts associated to samples (should make an object for this, but this works)
        theClass {int} -- Class associated with the sample being plotted.
    """
    colors = ["midnightblue","firebrick","forestgreen","teal"]
    
    for i,sample in enumerate(samples):
        predictions =  classifier.predict_proba(samples[sample])
        plt.hist(predictions[:,theClass],bins=np.linspace(0,1,50),
                histtype='step',color=colors[i],label=sample, density=True, weights=weights[sample])
    plt.legend(frameon=False)


def plot_training_metrics(classifier, name):
    """Plots the metrics from training (loss function, error)
    Classifier -> Trained xgb classifier
    name -> Prepend to save name
    """
    results = classifier.evals_result()
    epochs = len(results['validation_0']['merror'])
    x_axis = range(0, epochs)
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['mlogloss'], label='Train')
    ax.plot(x_axis, results['validation_1']['mlogloss'], label='Test')
    ax.legend()
    plt.ylabel('Log Loss')
    plt.title('XGBoost Log Loss')
    plt.savefig("run/xgb_multiclass/{0}_loss".format(name))
    plt.close()
    # plot classification error
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['merror'], label='Train')
    ax.plot(x_axis, results['validation_1']['merror'], label='Test')
    ax.legend()
    plt.ylabel('Classification Error')
    plt.title('XGBoost Classification Error')
    plt.savefig("run/xgb_multiclass/{0}_error".format(name))
    plt.close()


def pass_ggf(thearray):
    a = thearray[np.logical_or.reduce(
            [
            thearray["cutCategory"] > 0,
            thearray["bdtcat_HH"] > 0,
            thearray["bdtcat_HH_lowmass"] > 0,
            ]
        )]
    return a


def main():

    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    # Load and skim samples
    #// ---------------------------------
    # Setup VBF samples, split into training and testing
    #vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    vbf_files = [sourcedir + fname for fname in  ["bdtscore_vbf_mc16a.npy","bdtscore_vbf_mc16d.npy","bdtscore_vbf_mc16e.npy"]]
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples(vbf_files, ggf_preselection=True, test=False)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    ## Rescale weights for train/test split
    arr_vbf_train = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train) 
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    if args.jetRequirement:
        arr_vbf_train = arr_vbf_train[arr_vbf_train["jj_deta"] > 0]
        arr_vbf_test = arr_vbf_test[arr_vbf_test["jj_deta"] > 0]
        arr_vbf_train = pass_ggf(arr_vbf_train)
        arr_vbf_test = pass_ggf(arr_vbf_test)

    ## ggf
    #ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
    ggf_files = [sourcedir + fname for fname in  ["bdtscore_ggf_mc16a.npy","bdtscore_ggf_mc16d.npy","bdtscore_ggf_mc16e.npy"]]
    arr_ggf_train, ggf_eventweight_scalefactor_train = load_samples(ggf_files, ggf_preselection=True, test=False)
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)           
    arr_ggf_train = rescale_eventweight(arr_ggf_train, ggf_eventweight_scalefactor_train)
    arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)  
    if args.jetRequirement:
        arr_ggf_train = arr_ggf_train[arr_ggf_train["jj_deta"] > 0]
        arr_ggf_test = arr_ggf_test[arr_ggf_test["jj_deta"] > 0]
        arr_ggf_train = pass_ggf(arr_ggf_train)
        arr_ggf_test = pass_ggf(arr_ggf_test) 

    ## yy
    #yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
    yy_files = [sourcedir + fname for fname in  ["bdtscore_yy_mc16a.npy","bdtscore_yy_mc16d.npy","bdtscore_yy_mc16e.npy"]]
    arr_yy_train, yy_eventweight_scalefactor_train = load_samples(yy_files, ggf_preselection=True, test=False, myyCut=False)
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    arr_yy_train = rescale_eventweight(arr_yy_train, yy_eventweight_scalefactor_train)
    arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)      
    if args.jetRequirement:
        arr_yy_train = arr_yy_train[arr_yy_train["jj_deta"] > 0]
        arr_yy_test = arr_yy_test[arr_yy_test["jj_deta"] > 0]
        arr_yy_train = pass_ggf(arr_yy_train)
        arr_yy_test = pass_ggf(arr_yy_test)

    ## tth
    #tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
    tth_files = [sourcedir + fname for fname in  ["bdtscore_TTH_mc16a.npy","bdtscore_TTH_mc16d.npy","bdtscore_TTH_mc16e.npy"]]
    arr_tth_train, tth_eventweight_scalefactor_train = load_samples(tth_files, ggf_preselection=True, test=False)
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    arr_tth_train = rescale_eventweight(arr_tth_train, tth_eventweight_scalefactor_train)
    arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)     
    if args.jetRequirement:
        arr_tth_train = arr_tth_train[arr_tth_train["jj_deta"] > 0]
        arr_tth_test = arr_tth_test[arr_tth_test["jj_deta"] > 0]
        arr_tth_train = pass_ggf(arr_tth_train)
        arr_tth_test = pass_ggf(arr_tth_test)

    # Upsample
    training_samples = [arr_vbf_train, arr_ggf_train, arr_yy_train, arr_tth_train]
    testing_samples = [arr_vbf_test, arr_ggf_test, arr_yy_test, arr_tth_test]
    majority_size_train  = max([i.shape[0] for i in training_samples])
    majority_size_test  = max([i.shape[0] for i in testing_samples])

    arr_vbf_train, arr_vbf_test  = resample(arr_vbf_train, replace=True, n_samples=majority_size_train), resample(arr_vbf_test, replace=True, n_samples=majority_size_test)
    arr_ggf_train, arr_ggf_test  = resample(arr_ggf_train, replace=True, n_samples=majority_size_train), resample(arr_ggf_test, replace=True, n_samples=majority_size_test)
    arr_yy_train, arr_yy_test  = resample(arr_yy_train, replace=True, n_samples=majority_size_train), resample(arr_yy_test, replace=True, n_samples=majority_size_test)
    arr_tth_train, arr_tth_test  = resample(arr_tth_train, replace=True, n_samples=majority_size_train), resample(arr_tth_test, replace=True, n_samples=majority_size_test)

    # Reformat and reweight
    #// ---------------------------------
    ## Reshape to xgb inputs ([event,features] dimensional matrix)
    X_train_vbf = make_clf_compliant(arr_vbf_train, names=names)
    X_test_vbf = make_clf_compliant(arr_vbf_test, names=names)
    ## Set individual eventweights to be the average of the whole (found helps)
    logging.info("Original train VBF weight = {0:.3}".format(arr_vbf_train["eventweight"].sum()))
    logging.info("Original test VBF weight = {0:.3}".format(arr_vbf_test["eventweight"].sum()))
    w_train_vbf = np.ones(arr_vbf_train.shape[0]) * arr_vbf_train["eventweight"].sum() / arr_vbf_train["eventweight"].shape[0]
    w_test_vbf = np.ones(arr_vbf_test.shape[0]) * arr_vbf_test["eventweight"].sum() / arr_vbf_test["eventweight"].shape[0]
    logging.info("Rescaled to average train VBF weight = {0:.3}".format(w_train_vbf.sum()))
    logging.info("Original test VBF weight = {0:.3}".format(w_test_vbf.sum()))

    ## ggF samples - Same routine as above
    w_train_ggf = np.ones(arr_ggf_train.shape[0]) * arr_ggf_train["eventweight"].sum() / arr_ggf_train["eventweight"].shape[0]
    w_test_ggf = np.ones(arr_ggf_test.shape[0]) * arr_ggf_test["eventweight"].sum() / arr_ggf_test["eventweight"].shape[0]
    X_train_ggf = make_clf_compliant(arr_ggf_train, names=names)
    X_test_ggf = make_clf_compliant(arr_ggf_test, names=names)

    ## yy samples - Same routine as above    
    w_train_yy = np.ones(arr_yy_train.shape[0]) * arr_yy_train["eventweight"].sum() / arr_yy_train["eventweight"].shape[0]
    w_test_yy = np.ones(arr_yy_test.shape[0]) * arr_yy_test["eventweight"].sum() / arr_yy_test["eventweight"].shape[0]
    X_train_yy = make_clf_compliant(arr_yy_train, names=names)
    X_test_yy = make_clf_compliant(arr_yy_test, names=names)

    # Single Higgs - Same routine as above
    w_train_tth = ( np.ones(arr_tth_train.shape[0]) * arr_tth_train["eventweight"].sum() / arr_tth_train["eventweight"].shape[0] )
    w_test_tth = ( np.ones(arr_tth_test.shape[0]) * arr_tth_test["eventweight"].sum() / arr_tth_test["eventweight"].shape[0] )
    X_train_tth = make_clf_compliant(arr_tth_train, names=names)
    X_test_tth = make_clf_compliant(arr_tth_test, names=names)

    # Balance weights between signal and each background sample, both train and test
    w_train_vbf = w_train_vbf * (w_train_vbf.shape[0] / w_train_vbf.sum())
    w_train_ggf = w_train_ggf * (w_train_vbf.shape[0] / w_train_ggf.sum())
    w_train_yy = w_train_yy * (w_train_vbf.shape[0] / w_train_yy.sum())
    w_train_tth = w_train_tth * (w_train_vbf.shape[0] / w_train_tth.sum())

    w_test_vbf = w_test_vbf * (w_test_vbf.shape[0] / w_test_vbf.sum())
    w_test_ggf = w_test_ggf * (w_test_vbf.shape[0] / w_test_ggf.sum())
    w_test_yy = w_test_yy * (w_test_vbf.shape[0] / w_test_yy.sum())
    w_test_tth = w_test_tth * (w_test_vbf.shape[0] / w_test_tth.sum())

    # Create the ClassifySet object and load all the samples into it
    BDT_set = ClassifySet()
    if args.noWeight:
        BDT_set.add_sample(X_train_vbf, X_test_vbf, thisClass=0)
        BDT_set.add_sample(X_train_ggf, X_test_ggf, thisClass=1)
        BDT_set.add_sample(X_train_yy, X_test_yy, thisClass=2)
        BDT_set.add_sample(X_train_tth, X_test_tth, thisClass=3)
    else:
        BDT_set.add_sample(X_train_vbf, X_test_vbf, thisClass=0, w_train=w_train_vbf, w_test=w_test_vbf)
        BDT_set.add_sample(X_train_ggf, X_test_ggf, thisClass=1, w_train=w_train_ggf, w_test=w_test_ggf)
        BDT_set.add_sample(X_train_yy, X_test_yy, thisClass=2, w_train=w_train_yy, w_test=w_test_yy)
        BDT_set.add_sample(X_train_tth, X_test_tth, thisClass=3, w_train=w_train_tth, w_test=w_test_tth)

    # Post processing
    #BDT_set.rescale_features_to_unity()

    print("Number of training samples - ")
    print("                        VBF: {0}".format((BDT_set.y_train==0).sum()))
    print("                        ggF: {0}".format((BDT_set.y_train==1).sum()))
    print("                         yy: {0}".format((BDT_set.y_train==2).sum()))
    print("                        ttH: {0}".format((BDT_set.y_train==3).sum()))

    samples = OrderedDict()
    samples["VBF HH"] = X_test_vbf
    samples["ggF HH"] = X_test_ggf
    samples["$\gamma \gamma$"] = X_test_yy
    samples["ttH"] = X_test_tth
    weights = OrderedDict()
    weights["VBF HH"] = w_test_vbf
    weights["ggF HH"] = w_test_ggf
    weights["$\gamma \gamma$"] = w_test_yy
    weights["ttH"] = w_test_tth

    ################################ Model training #########################################
    # BDT
    if args.model == "BDT":
    # Do hyperparameter optimization if noted by the command line args, otherwise just train
        
        if args.optimizeDepth:
            best_depth = BDT_set.optimize_depth()        
            plt.savefig("run/xgb_multiclass/multiclass_optimized_depth")
            plt.close()
            # Train also at best depth
            classifier = BDT_set.train_bdt({"max_depth":best_depth})
        elif args.optimizeTrees:
            best_trees = BDT_set.optimize_trees(depth=args.depth)
            plt.savefig("run/xgb_multiclass/multiclass_optimized_trees")
            plt.close()             
            print("Training final model at {0} trees".format(best_trees))
            classifier = BDT_set.train_bdt({"n_estimators":best_trees})
        elif args.optimizeGamma:
            best_gamma = BDT_set.optimize_gamma(depth=args.depth)
            plt.savefig("run/xgb_multiclass/multiclass_optimized_gamma")
            plt.close()             
            print("Training final model at {0} trees".format(best_gamma))
            classifier = BDT_set.train_bdt({"gamma":best_gamma})
        else:
            classifier = BDT_set.train_bdt({"max_depth":args.depth, "n_estimators":args.nTrees})

        # Save output
        output_classifier = args.outputDir+'/xgb_multiclass.dat'
        print("Saving classifier: {0}".format(output_classifier))
        pickle.dump(classifier, open(output_classifier,'wb'))

        # Confusion Matrix
        try:
            print("XGBoost used Confusion!")
            conf_mat = confusion_matrix(BDT_set.y_test, classifier.predict(BDT_set.X_test))
            print(conf_mat)
            print("It's super effective!")
            classnames = ["VBF HH", "ggF HH", "yy", "ttH"]
            sns.heatmap(conf_mat, annot=True, cmap='Blues', fmt='g',
                        xticklabels=classnames, yticklabels=classnames)
            plt.savefig("run/xgb_multiclass/confusion_matrix")
            plt.close()
            conf_mat = conf_mat.astype('float') / conf_mat.sum(axis=1)[:, np.newaxis]
            np.set_printoptions(precision=2)
            sns.heatmap(conf_mat, annot=True, cmap='Blues', fmt='g',
                        xticklabels=classnames, yticklabels=classnames)
            plt.savefig("run/xgb_multiclass/confusion_matrix_normalized")
            plt.close()
        except:
            print("It's not very effective...")


        if not args.stepwiseVariablePop:
            for i in list(range(0,4)):
                plot_test_data(classifier, samples, weights, i)
                plt.savefig("run/xgb_multiclass/predictions_{0}".format(i))
                plt.close()

            # Plot other XGB plots - importance, loss, accuracy
            classifier.get_booster().feature_names = names
            plot_importance(classifier.get_booster())
            plt.tight_layout()
            plt.savefig("run/xgb_multiclass/feature_importance")
            plot_training_metrics(classifier, "multiclass")
            plt.close()


    if args.model == "NN":

        classifier = BDT_set.train_nn()
        classifier.save("saved_classifiers/nn.h5")


if __name__ == "__main__":
    main()
