""" 
Quick script to merge alex's ntuples into mine
python merge_bdt_scores.py my_file alex_highmass alex_lowmass output
"""

import numpy as np
import pandas as pd
import sys
import pdb

f_base = sys.argv[1]
f_highmass = sys.argv[2]
f_lowmass = sys.argv[3]
if "BSM" not in f_lowmass:
    print("Make sure the low mass (BSM) sample is second!")

# open as numpy arrays
a_base = np.load(f_base)
a_highmass = np.load(f_highmass)
a_lowmass = np.load(f_lowmass)

# Convert numpy arrays to pandas
d_base = pd.DataFrame(a_base)
d_highmass = pd.DataFrame(a_highmass)
d_lowmass = pd.DataFrame(a_lowmass)

merge1 = d_base.merge(d_highmass, left_on="eventNumber", right_on="event_number", how="left", suffixes=("","_highmass"), sort=False)
merge2 = merge1.merge(d_lowmass, left_on="eventNumber", right_on="event_number", how="left", suffixes=("","_lowmass"), sort=False)
merge2 = merge2.to_records()

np.save(sys.argv[4], merge2)