#!/usr/bin/python
print("Importing Modules")
import ROOT
ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
import sys
from vbf_classes import *
from preselection_helpers import *
from array import array
import copy
import argparse
import numpy as np
from collections import OrderedDict

# Flag for doing photon ID spectator variables.
doPhotonID = True

if len(sys.argv) < 3:
    print("Usage: python preselection.py inputfile.root outputfile.root [lumi in pb]")
    sys.exit(1)

# Define Functions to be used
#// ---------------------------------------------------

def propagate_cutflow(f_in, f_out, data=False):
    # Propagate cutflow to file
    if data:
        return None
    else:
        for key in f_in.GetListOfKeys():
            if "noDalitz_weighted" in key.GetName():
                cutflow_name = key.GetName()

                if cutflow_name is None:
                    raise RuntimeError("Unable to get cutflow!")

                cutflow = f_in.Get(cutflow_name)
                f_out.cd()
                cutflow.Write()
                return cutflow
        raise ValueError("Cutfow not found!")


# xAOD Functions
# ---------------------------------------------------
def char2bool(char):
    """Convert char to bool."""
    return {"\x00": False, "\x01": True}[char]


def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value


def get_tree_dictionary():
    # Defines dictionary with all branches
    long_initializer = 50*[0]
    short_initializer = 2*[0]
    variables = OrderedDict()    
    # Eventwise
    variables["weight/F"] = array("f", [0])
    variables["full_eventweight/F"] = array("f", [0])
    variables["isPassed/O"] = array("b", [0])
    variables["isPreselected/O"] = array("b", [0])
    variables["m_yy/F"] = array("f", [0])
    variables["m_yyjj_tilde/F"] = array("f", [0])
    variables["photon_n/I"] = array('i', [0])
    variables["jet_n/I"] = array('i', [0])
    variables["dR_yy/F"] = array("f", [0])
    variables["dR_bb/F"] = array("f", [0])
    variables["tagregion/I"] = array("i", [0])

    # VBF quantities
    variables["jjVBF_deta/F"] = array("f", [0])
    variables["jjVBF_m/F"] = array("f", [0])
    # Photon Variables
    variables["photon_pt[photon_n]/F"] = array("f", long_initializer)
    variables["photon_eta[photon_n]/F"] = array("f", long_initializer)
    variables["photon_phi[photon_n]/F"] = array("f", long_initializer)
    variables["photon_isTight[photon_n]/O"] = array("b", long_initializer)

    # H->bb Variables 
    variables["candidateJet_pt[2]/F"] = array("f", short_initializer)
    variables["candidateJet_eta[2]/F"] = array("f", short_initializer)
    variables["candidateJet_phi[2]/F"] = array("f", short_initializer)
    variables["candidateJet_m[2]/F"] = array("f", short_initializer)
    variables["candidateJet_MV2c10bin[2]/I"] = array('i', short_initializer)
    variables["candidateJet_MV2c10discriminant[2]/F"] = array('f', short_initializer)
    # Jet Variables
    variables["jet_pt[jet_n]/F"] = array("f", long_initializer)
    variables["jet_eta[jet_n]/F"] = array("f", long_initializer)
    variables["jet_phi[jet_n]/F"] = array("f", long_initializer)
    variables["jet_m[jet_n]/F"] = array("f", long_initializer)

    ## b-tagging
    variables["SF_MV2c10_FixedCutBEff_60[jet_n]/F"] = array("f", long_initializer)
    variables["SF_MV2c10_FixedCutBEff_70[jet_n]/F"] = array("f", long_initializer)
    variables["SF_MV2c10_FixedCutBEff_77[jet_n]/F"] = array("f", long_initializer)
    variables["SF_MV2c10_FixedCutBEff_85[jet_n]/F"] = array("f", long_initializer)

    variables["MV2c10_FixedCutBEff_60[jet_n]/O"] = array("b", long_initializer)
    variables["MV2c10_FixedCutBEff_70[jet_n]/O"] = array("b", long_initializer)
    variables["MV2c10_FixedCutBEff_77[jet_n]/O"] = array("b", long_initializer)
    variables["MV2c10_FixedCutBEff_85[jet_n]/O"] = array("b", long_initializer)

    # Event Shape Variables
    variables["aplanority/F"] = array("f", long_initializer)
    variables["aplanarity/F"] = array("f", long_initializer)
    variables["sphericity/F"] = array("f", long_initializer)
    variables["spherocity/F"] = array("f", long_initializer)
    variables["sphericityT/F"] = array("f", long_initializer)
    variables["planarity/F"] = array("f", long_initializer)
    variables["circularity/F"] = array("f", long_initializer)
    variables["planarFlow/F"] = array("f", long_initializer)
    variables["pt_balance/F"] = array("f", long_initializer)

    if doPhotonID:
        variables["y_ptcone20[photon_n]/F"] = array("f", long_initializer)
        variables["y_ptcone40[photon_n]/F"] = array("f", long_initializer)
        variables["y_topoetcone20[photon_n]/F"] = array("f", long_initializer)
        variables["y_topoetcone40[photon_n]/F"] = array("f", long_initializer)
        variables["y_iso_FixedCutLoose[photon_n]/F"] = array("f", long_initializer)
        variables["y_iso_FixedCutTight[photon_n]/F"] = array("f", long_initializer)
        variables["y_iso_FixedCutTightCaloOnly[photon_n]/F"] = array("f", long_initializer)
    else:
        variables["photon_isIsoFixedCutLoose[photon_n]/O"] = array("b", long_initializer)
        variables["photon_isIsoFixedCutTight[photon_n]/O"] = array("b", long_initializer)

    return variables


def get_lumi(filename, lumi=None):
    # Get Luminosity
    if lumi:
        return lumi # this is kind of silly
    elif "mc16a" in filename:
        return 36215.0
    elif "mc16d" in filename:
        return 44307.4
    elif "mc16e" in filename:
        return 58450.1
    elif "_data" in filename:
        return 9999999 # Dummy Value    
    else:
        raise IOError("Lumi can't be figured out by filename - provide as third arg")
    
def get_weights(cutflow):
    if cutflow is None:
        # Case for data
        return 1
    else:
        return (cutflow.GetBinContent(1) / cutflow.GetBinContent(2)) * cutflow.GetBinContent(3)


def main():

    # Get ready for xAOD
    print("Initializing xAOD Framework")
    ROOT.xAOD.Init()

    # Define input and output
    print("Loading Files")
    # ---------------------------------------------------
    f_in = ROOT.TFile.Open(sys.argv[1])
    input_tree = ROOT.xAOD.MakeTransientTree(f_in, "CollectionTree")
    import xAODRootAccess.GenerateDVIterators

    f_out = ROOT.TFile(sys.argv[2], "recreate")
    output_tree = ROOT.TTree("mini", "mini")

    isData = False
    if "_data" in str(sys.argv[1]):
        print("Input is data")
        isData = True
    else:
        print("Input is MC")

    print("Getting Cutflow")
    cutflow = propagate_cutflow(f_in, f_out, data=isData)



    print("Setting up Tree")
    variables = get_tree_dictionary()    
    for v in variables:
        if "[" in v :
            shortname = v.split("[")[0]
        else:
            shortname = v.split("/")[0]        
        output_tree.Branch(
            shortname, # name only
            variables[v], # array
            v
        )

    print("Getting Luminosity and weights")
    if len(sys.argv) == 4:
        lumi = get_lumi(filename=sys.argv[1], lumi=sys.argv[3])
    else:
        lumi = get_lumi(filename=sys.argv[1])

    sum_weight = get_weights(cutflow)

    # Load Tree
    print("Loading up xAOD tree to iterate on")
    entries = input_tree.GetEntries()
    print("%s Entries" % str(entries))

    # Begin Event Loop
    print("Beginning Event Loop")
    # ---------------------------------------------------
    for ientry in range(input_tree.GetEntries()):
        input_tree.GetEntry(ientry)

        if ientry % 10000 == 0:
            print(
                "ientry = %d out of %d, percent done = %.3f"
                % (
                    ientry,
                    input_tree.GetEntries(),
                    1.0 * ientry / input_tree.GetEntries() * 100,
                )
            )

        # Reset event level variables
        for v in [
            "full_eventweight/F", 
            "weight/F", 
            "isPassed/O", 
            "isPreselected/O", 
            "m_yy/F", 
            "m_yyjj_tilde/F", 
            "photon_n/I", 
            "jet_n/I",
            "dR_yy/F",
            "dR_bb/F"]:
            variables[v][0] = -99

        # Set eventwise variables
        ## Weights
        if not isData:
            variables["weight/F"][0] = read(input_tree.HGamEventInfo, "float", "weight")
            variables["full_eventweight/F"][0] = (
                read(input_tree.HGamEventInfo, "float", "crossSectionBRfilterEff")
                * read(input_tree.HGamEventInfo, "float", "weight")
                * read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_weight")
                * float(lumi)
                / float(sum_weight)
            )
        else: 
            variables["weight/F"][0] = 1
            variables["full_eventweight/F"][0] = 1


        ## Passing Criteria
        variables["isPassed/O"][0] = read(input_tree.HGamEventInfo, "char", "isPassed")
        if (
            read(input_tree.HGamEventInfo, "char", "isPassed")
            and read(input_tree.HGamEventInfo, "int", "N_lep") == 0
            and read(input_tree.HGamEventInfo, "int", "N_j_central") < 6
            and read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") > 70.0
            and read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj")
            <= 180.0
        ):
            variables["isPreselected/O"][0] = True
        else:
            variables["isPreselected/O"][0] = False

        # Uncomment me if you'd like to only save preselected events
        #if not variables["isPreselected/O"][0]: continue

        ## Eventwise quantities
        variables["m_yy/F"][0] = read(input_tree.HGamEventInfo, "float", "m_yy") * 0.001
        variables["m_yyjj_tilde/F"][0] = read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_yyjj_tilde")
        variables["jet_n/I"][0] = read(input_tree.HGamEventInfo, "int", "N_j")
        variables["photon_n/I"][0] = len(input_tree.HGamPhotons)
        variables["dR_yy/F"][0] = read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_deltaR_yy")
        variables["dR_bb/F"][0] = read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_deltaR_jj")

        # Set H->bb Variables
        variables["tagregion/I"][0] = 0
        for i,j in enumerate(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT):
          variables["candidateJet_pt[2]/F"][i]= j.pt() * 0.001
          variables["candidateJet_eta[2]/F"][i] = j.eta()
          variables["candidateJet_phi[2]/F"][i] = j.phi()
          variables["candidateJet_m[2]/F"][i] = j.m() * 0.001
          variables["candidateJet_MV2c10bin[2]/I"][i] = read(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[i], "int", "MV2c10bin")
          variables["candidateJet_MV2c10discriminant[2]/F"][i] = read(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT[i], "double", "MV2c10_discriminant")
          ibin = read(j, "int", "MV2c10bin")
          if ibin >= 1:
            variables["tagregion/I"][0] += 1

        # Set Photon Variables
        for i,p in enumerate(input_tree.HGamPhotons):
            variables["photon_pt[photon_n]/F"][i] = p.pt() * 0.001
            variables["photon_eta[photon_n]/F"][i] = p.eta()
            variables["photon_phi[photon_n]/F"][i] = p.phi()
            variables["photon_isTight[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isTight")

            if doPhotonID:
                variables["y_ptcone20[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"float","ptcone20")
                variables["y_ptcone40[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"float","ptcone40")
                variables["y_topoetcone20[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"float","topoetcone20")
                variables["y_topoetcone40[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"float","topoetcone40")
                variables["y_iso_FixedCutLoose[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"char","isIsoFixedCutLoose")
                variables["y_iso_FixedCutTight[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"char","isIsoFixedCutTight")
                variables["y_iso_FixedCutTightCaloOnly[photon_n]/F"][i] = read(input_tree.HGamPhotons[i],"char","isIsoFixedCutTightCaloOnly")
            else: 
                variables["photon_isIsoFixedCutTight[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isIsoFixedCutTight")
                variables["photon_isIsoFixedCutLoose[photon_n]/O"][i] = read(input_tree.HGamPhotons[i], "char", "isIsoFixedCutLoose")

        # Set VBF Variables + all jet variables
        # -----------------------------------------
        # First need collection of non-Higgs candidate jets
        jets = []
        for i,j in enumerate(input_tree.HGamAntiKt4EMTopoJets):
            # Jet Variables
            variables["jet_pt[jet_n]/F"][i] = j.pt() * 0.001
            variables["jet_eta[jet_n]/F"][i] = j.eta()
            variables["jet_phi[jet_n]/F"][i] = j.phi()
            variables["jet_m[jet_n]/F"][i] = j.m() * 0.001
            ## b-tagging
            if not isData:
                variables["SF_MV2c10_FixedCutBEff_60[jet_n]/F"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "float", "SF_MV2c10_FixedCutBEff_60")
                variables["SF_MV2c10_FixedCutBEff_70[jet_n]/F"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "float", "SF_MV2c10_FixedCutBEff_70")
                variables["SF_MV2c10_FixedCutBEff_77[jet_n]/F"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "float", "SF_MV2c10_FixedCutBEff_77")
                variables["SF_MV2c10_FixedCutBEff_85[jet_n]/F"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "float", "SF_MV2c10_FixedCutBEff_85")
            else:
                variables["SF_MV2c10_FixedCutBEff_60[jet_n]/F"][i] = 1
                variables["SF_MV2c10_FixedCutBEff_70[jet_n]/F"][i] = 1
                variables["SF_MV2c10_FixedCutBEff_77[jet_n]/F"][i] = 1
                variables["SF_MV2c10_FixedCutBEff_85[jet_n]/F"][i] = 1

            variables["MV2c10_FixedCutBEff_60[jet_n]/O"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "char", "MV2c10_FixedCutBEff_60") 
            variables["MV2c10_FixedCutBEff_70[jet_n]/O"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "char", "MV2c10_FixedCutBEff_70") 
            variables["MV2c10_FixedCutBEff_77[jet_n]/O"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "char", "MV2c10_FixedCutBEff_77") 
            variables["MV2c10_FixedCutBEff_85[jet_n]/O"][i] = read(input_tree.HGamAntiKt4EMTopoJets[i], "char", "MV2c10_FixedCutBEff_85")             

            # Build collection of local objects for dijet determination
            candiate_jet = False
            for cand in input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT:
                if (
                    j.eta() < cand.eta() + 0.01
                    and j.eta() > cand.eta() - 0.01
                    and j.phi() < cand.phi() + 0.01
                    and j.phi() > cand.phi() - 0.01
                ):
                    candiate_jet = True
            thisJet = Jet(
                j.pt() * 0.001,
                j.eta(),
                j.phi(),
                j.m() * 0.001,
                btag=(read(j, "char", "MV2c10_FixedCutBEff_85")),
                candidate=candiate_jet,
            )
            jets.append(thisJet)
        # Now construct dijets and find 
        n_jet = read(input_tree.HGamEventInfo, "int", "N_j")
        if n_jet >= 4:
            dijet = VBF_jets(jets)
            if dijet.deta == -99 or dijet.m == -99:
                ("Problem in Dijet!!!!")

            variables["jjVBF_deta/F"][0] = dijet.deta
            variables["jjVBF_m/F"][0] = dijet.m
        else:
            variables["jjVBF_deta/F"][0] = -99
            variables["jjVBF_m/F"][0] = -99


        # -----------------------------------------
        # Shape Variables
        # ------------------------------------------------------------------------------------------------
        if len(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT) >= 2 and variables["photon_n/I"][0] >= 2:
            sorted_jets = sorted(input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT, key=lambda x: x.pt(), reverse=True)
            lv_j1 = ROOT.TLorentzVector()
            lv_j2 = ROOT.TLorentzVector()
            lv_j1.SetPtEtaPhiM(sorted_jets[0].pt(),sorted_jets[0].eta(),sorted_jets[0].phi(),sorted_jets[0].m())
            lv_j2.SetPtEtaPhiM(sorted_jets[1].pt(),sorted_jets[1].eta(),sorted_jets[1].phi(),sorted_jets[1].m())

            y1, y2 = input_tree.HGamPhotons[0], input_tree.HGamPhotons[1]
            lv_y1 = ROOT.TLorentzVector()
            lv_y2 = ROOT.TLorentzVector()
            lv_y1.SetPtEtaPhiM(y1.pt(),y1.eta(),y1.phi(),0)
            lv_y2.SetPtEtaPhiM(y2.pt(),y2.eta(),y2.phi(),0)

            shape_dictionary = shape_variables(sorted_jets[0], sorted_jets[1], input_tree.HGamPhotons[0], input_tree.HGamPhotons[1])
            variables["aplanority/F"][0] = shape_dictionary["aplanority"]
            variables["aplanarity/F"][0] = shape_dictionary["aplanarity"]
            variables["sphericity/F"][0] = shape_dictionary["sphericity"]
            variables["spherocity/F"][0] = shape_dictionary["spherocity"]
            variables["sphericityT/F"][0] = shape_dictionary["sphericityT"]
            variables["planarity/F"][0] = shape_dictionary["planarity"]
            variables["circularity/F"][0] = shape_dictionary["circularity"]
            variables["planarFlow/F"][0] = shape_dictionary["planarFlow"]
            this_pt_balance = (lv_j1 + lv_j2 + lv_y1 + lv_y2).Pt() / (lv_j1.Pt() + lv_j2.Pt() + lv_y1.Pt() + lv_y2.Pt() )
            variables["pt_balance/F"][0] = this_pt_balance

        # Fill Tree
        output_tree.Fill()

    # End Event Loop, Save file, finish up
    f_out.cd()
    output_tree.Write()
    f_out.Close()
    ROOT.xAOD.ClearTransientTrees()
    f_in.Close()


if __name__ == "__main__":
    main()