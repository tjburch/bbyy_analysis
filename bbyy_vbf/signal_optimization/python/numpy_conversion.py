import root_numpy
import sys
import h5py

if len(sys.argv) < 1:
    print("Usage: python root_convert.py file_to_convert.py")
    sys.exit()

a = root_numpy.root2array(sys.argv[1])
name = sys.argv[1].strip(".root")
h5f = h5py.File(name + ".h5", "w")
h5f.create_dataset("mini", data=a)
h5f.close()
