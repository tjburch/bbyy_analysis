"""
Script to do plots that aren't already generated in quick-histograms or cut_analysis
"""

import numpy as np
import matplotlib.pyplot as plt
from utils import *
from scipy.stats import pearsonr
import seaborn as sns
sns.set(style="white", color_codes=True)

def myy_vs_dRyy(array, normed=False):

    if not normed:
        h = plt.hist2d(
            x=array["m_yy"],
            y=array["dR_yy"],
            weights=array["eventweight"],
            cmap='Blues',
            bins=25,
        )
        plt.colorbar(h[3], ax=plt.gca())
        plt.ylabel("$\Delta R_{\gamma\gamma}$")
        corr = pearsonr(array["m_yy"],array["dR_yy"])[0]

    else:
        h = plt.hist2d(
            x=array["m_yy"],
            y=array["dR_yy"],
            weights=array["eventweight"],
            cmap='Blues',
            bins=25,
            )
        plt.colorbar(h[3], ax=plt.gca())
        plt.ylabel("$\Delta R_{\gamma\gamma}/m_{\gamma\gamma}$")       
        corr = pearsonr(array["m_yy"],array["dR_yy"]/array["m_yy"])[0]
    
    plt.xlabel("$m_{\gamma\gamma}$")
    plt.annotate(
        "Pearson Correlation: {0:.3}".format(corr), 
        xy=(0.99,0.95),
        xycoords="axes fraction",
        ha='right'
        )


if __name__ == "__main__":
    
    # Load all samples and unpack for script
    sample_dictionary = import_all_samples()

    arr_ggf = sample_dictionary["arr_ggf"]
    arr_vbf = sample_dictionary["arr_vbf"]
    arr_yy = sample_dictionary["arr_yy"]    
    arr_ntni = sample_dictionary["arr_ntni"]    

    myy_vs_dRyy(arr_ggf)
    plt.annotate("ggF Sample", xy=(0.01,0.03),xycoords="figure fraction")
    plt.tight_layout()
    plt.savefig("run/checks/myy_v_dryy_ggf")
    plt.close()

    myy_vs_dRyy(arr_ntni)
    plt.annotate("NTNI Sample", xy=(0.01,0.03),xycoords="figure fraction")
    plt.tight_layout()
    plt.savefig("run/checks/myy_v_dryy_ntni")
    plt.close()

    myy_vs_dRyy(arr_yy)
    plt.annotate("$\gamma\gamma$-Continuum Sample", xy=(0.01,0.03),xycoords="figure fraction")
    plt.tight_layout()
    plt.savefig("run/checks/myy_v_dryy_yy")
    plt.close()    
