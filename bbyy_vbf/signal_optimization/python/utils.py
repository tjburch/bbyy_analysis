import numpy as np
from numpy.lib.recfunctions import append_fields, drop_fields, repack_fields
from math import log, sqrt
from collections import OrderedDict
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA # Principal components
from sklearn.preprocessing import StandardScaler, MinMaxScaler

def ntni_photons(array):
    # Already applied in preselection, but double guard
    y1_not_tight = array["y1_isTight"]
    y2_not_tight = array["y2_isTight"]
    y1_not_isolated = array["y1_isIsoFixedCutLoose"]
    y2_not_isolated = array["y2_isIsoFixedCutLoose"]
    return array[np.logical_or.reduce([
        y1_not_tight,
        y2_not_tight,
        y1_not_isolated,
        y2_not_isolated
    ])]  

def rand_sample_tagbin(model_array, replace_array, plot=False):
    """ Substitutes tagbin values in replace_array with a random sampling of a 2d histogram of those in model_array """

    # Stack data
    data = np.column_stack((model_array["j1_tagbin"],model_array["j2_tagbin"]))
    x, y = data.T                        
    # Make histogram as pdf
    hist, x_bins, y_bins = np.histogram2d(x, y, bins=[1,2,3,4,5])
    x_bin_midpoints = x_bins[:-1] + np.diff(x_bins)/2
    y_bin_midpoints = y_bins[:-1] + np.diff(y_bins)/2
    pdf = np.cumsum(hist.ravel())
    pdf = pdf / pdf[-1]

    # Sample distribution
    values = np.random.rand(replace_array.shape[0])
    value_bins = np.searchsorted(pdf, values)
    x_idx, y_idx = np.unravel_index(value_bins,
                                    (len(x_bin_midpoints),
                                    len(y_bin_midpoints)))
    random_from_pdf = np.column_stack((x_bin_midpoints[x_idx],
                                    y_bin_midpoints[y_idx]))
    new_x, new_y = np.floor(random_from_pdf.T)

    if plot:
        plt.subplot(121, aspect='equal')
        plt.hist2d(x, y, bins=[1,2,3,4,5])
        plt.title("${\gamma\gamma}-Continuum$ Tagbin")
        plt.subplot(122, aspect='equal')
        plt.hist2d(new_x, new_y, bins=[1,2,3,4,5])
        plt.title("Random Sampled Tagbin")
        plt.annotate("Candidate Jet 1 Tagbin", xy=(.5,.15), xycoords="figure fraction",ha="center")
        plt.annotate("Candidate Jet 2 Tagbin", xy=(.05,.63), xycoords="figure fraction",rotation=90, ha="center")
        #plt.tight_layout()

        plt.savefig("run/checks/sampling")
        plt.close()

    # Append new weight and return
    replace_array = drop_fields(replace_array, ["j1_tagbin", "j2_tagbin"])
    replace_array = append_fields(replace_array, data=[new_x, new_y],  names=["j1_tagbin", "j2_tagbin"] )
    return replace_array


def rebalance_categories(model_array, replace_array):
    """ Rebalances events in all 4 categories to match weight in model_array """

    scale_factor = np.ones(shape=replace_array.shape)
    for category in range(1,5):
        model_weight = model_array[model_array["cutCategory"]==category]["eventweight"].sum()
        replace_original_weight = replace_array[replace_array["cutCategory"]==category]["eventweight"].sum()

        calculated_scale_factor = model_weight / replace_original_weight        
        mask = (replace_array["cutCategory"] == category)
        scale_factor[mask] = calculated_scale_factor
    
    return rescale_eventweight(replace_array, scale_factor)


def join_signal_regions(samples, myyCut=True, ggf_preselection=True, tagReq=2):
    """Joins multiple arrays of the same dataset, along the way skims to signal region
    
    Arguments:
        samples {list} -- List of np arrays of the same type
    
    Keyword Arguments:
        myyCut {bool} -- [Cut to myy signal region?] (default: {True})
        ggf_preselection {bool} -- [pre-apply passing ggF categories] (default: {True})
        tagReq {bool} -- [2 tag requirement] (default: {True})
    
    Returns:
        [type] -- [description]
    """
    arrays = [np.load(s) for s in samples]
    arr = np.concatenate(arrays)

    # apply signal region cuts
    arr = arr[arr["tagregion"] == tagReq]

    if myyCut:
        arr = arr[np.logical_and((arr["m_yy"] > 120), (arr["m_yy"] < 130))]
    else:
        # If not on the signal region, cut between 105 and 160, then scale event weight
        arr =  arr[np.logical_and((arr["m_yy"] > 105), (arr["m_yy"] < 160))]
        arr["eventweight"] *= float(10.0/55.0)

    if ggf_preselection:
        # Pass ggF Selection
        arr = arr[arr["cutCategory"] >= 0]

    return arr



def rescale_eventweight(array, scale_factor):
    """Rescales the eventweight column to input luminosity
    Arguments:
        array {np structured array} -- array to rescale
        scale_factor {float} -- desired lumi/lumi in sample (e.g. 140/36.1)
    Returns:
        [np structured array] -- rescaled array
    """

    # Scale eventweight
    scaled_eventweight = array["eventweight"] * scale_factor
    # Drop old eventweight
    return_array = drop_fields(array, "eventweight")
    # Append new weight and return
    return append_fields(return_array, "eventweight", scaled_eventweight, usemask=False)


def load_samples(samples, test=True, **kwargs):
    """Loads samples from list of filenames, returns np array and scale factor
    
    Arguments:
        samples {list} -- all samples to load
    
    Keyword Arguments:
        test {bool} -- Use testing data. Turn off for testing (default: {True})
    
    Returns:
        [arr, scalefactor] -- np array of samples put together, scale factor for rescaling
    """

    arr = join_signal_regions(samples, **kwargs)    
    original_eventweight = float(arr["eventweight"].sum())
    if test:
        arr = split_by_index(arr)[1] 
    else:
        # Else return training
        arr = split_by_index(arr)[0] 
        
    scalefactor = (float(original_eventweight) / arr["eventweight"].sum())
    return arr, scalefactor  


def asimov_significance(s, b):
    """Calculate asimov significance
    Arguments:
        s {[float]} -- nSignal Events
        b {[float]} -- nBackground
    Returns:
        [float] -- Asimov significance
    """

    if s <= 0:
        return 0
    elif b <= 0:
        return 0
    elif 2 * ((s + b) * log(1 + (s / b))) - s < 0:
        return 0
    else:
        return sqrt(2 * (((s + b) * log(1 + (s / b))) - s))


def split_by_index(arr):
    """Splits a numpy array based on index to the rules for test/test splitting
    
    Arguments:
        arr {[numpy array]} -- numpy array to split into test/testing
    
    Returns:
        [np array, np array] -- test, test numpy arrays based on index
    """
    train = arr[np.logical_or(arr["eventNumber"] % 4 == 0, arr["eventNumber"] % 4 == 1)]    
    test = arr[arr["eventNumber"] % 4 == 2]
    return train, test


def find_scale_factor(arr1, arr2):
    return arr1["eventweight"].sum() / arr2["eventweight"].sum()


def reconstruct_categories(arr, btagging=True):
    if not btagging:
        logic4 = (
            (arr["m_yyjj"] >= 350.0)
            & (arr["j1_tagbin"] >= 3)
            & (arr["j2_tagbin"] >= 3)
            & (arr["dR_yy"] < 2.0)
            & (arr["dR_jj"] < 2.0)
            & (arr["dR_yyjj"] < 3.4)
        )
        logic3 = (
            (arr["m_yyjj"] >= 350.0)
            & (arr["dR_yy"] < 2.0)
            & (arr["dR_jj"] < 2.0)
            & (arr["dR_yyjj"] < 3.4)
        )
        logic2 = (
            (arr["m_yyjj"] < 350.0)
            & (arr["j1_tagbin"] >= 3)
            & (arr["j2_tagbin"] >= 3)
        )
        logic1 = arr["m_yyjj"] < 350.0

    else:
        logic4 = (
            (arr["m_yyjj"] >= 350.0)
            & (arr["n_btag70"] == 2)
            & (arr["dR_yy"] < 2.0)
            & (arr["dR_jj"] < 2.0)
            & (arr["dR_yyjj"] < 3.4)
        )
        logic3 = (
            (arr["m_yyjj"] >= 350.0)
            & (arr["dR_yy"] < 2.0)
            & (arr["dR_jj"] < 2.0)
            & (arr["dR_yyjj"] < 3.4)
        )
        logic2 = (arr["m_yyjj"] < 350.0) & (arr["n_btag70"] == 2)
        logic1 = arr["m_yyjj"] < 350.0

    custom_cut_category = np.full(shape=arr.shape[0], fill_value=-99)
    custom_cut_category[logic1] = 1
    custom_cut_category[logic2] = 2
    custom_cut_category[logic3] = 3
    custom_cut_category[logic4] = 4
    return custom_cut_category

def make_clf_compliant(
    arr,
    names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
        "aplanority",
        "aplanarity",
        "sphericity",
        "spherocity",
        "sphericityT",
        "planarity",
        "circularity",
        "planarFlow",
        "ht",
        "mjjb1",
        "mjjb2",
        "mjj_w1",
        "mjj_w2",
        "ptyy_myy_ratio",
        "pt_balance",
        "pt_qqbb",
        "pt_qq_bb_ratio",        
        #"bdt_score"
    ],
    ):
    """Reshapes structured numpy array to fit in classifier.
    Arguments:
        arr {np.recarray} -- array to restructure    
    Keyword Arguments:
        names {list} -- features necessary for clf (default: {['jj_deta','mjj','n_jet','m_yyjj','dR_yy','dR_jj','dR_yyjj']})
    Returns:
        [np.array] -- ready for predict
    """
    arr = arr[:][names]    
    try:
        arr = repack_fields(arr).view(np.float64).reshape(arr.shape + (-1,))
        return arr
    except ValueError:
        # This triggers when there's an array of length 0
        return None


def classifier_object():
    
    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    # Setup VBF samples, common between both signal and background
    vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples(vbf_files, ggf_preselection=True, test=False)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)

    ## Rescale to accomodate for lost weights
    arr_vbf_train = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train) 
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    ## Reshape to xgb inputs
    X_train_signal = make_clf_compliant(arr_vbf_train)
    X_test_signal = make_clf_compliant(arr_vbf_test)
    ## Set individual eventweights to be the average of the whole
    w_train_signal = np.ones(arr_vbf_train.shape[0]) * arr_vbf_train["eventweight"].sum() / arr_vbf_train["eventweight"].shape[0]
    w_test_signal = np.ones(arr_vbf_test.shape[0]) * arr_vbf_test["eventweight"].sum() / arr_vbf_test["eventweight"].shape[0]


    






def import_all_samples():
    """ I'm doing this a lot, so just going to make it a util function """
    # Global define sample names
    sourcedir = (
        "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"
    )
    vbf_mc16a = sourcedir + "forcuts_vbf_mc16a.npy"
    vbf_mc16d = sourcedir + "forcuts_vbf_mc16d.npy"
    vbf_mc16e = sourcedir + "forcuts_vbf_mc16e.npy"

    ggf_mc16a = sourcedir + "forcuts_ggf_mc16a.npy"
    ggf_mc16d = sourcedir + "forcuts_ggf_mc16d.npy"
    ggf_mc16e = sourcedir + "forcuts_ggf_mc16e.npy"

    yy_mc16a = sourcedir + "forcuts_yy_mc16a.npy"
    yy_mc16d = sourcedir + "forcuts_yy_mc16d.npy"
    yy_mc16e = sourcedir + "forcuts_yy_mc16e.npy"

    ntni1516 = sourcedir + "forcuts_ntni_mc16a.npy"
    ntni17 = sourcedir + "forcuts_ntni_mc16d.npy"
    ntni18 = sourcedir + "forcuts_ntni_mc16e.npy"

    tth_mc16a = sourcedir + "forcuts_TTH_mc16a.npy"
    tth_mc16d = sourcedir + "forcuts_TTH_mc16d.npy"
    tth_mc16e = sourcedir + "forcuts_TTH_mc16e.npy"

    ggh_mc16a = sourcedir + "forcuts_GGH_mc16a.npy"
    ggh_mc16d = sourcedir + "forcuts_GGH_mc16d.npy"
    ggh_mc16e = sourcedir + "forcuts_GGH_mc16e.npy"

    zh_mc16a = sourcedir + "forcuts_ZH_mc16a.npy"
    zh_mc16d = sourcedir + "forcuts_ZH_mc16d.npy"
    zh_mc16e = sourcedir + "forcuts_ZH_mc16e.npy"

    # Load samples
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples([ggf_mc16a, ggf_mc16d, ggf_mc16e], test=True)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples([vbf_mc16a, vbf_mc16d, vbf_mc16e], test=True)
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], myyCut=False, test=True)
    arr_ntni_test, ntni_eventweight_scalefactor_test = load_samples([ntni1516, ntni17, ntni18], myyCut=False, test=True)
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples([tth_mc16a, tth_mc16d, tth_mc16e], test=True)
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples([ggh_mc16a, ggh_mc16d, ggh_mc16e], test=True)
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples([zh_mc16a, zh_mc16d, zh_mc16e], test=True)

    arr_ggf_train, ggf_eventweight_scalefactor_train = load_samples([ggf_mc16a, ggf_mc16d, ggf_mc16e], test=False)
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples([vbf_mc16a, vbf_mc16d, vbf_mc16e], test=False)
    arr_yy_train, yy_eventweight_scalefactor_train = load_samples([yy_mc16a, yy_mc16d, yy_mc16e], myyCut=False, test=False)
    arr_ntni_train, ntni_eventweight_scalefactor_train = load_samples([ntni1516, ntni17, ntni18], myyCut=False, test=False)
    arr_tth_train, tth_eventweight_scalefactor_train = load_samples([tth_mc16a, tth_mc16d, tth_mc16e], test=False)
    arr_ggh_train, ggh_eventweight_scalefactor_train = load_samples([ggh_mc16a, ggh_mc16d, ggh_mc16e], test=False)
    arr_zh_train, zh_eventweight_scalefactor_train = load_samples([zh_mc16a, zh_mc16d, zh_mc16e], test=False)


    # Fractional decomposition here: https://indico.cern.ch/event/811843/contributions/3385512/attachments/1825000/2986398/Hyybb_studies_5thApril.pdf
    yy_scaled_weight_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)["eventweight"].sum()
    ntni_fractional_reweight_test = 0.16 * yy_scaled_weight_test / arr_ntni_test["eventweight"].sum()
    yy_fractional_reweight_test = 0.84 

    yy_scaled_weight_train = rescale_eventweight(arr_yy_train, yy_eventweight_scalefactor_train)["eventweight"].sum()
    ntni_fractional_reweight_train = 0.16 * yy_scaled_weight_train / arr_ntni_train["eventweight"].sum()
    yy_fractional_reweight_train = 0.84 


    sample_dictionary = OrderedDict()
    sample_dictionary['arr_vbf_train'] = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train)
    sample_dictionary['arr_ggf_train'] = rescale_eventweight(arr_ggf_train, ggf_eventweight_scalefactor_train)
    sample_dictionary['arr_yy_train'] = rescale_eventweight(arr_yy_train, yy_fractional_reweight_train * yy_eventweight_scalefactor_train)
    sample_dictionary['arr_ntni_train'] = rescale_eventweight(arr_ntni_train, ntni_fractional_reweight_train)
    sample_dictionary['arr_tth_train'] = rescale_eventweight(arr_tth_train, tth_eventweight_scalefactor_train)
    sample_dictionary['arr_ggh_train'] = rescale_eventweight(arr_ggh_train, ggh_eventweight_scalefactor_train)
    sample_dictionary['arr_zh_train'] = rescale_eventweight(arr_zh_train, zh_eventweight_scalefactor_train)

    sample_dictionary['arr_vbf_test'] = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    sample_dictionary['arr_ggf_test'] = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)
    sample_dictionary['arr_yy_test'] = rescale_eventweight(arr_yy_test, yy_fractional_reweight_test * yy_eventweight_scalefactor_test)
    sample_dictionary['arr_ntni_test'] = rescale_eventweight(arr_ntni_test, ntni_fractional_reweight_test)
    sample_dictionary['arr_tth_test'] = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)
    sample_dictionary['arr_ggh_test'] = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)
    sample_dictionary['arr_zh_test'] = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)


    # Make aggregate arrays where type doesn't matter
    sample_dictionary['arr_singleHiggs_train'] = np.concatenate([arr_tth_train, arr_ggh_train, arr_zh_train])    
    sample_dictionary['all_signal_train'] = np.concatenate([arr_ggf_train. arr_vbf_train])
    sample_dictionary['all_background_train'] = np.concatenate([arr_yy, arr_ntni_train, sample_dictionary['arr_singleHiggs_train']])

    sample_dictionary['arr_singleHiggs_test'] = np.concatenate([arr_tth_test, arr_ggh_test, arr_zh_test])
    sample_dictionary['all_signal_test'] = np.concatenate([arr_ggf_test. arr_vbf_test])
    sample_dictionary['all_background_test'] = np.concatenate([arr_yy, arr_ntni_test, sample_dictionary['arr_singleHiggs_test']])

    return sample_dictionary


class ClassifySet:
    # Object to house all the samples for passing to classifier, includes routines for joining signal and background oncce set
    # Also includes method for training and various optimizations

    def __init__(self):
        self.X_train, self.X_test, self.y_train, self.y_test, self.w_train, self.w_test = None, None, None, None, None, None
        self.X_train, self.X_test, self.y_train, self.y_test, self.w_train, self.w_test = None, None, None, None, None, None


    def add_sample(self, X_train, X_test, thisClass, w_train=None, w_test=None):
        self.X_train = np.concatenate(self.X_train, X_train) if self.X_train is None else X_train
        self.X_test = np.concatenate(self.X_test, X_test) if self.X_test is None else X_test
        self.y_train = np.concatenate(self.y_train,  np.ones(X_train.shape[0]) * thisClass) if self.y_train is None else  np.ones(X_train.shape[0]) * thisClass
        self.y_test =  np.concatenate(self.y_test,  np.ones(X_test.shape[0]) * thisClass) if self.y_test is None else  np.ones(X_test.shape[0]) * thisClass
        self.w_train = np.concatenate(self.w_train, w_train) if self.w_train is None else w_train
        self.w_test = np.concatenate(self.w_test, w_test) if self.w_test is None else w_test
            

    def optimize_depth(self, max_depth=10):
        print("Optimizing Depth")
        depth_scores_train=[]
        depth_scores_test=[]
        depth_range = np.arange(1, max_depth)
        for depth in depth_range:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict={"max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, bdt_kwargdict={"max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            depth_scores_train.append(accuracy_score(ypred_train,self.y_train))
            depth_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Depth {0} completed".format(depth))

        plt.plot(depth_range,depth_scores_train)
        plt.plot(depth_range,depth_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('Depth')
        plt.ylabel('Accuracy')
        plt.xticks(depth_range)
        best_depth = np.argmax(depth_scores_test)+1 #offset by one because rage starts at 1
        print("Best depth: {0}".format(best_depth))
        return best_depth
     
    def train(self, bdt_kwargdict=None):
        print("Training parameters: {0}".format(bdt_kwargdict))
        if self.w_train is not None and self.w_test is not None:
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names, bdt_kwargdict=bdt_kwargdict)
        else:
            return create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict=bdt_kwargdict)


    def optimize_trees(self, to_test=[25, 50, 100, 200, 300], depth=6):
        print("Optimizing Trees")
        tree_scores_train=[]
        tree_scores_test=[]
        for ntrees in to_test:
            if self.w_train is None or self.w_test is None:
                if depth == 1: print("No weights being used.")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            else:
                if depth == 1: print("Using weights")
                classifier = create_classifier(self.X_train, self.y_train, self.X_test, self.y_test, self.w_train, self.w_test, names=names, bdt_kwargdict={"n_estimators":ntrees, "max_depth":depth})
            
            ypred_train = classifier.predict(self.X_train)
            ypred_test = classifier.predict(self.X_test)
            ypred_train = [round(value) for value in ypred_train]
            ypred_test = [round(value) for value in ypred_test]
            tree_scores_train.append(accuracy_score(ypred_train,self.y_train))
            tree_scores_test.append(accuracy_score(ypred_test,self.y_test))
            print("Tested at {0} trees completed".format(ntrees))

        plt.plot(to_test, tree_scores_train)
        plt.plot(to_test, tree_scores_test)
        plt.legend(['Training','Validation'])
        plt.xlabel('nTrees')
        plt.ylabel('Accuracy')
        best_trees = to_test[np.argmax(tree_scores_test)] #offset by one because rage starts at 1
        print("Best trees: {0}".format(best_trees))
        return best_trees




def pca(X_train, X_test):
    pca = PCA(n_components=X_train.shape[1])
    X_train_PCA = pca.fit_transform(X_train)    
    X_test_PCA = pca.transform(X_test)

    print("PCA X train shape: {0}".format(X_train_PCA.shape))
    print("PCA X test shape: {0}".format(X_test_PCA.shape))
    return pca, X_train_PCA, X_test_PCA


if __name__ == "__main__":

    from xgb_training import ClassifySet
    import pandas as pd
    import seaborn as sns

    # Tests and Visualizations
    
    # Load vbf file
    sourcedir = "/xdata/tburch/workspace/bbyy_analysis/bbyy_vbf/signal_optimization/ntuples/"

    # Setup VBF samples, common between both signal and background
    vbf_files = [sourcedir + fname for fname in  ["forcuts_vbf_mc16a.npy","forcuts_vbf_mc16d.npy","forcuts_vbf_mc16e.npy"]]
    arr_vbf_train, vbf_eventweight_scalefactor_train = load_samples(vbf_files, ggf_preselection=True, test=False)
    arr_vbf_test, vbf_eventweight_scalefactor_test = load_samples(vbf_files, ggf_preselection=True, test=True)
    arr_vbf_train = arr_vbf_train[arr_vbf_train["jj_deta"] >= 0]
    arr_vbf_test = arr_vbf_test[arr_vbf_test["jj_deta"] >= 0]

    ## Rescale to accomodate for lost weights
    arr_vbf_train = rescale_eventweight(arr_vbf_train, vbf_eventweight_scalefactor_train) 
    arr_vbf_test = rescale_eventweight(arr_vbf_test, vbf_eventweight_scalefactor_test)
    ## Reshape to xgb inputs
    X_train_signal = make_clf_compliant(arr_vbf_train)
    X_test_signal = make_clf_compliant(arr_vbf_test)
    ## Set individual eventweights to be the average of the whole
    w_train_signal = np.ones(arr_vbf_train.shape[0]) * arr_vbf_train["eventweight"].sum() / arr_vbf_train["eventweight"].shape[0]
    w_test_signal = np.ones(arr_vbf_test.shape[0]) * arr_vbf_test["eventweight"].sum() / arr_vbf_test["eventweight"].shape[0]

    # Get ggF File
    ggf_files = [sourcedir + fname for fname in  ["forcuts_ggf_mc16a.npy","forcuts_ggf_mc16d.npy","forcuts_ggf_mc16e.npy"]]
    arr_ggf_train, ggf_eventweight_scalefactor_train = load_samples(ggf_files, ggf_preselection=True, test=False)
    arr_ggf_test, ggf_eventweight_scalefactor_test = load_samples(ggf_files, ggf_preselection=True, test=True)
    arr_ggf_train = arr_ggf_train[arr_ggf_train["jj_deta"] >= 0]
    arr_ggf_test = arr_ggf_test[arr_ggf_test["jj_deta"] >= 0]

    ## Rescale to accomodate for lost weights
    arr_ggf_train = rescale_eventweight(arr_ggf_train, ggf_eventweight_scalefactor_train)
    arr_ggf_test = rescale_eventweight(arr_ggf_test, ggf_eventweight_scalefactor_test)
    X_train_bkg = make_clf_compliant(arr_ggf_train)
    X_test_bkg = make_clf_compliant(arr_ggf_test)
    w_train_bkg = np.ones(arr_ggf_train.shape[0]) * arr_ggf_train["eventweight"].sum() / arr_ggf_train["eventweight"].shape[0]
    w_test_bkg = np.ones(arr_ggf_test.shape[0]) * arr_ggf_test["eventweight"].sum() / arr_ggf_test["eventweight"].shape[0]

    # Rebalance Classes
    w_train_signal = w_train_signal * (w_train_signal.shape[0] / w_train_signal.sum())
    w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())
    w_test_signal = w_test_signal * (w_test_signal.shape[0] / w_test_signal.sum())
    w_test_bkg = w_test_bkg * (w_test_signal.shape[0] / w_test_bkg.sum())
    
    # Set samples to Classifier Set object
    signalBDT_set = ClassifySet()
    signalBDT_set.set_signal_samples(X_train_signal, X_test_signal, w_train_signal, w_test_signal)
    signalBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg, w_train_bkg, w_test_bkg)
    # Finalize object
    signalBDT_set.join_samples()

    ## Background set
    yy_files = [sourcedir + fname for fname in  ["forcuts_yy_mc16a.npy","forcuts_yy_mc16d.npy","forcuts_yy_mc16e.npy"]]
    tth_files = [sourcedir + fname for fname in  ["forcuts_TTH_mc16a.npy","forcuts_TTH_mc16d.npy","forcuts_TTH_mc16e.npy"]]
    ggh_files = [sourcedir + fname for fname in  ["forcuts_GGH_mc16a.npy","forcuts_GGH_mc16d.npy","forcuts_GGH_mc16e.npy"]]
    zh_files = [sourcedir + fname for fname in  ["forcuts_ZH_mc16a.npy","forcuts_ZH_mc16d.npy","forcuts_ZH_mc16e.npy"]]

    arr_yy_train, yy_eventweight_scalefactor_train = load_samples(yy_files, ggf_preselection=True, test=False, myyCut=False)
    arr_yy_test, yy_eventweight_scalefactor_test = load_samples(yy_files, ggf_preselection=True, test=True, myyCut=False)
    arr_yy_train = rescale_eventweight(arr_yy_train, yy_eventweight_scalefactor_train)
    arr_yy_test = rescale_eventweight(arr_yy_test, yy_eventweight_scalefactor_test)
    w_train_yy = np.ones(arr_yy_train.shape[0]) * arr_yy_train["eventweight"].sum() / arr_yy_train["eventweight"].shape[0]
    w_test_yy = np.ones(arr_yy_test.shape[0]) * arr_yy_test["eventweight"].sum() / arr_yy_test["eventweight"].shape[0]

    # Single Higgs
    arr_tth_train, tth_eventweight_scalefactor_train = load_samples(tth_files, ggf_preselection=True, test=False)
    arr_tth_test, tth_eventweight_scalefactor_test = load_samples(tth_files, ggf_preselection=True, test=True)
    arr_tth_train = rescale_eventweight(arr_tth_train, tth_eventweight_scalefactor_train)
    arr_tth_test = rescale_eventweight(arr_tth_test, tth_eventweight_scalefactor_test)
    w_train_tth = ( np.ones(arr_tth_train.shape[0]) * arr_tth_train["eventweight"].sum() / arr_tth_train["eventweight"].shape[0] )
    w_test_tth = ( np.ones(arr_tth_test.shape[0]) * arr_tth_test["eventweight"].sum() / arr_tth_test["eventweight"].shape[0] )
    arr_ggh_train, ggh_eventweight_scalefactor_train = load_samples(ggh_files, ggf_preselection=True, test=False)
    arr_ggh_test, ggh_eventweight_scalefactor_test = load_samples(ggh_files, ggf_preselection=True, test=True)
    arr_ggh_train = rescale_eventweight(arr_ggh_train, ggh_eventweight_scalefactor_train)
    arr_ggh_test = rescale_eventweight(arr_ggh_test, ggh_eventweight_scalefactor_test)
    w_train_ggh = np.ones(arr_ggh_train.shape[0]) * arr_ggh_train["eventweight"].sum() / arr_ggh_train["eventweight"].shape[0]
    w_test_ggh = np.ones(arr_ggh_test.shape[0]) * arr_ggh_test["eventweight"].sum() / arr_ggh_test["eventweight"].shape[0]        
    arr_zh_train, zh_eventweight_scalefactor_train = load_samples(zh_files, ggf_preselection=True, test=False)
    arr_zh_test, zh_eventweight_scalefactor_test = load_samples(zh_files, ggf_preselection=True, test=True)
    arr_zh_train = rescale_eventweight(arr_zh_train, zh_eventweight_scalefactor_train)
    arr_zh_test = rescale_eventweight(arr_zh_test, zh_eventweight_scalefactor_test)   
    w_train_zh = np.ones(arr_zh_train.shape[0]) * arr_zh_train["eventweight"].sum() / arr_zh_train["eventweight"].shape[0]
    w_test_zh = np.ones(arr_zh_test.shape[0]) * arr_zh_test["eventweight"].sum() / arr_zh_test["eventweight"].shape[0] 

    #joined_backgrounds_train = np.concatenate([arr_yy_train, arr_tth_train, arr_ggh_train, arr_zh_train])
    #joined_backgrounds_test = np.concatenate([arr_yy_test, arr_tth_test, arr_ggh_test, arr_zh_test])
    joined_backgrounds_train = np.concatenate([arr_tth_train, arr_ggh_train, arr_zh_train])
    joined_backgrounds_test = np.concatenate([arr_tth_test, arr_ggh_test, arr_zh_test])

    X_train_bkg = make_clf_compliant(joined_backgrounds_train)
    X_test_bkg = make_clf_compliant(joined_backgrounds_test)
    #w_train_bkg = np.abs(np.concatenate([w_train_yy, w_train_tth, w_train_ggh, w_train_zh]))
    #w_test_bkg = np.abs(np.concatenate([w_test_yy, w_test_tth, w_test_ggh, w_test_zh]))
    w_train_bkg = np.abs(np.concatenate([w_train_tth, w_train_ggh, w_train_zh]))
    w_test_bkg = np.abs(np.concatenate([w_test_tth, w_test_ggh, w_test_zh]))

    w_train_signal = w_train_signal * (w_train_signal.shape[0] / w_train_signal.sum())
    w_train_bkg = w_train_bkg * (w_train_signal.shape[0] / w_train_bkg.sum())
    w_test_signal = w_test_signal * (w_test_signal.shape[0] / w_test_signal.sum())
    w_test_bkg = w_test_bkg * (w_test_signal.shape[0] / w_test_bkg.sum())        

    # Set samples to Classifier Set object
    backgroundBDT_set = ClassifySet()
    backgroundBDT_set.set_signal_samples(X_train_signal, X_test_signal, w_train_signal, w_test_signal)
    backgroundBDT_set.set_bkg_samples(X_train_bkg, X_test_bkg, w_train_bkg, w_test_bkg)
    backgroundBDT_set.join_samples()


    # PCA 
    # ------------------------------
    def pca_exp_var(pca, k):
        """ Print the total variance of the k principal components. """
        print (np.sum(pca.explained_variance_ratio_[:k]))

    signal_pca, X_train_PCA, X_test_PCA = pca(signalBDT_set.X_train, signalBDT_set.X_test)    
    # Plot explained variance PCA    
    plt.figure(figsize=(8, 6))
    plt.plot(signal_pca.explained_variance_ratio_, '--o')
    plt.xlabel('Principal component', fontsize=14)
    plt.ylabel('Variance explained (ratio)', fontsize=14)
    plt.grid(True)
    plt.savefig("run/signal_pca")
    plt.close()
    n_pca_features = 10
    sub_X_train_PCA = X_train_PCA[:,:n_pca_features]
    print("Explained Variance in signal sample by {0} features: ".format(n_pca_features))
    pca_exp_var(signal_pca, n_pca_features)
    #print (signal_pca.components_)
    print("Estimated components: {0}".format(signal_pca.n_components_))


    names=[
        "jj_deta",
        "mjj",
        "n_jet",
        "m_yyjj",
        "dR_yy",
        "dR_jj",
        "dR_yyjj",
        "j1_tagbin",
        "j2_tagbin",
        "aplanority",
        "aplanarity",
        "sphericity",
        "spherocity",
        "sphericityT",
        "planarity",
        "circularity",
        "planarFlow",
        "ht",
        "mjjb1",
        "mjjb2",
        "mjj_w1",
        "mjj_w2",        

    ]
    import pdb
    added_pca = np.concatenate([signalBDT_set.X_train, sub_X_train_PCA], axis=1)
    new_names =  ["PCA{0}".format(i) for i in range(0,n_pca_features)]
    total_names = names + new_names
    #pdb.set_trace()
    signal_df = pd.DataFrame(added_pca, columns=total_names) 

    corr = signal_df.corr()
    sns.heatmap(corr, 
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values,
            linewidths=.5,
            cmap="coolwarm",
            vmin=-1,
            vmax=1
            )
    plt.tight_layout()
    plt.savefig(sourcedir + "../run/signalPCA_corr.png")
    plt.close()


    background_pca, X_train_PCA, X_test_PCA = pca(backgroundBDT_set.X_train, backgroundBDT_set.X_test)
    # Plot explained variance PCA    
    plt.figure(figsize=(8, 6))
    plt.plot(background_pca.explained_variance_ratio_, '--o')
    plt.xlabel('Principal component', fontsize=14)
    plt.ylabel('Variance explained (ratio)', fontsize=14)
    plt.grid(True)
    plt.savefig("run/background_pca")
    plt.close()
    n_pca_features = 11
    sub_X_train_PCA = X_train_PCA[:,:n_pca_features]
    print("Explained Variance in signal sample by {0} features: ".format(n_pca_features))
    pca_exp_var(background_pca, n_pca_features)
    print("Estimated components: {0}".format(signal_pca.n_components_))

    added_pca = np.concatenate([backgroundBDT_set.X_train, sub_X_train_PCA], axis=1)
    new_names =  ["PCA{0}".format(i) for i in range(0,n_pca_features)]
    total_names = names + new_names
    background_df = pd.DataFrame(added_pca, columns=total_names) 

    corr = background_df.corr()
    sns.heatmap(corr, 
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values,
            linewidths=.5,
            cmap="coolwarm",
            vmin=-1,
            vmax=1
            )
    plt.tight_layout()
    plt.savefig(sourcedir + "../run/backgroundPCA_corr.png")
    plt.close()