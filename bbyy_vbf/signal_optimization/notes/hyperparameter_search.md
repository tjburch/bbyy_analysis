# This is just a log of the hyperparameter searches conducted



## Search 1
search_params = {
    'n_estimators': [10, 40, 80, 100, 200, 400, 800, 1200],
    'max_depth': [2, 3, 4, 5, 10, 20],
    'min_samples_leaf': [1, 2, 4],
    'min_samples_split': [2, 5, 10],
    'max_features': [None, 'auto', 'sqrt'],
}


### Background Tree:
{'max_features': None, 'min_samples_split': 10, 'n_estimators': 80, 'max_depth': 5, 'min_samples_leaf': 4}

min_samples_leaf and min_samples_split both took on the maximal value so investigate better values for that


### Signal Tree:
{'max_features': 'sqrt', 'min_samples_split': 2, 'n_estimators': 200, 'max_depth': 5, 'min_samples_leaf': 2}
