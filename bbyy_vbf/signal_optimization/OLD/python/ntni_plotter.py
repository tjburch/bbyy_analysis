import sys
import ROOT
from AtlasStyle import ATLASLabel
from histogram_tools import normalize_histos

data_directory = '/Users/tburch/Documents/workArea/aprWorkspace/bbyy_analysis/bbyy_vbf/signal_optimization/data/'
input_file = ROOT.TFile(data_directory + 'evaluated_histograms.root')
hists = {}
hists['vbf'] = input_file.Get('vbf_hist')
hists['ggf'] = input_file.Get('ggf_hist')
hists['bkg'] = input_file.Get('bkg_hist')
hists['ntni'] = input_file.Get('data_hist')


mycanvas = ROOT.TCanvas()

ROOT.gStyle.SetOptStat(0000000)

# Draw the sherpa histogram
hists['bkg'].SetFillColorAlpha(ROOT.kRed+1, .5)
hists['bkg'].SetLineColor(ROOT.kRed+1)

hists['bkg'].SetTitle('')
hists['bkg'].GetXaxis().SetTitle('BDT Response')
normalize_histos(hists)

hists['bkg'].GetXaxis().SetRangeUser(-0.6, 0.6)


hists['bkg'].Draw('hist')

# Draw signal
hists['vbf'].SetFillColorAlpha(ROOT.kAzure+1, .5)
hists['vbf'].SetLineColor(ROOT.kAzure+1)
hists['vbf'].Scale(1 / hists['vbf'].Integral())
hists['vbf'].Draw('hist sames')

# Draw the NTNI histogram
hists['ntni'].SetLineColor(ROOT.kGreen+2)
hists['ntni'].Scale(1 / hists['ntni'].Integral())
hists['ntni'].Draw('sames e')

# Atlas internal
ATLASLabel(0.15, 0.85, 'Internal', ROOT.kBlack)

# Make Legend
leg = ROOT.TLegend(0.63, 0.6, 0.88, 0.88)
leg.SetLineWidth(0)
leg.AddEntry(hists['bkg'], 'Sherpa #gamma#gamma+jets', 'f')
leg.AddEntry(hists['vbf'], 'hh #rightarrow b#bar{b}#gamma#gammajj MC', 'f')
leg.AddEntry(hists['ntni'],
             'NTNI Events from Data', 'le')
leg.Draw()

mycanvas.Print('plots/ntni_overlay.pdf')
