from ROOT import *
from math import *

#lum = 150/36.1
#####lum = lum * 10 ### SM * 10
#thes=lum*3.207264e-01
#theb=lum*7.899255e+01

## from tyler
thes=1.064
theb=63.672


file = TFile('data/evaluated_histograms.root', "READ")
###file = TFile('tmva_output.root',"READ")
sigtree = file.Get("sig_eval")
bkgtree = file.Get("bkg_eval")

cutlow=-1
cuthigh=1
dcut=0.06

nb = bkgtree.GetEntries()
ns = sigtree.GetEntries()

g = TGraph()
i=0
thiscut=cuthigh
while (thiscut >= cutlow):
  bintegral = bkgtree.Draw("bkg_eval","bkg_eval < %.3f"%(thiscut))
  sintegral = sigtree.Draw("signal_eval","signal_eval < %.3f"%(thiscut))
  b = theb * bintegral/nb
  s = thes * sintegral/ns
  b2 = theb - b
  s2 = thes - s

  significance = 0
  significance2 = 0
  if(b>1e-6):
    significance = sqrt(2*((s+b)*log(1+s/b)-s))
  if(b2>1e-6):
    significance2 = sqrt(2*((s2+b2)*log(1+s2/b2)-s2))
  combined = sqrt(significance*significance+significance2*significance2)

  ###print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f and s = %.3f and b=%.3f"%(thiscut,significance,significance2,combined,s,b))
  print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f"%(thiscut,significance,significance2,combined))
  g.SetPoint(i,thiscut,combined)

  thiscut = thiscut - dcut
  i=i+1

###g.Set(i)

canvas = TCanvas("c1","c1",800,600)
canvas.cd()
g.Draw("AP")
g.SetMarkerSize(1)
g.SetMarkerStyle(10)
g.SetMarkerColor(1)
g.GetXaxis().SetTitle("BDT cut")
g.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")

canvas.Print("nobtag_significance.pdf")


