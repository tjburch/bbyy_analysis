import ROOT
from math import ceil, sqrt, floor
from asimov_significance import *
import argparse


def signal_over_background(s, b):
    if b == 0:
        return 0
    else:
        return (s / sqrt(b))


parser = argparse.ArgumentParser(
    'Loads in 3d sig/background histogram, optimizes cut location')
parser.add_argument('-v', dest='vbf_signal_file',
                    required=True, help='vbf signal file')
parser.add_argument('-g', dest='ggf_signal_file',
                    required=True, help='ggf signal file')
parser.add_argument('-b', dest='background_file',
                    required=True, help='background file')
parser.add_argument('--verbose', dest='verbose', required=False,
                    default=False, action='store_true', help='verbose output')
parser.add_argument('-l', dest='lumi', type=float, required=False,
                    default=150, help="luminosity to scale")
parser.add_argument('-r', dest='rate', type=float, required=False,
                    default=1, help="enhanced VBF rate")
parser.add_argument('-o', dest='output_name', required=False,
                    default='plots/significance_histos.root', help='output file name')
args = parser.parse_args()

print "Options:"
print "     Verbose = ", args.verbose
print "        lumi = ", args.lumi, "fb-1"
print "    vbf rate = ", args.rate, "*SM"

# Get histograms from files
vbf_signal_file_load = ROOT.TFile(args.vbf_signal_file)
ggf_signal_file_load = ROOT.TFile(args.ggf_signal_file)
background_file_load = ROOT.TFile(args.background_file)

# Load VBF
signal_jet_n = vbf_signal_file_load.Get("jet_n")
signal_jet_n.SetName("signal_jet_n")
signal_dEta = vbf_signal_file_load.Get("mjj_dEta")
signal_dEta.SetName("signal_dEta")
signal_mjj = vbf_signal_file_load.Get("mjj")
signal_mjj.SetName("signal_mjj")
# Scale VBF
all_signal = [signal_jet_n, signal_dEta, signal_mjj]
for signal_hist in all_signal:
    signal_hist.Scale(args.rate)

# Add ggF
ggf_signal_jet_n = ggf_signal_file_load.Get("jet_n")
ggf_signal_dEta = ggf_signal_file_load.Get("mjj_dEta")
ggf_signal_mjj = ggf_signal_file_load.Get("mjj")
signal_jet_n.Add(ggf_signal_jet_n)
signal_dEta.Add(ggf_signal_dEta)
signal_mjj.Add(ggf_signal_mjj)

background_jet_n = background_file_load.Get("jet_n")
background_jet_n.SetName("background_jet_n")
background_dEta = background_file_load.Get("mjj_dEta")
background_dEta.SetName("background_dEta")
background_mjj = background_file_load.Get("mjj")
background_mjj.SetName("background_mjj")

all_background = [background_jet_n, background_dEta, background_mjj]


# Apply k-factor - hardcoded :(
k_factor = 5
for bkg_hist in all_background:
    bkg_hist.Scale(k_factor)


# lumi in fb
for hist in (all_signal + all_background):
    hist.Scale(args.lumi / 150)

# --------------------------------------------------------------------------
# Significance Code

# function to loop over an axis, apply cuts over full range


def optimize_cut(signal_histogram, background_histogram, fcn_merit, granularity):
    """
    bool cut_up - true if cutting above value, false if below
    returns: TH1F of significance at each value
    """

    # get useful stuff from histograms
    if signal_histogram.GetName() != 'signal_mjj':
        hist_min = 0
        hist_max = 14
    else: 
        hist_min = 0
        hist_max = 200
    #    hist_min = background_histogram.GetXaxis().GetXmin()
    cut_value = 0
    #hist_max = background_histogram.GetXaxis().GetXmax()
    # bins hypothetically should be an integer by math but round to be safe
    bins = int(floor((hist_max - hist_min) / granularity))

    cut_significance = ROOT.TH1F(signal_histogram.GetName() + "_sig",
                                 signal_histogram.GetName() + "_sig", bins, hist_min, hist_max)

    best_significance = 0
    if args.verbose:
        print "Cutting on: ", signal_histogram.GetName()

    # Scan histogram
    while cut_value < hist_max:

        # Cut signal histogram above and below
        # BELOW
        h_sig_below = signal_histogram.Clone()
        h_sig_below.GetXaxis().SetRangeUser(hist_min, cut_value)
        signal_below = h_sig_below.Integral()

        # ABOVE
        h_sig_above = signal_histogram.Clone()
        h_sig_above.GetXaxis().SetRangeUser(cut_value, hist_max)
        signal_above = h_sig_above.Integral()

        # Cut background histogram above and below
        # BELOW
        h_background_below = background_histogram.Clone()
        h_background_below.GetXaxis().SetRangeUser(hist_min, cut_value)
        background_below = h_background_below.Integral()

        # ABOVE
        h_background_above = background_histogram.Clone()
        h_background_above.GetXaxis().SetRangeUser(cut_value, hist_max)
        background_above = h_background_above.Integral()

        # Calculate significance
        significance_below = fcn_merit(
            signal_below, background_below)
        significance_above = fcn_merit(
            signal_above, background_above)
        total_significance = sqrt(
            significance_below**2 + significance_above**2)

        if cut_value > 0:
            if total_significance > best_significance:
                best_significance = total_significance
            cut_significance.SetBinContent(
                cut_significance.FindBin(cut_value), total_significance)

        if (args.verbose):
            print "  cut_value: ", cut_value
            print "    Bin no.: ", cut_significance.FindBin(cut_value)
            print "    Bin chk.: ", int(ceil(cut_value / granularity + 1))
            print "granularity.: ", granularity
            print "      signal below ", signal_below
            print "             above ", signal_above
            print "         bkg below ", background_below
            print "             above ", background_above
            print "  significance ", total_significance
            print "             Above ", significance_above
            print "             Below ", significance_below
            print "\n"

        # increment by granularity
        cut_value += granularity

    # if granularity == 1.0:
    #     print "Best Significance: ", best_significance
    return cut_significance


# Scan over parameters to cut on, getting histograms of significance at
# each value
h_jet_n_significance = optimize_cut(signal_jet_n,
                                    background_jet_n,
                                    asimov_significance,
                                    1.0)
h_dEta_significance = optimize_cut(signal_dEta,
                                   background_dEta,
                                   asimov_significance,
                                   0.25)
h_mjj_significance = optimize_cut(signal_mjj,
                                  background_mjj,
                                  asimov_significance,
                                  4)
h_jet_n_sbr = optimize_cut(signal_jet_n,
                           background_jet_n,
                           signal_over_background,
                           1.0)
h_dEta_sbr = optimize_cut(signal_dEta,
                          background_dEta,
                          signal_over_background,
                          0.25)
h_mjj_sbr = optimize_cut(signal_mjj,
                         background_mjj,
                         signal_over_background,
                         4)

# Go ahead and write those to a file
output_histo_list = [h_jet_n_significance,
                     h_dEta_significance,
                     h_mjj_significance]
output_histo_list = [h_jet_n_sbr,
                     h_dEta_sbr,
                     h_mjj_sbr]

output_file = ROOT.TFile(args.output_name, "RECREATE")
for h in output_histo_list:
    h.Write()


# Max value of those cuts will be the place where significance is optimized
best_cuts_significance = []  # list to house best cuts
for h in output_histo_list:
    binmax = h.GetMaximumBin()
    xmax = h.GetXaxis().GetBinCenter(binmax)
    best_cuts_significance.append(xmax)

best_cuts_sbr = []  # list to house best cuts
for h in output_histo_list:
    binmax = h.GetMaximumBin()
    xmax = h.GetXaxis().GetBinCenter(binmax)
    best_cuts_sbr.append(xmax)

# Print optimized rectangular cuts
print "Optmized cuts for significance: "
print "   jet_n: ", best_cuts_significance[0]
print "   dEta: ", best_cuts_significance[1]
print "   mjj : ", best_cuts_significance[2]

print "Optmized cuts for signal to background: "
print "   jet_n: ", best_cuts_sbr[0]
print "   dEta: ", best_cuts_sbr[1]
print "   mjj : ", best_cuts_sbr[2]


print "Total Signal Events: ", signal_jet_n.Integral()
print "               ggf: ", ggf_signal_jet_n.Integral()
print "               vbf: ", signal_jet_n.Integral() - ggf_signal_jet_n.Integral()
print "Total Background Events: ", background_jet_n.Integral()
