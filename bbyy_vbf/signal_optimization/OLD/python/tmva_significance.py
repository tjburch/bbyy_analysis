from ROOT import *
from math import *
from AtlasStyle import *
AtlasStyle()
#lum = 150/36.1
# lum = lum * 10 ### SM * 10
# thes=lum*3.207264e-01
# theb=lum*7.899255e+01

# from tyler
thes = 1.064
theb = 63.672


file = TFile('data/evaluated_histograms.root', "READ")
###file = TFile('tmva_output.root',"READ")
h_vbf = file.Get('vbf_hist')
h_ggf = file.Get('ggf_hist')
h_bkg = file.Get('bkg_hist')
h_sig = h_vbf.Clone()
h_sig.Add(h_ggf)

cutlow = -1
cuthigh = 1
dcut = 0.04

nb = h_sig.Integral()
ns = h_bkg.Integral()

g = TGraph()
i = 0
thiscut = cuthigh
canvas = TCanvas()
best_sig = 0
best_sig_cut = -1
while (thiscut >= cutlow):
  s_cuth = h_sig.Clone()
  s_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)
  b_cuth = h_sig.Clone()
  b_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)

  sintegral = s_cuth.Integral()
  bintegral = b_cuth.Integral()

  b = theb * bintegral / nb
  s = thes * sintegral / ns
  b2 = theb - b
  s2 = thes - s

  significance = 0
  significance2 = 0
  if(b > 1e-6):
    significance = sqrt(2 * ((s + b) * log(1 + s / b) - s))
  if(b2 > 1e-6):
    significance2 = sqrt(2 * ((s2 + b2) * log(1 + s2 / b2) - s2))
  combined = sqrt(significance * significance + significance2 * significance2)

  ###print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f and s = %.3f and b=%.3f"%(thiscut,significance,significance2,combined,s,b))
  print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f" %
        (thiscut, significance, significance2, combined))
  g.SetPoint(i, thiscut, combined)
  if (combined > best_sig):
    best_sig_cut = i
    best_sig = combined
  thiscut = thiscut - dcut
  i = i + 1

# g.Set(i)

canvas = TCanvas("c1", "c1", 900, 600)
canvas.cd()
g.Draw("AP")
g.SetMarkerSize(1)
g.SetMarkerStyle(10)
g.SetMarkerColor(1)
g.GetXaxis().SetTitle("BDT cut")
g.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")

canvas.Print("plots/nobtag_significance.pdf")
####################################################

canvas.Clear()
gStyle.SetOptStat(0000000)
hists = [h_vbf, h_ggf, h_bkg]
norm_val = [1.0 / h.Integral() for h in hists]

hists[0].Scale(norm_val[0])
hists[1].Scale(norm_val[1])
hists[2].Scale(norm_val[2])
maxval = max([h.GetMaximum() for h in hists])


hists[0].SetLineColor(2)
hists[0].GetXaxis().SetRangeUser(-0.4, 0.4)
hists[0].GetXaxis().SetTitle('BDT Response')
hists[0].GetYaxis().SetTitle('AU (normalized to 1)')
hists[0].GetYaxis().SetTitleOffset(1.1)
hists[0].SetMaximum(maxval * 1.2)
hists[0].Draw('hist')

hists[1].SetLineColor(4)
hists[1].Draw('hist sames')

hists[2].SetLineColor(6)
hists[2].Draw('hist sames')

leg = TLegend(.14, .7, .3, .9)
leg.AddEntry(hists[0], 'VBF', 'l')
leg.AddEntry(hists[1], 'ggF', 'l')
leg.AddEntry(hists[2], 'Sherpa', 'l')
leg.SetBorderSize(0)
leg.Draw()

canvas.Print('plots/bdt_response.pdf')


####################################################
canvas.Clear()
high_vbf = h_vbf.Clone()
high_vbf.GetXaxis().SetRangeUser(best_sig_cut, 1)
high_vbf.SetFillColor(5)

high_ggf = h_ggf.Clone()
high_ggf.GetXaxis().SetRangeUser(best_sig_cut, 1)
high_ggf.SetFillColor(3)

# h_vbf.GetXaxis().SetRangeUser(-1,best_sig_cut)
# h_vbf.SetFillColor(2)
# h_ggf.GetXaxis().SetRangeUser(-1,best_sig_cut)
# h_ggf.SetFillColor(3)

signal_stack = THStack("stack", "")
signal_stack.Add(high_ggf)
signal_stack.Add(high_vbf)
# signal_stack.Add(h_vbf)
# signal_stack.Add(h_ggf)

signal_stack.Draw()


high_bkg = h_bkg.Clone()
high_bkg.GetXaxis().SetRangeUser(best_sig_cut, 1)
high_bkg.SetLineColor(6)

h_bkg.SetFillColor(6)
h_bkg.GetXaxis().SetRangeUser(-1, best_sig_cut)

# h_bkg.Draw('SAMES')
leg = TLegend(.7, .7, .9, .9)
leg.AddEntry(high_vbf, 'cat1 VBF', 'l')
leg.AddEntry(high_ggf, 'cat1 ggF', 'l')
# leg.AddEntry(high_bkg, 'cat1 Sherpa bkg','l')
# leg.AddEntry(h_vbf, 'cat2 VBF','f')
# leg.AddEntry(h_ggf, 'cat2 ggF ','f')
#leg.AddEntry(h_bkg, 'cat2 Sherpa bkg','f')
leg.Draw()
line = TLine(best_sig_cut, 0, best_sig_cut + 1, 0.145)
line.Draw()
canvas.Print("plots/bdt_cut.pdf")
