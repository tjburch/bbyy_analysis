def weight_reference(sample_type):
    if sample_type == "sherpa":
        # Sherpa_2DP20_myy_100_165_3jets
        # Pulled from MCSamples.config
        cross_section_br = 41.504
        generator_efficiency = 0.49904
        sum_of_weights = 1.0527E8
    elif sample_type == "vbf":
        # This sample built by hand so derive these numbers
        cross_section = 1.62 / 1000 # 1.62 fb in pb
        BR_Hbb = 0.5809 # H->bb BR
        BR_Hyy = 2.270E-03 # H->yy BR
        cross_section_br = cross_section * BR_Hbb * BR_Hyy * 2
        generator_efficiency = 0.49354 # From event generation
        sum_of_weights = -1
    elif sample_type == "nonres":
        # This is for nonresonant HH->yybb
        # Sample name aMCnlo_Hwpp_hh_yybb
        # Values from MCSamples.config
        cross_section_br = 8.8111E-05
        generator_efficiency = 1.
        sum_of_weights = -1
    else:
        # Just so it doesn't break
        cross_section_br = 1.
        generator_efficiency = 1.
        sum_of_weights = 1.
    weight_dict = {'cross_section_br': cross_section_br,
                   'generator_efficiency': generator_efficiency,
                   'sum_of_weights': sum_of_weights}
    return weight_dict


def set_weight(lumi, xBR, genEff, skimEff, sumW):
    # Multiplies out all the components for weight
    return (lumi * xBR * genEff * skimEff / sumW)

def get_skim_eff(infile):
    # Gets skim efficiency from cutflow in input file
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency
