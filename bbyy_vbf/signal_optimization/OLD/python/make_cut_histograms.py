import ROOT
from make_2tag_histograms import get_mjj_pair, get_cutflow_weights
from weight_reference import weight_reference
import sys


def create_histograms():
    d_histograms = {}
    d_histograms['signature_jet_n'] = ROOT.TH1F(
        "signature_jet_n", "signature_jet_n", 20, 0, 20)
    d_histograms['nonsignature_jet_n'] = ROOT.TH1F("nonsignature_jet_n", "nonsignature_jet_n", 20, 0, 20)
    d_histograms['signature_mjj_dEta'] = ROOT.TH1F(
        "signature_mjj_dEta", "signature_mjj_dEta", 68, -1, 16)
    d_histograms['nonsignature_mjj_dEta'] = ROOT.TH1F(
        "nonsignature_mjj_dEta", "nonsignature_mjj_dEta", 68, -1, 16)

    for hist in d_histograms:
        d_histograms[hist].Sumw2()

    return d_histograms


if __name__ == "__main__":

    input_file = sys.argv[1]
    output_name = sys.argv[2]
    sample_type = sys.argv[3]

    njets_cut = 4
    deta_cut = 2.75

    print "Sample Type: ", sample_type

    # Get input tree
    f_input = ROOT.TFile(input_file)
    t_input = f_input.Get("mini")
    nEntries = t_input.GetEntries()
    print "Entries to use: ", nEntries

    # Get MC weight information
    h_cutflow = f_input.Get("cutflow_weighted")
    d_weights = weight_reference(sample_type)
    sum_of_weights, skim_efficiency = get_cutflow_weights(h_cutflow)
    if sample_type != 'sherpa':
        d_weights['sum_of_weights'] = sum_of_weights
    d_weights['skim_efficiency'] = skim_efficiency

    luminosity = 150000
    print "luminosity: ", luminosity / 1000.0
    print "BR * xsec: ", d_weights['cross_section_br']
    print "Sum of Weights: ", d_weights['sum_of_weights']
    print "skim_efficiency: ", d_weights['skim_efficiency']

    d_histograms = create_histograms()

    # -----------------------------------------------------------------
    # Loop over entries

    for event in range(nEntries):
        t_input.GetEntry(event)

        if sample_type != 'sherpa':
            if t_input.yybb_low_btagCat != 2:
                continue
        if t_input.jet_n < 2:
            continue

        # ______________________________
        # Evalutate VBF signature or not
        signature = True
        if t_input.jet_n < njets_cut:
            signature = False

        leading_idx, subleading_idx = get_mjj_pair(t_input)
        if leading_idx != -1 and subleading_idx != -1:
            deta = abs(t_input.jet_eta[leading_idx] - t_input.jet_eta[subleading_idx])
        else:
            deta = -1

        if deta < deta_cut:
            signature = False

        # ______________________________
        # Fill appropriate histograms
        w = (t_input.hgam_weight *
             t_input.yybb_low_weight *
             luminosity *
             d_weights['cross_section_br'] *
             d_weights['generator_efficiency'] *
             d_weights['skim_efficiency'] /
             d_weights['sum_of_weights'])

        if signature:
            d_histograms['signature_jet_n'].Fill(t_input.jet_n, w)
            d_histograms['signature_mjj_dEta'].Fill(deta, w)
        else:
            d_histograms['nonsignature_jet_n'].Fill(t_input.jet_n, w)
            d_histograms['nonsignature_mjj_dEta'].Fill(deta, w)

    # Scale to 2-tag set
    if sample_type == 'sherpa':
        scale_factor = 63.672 / 17804.823 # Value at 150fb-1
        for key in d_histograms:
            d_histograms[key].Scale(scale_factor)        

    f_output = ROOT.TFile(output_name, 'recreate')
    for key in d_histograms:
        d_histograms[key].Write()
    f_output.Close()
