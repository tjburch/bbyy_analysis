# Makes histograms using selected events
import ROOT
import sys
from weight_reference import weight_reference


def get_cutflow_weights(h_cutflow):
    # uses cutflow histogram to get sum_of_weights, skim efficiency
    # return as a tuple
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency


def get_mjj_pair(tree):

    leading_m, subleading_m = -1, -1
    leading_idx, subleading_idx = -1, -1
    for iJet in range(tree.jet_n):
        b_is_tagged = tree.jet_MV2c10_FixedCutBEff_85[iJet]
        if not b_is_tagged:
            if tree.jet_m[iJet] > leading_m:
                leading_idx = iJet
                leading_m = tree.jet_m[iJet]
            elif tree.jet_m[iJet] > subleading_m:
                subleading_idx = iJet
                subleading_m = tree.jet_m[iJet]
    return leading_idx, subleading_idx


def create_histograms():
    d_histograms = {}
    d_histograms['jet_n'] = ROOT.TH1F("jet_n", "jet_n", 20, 0, 20)
    d_histograms['mjj_dEta'] = ROOT.TH1F("mjj_dEta", "mjj_dEta", 68, -1, 16)
    d_histograms['mjj'] = ROOT.TH1F("mjj", "mjj", 50, 0, 200)

    for hist in d_histograms:
        d_histograms[hist].Sumw2()

    return d_histograms


if __name__ == "__main__":
    input_file = sys.argv[1]
    output_name = sys.argv[2]
    sample_type = sys.argv[3]

    print "Sample Type: ", sample_type

    # Get input tree
    f_input = ROOT.TFile(input_file)
    t_input = f_input.Get("mini")
    nEntries = t_input.GetEntries()
    print "Entries to use: ", nEntries

    # Get MC weight information
    h_cutflow = f_input.Get("cutflow_weighted")
    d_weights = weight_reference(sample_type)
    sum_of_weights, skim_efficiency = get_cutflow_weights(h_cutflow)
    if sample_type != 'sherpa':
        d_weights['sum_of_weights'] = sum_of_weights
    d_weights['skim_efficiency'] = skim_efficiency

    luminosity = 150000
    print "luminosity: ", luminosity / 1000.0
    print "BR * xsec: ", d_weights['cross_section_br']
    print "Sum of Weights: ", d_weights['sum_of_weights']
    print "skim_efficiency: ", d_weights['skim_efficiency']

    d_histograms = create_histograms()

    # -----------------------------------------------------------------
    # Loop over entries

    for event in range(nEntries):
        t_input.GetEntry(event)

        if t_input.yybb_low_btagCat != 2:
            continue

        w = (t_input.hgam_weight *
             t_input.yybb_low_weight *
             luminosity *
             d_weights['cross_section_br'] *
             d_weights['generator_efficiency'] *
             d_weights['skim_efficiency'] /
             d_weights['sum_of_weights'])

        d_histograms['jet_n'].Fill(t_input.jet_n, w)

        leading_idx, subleading_idx = get_mjj_pair(t_input)
        if leading_idx != -1 and subleading_idx != -1:
            deta = abs(t_input.jet_eta[leading_idx] - t_input.jet_eta[subleading_idx])
            mjj = t_input.jet_m[leading_idx] + t_input.jet_m[subleading_idx]
        else:
            deta = -1
            mjj = -1

        d_histograms['mjj_dEta'].Fill(deta, w)
        d_histograms['mjj'].Fill(mjj, w)

    f_output = ROOT.TFile(output_name, 'recreate')
    for key in d_histograms:
        d_histograms[key].Write()
    f_output.Close()
