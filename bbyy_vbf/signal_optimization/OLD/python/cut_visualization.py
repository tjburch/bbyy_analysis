import sys
from asimov_significance import asimov_significance
import ROOT
from math import sqrt
from AtlasStyle import *


def get_all_histograms(input_file):

    signature_jet_n = input_file.Get("signature_jet_n")
    nonsignature_jet_n = input_file.Get("nonsignature_jet_n")
    signature_mjj_dEta = input_file.Get("signature_mjj_dEta")
    nonsignature_mjj_dEta = input_file.Get("nonsignature_mjj_dEta")

    d_return = {'signature_jet_n': signature_jet_n,
                'nonsignature_jet_n': nonsignature_jet_n,
                'signature_mjj_dEta': signature_mjj_dEta,
                'nonsignature_mjj_dEta': nonsignature_mjj_dEta}

    return d_return


def add_histograms(d_histogram_list):

    return_jet_n = d_histogram_list['signature_jet_n'].Clone()
    return_dEta = d_histogram_list['signature_mjj_dEta'].Clone()

    # sum jet_n
    return_jet_n.Add(d_histogram_list['nonsignature_jet_n'])
    return_dEta.Add(d_histogram_list['nonsignature_mjj_dEta'])

    return return_jet_n, return_dEta


if __name__ == "__main__":

    vbf_sample = ROOT.TFile(sys.argv[1])
    ggf_sample = ROOT.TFile(sys.argv[2])
    sherpa_sample = ROOT.TFile(sys.argv[3])

    d_vbf_histo_list = get_all_histograms(vbf_sample)
    d_ggf_histo_list = get_all_histograms(ggf_sample)
    d_sherpa_histo_list = get_all_histograms(sherpa_sample)

    k_factor = 5
    for h in d_sherpa_histo_list:
        d_sherpa_histo_list[h].Scale(k_factor)

    signature_signal_integral = (d_vbf_histo_list['signature_jet_n'].Integral() +
                                 d_ggf_histo_list['signature_jet_n'].Integral())
    signature_background_integral = d_sherpa_histo_list['signature_jet_n'].Integral()

    nonsignature_signal_integral = (d_vbf_histo_list['nonsignature_jet_n'].Integral() +
                                    d_ggf_histo_list['nonsignature_jet_n'].Integral())
    nonsignature_background_integral = d_sherpa_histo_list['nonsignature_jet_n'].Integral()

    vbf_sum_jet_n, vbf_sum_deta = add_histograms(d_vbf_histo_list)
    ggf_sum_jet_n, ggf_sum_deta = add_histograms(d_ggf_histo_list)
    sherpa_sum_jet_n, sherpa_sum_deta = add_histograms(d_sherpa_histo_list)

    full_signal_integral = vbf_sum_jet_n.Integral() + ggf_sum_jet_n.Integral()
    full_background_integral = sherpa_sum_jet_n.Integral()

    print "Signal Events: "
    print "    Inclusive:  %.3f" % full_signal_integral
    print "    VBFsig:  %.3f" % signature_signal_integral
    print " nonVBFsig:  %.3f" % nonsignature_signal_integral

    print "background Events: "
    print "    Inclusive:  %.3f" % full_background_integral
    print "    VBFsig:  %.3f" % signature_background_integral
    print " nonVBFsig:  %.3f" % nonsignature_background_integral

    print " _____________________________________ "
    print "S/B : "
    print "    Inclusive:  %.5f" % (full_signal_integral / full_background_integral)
    if signature_background_integral != 0:
        print "    VBFsig:  %.5f" % (signature_signal_integral / signature_background_integral)
    print " nonVBFsig:  %.5f" % (nonsignature_signal_integral / nonsignature_background_integral)
    print "S/sqrt(B) : "
    print "    Inclusive:  %.5f" % (full_signal_integral / sqrt(full_background_integral))
    if signature_background_integral != 0:
        print "    VBFsig:  %.5f" % (signature_signal_integral / sqrt(signature_background_integral))
    print " nonVBFsig:  %.5f" % (nonsignature_signal_integral / sqrt(nonsignature_background_integral))
    print " _____________________________________ "

    inclusive = asimov_significance(
        full_signal_integral, full_background_integral)

    signature = asimov_significance(signature_signal_integral, signature_background_integral)

    nonsignature = asimov_significance(
        nonsignature_signal_integral, nonsignature_background_integral)

    print "Significance: "
    print "    Inclusive:  %.4f" % inclusive
    print "    VBFsig:  %.4f" % signature
    print " nonVBFsig:  %.4f" % nonsignature
    sum_significance = sqrt(signature**2 + nonsignature**2)
    print "   Sum: %.4f" % sum_significance
    increase_significance = sum_significance - inclusive
    print "   increase: %.4f" % increase_significance
    print "   percent increase: %.3f" % (100 * (increase_significance) / inclusive)

    # _____________________________________
    # Draw histograms

    AtlasStyle()

    # Fix dEta plots
    d_vbf_histo_list['signature_mjj_dEta'].GetXaxis().SetRangeUser(0, 16)
    d_vbf_histo_list['nonsignature_mjj_dEta'].GetXaxis().SetRangeUser(0, 16)
    d_ggf_histo_list['signature_mjj_dEta'].GetXaxis().SetRangeUser(0, 16)
    d_ggf_histo_list['nonsignature_mjj_dEta'].GetXaxis().SetRangeUser(0, 16)

    def plot_stack(hists, normed):

        # First half of hists should be signature
        hists_signature = hists[:len(hists) / 2]
        for h in hists_signature:
            h.SetFillStyle(3002)

        total_integral = 0
        for h in hists:
            h.SetFillColor(hists.index(h) + 1)
            if hists[0].GetName() == "dummy":
                h.SetLineColor(hists.index(h) + 8)

            if normed:
                total_integral += h.Integral()

        h_stack = ROOT.THStack("stack", "")

        for h in hists:
            if normed:
                h.Scale(1 / total_integral)
            h_stack.Add(h)

        return h_stack

    def make_legend(hists):
        leg = ROOT.TLegend(0.5, 0.6, 0.9, 0.9)
        leg.SetBorderSize(0)
        leg.SetFillStyle(0)
        leg.AddEntry(hists[0], "20 * VBF-like from VBF sample", "f")
        leg.AddEntry(hists[1], "VBF-like from ggF sample", "f")
        leg.AddEntry(hists[2], "20* nonVBF-like from VBF sample", "f")
        leg.AddEntry(hists[3], "nonVBF-like from ggF sample", "f")
        return leg

    mycanvas = ROOT.TCanvas()
    # gstyle.SetOptStat(00000000)

    # Plot jet n
    jet_n_hists = [d_vbf_histo_list['signature_jet_n'],
                   d_ggf_histo_list['signature_jet_n'],
                   d_vbf_histo_list['nonsignature_jet_n'],
                   d_ggf_histo_list['nonsignature_jet_n']]

    # scale so you can see the vbf
    jet_n_hists[0].Scale(20)
    jet_n_hists[2].Scale(20)

    jet_n_stack = plot_stack(jet_n_hists, True)
    jet_n_stack.Draw("hist")
    jet_n_stack.GetXaxis().SetTitle("Jet Multiplicity")
    jet_n_stack.GetYaxis().SetTitle("Arb. Units")
    legend = make_legend(jet_n_hists)
    legend.Draw()

    d_sherpa_histo_list['signature_jet_n'].SetName("dummy")
    sherpa_stack = plot_stack([d_sherpa_histo_list['signature_jet_n'],
                               d_sherpa_histo_list['nonsignature_jet_n']],
                              True)
    #sherpa_stack.Draw("EP Sames")

    mycanvas.Print("plots/categorization_njets.pdf")

    # Plot jet n
    dEta_hists = [d_vbf_histo_list['signature_mjj_dEta'],
                  d_ggf_histo_list['signature_mjj_dEta'],
                  d_vbf_histo_list['nonsignature_mjj_dEta'],
                  d_ggf_histo_list['nonsignature_mjj_dEta']]

    # scale so you can see the vbf
    dEta_hists[0].Scale(20)
    dEta_hists[2].Scale(20)

    dEta_stack = plot_stack(dEta_hists, True)
    dEta_stack.Draw("hist")
    dEta_stack.GetYaxis().SetTitle("Arb. Units")
    dEta_stack.GetYaxis().SetTitleOffset(0.95)
    dEta_stack.GetXaxis().SetTitle("#Delta #eta (highest mjj pair)")
    dEta_stack.GetXaxis().SetTitleOffset(0.95)
    legend = make_legend(dEta_hists)
    legend.Draw()

    d_sherpa_histo_list['signature_mjj_dEta'].SetName("dummy")
    sherpa_stack = plot_stack([d_sherpa_histo_list['signature_mjj_dEta'],
                               d_sherpa_histo_list['nonsignature_mjj_dEta']],
                              True)
    #sherpa_stack.Draw("EP Sames")
    # d_sherpa_histo_list['nonsignature_jet_n'].SetLineColor(2)
    #d_sherpa_histo_list['nonsignature_jet_n'].Draw("EP Sames")

    mycanvas.Print("plots/categorization_dEta.pdf")
