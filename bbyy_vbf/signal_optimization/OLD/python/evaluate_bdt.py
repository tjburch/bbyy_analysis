# Run bdt through ggf, vbf, and bkg events
# TODO: Let's just make this run over 1 file and pass all individually...
# See asimov significance
import sys
import array
import pdb
from weight_reference import *
import ROOT
if len(sys.argv) < 2:
    print "usage: arg1 = weights file ; arg2 = outfile name"
    sys.exit(1)
luminosity = 150000

workdir = '/Users/tburch/Documents/workArea/aprWorkspace/bbyy_analysis/bbyy_vbf/signal_optimization/'
vbffile = ROOT.TFile.Open(workdir + 'data/4jet_vbf_selection.root')
vbftree = vbffile.Get("mini")
cross_section = 1.62 / 1000
BR_Hbb = 0.5809
BR_Hyy = 2.270E-03
cross_section_br = cross_section * BR_Hbb * BR_Hyy * 2
vbfWeight = set_weight(luminosity, cross_section_br, 0.49354,
                       get_skim_eff(vbffile)[1], get_skim_eff(vbffile)[0])
# vbffile.Close(); vbffile.Clear()

ggffile = ROOT.TFile(workdir + '/data/4jet_nonres_selection.root')
ggftree = ggffile.Get("mini")
d_weights = weight_reference('nonres')
ggfWeight = set_weight(luminosity, d_weights['cross_section_br'], d_weights['generator_efficiency'], get_skim_eff(
    ggffile)[1], get_skim_eff(ggffile)[0])
# ggffile.Close(); ggffile.Clear()

sherpafile = ROOT.TFile(workdir + '/data/4jet_sherpa_selection.root')
scale_factor = 63.672 / 17804.823  # for 0 tag events
sherpaWeight = set_weight(luminosity, 41.504, 0.49904, get_skim_eff(
    sherpafile)[1], 1.0527E8) * scale_factor
sherpatree = sherpafile.Get("mini")
# sherpafile.Close()

datafile = ROOT.TFile(workdir + '/data/NTNI_data_selection.root')
sherpatree = sherpafile.Get("mini")


# Pass file with model (BDT)
model = sys.argv[1]
reader = ROOT.TMVA.Reader()

jj_m = array.array('f', [0])
reader.AddVariable('jj_m', jj_m)
bb_m = array.array('f', [0])
reader.AddVariable('bb_m', bb_m)

jj_dEta = array.array('f', [0])
reader.AddVariable('jj_dEta', jj_dEta)

y1_y2_dEta = array.array('f', [0])
reader.AddVariable('y1_y2_dEta', y1_y2_dEta)
b1_b2_dEta = array.array('f', [0])
reader.AddVariable('b1_b2_dEta', b1_b2_dEta)

yy_j1_dEta = array.array('f', [0])
reader.AddVariable('yy_j1_dEta', yy_j1_dEta)
yy_j2_dEta = array.array('f', [0])
reader.AddVariable('yy_j2_dEta', yy_j2_dEta)

jet_n = array.array('i', [0])
reader.AddSpectator('jet_n', jet_n)
reader.BookMVA("BDT::BDT", model)


signal_eval = array.array('f', [0])
bkg_eval = array.array('f', [0])
data_eval = array.array('f', [0])

ggf_signal = ROOT.TH1F('ggf_hist', 'ggf_hist', 50, -1, 1)
vbf_signal = ROOT.TH1F('vbf_hist', 'vbf_hist', 50, -1, 1)
histo_background = ROOT.TH1F('bkg_hist', 'bkg_hist', 50, -1, 1)

data_hist = ROOT.TH1F('data_hist', 'data_hist', 50, -1, 1)

outfile = ROOT.TFile.Open('data/'+sys.argv[2], 'recreate')

signaltree = ROOT.TTree("sig_eval", "eval1")
signaltree.Branch('signal_eval', signal_eval, 'signal_eval/F')

bkgtree = ROOT.TTree("bkg_eval", "eval1")
bkgtree.Branch('bkg_eval', bkg_eval, 'bkg_eval/F')

datatree = ROOT.TTree("data_eval", "eval1")
datatree.Branch('data_eval', data_eval, 'data_eval/F')

# vbf
nEntries = vbftree.GetEntries()
for ientry in range(nEntries):
    vbftree.GetEntry(ientry)
    w = vbfWeight * vbftree.yybb_low_weight * vbftree.hgam_weight

    jj_m[0] = vbftree.jj_m
    bb_m[0] = vbftree.bb_m
    jj_dEta[0] = vbftree.jj_dEta
    y1_y2_dEta[0] = vbftree.y1_y2_dEta
    b1_b2_dEta[0] = vbftree.b1_b2_dEta
    yy_j1_dEta[0] = vbftree.yy_j1_dEta
    yy_j2_dEta[0] = vbftree.yy_j2_dEta
    jet_n[0] = vbftree.jet_n
    mva_test = reader.EvaluateMVA('BDT::BDT')
    signal_eval[0] = mva_test
    vbf_signal.Fill(mva_test, w)
    signaltree.Fill()

# ggf
nEntries = ggftree.GetEntries()
for ientry in range(nEntries):
    ggftree.GetEntry(ientry)
    w = ggfWeight * ggftree.yybb_low_weight * ggftree.hgam_weight

    jj_m[0] = ggftree.jj_m
    bb_m[0] = ggftree.bb_m
    jj_dEta[0] = ggftree.jj_dEta
    jet_n[0] = ggftree.jet_n
    y1_y2_dEta[0] = ggftree.y1_y2_dEta
    b1_b2_dEta[0] = ggftree.b1_b2_dEta
    yy_j1_dEta[0] = ggftree.yy_j1_dEta
    yy_j2_dEta[0] = ggftree.yy_j2_dEta
    mva_test = reader.EvaluateMVA('BDT::BDT')
    signal_eval[0] = mva_test
    ggf_signal.Fill(mva_test, w)
    signaltree.Fill()

# # sherpa
nEntries = sherpatree.GetEntries()
for ientry in range(nEntries):
    sherpatree.GetEntry(ientry)
    w = sherpaWeight * sherpatree.yybb_low_weight * sherpatree.hgam_weight
    jj_m[0] = sherpatree.jj_m
    bb_m[0] = sherpatree.bb_m
    jj_dEta[0] = sherpatree.jj_dEta
    jet_n[0] = sherpatree.jet_n
    y1_y2_dEta[0] = sherpatree.y1_y2_dEta
    b1_b2_dEta[0] = sherpatree.b1_b2_dEta
    yy_j1_dEta[0] = sherpatree.yy_j1_dEta
    yy_j2_dEta[0] = sherpatree.yy_j2_dEta
    mva_test = reader.EvaluateMVA('BDT::BDT')
    histo_background.Fill(mva_test, w)
    bkg_eval[0] = mva_test
    bkgtree.Fill()

nEntries = sherpatree.GetEntries()
for ientry in range(nEntries):
    sherpatree.GetEntry(ientry)
    w = sherpaWeight * sherpatree.yybb_low_weight * sherpatree.hgam_weight
    jj_m[0] = sherpatree.jj_m
    bb_m[0] = sherpatree.bb_m
    jj_dEta[0] = sherpatree.jj_dEta
    jet_n[0] = sherpatree.jet_n
    y1_y2_dEta[0] = sherpatree.y1_y2_dEta
    b1_b2_dEta[0] = sherpatree.b1_b2_dEta
    yy_j1_dEta[0] = sherpatree.yy_j1_dEta
    yy_j2_dEta[0] = sherpatree.yy_j2_dEta
    mva_test = reader.EvaluateMVA('BDT::BDT')
    data_hist.Fill(mva_test, w)
    datatree.Fill()


vbf_signal.Write()
ggf_signal.Write()
histo_background.Write()
data_hist.Write()
signaltree.Write()
bkgtree.Write()
outfile.Close()
