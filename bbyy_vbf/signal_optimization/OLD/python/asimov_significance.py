from math import log, sqrt


def asimov_significance(s, b):
    # caluclates asmiov significance
    if s == 0:
        return 0
    elif b == 0:
        return 0
    elif (2 * ((s + b) * log(1 + (s / b))) - s < 0):
        print "\n     ERROR!Negative sqrt argument!"
        return 0

    else:
        return (sqrt(2 * (((s + b) * log(1 + (s / b))) - s)))
