# Function to be imported that will plot histograms along with
# visualization of cut suggested
from ROOT import *
from AtlasStyle import *
import argparse

parser = argparse.ArgumentParser(
    'Loads in 3d sig/background histogram, optimizes cut location')
parser.add_argument('-v', dest='vbf_signal_file',
                    required=True, help='signal file')
parser.add_argument('-g', dest='ggf_signal_file',
                    required=True, help='signal file')
parser.add_argument('-b', dest='background_file',
                    required=True, help='background file')
parser.add_argument('-o', dest='output_directory', required=False,
                    default='output_histos.root', help='output file name')
parser.add_argument('-c', dest='draw_cuts', required=False, action='store_true',
                    default=False, help='draw cuts')
args = parser.parse_args()

njets_cut = 4
deta_cut = 2.75
mjj_cut = 32

mycanvas = TCanvas("","",800,500)

vbf_signal_file = TFile(args.vbf_signal_file)
ggf_signal_file = TFile(args.ggf_signal_file)
background_file = TFile(args.background_file)

hists_to_draw = ['jet_n', 'mjj_dEta', 'mjj']

AtlasStyle()

for hist_name in hists_to_draw:
    vbf_signal_hist = vbf_signal_file.Get(hist_name)
    ggf_signal_hist = ggf_signal_file.Get(hist_name)
    background_hist = background_file.Get(hist_name)
    vbf_signal_hist.GetXaxis().SetRangeUser(0, 12)
    ggf_signal_hist.GetXaxis().SetRangeUser(0, 12)
    background_hist.GetXaxis().SetRangeUser(0, 12)

    save_name = args.output_directory + "/" + hist_name

    scale_factor = vbf_signal_hist.Integral() + ggf_signal_hist.Integral()
    vbf_signal_hist.Scale(20/scale_factor)
    ggf_signal_hist.Scale(1/scale_factor)

    vbf_signal_hist.SetFillColor(kRed)
    ggf_signal_hist.SetFillColor(kBlue)

    signal_hist = THStack("stack", "")
    signal_hist.Add(vbf_signal_hist)
    signal_hist.Add(ggf_signal_hist)

    # style
    background_hist.SetLineColor(kBlack)
    background_hist.SetMarkerColor(kBlack)

    # get maximum value
    max_val = 1.3 * max([signal_hist.GetMaximum(),
                         background_hist.GetMaximum()])
    min_val = min([signal_hist.GetMinimum(), background_hist.GetMinimum()])
    signal_hist.SetMaximum(max_val)

    signal_hist.SetMinimum(0)

    #    signal_hist.GetYaxis().SetTitle("Events/" + str(granularity))

    # Make legend
    leg = TLegend(.65, .65, .9, .85)
    leg.SetFillStyle(0)
    leg.AddEntry(vbf_signal_hist, '20 * VBF Signal', 'f')
    leg.AddEntry(ggf_signal_hist, 'ggF Signal', 'f')
    leg.AddEntry(background_hist, 'Sherpa Background', 'ep')
    leg.SetBorderSize(0)

    # Set Ranges
    # if x_title == 'Jet Multiplicity':
    #     signal_hist.GetXaxis().SetRangeUser(0, 12)
    # elif x_title == '#Delta #phi (highest m_{jj} pair)':
    #     signal_hist.GetXaxis().SetRangeUser(0, 3.5)
    # elif x_title == '#Delta #eta (highest m_{jj} pair)':
    #     signal_hist.GetXaxis().SetRangeUser(0, 9)

    # Draw
    # mycanvas.cd()
    # signal_hist.Draw('ep')
    # background_hist.Draw('same')
 
    # leg.Draw()
    # # l.Draw()
    # myLabel(0.7, 0.71, 1, "#int L dt = 150 fb^{-1}")
    # # save
    # mycanvas.SaveAs(save_name + '.pdf')

    # Now normalized
    mycanvas.Clear()
    signal_hist.Draw('hist')
    signal_hist.GetXaxis().SetRangeUser(0,12)
    signal_hist.GetXaxis().SetTitle(hist_name)
    signal_hist.GetYaxis().SetTitle("Arb. Units")
    signal_hist.GetYaxis().SetTitleOffset(0.87)

    background_hist.Scale(1 / background_hist.Integral())
    background_hist.Draw('same')

    max_val = 1.2 * max([signal_hist.GetMaximum(),
                         background_hist.GetMaximum()])
    min_val = min([signal_hist.GetMinimum(), background_hist.GetMinimum()])

    signal_hist.SetMaximum(max_val)

    if args.draw_cuts:
        if hist_name == 'jet_n':
            l2 = TLine(njets_cut, min_val, njets_cut, max_val)
        elif hist_name == 'mjj_dEta':
            l2 = TLine(deta_cut, min_val, deta_cut, max_val)
        elif hist_name == 'mjj':
            l2 = TLine(mjj_cut, min_val, mjj_cut, max_val)

        l2.SetLineColor(kMagenta)
        l2.Draw()
        leg.AddEntry(l2, 'Suggested Cut', 'l')

    leg.Draw()
    mycanvas.SaveAs(save_name + '_normed.pdf')
