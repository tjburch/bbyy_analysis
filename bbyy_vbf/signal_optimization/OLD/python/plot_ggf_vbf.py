# Function to be imported that will plot vbf and ggf side by side

from ROOT import *
from AtlasStyle import *
import argparse


parser = argparse.ArgumentParser('')
parser.add_argument('-v', dest='vbf_signal_file',
                    required=True, help='signal file')
parser.add_argument('-g', dest='ggf_signal_file',
                    required=True, help='signal file')
parser.add_argument('-o', dest='output_directory', required=False,
                    default='output_histos.root', help='output file name')
args = parser.parse_args()

vbf_signal_file = TFile(args.vbf_signal_file)
ggf_signal_file = TFile(args.ggf_signal_file)

hists_to_draw = ['jet_n', 'mjj_dEta', 'mjj']

AtlasStyle()

mycanvas = TCanvas("", "", 800, 500)

for hist_name in hists_to_draw:

    mycanvas.Clear()

    vbf_signal_hist = vbf_signal_file.Get(hist_name)
    ggf_signal_hist = ggf_signal_file.Get(hist_name)

    vbf_signal_hist.Scale(1 / vbf_signal_hist.Integral())
    ggf_signal_hist.Scale(1 / ggf_signal_hist.Integral())

    vbf_signal_hist.SetLineColor(kRed)
    vbf_signal_hist.SetMarkerColor(kRed)
    ggf_signal_hist.SetLineColor(kBlue)
    ggf_signal_hist.SetMarkerColor(kBlue)

    max_val = 1.2 * max([vbf_signal_hist.GetMaximum(), ggf_signal_hist.GetMaximum()])
    min_val = min([vbf_signal_hist.GetMinimum(), ggf_signal_hist.GetMinimum()])

    vbf_signal_hist.SetMaximum(max_val)
    vbf_signal_hist.GetXaxis().SetTitle(hist_name)
    vbf_signal_hist.GetYaxis().SetTitle('Arb. Units')

    vbf_signal_hist.Draw()
    ggf_signal_hist.Draw('SAMES')

    if hist_name == 'mjj':
        vbf_signal_hist.GetXaxis().SetRangeUser(0, 2000)

    # Make legend
    leg = TLegend(.65, .65, .9, .85)
    leg.SetFillStyle(0)
    leg.AddEntry(vbf_signal_hist, 'VBF Sample', 'l')
    leg.AddEntry(ggf_signal_hist, 'ggF Sample', 'l')
    leg.SetBorderSize(0)
    leg.Draw()

    mycanvas.SaveAs(args.output_directory + hist_name + '_vbfggf.pdf')