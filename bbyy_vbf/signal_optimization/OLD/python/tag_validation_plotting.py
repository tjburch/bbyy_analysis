# Function to be imported that will plot histograms along with
# visualization of cut suggested
from ROOT import *
from AtlasStyle import *
import sys

mycanvas = TCanvas()

input_file = TFile(sys.argv[1])

zero_tag_hist_names = ['jet_n_0tag', 'mjj_dEta_0tag']
two_tag_hist_names = ['jet_n_2tag', 'mjj_dEta_2tag']
x_title = ['Jet Multiplicity', '#Delta #eta mjj']
AtlasStyle()

for hist_name in zero_tag_hist_names:

    idx = zero_tag_hist_names.index(hist_name)

    zero_tag_hist = input_file.Get(zero_tag_hist_names[idx])
    two_tag_hist = input_file.Get(two_tag_hist_names[idx])

    two_tag_hist.SetLineColor(kRed)
    two_tag_hist.SetMarkerColor(kRed)

    mycanvas = TCanvas()
    #mycanvas.SetLogy()

    zero_tag_hist.Scale(1 / zero_tag_hist.Integral())
    two_tag_hist.Scale(1 / two_tag_hist.Integral())

    # get maximum value
    max_val = 1.3 * max([zero_tag_hist.GetMaximum(),
                         two_tag_hist.GetMaximum()])
    min_val = min([zero_tag_hist.GetMinimum(), two_tag_hist.GetMinimum()])
    zero_tag_hist.SetMaximum(max_val)

    # Make legend
    leg = TLegend(.65, .75, .9, .95)
    leg.SetFillStyle(0)
    leg.AddEntry(zero_tag_hist, '0-tag Selection', 'ep')
    leg.AddEntry(two_tag_hist, '2-tag Selection', 'ep')
    leg.SetBorderSize(0)


    zero_tag_hist.GetXaxis().SetTitle(x_title[idx])
    zero_tag_hist.GetXaxis().SetTitleOffset(1.1)
    zero_tag_hist.GetYaxis().SetTitle("Arb. Units")
    zero_tag_hist.GetYaxis().SetTitleOffset(1.1)

    myLabel(0.7, 0.71, 1, "#int L dt = 150 fb^{-1}")
    myLabel(0.7, 0.67, 1, "Sherpa MC")

    zero_tag_hist.Draw("ep")
    two_tag_hist.Draw("ep sames")
    leg.Draw()


    # save
    save_name = zero_tag_hist.GetName().split('0')[0]
    mycanvas.SaveAs('plots/' + save_name + 'tagValidation.pdf')
