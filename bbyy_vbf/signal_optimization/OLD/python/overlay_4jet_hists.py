# Function to be imported that will plot vbf and ggf side by side

from ROOT import *
from AtlasStyle import *
import argparse


parser = argparse.ArgumentParser('')
parser.add_argument('-f1', dest='file_1',
                    required=True, help='signal file')
parser.add_argument('-f2', dest='file_2',
                    required=True, help='signal file')
parser.add_argument('-o', dest='output_directory', required=False,
                    default='output_histos.root', help='output file name')
args = parser.parse_args()

file_1 = TFile(args.file_1)
file_2 = TFile(args.file_2)

hists_to_draw = ['jet_n', 'mjj_dEta', 'mjj_dPhi', 'mjj']

AtlasStyle()

mycanvas = TCanvas("a", "b", 800, 500)

for hist_name in hists_to_draw:

    mycanvas.Clear()

    hist_1 = file_1.Get(hist_name)
    hist_2 = file_2.Get(hist_name)

    hist_1.Scale(1 / hist_1.Integral())
    hist_2.Scale(1 / hist_2.Integral())

    hist_1.SetLineColor(kRed)
    hist_1.SetMarkerColor(kRed)
    hist_2.SetLineColor(kBlue)
    hist_2.SetMarkerColor(kBlue)

    max_val = 1.2 * max([hist_1.GetMaximum(), hist_2.GetMaximum()])
    min_val = min([hist_1.GetMinimum(), hist_2.GetMinimum()])

    hist_1.SetMaximum(max_val)
    hist_1.GetXaxis().SetTitle(hist_name)
    hist_1.GetYaxis().SetTitle('Arb. Units')

    hist_1.Draw()
    hist_2.Draw('SAMES')

    # Make legend
    leg = TLegend(.65, .65, .9, .85)
    leg.SetFillStyle(0)
    leg.AddEntry(hist_1, 'VBF Sample', 'l')
    leg.AddEntry(hist_2, 'Sherpa Sample', 'l')
    leg.SetBorderSize(0)
    leg.Draw()

    mycanvas.SaveAs(args.output_directory + hist_name + '_overlay.pdf')
