import ROOT
from weight_reference import weight_reference
from make_2tag_histograms import get_cutflow_weights, get_mjj_pair
import sys


sherpa_file = ROOT.TFile(sys.argv[1])
sample_type = 'sherpa'

# Get input tree
t_input = sherpa_file.Get("mini")

# Get MC weight information
h_cutflow = sherpa_file.Get("cutflow_weighted")
d_weights = weight_reference(sample_type)
sum_of_weights, skim_efficiency = get_cutflow_weights(h_cutflow)
d_weights['skim_efficiency'] = skim_efficiency

luminosity = 150000
print "luminosity: ", luminosity / 1000.0
print "BR * xsec: ", d_weights['cross_section_br']
print "Sum of Weights: ", d_weights['sum_of_weights']
print "skim_efficiency: ", d_weights['skim_efficiency']

# _______________________________

h_jet_n_2tag = ROOT.TH1F("jet_n_2tag", "jet_n_2tag", 9, 1, 10)
h_jet_n_0tag = ROOT.TH1F("jet_n_0tag", "jet_n_0tag", 9, 1, 10)

h_mjj_dEta_2tag = ROOT.TH1F("mjj_dEta_2tag", "mjj_dEta_2tag", 24, 0, 8)
h_mjj_dEta_0tag = ROOT.TH1F("mjj_dEta_0tag", "jet_n_0tag", 24, 0, 8)

for hist in [h_jet_n_0tag, h_jet_n_2tag, h_mjj_dEta_0tag, h_mjj_dEta_2tag]:
        hist.Sumw2()

# -----------------------------------------------------------------
# Loop over entries

nEntries = t_input.GetEntries()

for event in range(nEntries):
    t_input.GetEntry(event)

    w = (t_input.hgam_weight *
         t_input.yybb_low_weight *
         luminosity *
         d_weights['cross_section_br'] *
         d_weights['generator_efficiency'] *
         d_weights['skim_efficiency'] /
         d_weights['sum_of_weights'])

    # 2 Tag
    if t_input.yybb_low_btagCat == 2:
        h_jet_n_2tag.Fill(t_input.jet_n, w)

        leading_idx, subleading_idx = get_mjj_pair(t_input)
        if leading_idx != -1 and subleading_idx != -1:
            h_mjj_dEta_2tag.Fill(
                abs(t_input.jet_eta[leading_idx] - t_input.jet_eta[subleading_idx]), w)

    # other Tag
    elif t_input.yybb_low_btagCat == 0:
        h_jet_n_0tag.Fill(t_input.jet_n, w)

        leading_idx, subleading_idx = get_mjj_pair(t_input)
        if leading_idx != -1 and subleading_idx != -1:
            h_mjj_dEta_0tag.Fill(
                abs(t_input.jet_eta[leading_idx] - t_input.jet_eta[subleading_idx]), w)


output_file = ROOT.TFile(sys.argv[2], 'recreate')

for h in [h_jet_n_0tag, h_mjj_dEta_0tag, h_jet_n_2tag, h_mjj_dEta_2tag]:
    h.Write()    
output_file.Close()
