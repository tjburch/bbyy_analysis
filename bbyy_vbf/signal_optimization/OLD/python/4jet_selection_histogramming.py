# Makes histograms using selected events
import ROOT
import sys
from weight_reference import weight_reference


def get_cutflow_weights(h_cutflow):
    # uses cutflow histogram to get sum_of_weights, skim efficiency
    # return as a tuple
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency


def create_histograms():
    d_histograms = {}
    d_histograms['jet_n'] = ROOT.TH1F("jet_n", "jet_n", 20, 0, 20)
    d_histograms['mjj'] = ROOT.TH1F("mjj", "mjj", 60, 0, 3000)
    d_histograms['mjj_dEta'] = ROOT.TH1F("mjj_dEta", "mjj_dEta", 30, 0, 10)
    d_histograms['mjj_dPhi'] = ROOT.TH1F("mjj_dPhi", "mjj_dPhi", 21, 0, 7)
    d_histograms['selected_jet1_eta'] = ROOT.TH1F("selected_jet1_eta", "selected_jet1_eta", 30, -6, 6)
    d_histograms['selected_jet1_phi'] = ROOT.TH1F("selected_jet1_phi", "selected_jet1_phi", 32, -4, 4)
    d_histograms['selected_jet2_eta'] = ROOT.TH1F("selected_jet2_eta", "selected_jet2_eta", 30, -6, 6)
    d_histograms['selected_jet2_phi'] = ROOT.TH1F("selected_jet2_phi", "selected_jet2_phi", 32, -4, 4)

    for hist in d_histograms:
        d_histograms[hist].Sumw2()

    return d_histograms


if __name__ == "__main__":
    input_file = sys.argv[1]
    output_name = sys.argv[2]
    sample_type = sys.argv[3]

    print "Sample Type: ", sample_type

    # Get input tree
    f_input = ROOT.TFile(input_file)
    t_input = f_input.Get("mini")
    nEntries = t_input.GetEntries()
    print "Entries to use: ", nEntries

    # Get MC weight information
    h_cutflow = f_input.Get("cutflow_weighted")
    d_weights = weight_reference(sample_type)
    sum_of_weights, skim_efficiency = get_cutflow_weights(h_cutflow)
    if sample_type != 'sherpa':
        d_weights['sum_of_weights'] = sum_of_weights
    d_weights['skim_efficiency'] = skim_efficiency

    luminosity = 150000
    print "luminosity: ", luminosity / 1000.0
    print "BR * xsec: ", d_weights['cross_section_br']
    print "Sum of Weights: ", d_weights['sum_of_weights']
    print "skim_efficiency: ", d_weights['skim_efficiency']

    d_histograms = create_histograms()

    # -----------------------------------------------------------------
    # Loop over entries

    for event in range(nEntries):
        t_input.GetEntry(event)

        w = (t_input.hgam_weight *
             t_input.yybb_low_weight *
             luminosity *
             d_weights['cross_section_br'] *
             d_weights['generator_efficiency'] *
             d_weights['skim_efficiency'] /
             d_weights['sum_of_weights'])

        d_histograms['jet_n'].Fill(t_input.jet_n, w)
        d_histograms['mjj'].Fill(t_input.mjj, w)
        d_histograms['mjj_dEta'].Fill(t_input.mjj_dEta, w)
        d_histograms['mjj_dPhi'].Fill(t_input.mjj_dPhi, w)

        j1 = t_input.mjj_idx1
        j2 = t_input.mjj_idx2
        d_histograms['selected_jet1_eta'].Fill(t_input.jet_eta[j1], w)
        d_histograms['selected_jet2_eta'].Fill(t_input.jet_eta[j2], w)

        d_histograms['selected_jet1_phi'].Fill(t_input.jet_phi[j1], w)
        d_histograms['selected_jet2_phi'].Fill(t_input.jet_phi[j2], w)

    d_histograms['bothselected_jet_phi'] = d_histograms['selected_jet1_phi'].Clone()
    d_histograms['bothselected_jet_phi'].Add(d_histograms['selected_jet2_phi'])
    d_histograms['bothselected_jet_phi'].SetName('bothselected_jet_phi')

    d_histograms['bothselected_jet_eta'] = d_histograms['selected_jet1_eta'].Clone()
    d_histograms['bothselected_jet_eta'].Add(d_histograms['selected_jet2_eta'])
    d_histograms['bothselected_jet_eta'].SetName('bothselected_jet_eta')

    # Scale to 2-tag set
    if sample_type == 'sherpa':
        scale_factor = 63.672 / 17804.823  # Value at 150fb-1
        for key in d_histograms:
            d_histograms[key].Scale(scale_factor)

    f_output = ROOT.TFile(output_name, 'recreate')
    for key in d_histograms:
        d_histograms[key].Write()
    f_output.Close()
