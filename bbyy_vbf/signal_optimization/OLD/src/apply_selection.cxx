//////////////////////////////////
//   Pass File, apply Selection //
//       Return TEventList      //
//       Tyler James Burch      //
//////////////////////////////////
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <vector>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
using namespace std;


class Jet {

public:
	Jet(int i, float jet_pt, float jet_mass, float jet_eta, float jet_phi);
	int idx;
	float pt;
	float m;
	float eta;
	float phi;
	TLorentzVector four_vector;
};

Jet::Jet(int i, float jet_pt, float jet_mass, float jet_eta, float jet_phi) {
	idx = i;
	pt = jet_pt;
	m = jet_mass;
	eta = jet_eta;
	phi = jet_phi;
	four_vector.SetPtEtaPhiM(pt, eta, phi, m);
}

class Dijet {

public:
	Dijet();
	void setJet(Jet jet1, Jet jet2);
	int idx1;
	int idx2;
	float mjj;
	float dEta;
	float dPhi;
};

Dijet::Dijet() {
	idx1 = -100;
	idx2 = -100;
	mjj = -100;
	dEta = -100;
	dPhi = -100;
}


void Dijet::setJet(Jet jet1, Jet jet2) {

	idx1 = jet1.idx;
	idx2 = jet2.idx;

	TLorentzVector jet_sum = jet1.four_vector + jet2.four_vector;

	mjj = TMath::Abs(jet_sum.M());
	dEta = TMath::Abs(jet1.eta - jet2.eta);
	dPhi = TMath::Abs(jet1.phi - jet2.phi);
}

Dijet  get_mjj_pair();


int main(int argc, char *argv[]) {
	TString input_file(argv[1]);
	TString output_name(argv[2]);
	TString sample_type(argv[3]);


	//Get File, Tree
	TFile *f_input = new TFile(input_file);

	TTree *t_input = (TTree*)f_input->Get("mini");
	Long64_t nentries = t_input->GetEntries();
	TH1F *cutflow = (TH1F*)f_input->Get("cutflow_weighted");

	// Variables used for cuts
	int hgam_cutList = 0, yybb_cutList_low = 0, jet_n;
	Bool_t hgam_isPassed;
	Float_t m_yy;
	float jet_m[20], jet_eta[20], jet_phi[20], jet_pt[20];
	Bool_t jet_MV2c10_FixedCutBEff_85[20];

	t_input->SetBranchAddress("m_yy", &m_yy);
	t_input->SetBranchAddress("hgam_isPassed", &hgam_isPassed);
	t_input->SetBranchAddress("hgam_cutList", &hgam_cutList);
	t_input->SetBranchAddress("yybb_cutList_low", &yybb_cutList_low);
	t_input->SetBranchAddress("jet_n", &jet_n);
	t_input->SetBranchAddress("jet_m", &jet_m);
	t_input->SetBranchAddress("jet_eta", &jet_eta);
	t_input->SetBranchAddress("jet_phi", &jet_phi);
	t_input->SetBranchAddress("jet_pt", &jet_pt);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_85", &jet_MV2c10_FixedCutBEff_85);


	// clone tree format, 0 entries
	TFile *f_write = new TFile(output_name, "RECREATE");
	TTree *t_output = t_input->CloneTree(0);
	float jet_mjj, jet_dEta, jet_dPhi;

	t_output->Branch("jet_dEta", &jet_dEta);
	t_output->Branch("jet_dPhi", &jet_dPhi);
	t_output->Branch("jet_mjj", &jet_mjj);


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	cout << "nEntries Used: " << nentries << endl;


	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;

		//if (jentry % 1000 == 0) cout << "entry number " << jentry << endl;

		// Cuts
		if (sample_type == "vbf" and hgam_cutList < 14) continue;
		if (sample_type != "vbf" and !hgam_isPassed) continue;
		if (yybb_cutList_low <= 4) continue;
		//if (yybb_low_btagCat != 2) continue; // Only 2 btag events
		if (m_yy > 128 || m_yy < 122) continue; // tight cut for signal region


		TLorentzVector jet_sum;
		Dijet mjj_pair;

		// get highest mjj pair
		for (int iJet = 0 ; iJet < jet_n ; iJet++ ) {
			if (jet_MV2c10_FixedCutBEff_85[iJet]) continue;

			for (int jJet = 0 ; jJet < jet_n ; jJet++) {
				if (iJet == jJet) continue;
				if (jet_MV2c10_FixedCutBEff_85[jJet]) continue;

				Jet jet1 = Jet(iJet, jet_pt[iJet], jet_eta[iJet], jet_phi[iJet], jet_m[iJet]);
				Jet jet2 = Jet(jJet, jet_pt[jJet], jet_eta[jJet], jet_phi[jJet], jet_m[jJet]);
				Dijet thisJet;
				thisJet.setJet(jet1, jet2);

				if ( thisJet.mjj > mjj_pair.mjj) {
					mjj_pair = thisJet;
				}
			}
		}

		jet_dEta = mjj_pair.dEta;
		jet_dPhi = mjj_pair.dPhi;
		jet_mjj = mjj_pair.mjj;

		t_output->Fill();

	}

	f_write->cd();
	t_output->Write();
	cutflow->Write();

	f_write->Close();

	return 0;
}

