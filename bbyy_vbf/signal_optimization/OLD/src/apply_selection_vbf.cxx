//////////////////////////////////
//   Pass File, apply Selection //
//       Tyler James Burch      //
//////////////////////////////////
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
#include "jet_classes.cxx"
using namespace std;

float find_deta(float obj1, float obj2);

int main(int argc, char *argv[]) {
	TString input_file(argv[1]);
	TString output_name(argv[2]);
	TString sample_type(argv[3]);


	//Get File, Tree
	TFile *f_input = new TFile(input_file);

	TTree *t_input = (TTree*)f_input->Get("mini");
	Long64_t nentries = t_input->GetEntries();
	// cutflow not used but needs to be propagated to skimmed file
	TH1F *cutflow = (TH1F*)f_input->Get("cutflow_weighted"); 

	// Variables used for cuts
	int hgam_cutList, yybb_cutList_low, jet_n, photon_n;
	Bool_t hgam_isPassed;
	Float_t m_yy;
	float jet_m[50], jet_eta[50], jet_phi[50], jet_pt[50];
	Bool_t jet_MV2c10_FixedCutBEff_60[50], jet_MV2c10_FixedCutBEff_70[50], jet_MV2c10_FixedCutBEff_77[50], jet_MV2c10_FixedCutBEff_85[50];
	float photon_m[4], photon_eta[4], photon_phi[4], photon_pt[4];
	int yybb_low_btagCat;

	t_input->SetBranchAddress("m_yy", &m_yy);
	t_input->SetBranchAddress("hgam_isPassed", &hgam_isPassed);
	t_input->SetBranchAddress("hgam_cutList", &hgam_cutList);
	t_input->SetBranchAddress("yybb_cutList_low", &yybb_cutList_low);
	t_input->SetBranchAddress("yybb_low_btagCat", &yybb_low_btagCat);
	// jets
	t_input->SetBranchAddress("jet_n", &jet_n);
	t_input->SetBranchAddress("jet_m", &jet_m);
	t_input->SetBranchAddress("jet_eta", &jet_eta);
	t_input->SetBranchAddress("jet_phi", &jet_phi);
	t_input->SetBranchAddress("jet_pt", &jet_pt);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_60", &jet_MV2c10_FixedCutBEff_60);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_70", &jet_MV2c10_FixedCutBEff_70);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_77", &jet_MV2c10_FixedCutBEff_77);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_85", &jet_MV2c10_FixedCutBEff_85);


	// photons
	t_input->SetBranchAddress("photon_n", &photon_n);
	t_input->SetBranchAddress("photon_m", &photon_m);
	t_input->SetBranchAddress("photon_eta", &photon_eta);
	t_input->SetBranchAddress("photon_phi", &photon_phi);
	t_input->SetBranchAddress("photon_pt", &photon_pt);
	// clone tree format, 0 entries
	TFile *f_write = new TFile(output_name, "RECREATE");
	TTree *t_output = t_input->CloneTree(0);
	float jj_m, jj_dEta, jj_dPhi;
	int j1_idx, j2_idx;
	int b1_idx, b2_idx;
	float j1_eta, j2_eta;
	float jj_eta;
	float y1_y2_dEta, yy_j1_dEta, yy_j2_dEta, b1_b2_dEta;
	int jet1_tagbin, jet2_tagbin;
	float bb_m;

	// jj system variables
	t_output->Branch("jj_dEta", &jj_dEta);
	t_output->Branch("jj_eta", &jj_eta);
	t_output->Branch("jj_dPhi", &jj_dPhi);
	t_output->Branch("jj_m", &jj_m);	
	t_output->Branch("bb_m", &bb_m);
	t_output->Branch("j1_idx", &j1_idx);
	t_output->Branch("j2_idx", &j2_idx);

	t_output->Branch("j1_eta", &j1_eta);
	t_output->Branch("j2_eta", &j2_eta);
	t_output->Branch("b1_idx", &b1_idx);
	t_output->Branch("b2_idx", &b2_idx);

	// extras
	t_output->Branch("y1_y2_dEta", &y1_y2_dEta);
	t_output->Branch("b1_b2_dEta", &b1_b2_dEta);	
	t_output->Branch("yy_j1_dEta", &yy_j1_dEta);
	t_output->Branch("yy_j2_dEta", &yy_j2_dEta);

	t_output->Branch("jet1_tagbin", &jet1_tagbin);
	t_output->Branch("jet2_tagbin", &jet2_tagbin);


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	cout << "nEntries Used: " << nentries << endl;


	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;

		//if (jentry % 1000 == 0) cout << "entry number " << jentry << endl;

		// Cuts
		if (sample_type == "vbf" and hgam_cutList < 14) continue; // vbf ntuple indexed wrong
		if (sample_type != "vbf" and !hgam_isPassed) continue;
		if (yybb_cutList_low <= 4) continue;
		if (yybb_low_btagCat != 2 && sample_type != "sherpa") continue; // Only 2 btag - intentionally omit
		if (m_yy > 128 || m_yy < 122) continue; // tight cut for signal region


		// Require 4 jets, 2 for bb pair, 2 for VBF jets
		if (jet_n < 4) continue;

		Pair_bb bb_pair;
		Dijet jj_pair;

		// Initalize jets in event, find mbb pair
		for (int iJet = 0 ; iJet < jet_n ; iJet++ ) {
			Jet this_jet = Jet(iJet, jet_pt[iJet], jet_eta[iJet], jet_phi[iJet], jet_m[iJet],jet_MV2c10_FixedCutBEff_70[iJet]);
			bb_pair.check_jet(this_jet); // See if jet should be in mbb pair, if so, adds it 
		}	

		// If we don't have 2 bjets, skip (e.g. not 2 central jets)
		if (bb_pair.idx1 == -1 || bb_pair.idx2 == -1) continue;

		b1_idx = bb_pair.idx1;
		b2_idx = bb_pair.idx2;
		Jet b_jet1 = Jet( b1_idx, jet_pt[b1_idx], jet_m[b1_idx], jet_eta[b1_idx], jet_phi[b1_idx], jet_MV2c10_FixedCutBEff_70[b1_idx]);
		Jet b_jet2 = Jet( b2_idx, jet_pt[b2_idx], jet_m[b2_idx], jet_eta[b2_idx], jet_phi[b2_idx], jet_MV2c10_FixedCutBEff_70[b2_idx]);

		TLorentzVector b_sum = b_jet1.four_vector + b_jet2.four_vector;
		bb_m = b_sum.M(); // will be filled in tree!
		if (bb_m < 60 || bb_m > 140) continue; // Low Mass Selection	


		// get highest mjj pair
		for (int iJet = 0 ; iJet < jet_n ; iJet++ ) {
			if (iJet == bb_pair.idx1 || iJet == bb_pair.idx2) continue;

			for (int jJet = 0 ; jJet < jet_n ; jJet++) {
				if (iJet == jJet) continue;
				if (jJet == bb_pair.idx1 || jJet == bb_pair.idx2) continue;

				Jet jet1 = Jet(iJet, jet_pt[iJet], jet_m[iJet], jet_eta[iJet], jet_phi[iJet], jet_MV2c10_FixedCutBEff_70[iJet]);
				Jet jet2 = Jet(jJet, jet_pt[jJet], jet_m[jJet], jet_eta[jJet], jet_phi[jJet], jet_MV2c10_FixedCutBEff_70[jJet]);
				Dijet thisDijet;
				thisDijet.setJet(jet1, jet2);

				if ( thisDijet.mjj > jj_pair.mjj) {
					jj_pair = thisDijet;
				}
			}
		}

		// get Photons (0 and 1 are always higgs yy)
		Photon y1 = Photon(0, photon_pt[0], photon_m[0], photon_eta[0], photon_phi[0]);
		Photon y2 = Photon(1, photon_pt[1], photon_m[1], photon_eta[1], photon_phi[1]);
		Pair_yy yy_pair = Pair_yy(y1, y2);


		// Fill tagbin
		int idx1 = bb_pair.idx1;
		if(jet_MV2c10_FixedCutBEff_85[idx1] && !jet_MV2c10_FixedCutBEff_77[idx1]) jet1_tagbin = 0;
		else if(jet_MV2c10_FixedCutBEff_77[idx1] && !jet_MV2c10_FixedCutBEff_70[idx1]) jet1_tagbin = 1;
		else if(jet_MV2c10_FixedCutBEff_70[idx1] && !jet_MV2c10_FixedCutBEff_60[idx1]) jet1_tagbin = 2;
		else if(jet_MV2c10_FixedCutBEff_60[idx1]) jet1_tagbin = 3;
		else jet1_tagbin = 4; // for our "closest to b-tagged jets"

		int idx2 = bb_pair.idx2;
		if(jet_MV2c10_FixedCutBEff_85[idx2] && !jet_MV2c10_FixedCutBEff_77[idx2]) jet2_tagbin = 0;
		else if(jet_MV2c10_FixedCutBEff_77[idx2] && !jet_MV2c10_FixedCutBEff_70[idx2]) jet2_tagbin = 1;
		else if(jet_MV2c10_FixedCutBEff_70[idx2] && !jet_MV2c10_FixedCutBEff_60[idx2]) jet2_tagbin = 2;
		else if(jet_MV2c10_FixedCutBEff_60[idx2]) jet1_tagbin = 3;
		else jet2_tagbin = 4; // for our "closest to b-tagged jets"


		// Set values that will be filled
		jj_dEta = jj_pair.dEta;
		jj_dPhi = jj_pair.dPhi;
		jj_m = jj_pair.mjj;
		j1_idx = jj_pair.idx1;
		j2_idx = jj_pair.idx2;
		jj_eta = jj_pair.eta;


		b1_b2_dEta = TMath::Abs(jet_eta[b1_idx] - jet_eta[b2_idx]);
		if (b1_idx == b2_idx) cout << "problem " << endl;

		j1_eta = jet_eta[jj_pair.idx1];
		j2_eta = jet_eta[jj_pair.idx2];

		y1_y2_dEta = find_deta(y1.eta, y2.eta);
		yy_j1_dEta = find_deta(yy_pair.eta, jet_eta[jj_pair.idx1]);
		yy_j2_dEta = find_deta(yy_pair.eta, jet_eta[jj_pair.idx2]);

		t_output->Fill();

	}

	f_write->cd();
	t_output->Write();
	cutflow->Write();

	f_write->Close();

	return 0;
}

float find_deta(float eta1, float eta2){
	return TMath::Abs(eta1 - eta2);
}