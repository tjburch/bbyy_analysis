import ROOT

# Initalize files, TMVA objects
output = '../tmva_output/BDT.root'
outputFile = ROOT.TFile.Open(output, 'RECREATE')
ROOT.TMVA.UseOffsetMethod = True

# Create Factory Object
factoryOptions = '!V:!Silent:Transformations=I;D;P;G,D:Correlations'
factory = ROOT.TMVA.Factory('BoostedDecisionTree', outputFile, factoryOptions)

# Create Dataloader
dataloader = ROOT.TMVA.DataLoader('dataset')
dataloader.AddVariable('jj_m', 'F')
dataloader.AddVariable('bb_m', 'F')
# dataloader.AddVariable('jj_dPhi','F')
dataloader.AddVariable('jj_dEta', 'F')
dataloader.AddVariable('y1_y2_dEta', 'F')
dataloader.AddVariable('b1_b2_dEta', 'F')
dataloader.AddVariable('yy_j1_dEta', 'F')
dataloader.AddVariable('yy_j2_dEta', 'F')
# dataloader.AddVariable('jet1_tagbin','I')
# dataloader.AddVariable('jet2_tagbin','I')
dataloader.AddSpectator('jet_n', 'I')

# Get all trees
data_dir = '/Users/tburch/Documents/workArea/aprWorkspace/bbyy_analysis/bbyy_vbf/signal_optimization/data/'
vbfFile = ROOT.TFile(data_dir+'4jet_vbf_selection.root')
vbfTree = vbfFile.Get('mini')
backgroundFile = ROOT.TFile(data_dir+'4jet_sherpa_selection.root')
backgroundTree = backgroundFile.Get('mini')


# Set Weights
# ---------------------------------------------------------------
# Get weights - eventwise
# weight expression - (L)(xBR)(genEff)(skimEff)/ (sum_of_weights)
# Needs multiplied by yybb and hgam weights
luminosity = 150000


def set_weight(lumi, xBR, genEff, skimEff, sumW):
    return (lumi * xBR * genEff * skimEff / sumW)


def get_skim_eff(infile):
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency


# VBF
cross_section = 1.62 / 1000
BR_Hbb = 0.5809
BR_Hyy = 2.270E-03
cross_section_br = cross_section * BR_Hbb * BR_Hyy * 2
vbfWeight = set_weight(luminosity, cross_section_br,  0.49354, get_skim_eff(
    vbfFile)[1], get_skim_eff(vbfFile)[0])


# Sherpa
scale_factor = 63.672 / 17804.823  # for 0 tag events
sherpaWeight = set_weight(luminosity, 41.504, 0.49904, get_skim_eff(vbfFile)[
                          1], 1.0527E8) * scale_factor


dataloader.SetWeightExpression('yybb_low_weight*hgam_weight')

# Prep Dataset
# ------------------------------------------------------------------

#  Load in and prepare trees
dataloader.AddSignalTree(vbfTree, vbfWeight)
dataloader.AddBackgroundTree(backgroundTree, sherpaWeight)

preselection_cut = ROOT.TCut('jj_m > 0 && jj_dEta > 0')
split_options = 'nTrain_Signal=0:'  # all signal
split_options += 'nTrain_Background=0:'  # all background
split_options += 'SplitMode=Random:'
split_options += 'NormMode=NumEvents:'  # Normalization of weights (?)
split_options += '!V'  # Verbosity level

dataloader.PrepareTrainingAndTestTree(preselection_cut, split_options)

# MVA Settings
# ------------------------------------------------------------------

# BDT
bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
bdt_opts += 'NTrees=800'
bdt = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT', bdt_opts)

# Rectangular Cuts
rect = factory.BookMethod(dataloader, ROOT.TMVA.Types.kCuts, 'rect_cuts', '')


# Train, Test, Evaluate all methods
# ------------------------------------------------------------------
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Clean up

outputFile.Close()
