# TODO:
* MVA

	* Add b-tagging variables
	* Play with hyperparameters
	* ~~Compare Sherpa to sidebands/NTNI~~
	* Use other methods than BDT
	* Investigate significance gain after non-VBF events go to second BDT
	* Run with offical MC when available

# Presentations
### Early Data Exploration
* [December 8, 2017](https://indico.cern.ch/event/672707/contributions/2818720/attachments/1572427/2481443/vbf_mva_update.pdf)

### Signal Optimization
* [February 9, 2018](https://indico.cern.ch/event/693820/contributions/2891224/attachments/1598318/2532898/signal_optimization.pdf) - Rectangular Cuts (buggy)
* [March 9, 2018](https://indico.cern.ch/event/693824/contributions/2928815/attachments/1614472/2565265/nonresonant_improvements.pdf) - Rectangular Cuts Fixed
* [April 27, 2018](https://indico.cern.ch/event/721987/contributions/2984600/attachments/1641231/2621022/vbf_bdt.pdf) - BDT First Pass

### At 150 fb-1:
* Signal Events: 1.064
	* VBF Events: 0.019
	* ggF Events: 1.045711
* Background Events: 63.672158



### HH Domain Meetings
* [February 6, 2018](https://indico.cern.ch/event/700142/contributions/2872156/attachments/1595708/2527255/02-06-2018_hh_run2.pdf) - End of Run 2 Analysis Plans
* [March 7, 2018](https://indico.cern.ch/event/707740/contributions/2905395/attachments/1612104/2561595/20180307_hh_domain_VBF_ysano_v3.pdf) - Status (presented by Yuta Sano)