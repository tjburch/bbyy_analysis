setupATLAS
asetup AtlasProduction,19.2.5.34,here
lsetup panda

for JOBOPTIONS in *.py; do
    a=${JOBOPTIONS:52}
    COUPLINGSTRING=${a%.py}
    outDS=user.tburch.nonresonant.vbf.bsm.${COUPLINGSTRING}
    runNO=${joboptions:5:6}
    pathena --outDS $outDS --trf "Generate_tf.py --ecmEnergy '13000' --maxEvents 1000 --runNumber=${runNO} --firstEvent=1 --randomSeed=%RNDM:100 --outputEVNTFile=%OUT.root --jobConfig=${JOBOPTIONS}" --nEventsPerJob 1000 --nJobs 40 --extFile $JOBOPTIONS
done