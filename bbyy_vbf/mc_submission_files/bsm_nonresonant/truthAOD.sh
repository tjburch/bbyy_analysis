setupATLAS
asetup Athena,21.0.64
lsetup panda
couplings=(
l1cvv1cv1
l1cvv0cv1
l1cvv0p5cv1
l1cvv1p5cv1
l0cvv1cv1
l2cvv1cv1
l1cvv1cv0
l0cvv1cv0
l1cvv2cv1
l1cvv0cv0
l1cvv0cv0p5
)


for COUPLING in ${couplings[*]};
do
    inDS=user.tburch.nonresonant.vbf.bsm.${COUPLING}.1_EXT0
    outDS=user.tburch.vbf.bsm.truthAOD.${COUPLING}
    pathena --inDS $inDS --outDS $outDS --trf="Reco_tf.py --inputEVNTFile %IN  --outputDAODFile %OUT.root --reductionConf TRUTH3"
done
