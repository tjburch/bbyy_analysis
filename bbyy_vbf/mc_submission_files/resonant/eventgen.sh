setupATLAS
asetup MCProd,19.2.5.34.2,here
lsetup panda

for JOBOPTIONS in MC15*.py; do 
    MASSSTRING=$(cut -d'_' -f4 <<< ${JOBOPTIONS})
    outDS=user.tburch.resonant.${MASSSTRING}.withDSID
    runNO=${JOBOPTIONS:5:6}    
    echo "Job Options: ${JOBOPTIONS}"
    echo "Output: ${outDS}"
    echo "runNo: ${runNO}"
    pathena --outDS $outDS --trf "Generate_tf.py --ecmEnergy '13000' --maxEvents 5000 --runNumber=${runNO} --firstEvent=1 --randomSeed=%RNDM:100 --outputEVNTFile=%OUT.EVNT.root --jobConfig=${JOBOPTIONS}"  --nJobs 1 --extFile $JOBOPTIONS
done