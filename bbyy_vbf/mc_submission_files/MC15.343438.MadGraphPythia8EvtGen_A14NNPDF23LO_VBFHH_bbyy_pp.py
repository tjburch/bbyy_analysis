from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
parameters = {}

extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125'
           }

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
# No resonant contribution
#---------------------------------------------------------------------------------------------------
# generate p p > h h j j QCD=0 forces EW production

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    import model HeavyHiggsTHDM
    generate p p > h h j j QCD=0
    output -f""")
fcard.close()

beamEnergy=6500
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

runName='run_01'

process_dir = new_process(card_loc='proc_card_mg5.dat')


#---------------------------------------------------------------------------------------------------
# Setting the number of generated events to 30 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=30
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0,extras=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used
#---------------------------------------------------------------------------------------------------

build_param_card(param_card_old='%s/Cards/param_card.dat' % process_dir,param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)

print_cards()

runName='run_01'

generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=0,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')


#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------
include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_LHEF.py")



# Ev Gen Configuration
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = 'Nonresonant di-Higgs production through VBF decaying to yybb'
evgenConfig.keywords+=['Higgs', "hh"]
evgenConfig.contact = ['Tyler James Burch <tyler.james.burch@cern.ch>']
evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'

#---------------------------------------------------------------------------------------------------
# Decaying hh to bbyy with Pythia8
#---------------------------------------------------------------------------------------------------
genSeq.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                            "25:addChannel = on 0.5 100 22 22 "] # gammagamma decay
#---------------------------------------------------------------------------------------------------
# Generator Filters
# Use ParentTwoChildren filter to require:
#   Higgs(25) -> b(5)bbar(-5) AND
#   Higgs(25) -> gamma(22)gamma(22)
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentTwoChildrenFilter
filtSeq += ParentTwoChildrenFilter("HiggsToBBGamGamFilter")
filtSeq.HiggsToBBGamGamFilter.PDGParent = [25]
filtSeq.HiggsToBBGamGamFilter.PDGChild = [5,22]
