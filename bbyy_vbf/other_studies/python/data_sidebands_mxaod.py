from ROOT import *
import ROOT
from math import fabs

# Inputs 
s_20152016 = '/bdata/tburch/h024_skim/15_16_data.root'
s_2017 = '/bdata/tburch/h024_skim/17_data.root'
s_2018 = '/bdata/tburch/h024_skim/18_data.root'

# Files
f_20152016 = TFile(s_20152016, 'READ')
f_2017 = TFile(s_2017, 'READ')
f_2018 = TFile(s_2018,'READ')

f_output = TFile('data_vbf_selection.root','recreate')

# Define the histograms we'll need
h_alldata_0tag_myy = TH1F("h_alldata_myy_0tag","h_alldata_myy_0tag", 55, 105, 160)
h_vbfSelect_0tag_myy = TH1F("h_vbfSelect_myy_0tag","h_vbfSelect_myy_0tag", 55, 105, 160)
h_vbfReject_0tag_myy = TH1F("h_vbfReject_myy_0tag","h_vbfReject_myy_0tag", 55, 105, 160)

h_alldata_2tag_myy = TH1F("h_alldata_myy_2tag","h_alldata_myy_2tag", 55, 105, 160)
h_vbfSelect_2tag_myy = TH1F("h_vbfSelect_myy_2tag","h_vbfSelect_myy_2tag", 55, 105, 160)
h_vbfReject_2tag_myy = TH1F("h_vbfReject_myy_2tag","h_vbfReject_myy_2tag", 55, 105, 160)

h_njet =  TH1F("h_njet","h_njet", 55, 105, 160)
h_njet_deta =  TH1F("h_njet_deta","h_njet_deta", 55, 105, 160)

# xAOD Framework
# ---------------------------------------------------
ROOT.xAOD.Init()
files ={
    "2015_2016": f_20152016,
    "2017": f_2017,
    "2018": f_2018
}
def char2bool(char):
    """Convert char to bool."""
    return {'\x00': False, '\x01': True}[char]

def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value

# Data classes
# ---------------------------------------------------
class Jet:

    def __init__(self, pt, eta, phi, m, btag):
        self.four_vector = ROOT.TLorentzVector()
        self.four_vector.SetPtEtaPhiM(pt, eta, phi, m)
        self.b_tagged = btag

class Dijet:

    def __init__(self, jet_collection):
        self.idx1, self.idx2 = -1, -1
        self.jet_pair = -2
        self.deta, self.m = -99, -99
        # Skip b-tags
        non_b_jets = [j for j in jet_collection if j.b_tagged == False]
        if len([j for j in jet_collection if j.b_tagged == True]) > 2:
            pt_btags = [j.four_vector.Pt() for j in jet_collection if j.b_tagged == True]
            # btags sorted by pt anyway
            low_pt = pt_btags[-1]
            second_pt= pt_btags[-2]
                        
            non_b_jets = [j for j in jet_collection if j.b_tagged == False]
            for j in jets:
                if j.four_vector.Pt() == low_pt:
                    non_b_jets.append(j)
            # Reevaluate
            if len(non_b_jets) < 2:
                for j in jets:
                    if j.four_vector.Pt() == second_pt:
                        non_b_jets.append(j)          

        # If 2 extra jets, fill object
        if len(non_b_jets) < 2 : return
        self.jet_pair = -1
        max_mass = -1
        for j in non_b_jets:
            for i in non_b_jets:
                if i == j: continue
                else:
                    iPair = j.four_vector + i.four_vector
                    if iPair.M() > max_mass:
                        self.jet_pair = iPair
                        self.idx1 = non_b_jets.index(i)
                        self.idx2 = non_b_jets.index(j)
                        self.deta = fabs(i.four_vector.Eta() - j.four_vector.Eta())
                        self.m = iPair.M()




# Iterate over files, events
# ---------------------------------------------------
for iSample in files:
    file = files[iSample]

    input_tree = ROOT.xAOD.MakeTransientTree(file, "CollectionTree")
    entries = input_tree.GetEntries()
    print "tree " + iSample + " entries: " + str(entries)
    for ientry in range(input_tree.GetEntries()):
        if (ientry % 10000 == 0): print("ientry = %d out of %d, percent done = %.3f"%(ientry,input_tree.GetEntries(),1.*ientry/input_tree.GetEntries()*100))
        input_tree.GetEntry(ientry)
      
        # Get myy
        m_yy = read(input_tree.HGamEventInfo, "float", "m_yy")*0.001  
        passing = False
        if ( read(input_tree.HGamEventInfo, "char", "isPassed") and
        read(input_tree.HGamEventInfo, "int", "N_lep") == 0 and
        read(input_tree.HGamEventInfo, "int", "N_j_central") < 6 and
        read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3 and
        read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") > 70.0 and
        read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_m_jj") <= 180.0 and
        m_yy > 105 and m_yy < 160) :

            passing = True

        if not passing: continue


        # Get the tag region
        tagRegion = 0
        for j in input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT_lepCorrAllMu:
            bin = read(j, 'int', "MV2c10bin")
            if bin >= 1:
                tagRegion += 1

        # Blind        
        if (tagRegion == 2 and m_yy > 120 and m_yy < 130): continue

        # Define Jets
        # ------------------------------------------------------------------------------------------------
        # Read
        jets =[]
        for j in input_tree.HGamAntiKt4EMTopoJets:
            thisJet = Jet(j.pt()*0.001, j.eta(), j.phi(), j.m()*0.001, btag=(read(j, 'char', "MV2c10_FixedCutBEff_85")))
            jets.append(thisJet)


        # Do Cuts
        # ------------------------------------------------------------------------------------------------
        isVBF = False
        # 4 non-btagged Jets Requirement
        non_b_jets = [j for j in jets if j.b_tagged == False]  
        if read(input_tree.HGamEventInfo, "int", "N_j") >= 4:
            h_njet.Fill(m_yy)            
            dijet = Dijet(jets)
            # Check
            if dijet.deta == -99 or dijet.m == -99: 
                print "Problem in Dijet!!!!"
            
            # Requirements
            if dijet.deta > 3.5: 
                h_njet_deta.Fill(m_yy)
                if dijet.m > 400: 
                    isVBF = True


        if isVBF:
            if tagRegion == 2:
                h_vbfSelect_2tag_myy.Fill(m_yy)
            if tagRegion == 0:
                h_vbfSelect_0tag_myy.Fill(m_yy)
        else:
            if tagRegion == 2:
                h_vbfReject_2tag_myy.Fill(m_yy)
            if tagRegion == 0:
                h_vbfReject_0tag_myy.Fill(m_yy)            

        if tagRegion == 2:
            h_alldata_2tag_myy.Fill(m_yy)
        if tagRegion == 0:
            h_alldata_0tag_myy.Fill(m_yy)
    
    ROOT.xAOD.ClearTransientTrees()
    file.Close()

f_output.cd()
hists = [h_vbfSelect_2tag_myy, h_vbfSelect_0tag_myy, h_vbfReject_2tag_myy, h_vbfReject_0tag_myy,h_alldata_2tag_myy, h_alldata_0tag_myy, h_njet, h_njet_deta]
for h in hists:
    h.Write()

