from rootpy.io import root_open
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter
from matplotlib import gridspec
set_style(get_style('ATLAS'))

# Get VBF histograms
vbf_file = root_open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/other_studies/data/vbf_mHH_hists.root')
h_vbf_myy = vbf_file.m_yy_noSelection
h_vbf_mjj = vbf_file.m_jj_noSelection
h_vbf_myyjj = vbf_file.m_yyjj_noSelection

ggf_file = root_open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/other_studies/data/ggF_mHH_hists.root')
mc16a_SF = 36100./(36100+43700)
mc16d_SF = 43700./(36100+43700)

h_ggf_myy_mc16a = ggf_file.h_myy_mc16a
h_ggf_myy_mc16d = ggf_file.h_myy_mc16d
h_ggf_mjj_mc16a = ggf_file.h_mjj_mc16a
h_ggf_mjj_mc16d = ggf_file.h_mjj_mc16d
h_ggf_myyjj_mc16a = ggf_file.h_myybb_mc16a
h_ggf_myyjj_mc16d = ggf_file.h_myybb_mc16d

for h in [h_ggf_myy_mc16a, h_ggf_mjj_mc16a, h_ggf_myyjj_mc16a]:
    h.Scale(mc16a_SF)
for h in [h_ggf_myy_mc16d, h_ggf_mjj_mc16d, h_ggf_myyjj_mc16d]:
    h.Scale(mc16d_SF)

h_ggf_myy = h_ggf_myy_mc16a.Clone()
h_ggf_myy.Add(h_ggf_myy_mc16d)
h_ggf_mjj = h_ggf_mjj_mc16a.Clone()
h_ggf_mjj.Add(h_ggf_mjj_mc16d)
h_ggf_myyjj = h_ggf_myyjj_mc16a.Clone()
h_ggf_myyjj.Add(h_ggf_myyjj_mc16d)

def ratio_plot_configuration():

    fig = plt.figure(figsize=(6.5,7))
    gs = gridspec.GridSpec(nrows=2, ncols=1, height_ratios=[3,1])
    ax0 = plt.subplot(gs[0])
    ax0.get_xaxis().set_visible(False)
    yticks = ax0.yaxis.get_major_ticks()
    yticks[0].set_visible(False)
    ax1 = plt.subplot(gs[1])
    gs.update(wspace=0, hspace=0)
    return fig, [ax0, ax1]

def fill_ratio(h_list):
    # Divide histograms, looks like cloning happens under the hood
    h_ratio = h_list[0].divide(h_list[0], h_list[1]) # This is weird.
    h_ratio.linecolor = 'navy'
    plt.tight_layout()
    rplt.errorbar(h_ratio, markersize=0)
    plt.axhline(y=1, color='black', linestyle=':')

def plot(h_vbf, h_ggf):
    h_vbf.Scale(1.0/h_vbf.Integral())
    h_ggf.Scale(1.0/h_ggf.Integral())
    fig, ax_list = ratio_plot_configuration()
    h_vbf.linecolor = 'firebrick'
    h_ggf.linecolor = 'cornflowerblue'
    plt.ylim(0,max([1.1*h.GetMaximum() for h in [h_vbf,h_ggf]]))
    plt.xlim(h_ggf.GetXaxis().GetBinCenter(h_ggf.GetMinimumBin()), h_ggf.GetXaxis().GetBinCenter(h_ggf.GetMaximumBin()))

    rplt.errorbar(h_vbf, markersize=0, axes=ax_list[0])
    rplt.errorbar(h_ggf, markersize=0, axes=ax_list[0])
    plt.sca(ax_list[0])
    leg = plt.legend(['VBF', 'ggF'])
    plt.sca(ax_list[1])
    fill_ratio([h_vbf,h_ggf])
    ax_list[1].set_ylabel(r'$\frac{VBF}{ggF}$', fontsize=16)
    plt.ylim(-5,5)

    return fig, ax_list



fig, ax_list =plot(h_vbf_myy,h_ggf_myy)
plt.sca(ax_list[1])
plt.xlabel('$m_{\gamma\gamma}$ [GeV]',x=1.0, fontsize=16)
ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)
plt.tight_layout()
plt.savefig('myy.pdf')

fig, ax_list = plot(h_vbf_mjj,h_ggf_mjj)
plt.sca(ax_list[1])
plt.xlabel('$m_{bb}$ [GeV]',x=1.0, fontsize=16)
ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)
plt.tight_layout()
plt.savefig('mjj.pdf')

pfig, ax_list = plot(h_vbf_myyjj,h_ggf_myyjj)
plt.sca(ax_list[1])
plt.xlabel('$m_{\gamma\gamma bb}$ [GeV]',x=1.0, fontsize=16)
ax_list[0].set_ylabel('A.U. (10 GeV Bins)', fontsize=16)
plt.tight_layout()
plt.savefig('myyjj.pdf')
