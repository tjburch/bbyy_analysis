from rootpy.io import root_open
import rootpy.plotting.root2matplotlib as rplt
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter
from matplotlib import gridspec, rc
import numpy as np
from plotting_tools import add_watermark

doData = True
doggF = True
doVBF = True

set_style(get_style('ATLAS'))
hfont = {'fontname':'Helvetica'}

histogram_file = root_open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/other_studies/run/data_vbf_selection.root')

h_vbf_0tag = histogram_file.h_vbfSelect_myy_0tag
h_nonvbf_0tag = histogram_file.h_vbfReject_myy_0tag

# Make for cutflow later
n_all = histogram_file.h_vbfSelect_myy_0tag.Integral() + histogram_file.h_vbfReject_myy_0tag.Integral()
n_pass_njet = histogram_file.h_njet.Integral() + histogram_file.h_njet_deta.Integral() + histogram_file.h_vbfSelect_myy_0tag.Integral()
n_pass_njet_deta = histogram_file.h_njet_deta.Integral() + histogram_file.h_vbfSelect_myy_0tag.Integral()
n_pass_all = histogram_file.h_vbfSelect_myy_0tag.Integral()

h_vbf_2tag = histogram_file.h_vbfSelect_myy_2tag
h_nonvbf_2tag = histogram_file.h_vbfReject_myy_2tag

def ratio_plot_configuration():

    fig = plt.figure(figsize=(6.5,7))
    gs = gridspec.GridSpec(nrows=2, ncols=1, height_ratios=[3,1])
    ax0 = plt.subplot(gs[0])
    ax0.get_xaxis().set_visible(False)
    yticks = ax0.yaxis.get_major_ticks()
    yticks[0].set_visible(False)
    ax1 = plt.subplot(gs[1])
    gs.update(wspace=0, hspace=0)
    return fig, [ax0, ax1]

def fill_ratio(h_list):
    # Divide histograms, looks like cloning happens under the hood
    h_ratio = h_list[0].divide(h_list[0], h_list[1]) # This is weird.
    h_ratio.linecolor = 'navy'
    plt.tight_layout()
    rplt.errorbar(h_ratio, markersize=0)
    plt.axhline(y=1, color='black', linestyle=':')

def plot(h_vbf, h_ggf, normalize=True, ratio=True):
    if normalize:
        h_vbf.Scale(1.0/h_vbf.Integral())
        h_ggf.Scale(1.0/h_ggf.Integral())
    fig, ax_list = ratio_plot_configuration()
    h_vbf.linecolor = 'firebrick'
    h_ggf.linecolor = 'cornflowerblue'
    plt.ylim(0,max([1.3*h.GetMaximum() for h in [h_vbf,h_ggf]]))
    plt.xlim(h_ggf.GetXaxis().GetBinCenter(h_ggf.GetMinimumBin()), h_ggf.GetXaxis().GetBinCenter(h_ggf.GetMaximumBin()))

    rplt.errorbar(h_vbf, markersize=0, axes=ax_list[0])
    rplt.errorbar(h_ggf, markersize=0, axes=ax_list[0])
    plt.sca(ax_list[0])
    leg = plt.legend(['Pass VBF Cuts', 'Fail VBF Cuts'], fontsize=12)
    plt.sca(ax_list[1])
    fill_ratio([h_vbf,h_ggf])
    ax_list[1].set_ylabel(r'$\frac{Pass}{Fail}$', fontsize=16)
    plt.ylim(0,2)

    return fig, ax_list

def find_passfail(h_pass,h_fail):
    n_pass = h_pass.Integral()
    n_fail = h_fail.Integral()
    return n_pass, n_fail

if doData:
        # Plot vbf/nonvbf normalized
        pass0, fail0 = find_passfail(h_vbf_0tag, h_nonvbf_0tag)
        fig, ax_list = plot(h_vbf_0tag, h_nonvbf_0tag, normalize=True)
        plt.sca(ax_list[1])
        plt.xlabel('$m_{\gamma\gamma}$ [GeV]',x=1.0, fontsize=16)
        ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)
        plt.sca(ax_list[0])
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.3,.79),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$\int L dt = 140.5$ fb$^{-1} $", xy=(.98,.72),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"0-tag region", xy=(.98,.65),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        # Entry annotation
        total = pass0+fail0
        plt.annotate(r"%i Pass VBF Cuts (%.2f%%)"%(pass0,100*float(pass0)/total), xy=(.02,.98),xycoords='axes fraction', horizontalalignment='left',va='top', fontsize=16,  **hfont)
        plt.annotate(r"%i Fail VBF Cuts (%.2f%%)"%(fail0 ,100*float(fail0)/total), xy=(.02,.93),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        plt.annotate(r"%i Total Events"%(pass0+fail0), xy=(.02,.88),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/m_yy_data_0tag.pdf')
        plt.close()

        pass2, fail2 = find_passfail(h_vbf_2tag, h_nonvbf_2tag)
        fig, ax_list = plot(h_vbf_2tag, h_nonvbf_2tag, normalize=True)
        plt.sca(ax_list[1])
        plt.xlabel('$m_{\gamma\gamma}$ [GeV]',x=1.0, fontsize=16)
        ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)
        plt.sca(ax_list[0])
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.3,.83),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$\int L dt = 140.5$ fb$^{-1} $", xy=(.98,.76),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"2-tag region", xy=(.98,.69),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        # Entry annotation
        total = pass2+fail2
        plt.annotate(r"%i Pass VBF Cuts (%.2f%%)"%(pass0,100*float(pass0)/total), xy=(.02,.98),xycoords='axes fraction', horizontalalignment='left',va='top', fontsize=16,  **hfont)
        plt.annotate(r"%i Fail VBF Cuts (%.2f%%)"%(fail0 ,100*float(fail0)/total), xy=(.02,.93),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        plt.annotate(r"%i Total Events"%(pass0+fail0), xy=(.02,.88),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/m_yy_data_2tag.pdf')
        plt.close()




        # Make Cutflow
        f = plt.figure()
        ax = plt.gca()
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]
        x = range(len(y))
        bin_names = ['All',
                'nJet $\geq 4$', 
                '$\Delta \eta > 3.5$',
                '$m_{jj} > 400$']

        plt.sca(ax)
        plt.bar(x, y)
        # plt.sca(ax2)
        # plt.bar(x, y)
        plt.yscale('log')
        plt.xticks(x, bin_names, rotation=30)
        plt.xlabel('Cut', fontsize=16)
        plt.ylabel(' ', fontsize=24)

        plt.annotate('Events (Log Scale)', xy=(0.025,0.6), xycoords='figure fraction', ha='center', va='center', rotation=90, fontsize=16)

        print "TJB DATA : ", y
        # Set limits of break
        # ax.set_ylim(68000, 70000)  # outliers only
        # ax2.set_ylim(0, 9000)  # most of the data
        #ax.spines['bottom'].set_visible(False)
        # ax2.spines['top'].set_visible(False)
        # ax.xaxis.tick_top()
        #ax.tick_params(labeltop='off')  # don't put tick labels at the top
        #ax2.xaxis.tick_bottom()


        #Cut lines
        """
        d = .015  # how big to make the diagonal lines in axes coordinates
        # arguments to pass to plot, just so we don't keep repeating them
        kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
        ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
        ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

        kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
        ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
        ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
        """
        fractional_cuts = [z/float(n_all) for z in y] # big yikes on the naming here, tyler
        labels = ['$\epsilon_{abs}=$%.3f'% cut for cut in fractional_cuts]
        relative_cuts = [1]
        for i in range(len(y)-1):    
                relative_cuts.append(float(y[i+1])/y[i])
                rel_labels = ['$\epsilon_{rel}=$%.3f'% cut for cut in relative_cuts]

        rects = ax.patches
        for rect, label,rel_label in zip(rects,labels,rel_labels):
                if rects.index(rect) == 0 : continue
                height = rect.get_height()
                ax.text(rect.get_x() + rect.get_width() / 2, height + height*0.01, label,
                        ha='center', va='bottom')
                ax.text(rect.get_x() + rect.get_width() / 2, height + height*0.20, rel_label,
                        ha='center', va='bottom')

        # Labels
        plt.sca(ax)
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.28,.83),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$\int L dt = 140.5$ fb$^{-1} $", xy=(0.99,.65),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"0-tag region", xy=(.98,.50),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        add_watermark()

        plt.tight_layout()
        plt.savefig('run/data_cutflow.pdf')



# ---------------------
# Deal with MC here
# ---------------------
if doggF:
        f_ggf = root_open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/other_studies/run/ggf_categorization.root')


        # name convention h = histogram, ggf = signal sample, select/rejct = selected or rejected in ggf category
        h_ggf_select = f_ggf.h_vbfSelect_myy
        h_ggf_reject = f_ggf.h_vbfReject_myy
        h_njet = f_ggf.h_jets_dist
        h_pass_njet = f_ggf.h_njet
        h_pass_njet_deta = f_ggf.h_njet_deta

        all = h_ggf_reject.Integral() + h_ggf_select.Integral()
        for h in [h_ggf_reject, h_ggf_select, h_njet, h_pass_njet, h_pass_njet_deta]:                
                h.Scale(1/all)
                #h.Scale(140.5/(36.1+43.7))

        n_all = h_ggf_reject.Integral() + h_ggf_select.Integral()
        n_pass_njet = h_pass_njet.Integral() 
        n_pass_njet_deta = h_pass_njet_deta.Integral()
        n_pass_all = h_ggf_select.Integral()
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]


        # Plot vbf/nonvbf normalized
        pass0, fail0 = find_passfail(h_ggf_select, h_ggf_reject)
        fig, ax_list = plot(h_ggf_select, h_ggf_reject, normalize=True)
        plt.sca(ax_list[1])
        ax_list[0].set_ylim(ymin=0,ymax=ax_list[0].get_ylim()[1]+0.1)        
        plt.xlabel('$m_{\gamma\gamma}$ [GeV]',x=1.0, fontsize=16)
        ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)        
        plt.sca(ax_list[0])
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.3,.79),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$HH\rightarrow \gamma\gamma b\bar{b}$ (ggF)", xy=(.98,.72),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"b-Tag Inclusive", xy=(.98,.65),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        # Entry annotation
        total = pass0+fail0
        #plt.annotate(r"%.3f Pass VBF Cuts (%.2f%%)"%(pass0,100*float(pass0)/total), xy=(.02,.98),xycoords='axes fraction', horizontalalignment='left',va='top', fontsize=16,  **hfont)
        #plt.annotate(r"%.3f Fail VBF Cuts (%.2f%%)"%(fail0 ,100*float(fail0)/total), xy=(.02,.93),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        #plt.annotate(r"%.3f Total Events"%(pass0+fail0), xy=(.02,.88),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/m_yy_ggF.pdf')
        plt.close()

        # Make Cutflow
        f = plt.figure()
        ax = plt.gca()
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]
        print y 
        x = range(len(y))
        bin_names = ['All',
                'nJet $\geq 4$', 
                '$\Delta \eta > 3.5$',
                '$m_{jj} > 400$']

        plt.bar(x, y)
        plt.xticks(x, bin_names, rotation=30)
        plt.xlabel('Cut', fontsize=16)
        plt.ylabel(' ', fontsize=24)

        plt.annotate('Efficiency', xy=(0.025,0.6), xycoords='figure fraction', ha='center', va='center', rotation=90, fontsize=16)

        # Set limits of break
        ax.set_ylim(0, n_all*1.1)  # outliers only

        """
        f, (ax, ax2) = plt.subplots(2, 1, sharex=True)
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]
        x = range(len(y))
        bin_names = ['All',
                'nJet $\geq 4$', 
                '$\Delta \eta > 3.5$',
                '$m_{jj} > 400$']

        plt.sca(ax)
        plt.bar(x, y)
        plt.sca(ax2)
        plt.bar(x, y)
        plt.xticks(x, bin_names, rotation=30)
        plt.xlabel('Cut', fontsize=16)
        plt.ylabel(' ', fontsize=24)

        plt.annotate('A.U.', xy=(0.025,0.6), xycoords='figure fraction', ha='center', va='center', rotation=90, fontsize=16)

        # Set limits of break
        ax.set_ylim(n_all*0.9, n_all*1.1)  # outliers only
        ax2.set_ylim(0, n_pass_njet*1.3)  # most of the data
        ax.spines['bottom'].set_visible(False)
        ax2.spines['top'].set_visible(False)
        ax.xaxis.tick_top()
        ax.tick_params(labeltop='off')  # don't put tick labels at the top
        ax2.xaxis.tick_bottom()


        #Cut lines
        d = .015  # how big to make the diagonal lines in axes coordinates
        # arguments to pass to plot, just so we don't keep repeating them
        kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
        ax.plot((-d, +d), (-d, +d), **kwargs)        # top-left diagonal
        ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

        kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
        ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
        ax2.plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  # bottom-right diagonal
        """
        fractional_cuts = [z/float(n_all) for z in y] # big yikes on the naming here, tyler
        labels = ['$\epsilon_{abs}=$%.3f'% cut for cut in fractional_cuts]
        relative_cuts = [1]
        for i in range(len(y)-1):    
                relative_cuts.append(float(y[i+1])/y[i])
                rel_labels = ['$\epsilon_{rel}=$%.3f'% cut for cut in relative_cuts]

        rects = ax.patches
        for rect, label,rel_label in zip(rects,labels,rel_labels):
                if rects.index(rect) == 0 : continue
                height = rect.get_height()
                ax.text(rect.get_x() + rect.get_width() / 2, height + 0.01, label,
                        ha='center', va='bottom')
                ax.text(rect.get_x() + rect.get_width() / 2, height + 0.06, rel_label,
                        ha='center', va='bottom')

        # Labels
        plt.sca(ax)
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.28,.83),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$HH\rightarrow \gamma\gamma b\bar{b}$ (ggF)", xy=(0.99,.65),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"b-Tag Inclusive", xy=(.98,.50),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/ggf_cutflow.pdf')



## VBF ##
if doVBF:
        f_vbf = root_open('/Users/tburch/Documents/gitDevelopment/bbyy_analysis/bbyy_vbf/other_studies/run/vbf_categorization.root')

        # name convention h = histogram, vbf = signal sample, select/rejct = selected or rejected in VBF category
        h_vbf_select = f_vbf.h_vbfSelect_myy
        h_vbf_reject = f_vbf.h_vbfReject_myy
        h_njet = f_vbf.h_jets_dist
        h_pass_njet = f_vbf.h_njet
        h_pass_njet_deta = f_vbf.h_njet_deta

        all = h_vbf_reject.Integral() + h_vbf_select.Integral()
        for h in [h_vbf_reject, h_vbf_select, h_njet, h_pass_njet, h_pass_njet_deta]:                
                h.Scale(1/all)
                #h.Scale(140.5/(36.1+43.7))

        n_all = h_vbf_reject.Integral() + h_vbf_select.Integral()
        n_pass_njet = h_pass_njet.Integral() 
        n_pass_njet_deta = h_pass_njet_deta.Integral()
        n_pass_all = h_vbf_select.Integral()
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]


        # Plot vbf/nonvbf normalized
        pass0, fail0 = find_passfail(h_vbf_select, h_vbf_reject)
        fig, ax_list = plot(h_vbf_select, h_vbf_reject, normalize=True)
        ax_list[0].set_ylim(ymin=0,ymax=ax_list[0].get_ylim()[1]+0.1)        
        plt.sca(ax_list[1])        
        plt.xlabel('$m_{\gamma\gamma}$ [GeV]',x=1.0, fontsize=16)
        ax_list[0].set_ylabel('A.U. (2.5 GeV Bins)', fontsize=16)
        plt.sca(ax_list[0])
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.3,.79),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$HH\rightarrow \gamma\gamma b\bar{b}jj$ (VBF)", xy=(.98,.72),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"b-Tag Inclusive", xy=(.98,.65),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        # Entry annotation
        total = pass0+fail0
        #plt.annotate(r"%.3f Pass VBF Cuts (%.2f%%)"%(pass0,100*float(pass0)/total), xy=(.02,.98),xycoords='axes fraction', horizontalalignment='left',va='top', fontsize=16,  **hfont)
        #plt.annotate(r"%.3f Fail VBF Cuts (%.2f%%)"%(fail0 ,100*float(fail0)/total), xy=(.02,.93),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        #plt.annotate(r"%.3f Total Events"%(pass0+fail0), xy=(.02,.88),xycoords='axes fraction', horizontalalignment='left',va='top',fontsize=16,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/m_yy_vbf.pdf')
        plt.close()

        # Make Cutflow
        f = plt.figure()
        ax = plt.gca()
        y = [n_all, n_pass_njet, n_pass_njet_deta, n_pass_all]
        print y 
        x = range(len(y))
        bin_names = ['All',
                'nJet $\geq 4$', 
                '$\Delta \eta > 3.5$',
                '$m_{jj} > 400$']

        plt.bar(x, y)
        plt.xticks(x, bin_names, rotation=30)
        plt.xlabel('Cut', fontsize=16)
        plt.ylabel(' ', fontsize=24)

        plt.annotate('Efficiency', xy=(0.025,0.6), xycoords='figure fraction', ha='center', va='center', rotation=90, fontsize=16)

        # Set limits of break
        ax.set_ylim(0, n_all*1.1)  # outliers only

        fractional_cuts = [z/float(n_all) for z in y] # big yikes on the naming here, tyler
        labels = ['$\epsilon_{abs}=$%.3f'% cut for cut in fractional_cuts]
        relative_cuts = [1]
        for i in range(len(y)-1):    
                relative_cuts.append(float(y[i+1])/y[i])
                rel_labels = ['$\epsilon_{rel}=$%.3f'% cut for cut in relative_cuts]

        rects = ax.patches
        for rect, label,rel_label in zip(rects,labels,rel_labels):
                if rects.index(rect) == 0 : continue
                height = rect.get_height()
                ax.text(rect.get_x() + rect.get_width() / 2, height + 0.01, label,
                        ha='center', va='bottom')
                ax.text(rect.get_x() + rect.get_width() / 2, height + 0.06, rel_label,
                        ha='center', va='bottom')

        # Labels
        plt.sca(ax)
        rc('text',usetex=True)
        plt.annotate(r"\textit{\textbf{ATLAS}} Internal", xy=(1.28,.79),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        rc('text',usetex=False)
        plt.annotate(r"$HH\rightarrow \gamma\gamma b\bar{b}jj$ (VBF)", xy=(0.99,.63),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        plt.annotate(r"b-Tag Inclusive", xy=(.98,.48),xycoords='axes fraction', horizontalalignment='right',fontsize=18,  **hfont)
        add_watermark()
        plt.tight_layout()
        plt.savefig('run/vbf_cutflow.pdf')
        plt.close()

        fig = plt.figure()        
        rplt.errorbar(h_njet, markersize=0)
        plt.xlabel('nJets')
        plt.ylabel('A.U.')
        plt.ylim(ymin=0)
        add_watermark()

        plt.savefig('run/vbf_njets.pdf')
        print y 

        