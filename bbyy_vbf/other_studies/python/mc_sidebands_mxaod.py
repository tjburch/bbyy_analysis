## TODO: combine with data script
from ROOT import *
import ROOT
from math import fabs
from array import array

isggF = True

# Inputs 
if isggF:
    s_20152016 = '/bdata/tburch/h024_skim/mc16a_hh_yybb_NLO.root'
    s_2017 = '/bdata/tburch/h024_skim/mc16d_hh_yybb_NLO.root'
else:
    s_20152016 = '/bdata/tburch/vbf_personal_mxaod/mc16a_sm_hh_vbf.root'    

# Files
f_20152016 = TFile(s_20152016, 'READ')
if isggF:
    f_2017 = TFile(s_2017, 'READ')

if isggF:
    f_output = TFile('ggf_categorization.root','recreate')
    f_tree = TFile('ggf_ntuple.root','recreate')
else:
    f_output = TFile('vbf_categorization.root','recreate')
    f_tree = TFile('vbf_ntuple.root','recreate')

# Define the histograms we'll need
h_alldata_0tag_myy = TH1F("h_alldata_myy_0tag","h_alldata_myy_0tag", 55, 105, 160)
h_vbfSelect_0tag_myy = TH1F("h_vbfSelect_myy_0tag","h_vbfSelect_myy_0tag", 55, 105, 160)
h_vbfReject_0tag_myy = TH1F("h_vbfReject_myy_0tag","h_vbfReject_myy_0tag", 55, 105, 160)

h_alldata_2tag_myy = TH1F("h_alldata_myy_2tag","h_alldata_myy_2tag", 55, 105, 160)
h_vbfSelect_2tag_myy = TH1F("h_vbfSelect_myy_2tag","h_vbfSelect_myy_2tag", 55, 105, 160)
h_vbfReject_2tag_myy = TH1F("h_vbfReject_myy_2tag","h_vbfReject_myy_2tag", 55, 105, 160)

h_vbfSelect_myy = TH1F("h_vbfSelect_myy","h_vbfSelect_myy", 55, 105, 160)
h_vbfReject_myy = TH1F("h_vbfReject_myy","h_vbfReject_myy", 55, 105, 160)

h_njet =  TH1F("h_njet","h_njet", 55, 105, 160)
h_njet_deta =  TH1F("h_njet_deta","h_njet_deta", 55, 105, 160)

h_jets_dist = TH1F("h_jets_dist","h_jets_dist", 9, 1, 10)
h_jets_dist_pass = TH1F("h_jets_pass_dist","h_jets_pass_dist", 9, 1, 10)

pt_y1 = TH1F("h_pT_y1","h_pT_y1",100,0,1000)
pt_y2 = TH1F("h_pT_y2","h_pT_y2",100,0,1000)

# xAOD Framework
# ---------------------------------------------------
ROOT.xAOD.Init()
if isggF:
    files ={
        "2015_2016": f_20152016,
        "2017": f_2017,
    }
else:
    files ={
        "2015_2016": f_20152016,
    }

def char2bool(char):
    """Convert char to bool."""
    return {'\x00': False, '\x01': True}[char]

def read(obj, cpp_type, name):
    """Document here."""
    raw_value = obj.auxdataConst(cpp_type)(name)
    if cpp_type == "char":
        return char2bool(raw_value)
    if cpp_type == "float":
        return float(raw_value)
    if "int" in cpp_type:
        return int(raw_value)
    if "long" in cpp_type:
        return int(raw_value)
    return raw_value

# Data classes
# ---------------------------------------------------
class Jet:

    def __init__(self, pt, eta, phi, m, btag):
        self.four_vector = ROOT.TLorentzVector()
        self.four_vector.SetPtEtaPhiM(pt, eta, phi, m)
        self.b_tagged = btag

class Dijet:

    def __init__(self, jet_collection):
        self.idx1, self.idx2 = -1, -1
        self.jet_pair = -2
        self.deta, self.m = -99, -99
        # Skip b-tags
        non_b_jets = [j for j in jet_collection if j.b_tagged == False]
        if len([j for j in jet_collection if j.b_tagged == True]) > 2:
            pt_btags = [j.four_vector.Pt() for j in jet_collection if j.b_tagged == True]
            # btags sorted by pt anyway
            low_pt = pt_btags[-1]
            second_pt= pt_btags[-2]
                        
            non_b_jets = [j for j in jet_collection if j.b_tagged == False]
            for j in jets:
                if j.four_vector.Pt() == low_pt:
                    non_b_jets.append(j)
            # Reevaluate
            if len(non_b_jets) < 2:
                for j in jets:
                    if j.four_vector.Pt() == second_pt:
                        non_b_jets.append(j)                       
                  

        # If 2 extra jets, fill object
        if len(non_b_jets) < 2 : return
        self.jet_pair = -1
        max_mass = -1
        for j in non_b_jets:
            for i in non_b_jets:
                if i == j: continue
                else:
                    iPair = j.four_vector + i.four_vector
                    if iPair.M() > max_mass:
                        self.jet_pair = iPair
                        self.idx1 = non_b_jets.index(i)
                        self.idx2 = non_b_jets.index(j)
                        self.deta = fabs(i.four_vector.Eta() - j.four_vector.Eta())
                        self.m = iPair.M()


# Define Tree
# ---------------------------------------------------
if isggF: output_tree = TTree('hh_yybb_ggF','hh_yybb_ggF')
else: output_tree = TTree('hh_yybb_VBF','hh_yybb_VBF')

m_yy = array('f',[0])
jj_deta = array('f',[0])
mjj = array('f',[0])
n_jet = array('i',[0])
eventWeight = array('f',[0])

output_tree.Branch('m_yy', m_yy, 'm_yy/F')
output_tree.Branch('jj_deta', jj_deta,'jj_deta/F')
output_tree.Branch('mjj', mjj,'mjj/F')
output_tree.Branch('n_jet', n_jet,'n_jet/I')
output_tree.Branch('eventWeight', eventWeight,'eventWeight/F')

# Iterate over files, events
# ---------------------------------------------------
for iSample in files:
    file = files[iSample]

    if isggF: cutflow_name = 'CutFlow_aMCnlo_Hwpp_hh_yybb_noDalitz_weighted'
    else: cutflow_name = 'CutFlow_MGH7EvtGen_MMHT2014_hh_bbyy_vbf_l1cvv1cv1_noDalitz_weighted'
    
    if iSample == "2015_2016": lumi = 36100
    elif iSample == "2017": lumi = 43700
    
    sum_weight = (file.Get(cutflow_name).GetBinContent(1) / file.Get(cutflow_name).GetBinContent(2) )*file.Get(cutflow_name).GetBinContent(3)


    input_tree = ROOT.xAOD.MakeTransientTree(file, "CollectionTree")
    entries = input_tree.GetEntries()
    print "tree " + iSample + " entries: " + str(entries)
    for ientry in range(input_tree.GetEntries()):
        # Reset TTree values
        m_yy, jj_deta, mjj, n_jet, eventWeight = -99, -99, -99, -99, -99

        if (ientry % 10000 == 0): print("ientry = %d out of %d, percent done = %.3f"%(ientry,input_tree.GetEntries(),1.*ientry/input_tree.GetEntries()*100))
        input_tree.GetEntry(ientry)

        # Get myy
        m_yy = read(input_tree.HGamEventInfo, "float", "m_yy")*0.001  
        passing = False
        # Turn this block off to compare with jahred's
        
        if ( read(input_tree.HGamEventInfo, "char", "isPassed") and
            read(input_tree.HGamEventInfo, "int", "N_lep") == 0 and
            read(input_tree.HGamEventInfo, "int", "N_j_central") < 6 and
            read(input_tree.HGamEventInfo, "int", "N_j_btag") < 3 and
            read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_AllMu_m_jj") > 70.0 and
            read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_AllMu_m_jj") <= 180.0):
            passing = True

        if not passing: continue

        # Get the tag region
        tagRegion = 0
        for j in input_tree.yybbAntiKt4EMTopoJets_discreteMV2c10pT_lepCorrAllMu:
            bin = read(j, 'int', "MV2c10bin")
            if bin >= 1:
                tagRegion += 1

        # Blind - NOT NEEDED FOR MC
        # if (tagRegion == 2 and m_yy > 120 and m_yy < 130): continue

        # Get weights
        weight = ( read(input_tree.HGamEventInfo, "float", "crossSectionBRfilterEff") *\
                read(input_tree.HGamEventInfo, "float", "weight") *\
                read(input_tree.HGamEventInfo, "float", "yybb_discreteMV2c10pT_weight") 
                * float(lumi) / float(sum_weight))                             

        # Define Jets
        # ------------------------------------------------------------------------------------------------
        # Read
        jets =[]
        for j in input_tree.HGamAntiKt4EMTopoJets:
            thisJet = Jet(j.pt()*0.001, j.eta(), j.phi(), j.m()*0.001, btag=(read(j, 'char', "MV2c10_FixedCutBEff_85")))
            jets.append(thisJet)


        # Do Cuts
        # ------------------------------------------------------------------------------------------------
        isVBF = False
        # 4 non-btagged Jets Requirement
        non_b_jets = [j for j in jets if j.b_tagged == False]  
        n_jet = read(input_tree.HGamEventInfo, "int", "N_j")
        h_jets_dist.Fill(n_jet, weight)

        if n_jet >= 4:
            h_jets_dist_pass.Fill(n_jet, weight)
            h_njet.Fill(m_yy, weight)            
            dijet = Dijet(jets)
            # Check
            if dijet.deta == -99 or dijet.m == -99: 
                print "Problem in Dijet!!!!"
            
            # Requirements
            if dijet.deta > 3.5: 
                h_njet_deta.Fill(m_yy, weight)
                if dijet.m > 400: 
                    isVBF = True
            # Set tree variables
            jj_deta = dijet.deta
            mjj = dijet.m

        if isVBF:
            h_vbfSelect_myy.Fill(m_yy, weight)

            if tagRegion == 2:
                h_vbfSelect_2tag_myy.Fill(m_yy, weight)
            if tagRegion == 0:
                h_vbfSelect_0tag_myy.Fill(m_yy, weight)

        else:
            h_vbfReject_myy.Fill(m_yy, weight)

            if tagRegion == 2:
                h_vbfReject_2tag_myy.Fill(m_yy, weight)
            if tagRegion == 0:
                h_vbfReject_0tag_myy.Fill(m_yy, weight)            

        if tagRegion == 2:
            h_alldata_2tag_myy.Fill(m_yy, weight)
        if tagRegion == 0:
            h_alldata_0tag_myy.Fill(m_yy, weight)

        # Fill TTree values
        eventWeight = weight
        output_tree.Fill()



    ROOT.xAOD.ClearTransientTrees()
    file.Close()

f_output.cd()


hists = [h_vbfSelect_2tag_myy, h_vbfSelect_0tag_myy, h_vbfReject_2tag_myy, h_vbfReject_0tag_myy,h_alldata_2tag_myy, h_alldata_0tag_myy, h_njet, h_njet_deta ,h_vbfSelect_myy, h_vbfReject_myy, h_jets_dist, h_jets_dist_pass]
for h in hists:
    if isggF:
        h.Scale(140/79.8) # Scale to 140fb/(43.7+36.1)fb
    else:
        h.Scale(140/36.1) # Scale to 140fb/(36.1)fb
    h.Write()
    print h.GetName(), " ", h.Integral()

f_output.Close()

f_tree.cd()
output_tree.Write()
f_tree.Close()