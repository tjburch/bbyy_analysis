#include <TMath.h>
#include <TLorentzVector.h>
using namespace std;

// Single Jet
//--------------------------------------------------------------------------------------
class Jet
{

public:
    Jet(int i, float jet_pt, float jet_mass, float jet_eta, float jet_phi, Bool_t btag);
    int idx;
    float pt;
    float m;
    float eta;
    float phi;
    TLorentzVector four_vector;
    Bool_t btagged;
};

Jet::Jet(int i, float jet_pt, float jet_mass, float jet_eta, float jet_phi, Bool_t btag)
{
    idx = i;
    pt = jet_pt;
    m = jet_mass;
    eta = jet_eta;
    phi = jet_phi;
    four_vector.SetPtEtaPhiM(pt, eta, phi, m);
    btagged = btag;
}

// mbb pair
//--------------------------------------------------------------------------------------
class Pair_bb
{

public:
    Pair_bb();
    void check_jet(Jet jet, bool enable_pseudoB);
    void add_jet(Jet jet, int idx_to_fill);
    void set_four_vector();

    int idx1, idx2;
    float pt1, pt2;
    float m1, m2;
    float eta1, eta2;
    float phi1, phi2;
    bool CPtagged1, CPtagged2; // bool to keep track of offically tagged jets
    TLorentzVector four_vector;

    float mjj;
    float dEta;
    float dPhi;
};

// Standard constructor
Pair_bb::Pair_bb()
{
    idx1 = -1; idx2 = -1;
    pt1 = -100; pt2 = -100;
    m1 = -100; m2 = -100;
    eta1 = -100; eta2 = -100;
    phi1 = -100; phi2 = -100;
    CPtagged1 = false; CPtagged2 = false;
}

// function to replace jet in pair with current jet
// idx_to_fill is a
void Pair_bb::add_jet(Jet jet, int idx_to_fill)
{
    if (idx_to_fill == 1){
        // move current leading to subleading
        if (idx1 != -1){
            Jet old_leading = Jet(idx1, pt1, m1, eta1, phi1, CPtagged1);
            add_jet(old_leading, 2);
        }
        // replace values
        idx1 = jet.idx;
        pt1 = jet.pt;
        m1 = jet.m;
        eta1 = jet.eta;
        phi1 = jet.phi;
        CPtagged1 = jet.btagged;
    }
    else if (idx_to_fill == 2){
        idx2 = jet.idx;
        pt2 = jet.pt;
        m2 = jet.m;
        eta2 = jet.eta;
        phi2 = jet.phi;
        CPtagged2 = jet.btagged;
    }
}

// check if jet should be part of the mbb pair
void Pair_bb::check_jet(Jet jet, bool enable_pseudoB)
{
    // if CP group has tagged, prioritize those
    if (jet.btagged && !CPtagged1) add_jet(jet, 1);
    else if (jet.btagged && !CPtagged2) add_jet(jet, 2);

    // if both are CP-tagged, we're done.
    if(CPtagged1 && CPtagged2) return;

    // Create 'closest to b-tagged' jets
    if (enable_pseudoB){
        if (TMath::Abs(jet.eta) < 2.5 && !jet.btagged){ // central
            if (jet.pt > pt1 && !CPtagged1) {add_jet(jet, 1);}
            else if (jet.pt > pt2 && !CPtagged2) {add_jet(jet, 2);}
        }
    }
    else return;
}

void Pair_bb::set_four_vector()
// Sets four vector - make sure to do this after construction
{
    TLorentzVector jet1, jet2; 
    jet1.SetPtEtaPhiM(pt1, eta1, phi1, m1);
    jet2.SetPtEtaPhiM(pt2, eta2, phi2, m2);
    four_vector = jet1 + jet2;
}

// Single Photon
//--------------------------------------------------------------------------------------
class Photon
{
public:
    Photon(int i, float y_pt, float y_mass, float y_eta, float y_phi);
    int idx;
    float pt;
    float m;
    float eta;
    float phi;
    TLorentzVector four_vector;
};

Photon::Photon(int i, float y_pt, float y_mass, float y_eta, float y_phi)
{
    idx = i;
    pt = y_pt;
    m = y_mass;
    eta = y_eta;
    phi = y_phi;
    four_vector.SetPtEtaPhiM(pt, eta, phi, m);
}

// Photon System
//--------------------------------------------------------------------------------------


class Pair_yy
{

public:
    Pair_yy(Photon y1, Photon y2);
    int idx1, idx2;
    float eta, pt, phi;
    float dEta;
    float dPhi;
    float sum_pt;
    TLorentzVector four_vector;
};

Pair_yy::Pair_yy(Photon y1, Photon y2)
{
    idx1 = y1.idx;
    idx2 = y2.idx;

    TLorentzVector yy_sum = y1.four_vector + y2.four_vector;
    eta = yy_sum.Eta();
    pt = yy_sum.Pt();
    phi = yy_sum.Phi();

    four_vector = yy_sum;
    dEta = TMath::Abs(y1.eta - y2.eta);
    dPhi = TMath::Abs(y1.phi - y2.phi);
    // force 0 < dPhi < 2pi
    while (TMath::Abs(dPhi) > 2 * TMath::Pi()){
      dPhi = dPhi - 2 * TMath::Pi();
    }
    if (dPhi > TMath::Pi()) dPhi = (2 * TMath::Pi()) - dPhi;
}
