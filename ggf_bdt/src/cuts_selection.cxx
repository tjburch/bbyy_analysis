//////////////////////////////////
//   Pass File, apply Selection //
//       Tyler James Burch      //
//////////////////////////////////
#include <TROOT.h>
#include <TCanvas.h>
#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TMath.h>
#include <TH1F.h>
#include <utility>
#include <TLorentzVector.h>
#include <TBranch.h>
#include "jet_classes.cxx"
using namespace std;

float find_deta(float obj1, float obj2);

int main(int argc, char *argv[]) {
	TString input_file(argv[1]);
	TString output_name(argv[2]);
	TString sample_type(argv[3]);


	//Get File, Tree
	TFile *f_input = new TFile(input_file);

	TTree *t_input = (TTree*)f_input->Get("mini");
	Long64_t nentries = t_input->GetEntries();
	// cutflow not used but needs to be propagated to skimmed file
	TH1F *cutflow = (TH1F*)f_input->Get("cutflow_weighted");

	// Variables used for cuts
	int hgam_cutList, yybb_cutList_low, jet_n, photon_n;
	Bool_t hgam_isPassed;
	Float_t m_yy, m_yybb, m_bb_low;
	float jet_m[50], jet_eta[50], jet_phi[50], jet_pt[50];
	Bool_t jet_MV2c10_FixedCutBEff_60[50], jet_MV2c10_FixedCutBEff_70[50], jet_MV2c10_FixedCutBEff_77[50], jet_MV2c10_FixedCutBEff_85[50];
	Double_t jet_MV2c10_discriminant[50];
	float photon_m[4], photon_eta[4], photon_phi[4], photon_pt[4];
	int yybb_low_btagCat;

	t_input->SetBranchAddress("m_yy", &m_yy);
	t_input->SetBranchAddress("m_bb_low", &m_bb_low);
	//t_input->SetBranchAddress("m_yybb", &m_yybb);
	t_input->SetBranchAddress("hgam_isPassed", &hgam_isPassed);
	t_input->SetBranchAddress("hgam_cutList", &hgam_cutList);
	t_input->SetBranchAddress("yybb_cutList_low", &yybb_cutList_low);
	t_input->SetBranchAddress("yybb_low_btagCat", &yybb_low_btagCat);

	// jets
	t_input->SetBranchAddress("jet_n", &jet_n);
	t_input->SetBranchAddress("jet_m", &jet_m);
	t_input->SetBranchAddress("jet_eta", &jet_eta);
	t_input->SetBranchAddress("jet_phi", &jet_phi);
	t_input->SetBranchAddress("jet_pt", &jet_pt);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_60", &jet_MV2c10_FixedCutBEff_60);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_70", &jet_MV2c10_FixedCutBEff_70);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_77", &jet_MV2c10_FixedCutBEff_77);
	t_input->SetBranchAddress("jet_MV2c10_FixedCutBEff_85", &jet_MV2c10_FixedCutBEff_85);
	t_input->SetBranchAddress("jet_MV2c10_discriminant", &jet_MV2c10_discriminant);

	// photons
	t_input->SetBranchAddress("photon_n", &photon_n);
	t_input->SetBranchAddress("photon_m", &photon_m);
	t_input->SetBranchAddress("photon_eta", &photon_eta);
	t_input->SetBranchAddress("photon_phi", &photon_phi);
	t_input->SetBranchAddress("photon_pt", &photon_pt);

	// leptons 
	int muon_n, electron_n;
	t_input->SetBranchAddress("muon_n", &muon_n);
	t_input->SetBranchAddress("electron_n", &electron_n);
	
	// clone tree format, 0 entries
	TFile *f_write = new TFile(output_name, "RECREATE");
	TTree *t_output = t_input->CloneTree(0);
	int j1_idx, j2_idx;
	int b1_idx, b2_idx;
	float j1_eta, j2_eta;
	float y1_y2_dEta, yy_b1_dEta, yy_b2_dEta, b1_b2_dEta;
	float jet1_discriminant, jet2_discriminant;
	int jet1_tagbin, jet2_tagbin;
	float bb_m;
	float cos_bb, cos_yy, cos_yybb;

	float  V_bb_dEta, V_yy_dEta, V_bb_dPhi, V_yy_dPhi;

	// jj system variables
	t_output->Branch("bb_m", &bb_m);
	t_output->Branch("j1_idx", &j1_idx);
	t_output->Branch("j2_idx", &j2_idx);

	t_output->Branch("j1_eta", &j1_eta);
	t_output->Branch("j2_eta", &j2_eta);
	t_output->Branch("b1_idx", &b1_idx);
	t_output->Branch("b2_idx", &b2_idx);

	// extras
	t_output->Branch("yy_b1_dEta", &yy_b1_dEta);
	t_output->Branch("yy_b2_dEta", &yy_b2_dEta);
	t_output->Branch("y1_y2_dEta", &y1_y2_dEta);
	t_output->Branch("b1_b2_dEta", &b1_b2_dEta);

	t_output->Branch("jet1_tagbin", &jet1_tagbin);
	t_output->Branch("jet2_tagbin", &jet2_tagbin);
	t_output->Branch("jet1_discriminant", &jet1_discriminant);
	t_output->Branch("jet2_discriminant", &jet2_discriminant);
	t_output->Branch("cos_yy", &cos_yy);
	t_output->Branch("cos_bb", &cos_bb);
	t_output->Branch("cos_yybb", &cos_yybb);


	// ------------------------------------------------------------------------
	// Loop over entries, skim
	cout << "nEntries Used: " << nentries << endl;


	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;

		//if (jentry % 1000 == 0) cout << "entry number " << jentry << endl;

		// Cuts
		if ((sample_type == "ggf") &&
			(m_yy > 130 ||m_yy < 120)){ continue;} // tight cut for signal region
		if ((sample_type == "sherpa") &&
			(m_yy > 160 ||m_yy < 105)){ continue;} // tight cut for signal region

		// Preselection
		// ------------------------------------------------------------------------
		// ADD ALAN/WISCONSIN PRESELECTION (some overlap with previous)
		bool pass_preselection = true;
		
		// Standard Diphoton
		if (!hgam_isPassed) {pass_preselection = false;}		
		// n{leptons} == 0
		if (muon_n > 0){pass_preselection = false;}
		if (electron_n > 0){pass_preselection = false;}

		// n{jet central} < 6, n{bJet 70} < 3, n{bjet 85} >=2
		int central_jet_count = 0, n_b70_count=0, n_b85_count =0;
		for (int iJet = 0 ; iJet < jet_n ; iJet++ ) {
			if (TMath::Abs(jet_eta[iJet]) < 2.5){central_jet_count++;}
			if (jet_MV2c10_FixedCutBEff_70[iJet]){n_b70_count++;}
			if (jet_MV2c10_FixedCutBEff_85[iJet]){n_b85_count++;}			
		}
		if ( (central_jet_count >= 6) || (n_b70_count >= 3) || (n_b85_count < 2) ) {pass_preselection = false;}
		// myybb_mod >= 350 (need yybb variable, done at bottom)
		//float m_yybb_mod = m_yybb - m_yy - m_bb_low + 250;
		//if (m_yybb_mod < 350){pass_preselection = false;}

		if (!pass_preselection){continue;}


		// Fill other Variables
		// -----------------------------------------------------------------------
		Pair_bb bb_pair;

		// Initalize jets in event, find mbb pair
		for (int iJet = 0 ; iJet < jet_n ; iJet++ ) {
			if (TMath::Abs(jet_eta[iJet]) > 2.5){continue;}
			Jet this_jet = Jet(iJet, jet_pt[iJet], jet_eta[iJet], jet_phi[iJet], jet_m[iJet],jet_MV2c10_FixedCutBEff_85[iJet]);
			bb_pair.check_jet(this_jet, false); // See if jet should be in mbb pair, if so, adds it
		}

		// If we don't have 2 bjets, skip (e.g. not 2 central jets)
		//if (bb_pair.idx1 == -1 || bb_pair.idx2 == -1) continue; // should be checked earlier

		b1_idx = bb_pair.idx1;
		b2_idx = bb_pair.idx2;
		Jet b_jet1 = Jet( b1_idx, jet_pt[b1_idx], jet_m[b1_idx], jet_eta[b1_idx], jet_phi[b1_idx], jet_MV2c10_FixedCutBEff_85[b1_idx]);
		Jet b_jet2 = Jet( b2_idx, jet_pt[b2_idx], jet_m[b2_idx], jet_eta[b2_idx], jet_phi[b2_idx], jet_MV2c10_FixedCutBEff_85[b2_idx]);

		TLorentzVector b_sum = b_jet1.four_vector + b_jet2.four_vector;
		bb_m = b_sum.M(); // will be filled in tree!

		// get Photons (0 and 1 are always higgs yy)
		Photon y1 = Photon(0, photon_pt[0], photon_m[0], photon_eta[0], photon_phi[0]);
		Photon y2 = Photon(1, photon_pt[1], photon_m[1], photon_eta[1], photon_phi[1]);
		Pair_yy yy_pair = Pair_yy(y1, y2);


		// Finish selection here now that objects are created
		TLorentzVector yybb = b_sum + yy_pair.four_vector;
		// myybb_mod >= 350 (need yybb variable, done at bottom)
		float m_yybb_mod = yybb.M() - m_yy - m_bb_low + 250;
		if (m_yybb_mod < 350 || b_jet1.pt < 25 || b_jet2.pt < 25 ){continue;}


		// Get CS variables https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/blob/master/HGamAnalysisFramework/HGamAnalysisFramework/HGamVariables.h
		cos_yy = TMath::Abs(((y1.four_vector.E() + y1.four_vector.Pz()) * (y2.four_vector.E() - y2.four_vector.Pz()) - (y1.four_vector.E() - y1.four_vector.Pz()) * (y2.four_vector.E() + y2.four_vector.Pz()))
                      / ((y1.four_vector + y2.four_vector).M() * sqrt(pow((y1.four_vector + y2.four_vector).M(), 2) + pow((y1.four_vector + y2.four_vector).Pt(), 2))));

		TLorentzVector b1, b2;  // just want the raw 4 vectors of the b-jets
		b1.SetPtEtaPhiM( jet_pt[b1_idx], jet_eta[b1_idx], jet_phi[b1_idx], jet_m[b1_idx]);
		b2.SetPtEtaPhiM( jet_pt[b2_idx], jet_eta[b2_idx], jet_phi[b2_idx], jet_m[b2_idx]);
		cos_bb = TMath::Abs(((b1.E() + b1.Pz()) * (b2.E() - b2.Pz()) - (b1.E() - b1.Pz()) * (b2.E() + b2.Pz()))
                      / ((b1 + b2).M() * sqrt(pow((b1 + b2).M(), 2) + pow((b1 + b2).Pt(), 2))));


		TLorentzVector vH, vZ, vg, vl1, vl2;
		vg = b1 + b2;
		vl1 = y1.four_vector;
		vl2 = y2.four_vector;
		vZ = yy_pair.four_vector;
		vH = vZ + vg;

		TVector3 boost = -vH.BoostVector();
		vH.Boost(boost);
		vZ.Boost(boost);
		vg.Boost(boost);
		vl1.Boost(boost);
		vl2.Boost(boost);

		TLorentzVector q, qbar;
		q.SetPxPyPzE(0, 0, vH.M() / 2, vH.M() / 2);
		qbar.SetPxPyPzE(0, 0, -vH.M() / 2, vH.M() / 2);

		cos_yybb = (q - qbar).Dot(vZ) / (vH.M() * vZ.P());


		// Fill tagbin
		int idx1 = bb_pair.idx1;
		if(jet_MV2c10_FixedCutBEff_60[idx1]){jet1_tagbin = 0;}
		else if(jet_MV2c10_FixedCutBEff_70[idx1]){jet1_tagbin = 1;}
		else if(jet_MV2c10_FixedCutBEff_77[idx1]){jet1_tagbin = 2;}
		else if(jet_MV2c10_FixedCutBEff_85[idx1]){jet1_tagbin = 3;}
		else {jet1_tagbin = 4;}

		int idx2 = bb_pair.idx2;
		if(jet_MV2c10_FixedCutBEff_60[idx2]){jet2_tagbin = 0;}
		else if(jet_MV2c10_FixedCutBEff_70[idx2]){jet2_tagbin = 1;}
		else if(jet_MV2c10_FixedCutBEff_77[idx2]){jet2_tagbin = 2;}
		else if(jet_MV2c10_FixedCutBEff_85[idx2]){jet2_tagbin = 3;}
		else {jet2_tagbin = 4;}

		if (b1_idx == b2_idx) cout << "problem " << endl;
		b1_b2_dEta = TMath::Abs(jet_eta[b1_idx] - jet_eta[b2_idx]);
		jet1_discriminant = jet_MV2c10_discriminant[b1_idx];
		jet2_discriminant = jet_MV2c10_discriminant[b2_idx];

		y1_y2_dEta = find_deta(y1.eta, y2.eta);
		yy_b1_dEta = find_deta(yy_pair.eta, jet_eta[bb_pair.idx1]);
		yy_b2_dEta = find_deta(yy_pair.eta, jet_eta[bb_pair.idx2]);



		t_output->Fill();

	}

	f_write->cd();
	t_output->Write();
	cutflow->Write();

	f_write->Close();

	return 0;
}

float find_deta(float eta1, float eta2){
	return TMath::Abs(eta1 - eta2);
}
