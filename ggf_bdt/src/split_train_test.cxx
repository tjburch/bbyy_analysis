////////////////////////////////////
//  Split into train/test/private //
//       Tyler James Burch        //
////////////////////////////////////
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
#include <iostream>
#include <TH1F.h>
#include <utility>
#include <TBranch.h>
using namespace std;

int main(int argc, char *argv[]) {
	TString input_file(argv[1]);
	TString output_name(argv[2]);


	//Get File, Tree
	TFile *f_input = new TFile(input_file);
	TTree *t_input = (TTree*)f_input->Get("mini");
	Long64_t nentries = t_input->GetEntries();
	// cutflow not used but needs to be propagated to skimmed file
	TH1F *cutflow = (TH1F*)f_input->Get("cutflow_weighted");

    // split on event number
	int eventNumber;
	t_input->SetBranchAddress("eventNumber", &eventNumber);

	// clone tree format, 0 entries
	TFile *f_write = new TFile(output_name, "RECREATE");
	TTree *t_train = t_input->CloneTree(0);
    t_train->SetName("mini_train");
	TTree *t_test = t_input->CloneTree(0);
    t_test->SetName("mini_test");
	// TTree *t_private = t_input->CloneTree(0);  DON'T TOUCH

	// ------------------------------------------------------------------------
	// Loop over entries, skim
	cout << "nEntries Used: " << nentries << endl;
	Long64_t nbytes = 0, nb = 0;
	for (Long64_t jentry = 0; jentry < nentries; jentry++) {
		t_input->GetEntry(jentry);   nbytes += nb;

        // follow guidelines: https://indico.cern.ch/event/753731/contributions/3150550/attachments/1720310/2776982/Proposal.pdf
        if (eventNumber % 4 == 0 || eventNumber % 4 == 1) {t_train->Fill();}
        else if (eventNumber % 4 == 2) {t_test->Fill();}
        //DON'T TOUCH
        //else t_private->Fill();
	}

	f_write->cd();
	t_train->Write();
    t_test->Write();
	cutflow->Write();
	f_write->Close();

	return 0;
}