#/bin/sh

# This script just sets up the proper directory structure from wherever you call it
for DIRECTORY in models data; do
    if [ ! -d $DIRECTORY ]; then 
        mkdir $DIRECTORY
    fi  
done

# Set some environment variables
WORKSPACE=${PWD}