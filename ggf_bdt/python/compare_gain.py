# TODO: manufacture to new workflow
from ROOT import *
from math import *
from AtlasStyle import *
import sys
AtlasStyle()



def make_graph(inputfile):
    #lum = 150/36.1
    # lum = lum * 10 ### SM * 10
    # thes=lum*3.207264e-01
    # theb=lum*7.899255e+01

    # from tyler
    thes = 1.064
    theb = 63.672

    inputfile = TFile(sys.argv[1], "READ")
    h_ggf = inputfile.Get('ggf_hist')
    h_bkg = inputfile.Get('bkg_hist')

    cutlow = -1
    cuthigh = 1
    dcut = 0.04

    ns = h_ggf.Integral()
    nb = h_bkg.Integral()
    print "nb: ", nb
    print "ns: ", ns

    g = TGraph()
    g_events = TGraph()

    i = 0
    thiscut = cuthigh
    canvas = TCanvas()
    best_sig = 0
    best_sig_cut = -1
    # TODO: Change this out with functions in mva functions file
    while (thiscut >= cutlow):
        s_cuth = h_ggf.Clone()
        s_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)
        b_cuth = h_bkg.Clone()
        b_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)

        sintegral = s_cuth.Integral()
        bintegral = b_cuth.Integral()
        #print "bintegral", bintegral
        b = theb * bintegral / nb    
        s = thes * sintegral / ns
        b2 = theb - b
        s2 = thes - s

        significance = 0
        significance2 = 0
        if(b > 1e-6):
            significance = sqrt(2 * ((s + b) * log(1 + s / b) - s))
        if(b2 > 1e-6):
            significance2 = sqrt(2 * ((s2 + b2) * log(1 + s2 / b2) - s2))
        combined = sqrt(significance * significance +
                        significance2 * significance2)

        ###print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f and s = %.3f and b=%.3f"%(thiscut,significance,significance2,combined,s,b))
        print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f" %
            (thiscut, significance, significance2, combined))
        g.SetPoint(i, thiscut, combined)
        try:
            g_events.SetPoint(i, thiscut, s / b)
            print "signal: ", s, " background: ", b, " s/b: ", s/b
        except ZeroDivisionError:
            pass
        if (combined > best_sig):
            best_sig_cut = i
            best_sig = combined
        thiscut = thiscut - dcut
        i = i + 1

    # g.Set(i)

    canvas = TCanvas("c1", "c1", 900, 600)
    canvas.cd()
    g.Draw("AP")
    g.SetMarkerSize(1)
    g.SetMarkerStyle(10)
    g.SetMarkerColor(1)
    g.GetXaxis().SetTitle("BDT cut")
    g.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")
    f_output = TFile('data/sig_gain.root','recreate')

    return g

g1 = make_graph(sys.argv[1])
#g1.SetTitle('Without $|cos*\gamma\gamma bb|$')
g1.SetMarkerSize(1.2)
g1.SetMarkerColor(kRed)


g2 = make_graph(sys.argv[2])
#g2.SetTitle('Including $|cos*\gamma\gamma bb|$')
g2.SetMarkerStyle(2)
g2.SetLineColor(0)

leg = TLegend(0.15,0.65, 0.4,0.9)
leg.AddEntry(g1,'Without |cos*#gamma#gamma bb|','P')
leg.AddEntry(g2,'Including |cos*#gamma#gamma bb|')
leg.SetTextSize(0.045)
leg.SetBorderSize(0)

newcanvas = TCanvas()
g1.Draw('AP')
g2.Draw('P')
leg.Draw()

newcanvas.SaveAs('test.pdf')


