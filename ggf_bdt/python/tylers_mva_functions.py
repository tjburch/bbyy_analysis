from math import sqrt
from ROOT import *
import math


def make_graph(g, name):
    """Quick plot of TGraph
    
    Arguments:
        g {TGraph} -- Graph to plot
        name {string} -- come on. You really can't figure that out?
    """

    canvas = TCanvas("c1", "c1", 900, 600)
    canvas.cd()
    g.Draw("AP")
    g.SetMarkerSize(1)
    g.SetMarkerStyle(10)
    g.SetMarkerColor(1)
    g.GetXaxis().SetTitle("BDT cut")
    g.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")
    canvas.Print("%s.pdf" % name)


def find_significance(s, b):
    return sqrt(2 * ((s + b) * log(1 + s / b) - s))


def find_optimum_cut(h_sig,
                     h_bkg,
                     f_output,
                     g_name,
                     full_signal,
                     full_background,
                     cutlow=-1,
                     cuthigh=1,
                     dcut=0.04):
    """ Cuts on a histrogram until optimum BDT cut is found. 
    Generates and saves graph 
    
    Arguments:
        h_sig {TH1F/D} -- Signal Histogram
        h_bkg {TH1F/D} -- Background Histogram
        g_name {string} -- Name of output graph
        full_signal {float} -- Full no events/integral of signal
        full_background {float} -- Full no events/integral of background

    Keyword Arguments:
        cutlow {int} -- minimum cut value (default: {-1})
        cuthigh {int} -- maximum cut value (default: {1})
        dcut {float} -- cut step size (default: {0.04})
    
    Returns:
        best_sig_cut {float} -- value of best cut significance 
    """

    g = TGraph(g_name)

    i = 0
    nb = h_sig.Integral()
    ns = h_bkg.Integral()
    thiscut = cuthigh
    canvas = TCanvas()
    best_sig = 0
    best_sig_cut = -1
    while (thiscut >= cutlow):
        s_cuth = h_sig.Clone()
        s_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)
        b_cuth = h_sig.Clone()
        b_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)

        sintegral = s_cuth.Integral()
        bintegral = b_cuth.Integral()

        b = full_background * bintegral / nb
        s = full_signal * sintegral / ns
        b2 = full_background - b
        s2 = full_signal - s

        significance = 0
        significance2 = 0
        if (b > 1e-6):
            significance = sqrt(2 * ((s + b) * log(1 + s / b) - s))
        if (b2 > 1e-6):
            significance2 = sqrt(2 * ((s2 + b2) * log(1 + s2 / b2) - s2))
        combined = sqrt(significance * significance +
                        significance2 * significance2)

        print(
            "cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f"
            % (thiscut, significance, significance2, combined))
        g.SetPoint(i, thiscut, combined)
        try:
            g_events.SetPoint(i, thiscut, s / b)
            print "signal: ", s, " background: ", b, " s/b: ", s / b
        except ZeroDivisionError:
            pass
        if (combined > best_sig):
            best_sig_cut = i
            best_sig = combined
        thiscut = thiscut - dcut
        i = i + 1

    f_output.Write(g)
    return best_sig_cut, best_sig