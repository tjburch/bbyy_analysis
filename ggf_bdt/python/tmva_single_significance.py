# TODO: manufacture to new workflow
from ROOT import *
from math import *
from AtlasStyle import *
import sys
AtlasStyle()
#lum = 150/36.1
# lum = lum * 10 ### SM * 10
# thes=lum*3.207264e-01
# theb=lum*7.899255e+01

# from tyler
thes = 1.064
theb = 63.672

# These don't seem right 
thes = 0.526
theb = 0.925

file = TFile(sys.argv[1], "READ")
h_ggf = file.Get('h_ggf_mc16a')
h_bkg = file.Get('h_sherpa_mc16a')
# add mc16d
h_ggf.Add(file.Get('h_ggf_mc16d'))
h_bkg.Add(file.Get('h_sherpa_mc16d'))

cutlow = -1
cuthigh = 1
dcut = 0.04

ns = h_ggf.Integral()
nb = h_bkg.Integral()
print "nb: ", nb
print "ns: ", ns

g = TGraph()
g_events = TGraph()

i = 0
thiscut = cuthigh
canvas = TCanvas()
best_sig = 0
best_sig_cut = -1
# TODO: Change this out with functions in mva functions file
while (thiscut >= cutlow):
    s_cuth = h_ggf.Clone()
    s_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)
    b_cuth = h_bkg.Clone()
    b_cuth.GetXaxis().SetRangeUser(cutlow, thiscut)

    sintegral = s_cuth.Integral()
    bintegral = b_cuth.Integral()
    #print "bintegral", bintegral
     
    b = theb * bintegral / nb    
    s = thes * sintegral / ns
    b2 = theb - b
    s2 = thes - s
    """
    b = bintegral
    s = sintegral
    b2 = nb - b
    s2 = ns - s
    """
    significance = 0
    significance2 = 0
    if(b > 1e-6 ):
        significance = sqrt(2 * ((s + b) * log(1 + s / b) - s))
    if(b2 > 1e-6):
        significance2 = sqrt(2 * ((s2 + b2) * log(1 + s2 / b2) - s2))
    combined = sqrt(significance * significance +
                    significance2 * significance2)

    ###print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f and s = %.3f and b=%.3f"%(thiscut,significance,significance2,combined,s,b))
    print("JAAA cut = %.3f and significance = %.3f significance2 = %.3f and combined = %.3f" %
          (thiscut, significance, significance2, combined))
    g.SetPoint(i, thiscut, combined)
    try:
        g_events.SetPoint(i, thiscut, s / b)
        print "signal: ", s, " background: ", b, " s/b: ", s/b
    except ZeroDivisionError:
        pass
    if (combined > best_sig):
        best_sig_cut = i
        best_sig = combined
    thiscut = thiscut - dcut
    i = i + 1

# g.Set(i)

canvas = TCanvas("c1", "c1", 900, 600)
canvas.cd()
g.Draw("AP")
g.SetMarkerSize(1)
g.SetMarkerStyle(10)
g.SetMarkerColor(1)
g.GetXaxis().SetTitle("BDT cut")
g.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")
f_output = TFile('data/sig_gain.root','recreate')
g.SetName("sig_gain")
g.Write()

canvas.Print("plots/significance.pdf")
####################################################

# canvas.Clear()
# gStyle.SetOptStat(0000000)
# hists = [h_ggf, h_bkg]
# norm_val = [1.0 / h.Integral() for h in hists]

# hists[0].Scale(norm_val[0])
# hists[1].Scale(norm_val[1])
# hists[2].Scale(norm_val[2])
# maxval = max([h.GetMaximum() for h in hists])


# hists[0].SetLineColor(2)
# hists[0].GetXaxis().SetRangeUser(-0.4, 0.4)
# hists[0].GetXaxis().SetTitle('BDT Response')
# hists[0].GetYaxis().SetTitle('AU (normalized to 1)')
# hists[0].GetYaxis().SetTitleOffset(1.1)
# hists[0].SetMaximum(maxval * 1.2)
# hists[0].Draw('hist')

# hists[1].SetLineColor(4)
# hists[1].Draw('hist sames')

# hists[2].SetLineColor(6)
# hists[2].Draw('hist sames')

# leg = TLegend(.14, .7, .3, .9)
# leg.AddEntry(hists[0], 'ggF', 'l')
# leg.AddEntry(hists[1], 'Sherpa', 'l')
# leg.SetBorderSize(0)
# leg.Draw()

# canvas.Print('plots/bdt_response.pdf')


# ####################################################
# canvas.Clear()
# g_events.Draw("AP")
# g_events.SetMarkerSize(1)
# g_events.SetMarkerStyle(10)
# g_events.SetMarkerColor(1)
# g_events.Draw()

# canvas.Print('TEMP.pdf')
