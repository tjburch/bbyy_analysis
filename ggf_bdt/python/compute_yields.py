# Largely taken from Alan Taylor! Thanks!

import ROOT
from ROOT import *
import sys

from array import array
ROOT.gROOT.SetBatch(ROOT.kTRUE) # turn off visual plotting, hopefully
mc16a_file_sig = TFile("/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/ggf_selection_mc16a.root")
mc16d_file_sig = TFile("/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/ggf_selection_mc16d.root")

mc16a_file_bkg = TFile("/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/sherpa_selection_mc16a.root")
mc16d_file_bkg = TFile("/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/sherpa_selection_mc16d.root")


sum_weights_mc16a_sig = mc16a_file_sig.Get("cutflow_weighted").GetBinContent(3)
sum_weights_mc16d_sig = mc16d_file_sig.Get("cutflow_weighted").GetBinContent(3)
sum_weights_mc16a_bkg = mc16a_file_bkg.Get("cutflow_weighted").GetBinContent(3)
sum_weights_mc16d_bkg = mc16d_file_bkg.Get("cutflow_weighted").GetBinContent(3)
sum_weights = []

sigtrees = [mc16a_file_sig.Get("mini"), mc16d_file_sig.Get("mini") ]
bkgtrees = [mc16a_file_bkg.Get("mini"), mc16d_file_bkg.Get("mini") ]

#myy_var = "m_yy*0.001"
#myy_var = "HGamEventInfoAuxDyn.m_yy*0.001"
myy_var = "m_yy"


### 2-tag high mass selection
sig_selection = '(hgam_isPassed && yybb_low_btagCat == 2 && yybb_cutList_low > 3 && m_yy >= 123.0 && m_yy <= 127.0   ) * crossSectionBRfilterEff * hgam_weight'

bkg_selection = '(hgam_isPassed && yybb_low_btagCat == 2 && yybb_cutList_low > 3  ) * crossSectionBRfilterEff * hgam_weight'

nbins_myy = 55
lbins_myy = 105
ubins_myy = 160


## define m_yy histograms for mc16a and mc16d
h_myy_mc16a_sig = TH1F("h_myy_mc16a_sig", "h_myy_mc16a_sig", nbins_myy, lbins_myy, ubins_myy)
h_myy_mc16d_sig = TH1F("h_myy_mc16d_sig", "h_myy_mc16d_sig", nbins_myy, lbins_myy, ubins_myy)
h_myy_mc16a_bkg = TH1F("h_myy_mc16a_bkg", "h_myy_mc16a_bkg", nbins_myy, lbins_myy, ubins_myy)
h_myy_mc16d_bkg = TH1F("h_myy_mc16d_bkg", "h_myy_mc16d_bkg", nbins_myy, lbins_myy, ubins_myy)
sighist = [h_myy_mc16a_sig, h_myy_mc16d_sig]
bkghist = [h_myy_mc16a_bkg, h_myy_mc16d_bkg]

for tree, hist in zip(sigtrees, sighist):
    tree.Draw("%s >> %s" % (myy_var, hist.GetName()), "%s" %bkg_selection)
for tree, hist in zip(bkgtrees, bkghist):
    tree.Draw("%s >> %s" % (myy_var, hist.GetName()), "%s" %bkg_selection)


mc16a_lumi = 36100
mc16d_lumi = 43700

## scale histogram by factor of lumi / event weights
def scale(hlist):
    for hist in hlist:
        if 'mc16a' in hist.GetName():
            hist.Scale(float(mc16a_lumi) / sum_weights_mc16a)
        elif 'mc16d' in hist.GetName():
            hist.Scale( float(mc16d_lumi) / sum_weights_mc16d )
        else: print "bug!"

# This is hacky trash. Really could do a better job with data structures.
sighist[0].Scale(float(mc16a_lumi) / sum_weights_mc16a_sig)
sighist[1].Scale(float(mc16d_lumi) / sum_weights_mc16d_sig)
bkghist[0].Scale(float(mc16a_lumi) / sum_weights_mc16a_bkg)
bkghist[1].Scale(float(mc16d_lumi) / sum_weights_mc16d_bkg)

def print_total(hlist):
    for h in hlist:
        if 'mc16a' in h.GetName(): print ' mc16a total yield :: ' , round(h.Integral(),3)
        elif 'mc16d' in h.GetName(): print ' mc16d total yield :: ' , round(h.Integral(),3)           
        else: print "bug!"
    print '   sum total yield :: ', round(sum([h.Integral() for h in hlist]),3)


print "Signal:"
print_total(sighist) # these will be the same because of sig_selection applied above.
print "Background:"
print_total(bkghist)
print "-------------------"

def print_sr(hlist):
    for h in hlist:
        binA = h.FindBin(123.0)
        binB = h.FindBin(127.0)    
        if 'mc16a' in h.GetName(): print ' mc16a SR yield :: ' , round(h.Integral(binA,binB),3)
        elif 'mc16d' in h.GetName(): print ' mc16d SR yield :: ' , round(h.Integral(binA,binB),3)            
        else: print "bug!"
    print '   sum SR yield :: ', round(sum([h.Integral(binA,binB) for h in hlist]),3)


print "Signal:"
print_sr(sighist)
print "Background:"
print_sr(bkghist)

