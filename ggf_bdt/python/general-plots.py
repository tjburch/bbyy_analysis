from rootpy.tree import Tree
from rootpy.io import root_open
import matplotlib.pyplot as plt
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator
#import seaborn as sns
# set the style
style = get_style('ATLAS')

direct = '/Users/tburch/Documents/gitDevelopment/bbyy_analysis/ggf_bdt/data/'
f_ggf = root_open(direct+'/ggf_histograms.root')
f_sherpa = root_open(direct+'/sherpa_histograms.root')


# Tagbin plots
# -------------------------------------------------------------------------------------
j1_ggf = f_ggf.selected_jet1_tagbin.Clone(title='ggF HH')
j1_ggf.Scale(1/j1_ggf.Integral())
j1_ggf.linecolor = 'firebrick'
j1_sherpa = f_sherpa.selected_jet1_tagbin.Clone(
    title='Sherpa $\gamma\gamma$ + jets')
j1_sherpa.Scale(1/j1_sherpa.Integral())
j1_sherpa.linecolor = 'cornflowerblue'

set_style('ATLAS', mpl=True)
fig = plt.figure(figsize=(7, 5), dpi=100)
axes = plt.axes()
# axes.xaxis.set_minor_locator(AutoMinorLocator())
axes.yaxis.set_minor_locator(AutoMinorLocator())
h = rplt.errorbar(j1_ggf)
rplt.errorbar(j1_sherpa)
plt.xlabel('j1 tagbin', position=(1, 0), va='bottom', ha='right', labelpad=20)
plt.ylabel('AU', position=(0, 1), va='top', ha='right', labelpad=20)
plt.ylim((0, 1))


names = ['  60% WP', '  70% WP', '  77% WP', '  85% WP']
axes.set_xticklabels(names, ha='left', fontsize=12)

leg = plt.legend()
plt.savefig('plots/tagbin1.pdf')

##
j2_ggf = f_ggf.selected_jet2_tagbin.Clone(title='ggF HH')
j2_ggf.Scale(1/j2_ggf.Integral())
j2_ggf.linecolor = 'firebrick'
j2_sherpa = f_sherpa.selected_jet1_tagbin.Clone(
    title='Sherpa $\gamma\gamma$ + jets')
j2_sherpa.Scale(1/j2_sherpa.Integral())
j2_sherpa.linecolor = 'cornflowerblue'

set_style('ATLAS', mpl=True)
fig = plt.figure(figsize=(7, 5), dpi=100)
axes = plt.axes()
axes.yaxis.set_minor_locator(AutoMinorLocator())
h = rplt.errorbar(j2_ggf)
rplt.errorbar(j2_sherpa)
plt.xlabel('j2 tagbin', position=(1, 0), va='bottom', ha='right', labelpad=20)
plt.ylabel('AU', position=(0, 1), va='top', ha='right', labelpad=20)
plt.ylim((0, 1))


#names = ['  60% WP', '  70% WP', '  77% WP', '  85% WP']
names = ['','60% WP         ', '70% WP       ', '77% WP       ', '85% WP       ']
axes.set_xticklabels(names, ha='right', fontsize=12)

leg = plt.legend()
plt.savefig('plots/tagbin2.pdf')



# Generic plots
# -------------------------------------------------------------------------------------
set_style('ATLAS', mpl=True)


def plot_variable(h_ggf, h_sherpa, x_title):

    h_ggf.Scale(1/h_ggf.Integral())
    h_ggf.linecolor = 'firebrick'
    h_sherpa.Scale(1/h_sherpa.Integral())
    h_sherpa.linecolor = 'cornflowerblue'
    fig = plt.figure(figsize=(7, 5), dpi=100)
    axes = plt.axes()
    axes.xaxis.set_minor_locator(AutoMinorLocator())
    axes.yaxis.set_minor_locator(AutoMinorLocator())
    h = rplt.errorbar(h_ggf)
    rplt.errorbar(h_sherpa)
    plt.xlabel(x_title, position=(1, 0), va='bottom', ha='right', labelpad=20)
    plt.ylabel('AU', position=(0, 1), va='top', ha='right', labelpad=20)
    plt.ylim(ymin=0)
    leg = plt.legend()


# jj_m
h_jj_m_ggf = f_ggf.jj_m.Clone(title='ggF HH')
h_jj_m_sherpa = f_sherpa.jj_m.Clone(title='Sherpa $\gamma\gamma$ + jets')

plot_variable(h_jj_m_ggf, h_jj_m_sherpa, '$m_{jj}$')
plt.xlim((90, 140))
plt.savefig('plots/jj_m.pdf')


# j1_j1_deta
h_jj_dEta_ggf = f_ggf.jj_dEta.Clone(title='ggF HH')
h_jj_dEta_sherpa = f_sherpa.jj_dEta.Clone(title='Sherpa $\gamma\gamma$ + jets')

plot_variable(h_jj_dEta_ggf, h_jj_dEta_sherpa, '$\Delta \eta_{jj}$')
plt.xlim((0, 3))
plt.savefig('plots/jj_deta.pdf')

# y1_y2_deta
h_y1_y2_dEta_ggf = f_ggf.y1_y2_dEta.Clone(title='ggF HH')
h_y1_y2_dEta_sherpa = f_sherpa.y1_y2_dEta.Clone(title='Sherpa $\gamma\gamma$ + jets')

plot_variable(h_y1_y2_dEta_ggf, h_y1_y2_dEta_sherpa, '$\Delta \eta_{\gamma\gamma}$')
plt.xlim((0, 3))
plt.savefig('plots/y1_y2_deta.pdf')

# yy_j1_deta
h_yy_j1_dEta_ggf = f_ggf.yy_j1_dEta.Clone(title='ggF HH')
h_yy_j1_dEta_sherpa = f_sherpa.yy_j1_dEta.Clone(title='Sherpa $\gamma\gamma$ + jets')

plot_variable(h_yy_j1_dEta_ggf, h_yy_j1_dEta_sherpa, '$\Delta \eta_{\gamma\gamma, j_{2}}$')
plt.xlim((0, 6))
plt.savefig('plots/yy_j1_deta.pdf')

# yy_j2_deta
h_yy_j2_dEta_ggf = f_ggf.yy_j2_dEta.Clone(title='ggF HH')
h_yy_j2_dEta_sherpa = f_sherpa.yy_j2_dEta.Clone(title='Sherpa $\gamma\gamma$ + jets')

plot_variable(h_yy_j2_dEta_ggf, h_yy_j2_dEta_sherpa, '$\Delta \eta_{\gamma\gamma, j_{2}}$')
plt.xlim((0, 6))
plt.savefig('plots/yy_j2_deta.pdf')

## CS frame variables
h_cos_yy_ggf = f_ggf.cos_yy.Clone(title='ggF HH')
h_cos_yy_sherpa = f_sherpa.cos_yy.Clone(title='Sherpa $\gamma\gamma$ + jets')
plot_variable(h_cos_yy_ggf, h_cos_yy_sherpa, '$|cos*\gamma\gamma|$')
plt.savefig('plots/cos_yy.pdf')


h_cos_bb_ggf = f_ggf.cos_bb.Clone(title='ggF HH')
h_cos_bb_sherpa = f_sherpa.cos_bb.Clone(title='Sherpa $\gamma\gamma$ + jets')
plot_variable(h_cos_bb_ggf, h_cos_bb_sherpa, '$|cos*bb|$')
plt.savefig('plots/cos_bb.pdf')


h_cos_yybb_ggf = f_ggf.cos_yybb.Clone(title='ggF HH')
h_cos_yybb_sherpa = f_sherpa.cos_yybb.Clone(title='Sherpa $\gamma\gamma$ + jets')
plot_variable(h_cos_yybb_ggf, h_cos_yybb_sherpa, '$|cos*yybb|$')
plt.savefig('plots/cos_yybb.pdf')
