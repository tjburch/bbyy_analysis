def weight_reference(sample_type):
    if sample_type == "sherpa":
        cross_section_br = 41.504
        generator_efficiency = 0.49904
        sum_of_weights = 1.0527E8
    elif sample_type =="sherpa21_mc16a":
        cross_section =   51.823 * 1000
        sum_of_weights = 71507816
    elif sample_type =="sherpa21_mc16d":
        cross_section = 51.823 * 1000
        sum_of_weights = 88440776        

    elif sample_type == "vbf":
        cross_section = 1.62 / 1000
        BR_Hbb = 0.5809
        BR_Hyy = 2.270E-03
        cross_section_br = cross_section * BR_Hbb * BR_Hyy * 2
        generator_efficiency = 0.49354
        sum_of_weights = -1
    elif sample_type == "ggf":
        cross_section_br = 8.8111E-05
        generator_efficiency = 1.
        sum_of_weights = -1
    else:
        cross_section_br = 1.
        generator_efficiency = 1.
        sum_of_weights = 1.
    weight_dict = {'cross_section_br': cross_section_br,
                   'generator_efficiency': generator_efficiency,
                   'sum_of_weights': sum_of_weights}
    return weight_dict


def set_weight(lumi, xBR, genEff, skimEff, sumW):
    return (lumi * xBR * genEff * skimEff / sumW)


def get_skim_eff(infile):
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency
