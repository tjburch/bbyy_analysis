import sys
from rootpy.io import root_open
from rootpy.plotting.style import get_style, set_style
import rootpy.plotting.root2matplotlib as rplt
from rootpy.plotting import graph
import matplotlib.pyplot as plt
import ROOT

style = get_style('ATLAS',mpl=True)
doROC = True
doSigGain = False


# import two tmva files
f1 = root_open(sys.argv[1])
f2 = root_open(sys.argv[2])


if doROC:
    roc1 = f1.Get('dataset').Get('Method_BDT').Get('BDT').Get('MVA_BDT_trainingRejBvsS')
    roc2 = f2.Get('dataset').Get('Method_BDT').Get('BDT').Get('MVA_BDT_trainingRejBvsS')

    roc1.linecolor = 'firebrick'
    roc2.linecolor = 'cornflowerblue'

    roc1.SetTitle('Without $|cos*\gamma\gamma bb|$')
    roc2.SetTitle('Including $|cos*\gamma\gamma bb|$')

    fig = plt.figure(figsize=(7, 5), dpi=100)
    ax = plt.axes()
    rplt.step(roc1, axes=ax )
    rplt.step(roc2, axes=ax )

    plt.xlabel('$\epsilon_{signal}$', position=(1, 0), va='bottom', ha='right', labelpad=20)
    plt.ylabel('Background Rejection', position=(0, 1), va='top', ha='right', labelpad=20)

    plt.xlim((0, 1))
    plt.ylim((0, 1))
    ax.annotate('%.2f AUC' % roc1.Integral(), xy=(.02,.8), xycoords='axes fraction', color='firebrick')
    ax.annotate('%.2f AUC' % roc2.Integral(), xy=(.02,.76), xycoords='axes fraction', color='cornflowerblue')


    leg = plt.legend()
    plt.savefig('plots/roc_curves.pdf')

if doSigGain:
    f1 = ROOT.TFile(sys.argv[1])
    f2 = ROOT.TFile(sys.argv[2])
    g1 = f1.Get('sig_gain')
    g2 = f2.Get('sig_gain')
    mycanvas = ROOT.TCanvas()

    #g1.linecolor = 'firebrick'
    #g2.linecolor = 'cornflowerblue'

    #g1.SetTitle('Without $|cos*\gamma\gamma bb|$')
    #g2.SetTitle('Including $|cos*\gamma\gamma bb|$')

    #fig = plt.figure(figsize=(7, 5), dpi=100)
    #ax = plt.axes()
    #rplt.errorbar(g1)
    g1.Draw('APL')
    #g2.Draw('sames')

    #plt.xlabel('BDT Cut', position=(1, 0), va='bottom', ha='right', labelpad=20)
    #plt.ylabel('Significance (at 150 fb-1)', position=(0, 1), va='top', ha='right', labelpad=20)
    #plt.show()
    #plt.waitforbuttonpress()
    #leg = plt.legend()
    #plt.savefig('plots/gain_overlay.pdf')
    mycanvas.SaveAs('test.pdf')
