"""Function to get yields. If called as main, we'll have it dump to screen for argv[1], type argv[2]
"""
import ROOT 
import sys

infile = ROOT.TFile(sys.argv[1])
t_input = infile.Get('mini')
nEntries = t_input.GetEntries()
print "Entries to use: ", nEntries

# Get MC weight information
h_cutflow = infile.Get("cutflow_weighted")

### Figure out what we're putting in
# -----------------------------------------------------------------
if 'ggf' in sys.argv[2]:
    cross_section_br = 8.8111E-05
elif 'sherpa' in sys.argv[2]:
    cross_section_br = 51.823/1000
else:
    print 'Something is wrong.'


if sys.argv[2] == 'sherpa_mc16a':
    sumW=71507816
elif sys.argv[2]  == 'sherpa_mc16d':   
    sumW=88440776
else:
    sumW=1


# Define some crap
# -----------------------------------------------------------------
def set_weight(lumi, xBR, genEff, skimEff, sumW):
    return (lumi * xBR * genEff * skimEff / sumW)


def get_skim_eff(infile):
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    print infile

    return sum_of_weights, skim_efficiency


genEff =1 
skimEff = get_skim_eff(infile)[1]

luminosity = 150000
print "luminosity: ", luminosity / 1000.0
print "BR * xsec: ", cross_section_br
print "Sum of Weights: ", sumW
print "skim_efficiency: ", skimEff
print "genEff: ", genEff

h = ROOT.TH1F('njets','njets',20, 0, 20)

# Loop over entries
# -----------------------------------------------------------------
for event in range(t_input.GetEntries()):

    t_input.GetEntry(event)

    w = (t_input.hgam_weight *
            t_input.yybb_low_weight *
            luminosity *
            cross_section_br *
            genEff *
            skimEff /
            sumW)

    h.Fill(t_input.jet_n, w)

# Print final yield
print "integral after weighting: ", h.Integral()