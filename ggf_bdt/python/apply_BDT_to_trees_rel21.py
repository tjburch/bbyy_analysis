import sys
import os
import array
from weight_reference import *
import ROOT
from collections import namedtuple
if len(sys.argv) < 2:
    print "usage: arg1 = weights file ; arg2 = outfile name"
    sys.exit(1)
luminosity = 150000

workdir = os.getcwd().split('ggf_bdt')[0]+'/ggf_bdt/'

class Sample:
    """ Class outlining input sample and parameters """


    def __init__(self, filename):
        # Set things from Filename
        if 'mc16a' in filename: 
            self.Period = 'mc16a'
            self.Luminosity = 36100.
        if 'mc16d' in filename: 
            self.Period = 'mc16d'
            self.Luminosity = 43700.
        if 'ggf' in filename: self.SampleType = 'ggf'
        if 'sherpa' in filename: self.SampleType = 'sherpa'
                    
        self.NameShort = '{0}_{1}'.format(self.SampleType, self.Period)
        self.File = ROOT.TFile(filename)
        self.Tree = self.File.Get('mini_test')
        self.Cutflow = self.File.Get('cutflow_weighted')
        self.SumWeight = self.Cutflow.GetBinContent(3)

files = [
    '/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/ggf_mc16a_trainTest.root',
    '/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/ggf_mc16d_trainTest.root',
    '/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/sherpa_mc16a_trainTest.root',
    '/afs/cern.ch/work/t/tburch/public/workspace/bbyy_analysis/ggf_bdt/data/sherpa_mc16d_trainTest.root'
]

all_samples = []
for f in files:
    all_samples.append(Sample(f))

outfile = ROOT.TFile.Open('data/'+sys.argv[2], 'recreate')

# Pass file with model (BDT)
model = sys.argv[1]

# Build TMVA Reader with all the inputs 
reader = ROOT.TMVA.Reader()

bb_m = array.array('f', [0])
reader.AddVariable('bb_m', bb_m)
y1_y2_dEta = array.array('f', [0])
reader.AddVariable('y1_y2_dEta', y1_y2_dEta)
b1_b2_dEta = array.array('f', [0])
reader.AddVariable('b1_b2_dEta', b1_b2_dEta)
yy_j1_dEta = array.array('f', [0])
reader.AddVariable('yy_b1_dEta', yy_j1_dEta)
yy_j2_dEta = array.array('f', [0])
reader.AddVariable('yy_b2_dEta', yy_j2_dEta)
jet1_tagbin = array.array('f', [0])
reader.AddVariable('jet1_tagbin', jet1_tagbin)
jet2_tagbin = array.array('f', [0])
reader.AddVariable('jet2_tagbin', jet2_tagbin)
jet_n = array.array('f', [0])
reader.AddSpectator('jet_n', jet_n)

reader.BookMVA("BDT::BDT", model)


# Create the output objects
class Output:
    """ Class outlining output sample and objects """

    def __init__(self, input):
        """
        Arguments:
            input {Sample} -- input sample defined above
        """
        n = input.NameShort
        self._input = input
        self.Eval = array.array('f', [0])
        self.Hist = ROOT.TH1F('h_'+n, n, 50, -1, 1)
        self.Tree =  ROOT.TTree('t_'+n, 't_'+n)
        self.Tree.Branch(n+'_eval', self.Eval, n+'_eval/F')

    def Finalize(self):
        scale_lumi = 150000
        self.Hist.Scale(self._input.Luminosity / 150000)

Pairing = namedtuple('Pairing','input output')
inout_pairs = []
for iSample in all_samples:
    inout_pairs.append( Pairing(input=iSample, output=Output(iSample) ))

def eventloop(input, output):
    """Loops over events and evaluates the BDT for it
    """

    print "Starting on: ", input.NameShort
    nEntries = input.Tree.GetEntries()
    for ientry in range(nEntries):
        input.Tree.GetEntry(ientry)
        w = input.Tree.crossSectionBRfilterEff * input.Tree.hgam_weight

        # Get stuff from inputs
        bb_m[0] = input.Tree.bb_m
        y1_y2_dEta[0] = input.Tree.y1_y2_dEta
        b1_b2_dEta[0] = input.Tree.b1_b2_dEta
        yy_j1_dEta[0] = input.Tree.yy_b1_dEta
        yy_j2_dEta[0] = input.Tree.yy_b2_dEta
        jet1_tagbin[0] = input.Tree.jet1_tagbin
        jet2_tagbin[0] = input.Tree.jet2_tagbin
        jet_n[0] = input.Tree.jet_n

        # Write to outputs
        mva_test = reader.EvaluateMVA('BDT::BDT')
        output.Eval[0] = mva_test
        output.Hist.Fill(mva_test, w)
        output.Tree.Fill()

for sample_pair in inout_pairs:
    eventloop(sample_pair.input, sample_pair.output)
    sample_pair.output.Finalize()
    sample_pair.output.Hist.Write()
    sample_pair.output.Tree.Write()

outfile.Close()
