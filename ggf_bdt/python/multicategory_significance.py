from ROOT import *
from math import *
from AtlasStyle import *
from tylers_mva_functions import *

AtlasStyle()

# from tyler
thes = 1.064
theb = 63.672

f_input = TFile('data/evaluated_histograms.root', "READ")
h_ggf = f_input.Get('ggf_hist')
h_sherpa = f_input.Get('bkg_hist')

f_output = TFile('data/output.root', 'RECREATE')

# Get inital significance
sig0 = find_significance(thes, theb)

# Apply optimization 1
cut1, sig1 = find_optimum_cut(
    h_sig=h_ggf,
    h_bkg=h_sherpa,
    f_output=f_output,
    g_name='cut1',
    full_signal=thes,
    full_background=theb)

# Cut histograms on significance 1
h_cut1_ggf = h_ggf.clone()
h_cut1_ggf.GetXaxis().SetRange(h_cut1_ggf.GetMinimum(), cut1)
h_cut1_sherpa = h_sherpa.clone()
h_cut1_sherpa.GetXaxis().SetRange(h_cut1_sherpa.GetMinimum(), cut1)

# Apply optimization 2
cut2, sig2 = find_optimum_cut(
    h_sig=h_cut1_ggf,
    h_bkg=h_cut1_sherpa,
    f_output=f_output,
    g_name='cut2',
    full_signal=thes,
    full_background=theb)

print "Inital significance: ", sig0
print "Best BDT cut 1: ", cut1, " yielding significance: ", sig1
print "Best BDT cut 2: ", cut2, " yielding significance: ", sig2


# Plot each of those 
canvas = TCanvas("c1", "c1", 900, 600)
canvas.cd()
g1 = f_output.Get('cut1')
g2 = f_output.Get('cut2')
g1.SetMarkerColor(kRed)
g1.Draw("AP")
g2.SetMarkerColor(kBlue)
g2.Draw("AP")

for g in [g1, g2]:
    g.SetMarkerSize(1)
    g.SetMarkerStyle(10)

g1.GetXaxis().SetTitle("BDT cut")
g1.GetYaxis().SetTitle("Combined Asimov Significance (150 fb-1)")
canvas.Print("plots/significance.pdf")
