SIGNAL=${PWD}/data/ggf_selection.root
BACKGROUND=${PWD}/data/sherpa_selection.root

# # run BDT
python ${DIRECTORY}/learning/TMVA.py

# Apply BDT to Histograms
python ${DIRECTORY}/python/make_evaluated_histograms.py

# # Find maximum significance 1 and 2
python ${DIRECTORY}/python/multicategory_significance.py