# TODO manufacture to new workflow 
import ROOT
import shutil
import os

# Initalize files, TMVA objects
workdir = os.environ["WORKSPACE"]
writedir = workdir+'/models/'
output = writedir+'tmva_output.root'
outputFile = ROOT.TFile.Open(output, 'RECREATE')
ROOT.TMVA.UseOffsetMethod = True

# Create Factory Object
factoryOptions = '!V:!Silent:Transformations=I;D;P;G,D:Correlations'
factory = ROOT.TMVA.Factory('BoostedDecisionTree', outputFile, factoryOptions)

# Create Dataloader
dataloader = ROOT.TMVA.DataLoader('dataset')
dataloader.AddVariable('jj_m', 'F')
dataloader.AddVariable('bb_m', 'F')
dataloader.AddVariable('jj_dEta', 'F')
dataloader.AddVariable('y1_y2_dEta', 'F')
dataloader.AddVariable('b1_b2_dEta', 'F')
dataloader.AddVariable('yy_j1_dEta', 'F')
dataloader.AddVariable('yy_j2_dEta', 'F')
dataloader.AddVariable('jet1_tagbin', 'I')
dataloader.AddVariable('jet2_tagbin', 'I')
dataloader.AddSpectator('jet_n', 'I')

# Get all trees
data_dir = workdir+'/data/'
ggfFile = ROOT.TFile(data_dir+'ggf_selection.root')
ggfTree = ggfFile.Get('mini')
backgroundFile = ROOT.TFile(data_dir+'sherpa_selection.root')
backgroundTree = backgroundFile.Get('mini')


# Set Weights
# ---------------------------------------------------------------
# Get weights - eventwise
# weight expression - (L)(xBR)(genEff)(skimEff)/ (sum_of_weights)
# Needs multiplied by yybb and hgam weights
luminosity = 150000


def set_weight(lumi, xBR, genEff, skimEff, sumW):
    return (lumi * xBR * genEff * skimEff / sumW)


def get_skim_eff(infile):
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    return sum_of_weights, skim_efficiency


# ggf
cross_section_br = 8.8111E-05
ggfWeight = set_weight(luminosity, cross_section_br,  1, get_skim_eff(
    ggfFile)[1], 1)


# Sherpa
scale_factor = 63.672 / 17804.823  # for 0 tag events
sherpaWeight = set_weight(luminosity, 41.504, 0.49904, get_skim_eff(ggfFile)[
                          1], 1.0527E8) * scale_factor


dataloader.SetWeightExpression('yybb_low_weight*hgam_weight')

# Prep Dataset
# ------------------------------------------------------------------

#  Load in and prepare trees
dataloader.AddSignalTree(ggfTree, ggfWeight)
dataloader.AddBackgroundTree(backgroundTree, sherpaWeight)

preselection_cut = ROOT.TCut('jj_m > 0 && jj_dEta > 0')
split_options = 'nTrain_Signal=0:'  # all signal
split_options += 'nTrain_Background=0:'  # all background
split_options += 'SplitMode=Random:'
split_options += 'NormMode=NumEvents:'  # Normalization of weights (?)
split_options += '!V'  # Verbosity level

dataloader.PrepareTrainingAndTestTree(preselection_cut, split_options)

# MVA Settings
# ------------------------------------------------------------------

# BDT (adaboost)
bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
bdt_opts += 'NTrees=800'
bdt = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT', bdt_opts)

# Rectangular Cuts
#rect = factory.BookMethod(dataloader, ROOT.TMVA.Types.kCuts, 'rect_cuts', '')


# Train, Test, Evaluate all methods
# ------------------------------------------------------------------
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Clean up

outputFile.Close()
writedir = workdir+'/models/'
os.rename('dataset', 'bdt1')
shutil.move('bdt1', writedir)