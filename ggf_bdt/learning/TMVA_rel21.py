import ROOT
from shutil import move
import os
from datetime import datetime

# Initalize files, TMVA objects
time = str(datetime.now()).split('.')[0].replace(':','').replace(' ','_')

writedir = os.getcwd().split('ggf_bdt')[0]+'/ggf_bdt/models/'
output = writedir+'tmva_output_{0}.root'.format(time)
outputFile = ROOT.TFile.Open(output, 'RECREATE')
ROOT.TMVA.UseOffsetMethod = True

# Create Factory Object
factoryOptions = '!V:!Silent:Transformations=I;D;P;G,D:Correlations'
factory = ROOT.TMVA.Factory('BoostedDecisionTree', outputFile, factoryOptions)

# Create Dataloader
dataloader = ROOT.TMVA.DataLoader('dataset')
dataloader.AddVariable('bb_m', 'F')
dataloader.AddVariable('y1_y2_dEta', 'F')
dataloader.AddVariable('b1_b2_dEta', 'F')
dataloader.AddVariable('yy_b1_dEta', 'F')
dataloader.AddVariable('yy_b2_dEta', 'F')
dataloader.AddVariable('jet1_tagbin', 'F')
dataloader.AddVariable('jet2_tagbin', 'F')
dataloader.AddSpectator('jet_n', 'F')


# Get all trees
data_dir =  os.getcwd().split('ggf_bdt')[0]+'/ggf_bdt/data/' 

ggfFile_mc16a = ROOT.TFile(data_dir+'ggf_mc16a_trainTest.root')
ggfFile_mc16d = ROOT.TFile(data_dir+'ggf_mc16d_trainTest.root')
backgroundFile_mc16a = ROOT.TFile(data_dir+'sherpa_mc16a_trainTest.root')
backgroundFile_mc16d = ROOT.TFile(data_dir+'sherpa_mc16d_trainTest.root')


ggfTree_mc16a_train = ggfFile_mc16a.Get('mini_train')
ggfTree_mc16d_train = ggfFile_mc16d.Get('mini_train')
backgroundTree_mc16a_train = backgroundFile_mc16a.Get('mini_train')
backgroundTree_mc16d_train = backgroundFile_mc16d.Get('mini_train')

ggfTree_mc16a_test = ggfFile_mc16a.Get('mini_test')
ggfTree_mc16d_test = ggfFile_mc16d.Get('mini_test')
backgroundTree_mc16a_test = backgroundFile_mc16a.Get('mini_test')
backgroundTree_mc16d_test = backgroundFile_mc16d.Get('mini_test')


print "Trees Used: "
print "ggf mc16a - train: %i , test: %i " %   (ggfTree_mc16a_train.GetEntries(), ggfTree_mc16a_test.GetEntries())
print "ggf mc16d - train: %i , test: %i" %    (ggfTree_mc16d_train.GetEntries(), ggfTree_mc16d_test.GetEntries())
print "sherpa mc16a - train: %i , test: %i" % (backgroundTree_mc16a_train.GetEntries(), backgroundTree_mc16a_test.GetEntries())
print "sherpa mc16d - train: %i , test: %i" % (backgroundTree_mc16d_train.GetEntries(), backgroundTree_mc16d_test.GetEntries())

outputFile.cd()

# Set Weights
# ---------------------------------------------------------------
# Get weights - eventwise
# weight expression - (L)(xBR)(genEff)(skimEff)/ (sum_of_weights)
# Needs multiplied by yybb and hgam weights
luminosity = 150000


def set_weight(lumi, xBR, genEff, skimEff, sumW):
    return (lumi * xBR * genEff * skimEff / sumW)


def get_skim_eff(infile):
    h_cutflow = infile.Get('cutflow_weighted')
    n_xAOD = h_cutflow.GetBinContent(1)
    n_DxAOD = h_cutflow.GetBinContent(2)
    sum_of_weights = h_cutflow.GetBinContent(3)
    skim_efficiency = n_DxAOD / n_xAOD
    print infile

    return sum_of_weights, skim_efficiency


mc16a_ggf_yield = .247
mc16d_ggf_yield = .279
mc16a_sherpa_yield = .497
mc16d_sherpa_yield = .429

mc16a_ggf_sf = ((36.0*mc16a_ggf_yield)/(44.0*mc16d_ggf_yield)) 
mc16d_ggf_sf = ((44.0*mc16d_ggf_yield)/(36.0*mc16a_ggf_yield))
ggf_rescale_factor = (luminosity/(36.0*mc16a_ggf_yield + 44.0*mc16d_ggf_yield))

mc16a_sherpa_sf = ((36.0*mc16a_sherpa_yield)/(44.0*mc16d_sherpa_yield)) 
mc16d_sherpa_sf = ((44.0*mc16d_sherpa_yield)/(36.0*mc16a_sherpa_yield))
sherpa_rescale_factor = (luminosity/(36.0*mc16a_sherpa_yield + 44.0*mc16d_sherpa_yield))

# ggf
cross_section_br = 8.8111E-05
ggf_mc16a_weight = set_weight(luminosity, cross_section_br,  1, get_skim_eff(ggfFile_mc16a)[1], 1) * mc16a_ggf_sf * ggf_rescale_factor
ggf_mc16d_weight = set_weight(luminosity, cross_section_br,  1, get_skim_eff(ggfFile_mc16d)[1], 1) * mc16d_ggf_sf * ggf_rescale_factor
print "skim: ",  get_skim_eff(ggfFile_mc16a)[1]
print "mc16a_ggf_sf ", mc16a_ggf_sf
print "mc16a_ggf_rescale ", ggf_rescale_factor


# Sherpa
scale_factor = 10./55. # compensate for wide window
crossSec = 51.823*1000
sherpa_mc16a_weight = set_weight(lumi=luminosity, xBR=crossSec, genEff=1, skimEff=get_skim_eff(backgroundFile_mc16a)[1], sumW=71507816.0) * scale_factor * sherpa_rescale_factor
sherpa_mc16d_weight = set_weight(lumi=luminosity, xBR=crossSec, genEff=1, skimEff=get_skim_eff(backgroundFile_mc16d)[1], sumW=88440776.0) * scale_factor * sherpa_rescale_factor
#print "check here: l %f, xbr %f, g %f, sk %f, %f, sw %f" % (luminosity, crossSec, 1, get_skim_eff(backgroundFile_mc16a)[1], 71507816.0, )
#for file in [ggfFile_mc16a, ggfFile_mc16d, backgroundFile_mc16a, backgroundFile_mc16d]:
#    file.Close()

dataloader.SetWeightExpression('yybb_low_weight*hgam_weight')

# Prep Dataset
# ------------------------------------------------------------------

#  Load in and prepare trees
cut_train = ROOT.TCut('yy_b1_dEta > 0 && yy_b2_dEta > 0')
cut_test = ROOT.TCut('yy_b1_dEta > 0 && yy_b2_dEta > 0')

# Training = 0, testing =1
print "ggf mc16a weight: ", ggf_mc16a_weight
print "ggf mc16d weight: ", ggf_mc16d_weight
print "sherpa mc16a weight: ", sherpa_mc16a_weight
print "sherpa mc16d weight: ", sherpa_mc16d_weight

print "cut : ", cut_train

#
dataloader.AddTree(ggfTree_mc16a_train,"Signal", 1, cut_train, ROOT.TMVA.Types.kTraining)
dataloader.AddTree(ggfTree_mc16a_test, "Signal", 1, cut_test, ROOT.TMVA.Types.kTesting)

dataloader.AddTree(ggfTree_mc16d_train, "Signal", 1, cut_train, ROOT.TMVA.Types.kTraining)
dataloader.AddTree(ggfTree_mc16d_test, "Signal", 1, cut_test, ROOT.TMVA.Types.kTesting)

dataloader.AddTree(backgroundTree_mc16a_train, "Background", 1, cut_train, ROOT.TMVA.Types.kTraining)
dataloader.AddTree(backgroundTree_mc16a_test, "Background", 1, cut_test, ROOT.TMVA.Types.kTesting)

dataloader.AddTree(backgroundTree_mc16d_train, "Background", 1, cut_train, ROOT.TMVA.Types.kTraining)
dataloader.AddTree(backgroundTree_mc16d_test, "Background", 1, cut_test, ROOT.TMVA.Types.kTesting)

dataloader.PrepareTrainingAndTestTree(ROOT.TCut(""),ROOT.TCut(""),"NormMode=NumEvents:!V")

# MVA Settings
# ------------------------------------------------------------------

# BDT (adaboost)
bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
bdt_opts += 'NTrees=800'
bdt = factory.BookMethod(dataloader, ROOT.TMVA.Types.kBDT, 'BDT', bdt_opts)

# # BDT (ignore negative weights)
# bdt_opts = 'NegWeightTreatment=IgnoreNegWeightsInTraining:'
# bdt_opts += 'NTrees=800'
# bdt_ignoreNeg = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_ignoreNeg', bdt_opts)

# # BDT (gradient boosting)
# bdt_opts = 'NegWeightTreatment=IgnoreNegWeightsInTraining:'
# bdt_opts += 'NTrees=800'
# bdt_opts += 'BoostType=Grad'
# bdt_ignoreNeg = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_gradBoost', bdt_opts)

# # BDT (Max depth 5)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=800:'
# bdt_opts += 'MaxDepth=5'
# bdt_depth5 = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_depth5', bdt_opts)

# # BDT (Max depth 2)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=800:'
# bdt_opts += 'MaxDepth=2'
# bdt_depth2 = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_depth2', bdt_opts)

# # BDT (400 trees)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=400'
# bdt_400tree = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_400tree', bdt_opts)

# # BDT (400 trees)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=200'
# bdt_200tree = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_200tree', bdt_opts)

# # BDT (100 trees)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=100'
# bdt_100tree = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_100tree', bdt_opts)

# BDT (75 trees)
bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
bdt_opts += 'NTrees=75'
bdt_75tree = factory.BookMethod(
    dataloader, ROOT.TMVA.Types.kBDT, 'BDT_75tree', bdt_opts)

# # BDT (50 trees)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=50'
# bdt_50tree = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_50tree', bdt_opts)

# # BDT (25 trees)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=25'
# bdt_25tree = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_25tree', bdt_opts)

# # BDT (75 trees, depth 2)
# bdt_opts = 'NegWeightTreatment=InverseBoostNegWeights:'
# bdt_opts += 'NTrees=75:'
# bdt_opts += 'MaxDepth=2'
# bdt_75tree_ = factory.BookMethod(
#     dataloader, ROOT.TMVA.Types.kBDT, 'BDT_75tree_depth2', bdt_opts)


# Rectangular Cuts
#rect = factory.BookMethod(dataloader, ROOT.TMVA.Types.kCuts, 'rect_cuts', '')


# Train, Test, Evaluate all methods
# ------------------------------------------------------------------
factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()

# Clean up

outputFile.Close()
os.rename('dataset','dataset_{0}'.format(time))
writedir = os.getcwd().split('ggf_bdt')[0]+'/ggf_bdt/models/'
move('dataset_{0}'.format(time), writedir)