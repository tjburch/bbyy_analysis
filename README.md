Various Scripts for yybb analysis.

## Navigating:

### backgroundComposition/:
Tools for background composition analysis


### cutflowTools/:
Tools for backend files created from cutflow.tex files

### bbyy_vbf/:
Scripts for analyzing VBF samples


### scripts/:
Misc. other scripts 
	efficiencyVsMu.py - plots the efficiency vs pileup
	tinyFunctions.py - Functions to be imported and used on tiny nTuples


## Di-higgs talks

[September 7, 2018](https://indico.cern.ch/event/731450/contributions/3099727/attachments/1712396/2761176/vbf_hh_burch2018_final.pdf) - VBF HH Production; di-Higgs Workshop, Fermilab

[April 10, 2018](https://indico.cern.ch/event/719888/contributions/2963135/attachments/1628890/2598013/AtlasWeekly_burch.pdf) - ATLAS Weekly Paper Presentation

[July 25, 2017](https://indico.cern.ch/event/655823/contributions/2671436/attachments/1498336/2332542/yybb_HGam_update.pdf) - HGamma Update

[July 20, 2017](https://indico.cern.ch/event/654626/contributions/2665533/attachments/1496097/2327906/yybb_HBSM_update.pdf#search=tyler%20james%20burch) - HBSM Update

